<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = [ 'category_type','category_rate','fixed_ammount','interest','duration_period','instalment'];
}
