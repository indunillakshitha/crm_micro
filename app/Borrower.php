<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrower extends Model
{
    protected $table = 'borrowers';
    protected $fillable = [
        // 'application_no',
        'branch',
        'branch_no',
        'center',
        'group_no',
        'borrower_no',
        'full_name',
        'nic',
        'birthday',
        'address',
        'gender',
        'civil_status',
        'related_civil_status',
        'h_g_name',
        'h_g_number',
        'h_g_sallery',
        'lp_no',
        'mobile_no',
        'email',
        'occupation',
        'remarks',
        'status',
        'n_of_loans'
    ];
    protected $primaryKey = 'borrower_no';
    public $incrementing = false;
    protected $keyType = 'string';
}
