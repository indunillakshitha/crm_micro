<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppraisalIngredient extends Model
{
    protected $fillable = [
        'ingredient',
        'product',
        'min_price',
        'max_price',
    ];
}
