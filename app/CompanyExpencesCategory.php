<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyExpencesCategory extends Model
{
    protected $table='new_company_expences_categories';
    protected $fillable =['category'];
}
