<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guarantor extends Model
{
    protected $table = 'guarantors';
    protected $fillable = [
        'guarantor_no',
        'guarantor_occupation',
        'guarantor_email',
        'guarantor_mobile_no',
        'guarantor_lp_no',
        'guarantor_civil_status',
        'guarantor_address',
        'guarantor_gender',
        'guarantor_birthday',
        'guarantor_nic',
        'guarantor_full_name',
    ];
}
