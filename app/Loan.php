<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $table = 'loans';
    protected $fillable = [
        'borrower_no',
        'branch',
        'center',
        'group_no',
        'loan_stage',
        'loan_amount',
        'release_date',
        'loan_duration',
        'duration_period',
        'repayment_cycle',
        'interest',
        'instalment',
        'number_of_repayments',
        'loan_purpose',
        'interest_rate',
        'principal',
        'maturity',
        'fees',
        'penalty',
        'due_date',
        'paid',
        'balance',
        'comment',
        'witnessA',
        'witnessC',
        'due_month',
        'last_payment_date',
        'last_payment_month',
        'payment_starting_date',
        'payment_starting_date_index',
        'payment_cycle_date',
        'issued_date',
        'issued_date_index',
        
    ];
    // protected $primaryKey = 'borrower_no';
    // public $incrementing = false;
    // protected $keyType = 'string';
}
