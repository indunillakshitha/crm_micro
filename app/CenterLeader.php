<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CenterLeader extends Model
{
    protected $table = 'center_leaders';
    protected $fillable =[
        'branch',
        'center_no',
        'name',
        'borrower_id',
        'nic',
        'address',
        'mobile',
        'type',

    ];
}
