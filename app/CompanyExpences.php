<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyExpences extends Model
{
    protected $table = 'new_company_expences';
    protected $fillable = [
        'voucher_id',
        'category',
        'date',
        'payee',
        'remarks',
        'description',
        'amount',
        'quantity',
        'item_total',
        'supporting_document'
    ];
}
