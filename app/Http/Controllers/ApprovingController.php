<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Loan;
use App\Appraisal;
use App\Borrower;
use App\Approving;
use App\BorrowerEarning;
use App\BorrowerExpense;
use App\BorrowerMainEarning;
use App\BorrowerOtherLoan;
use DB;
use Exception;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Arr;


class ApprovingController extends Controller
{
    public function index(Request $request)
    {
    //     if(request()->ajax())
    //  {
    //   if(!empty($request->center_selector))
    //   {
    //    $data = Loan::
    //      select('borrower_no', 'branch', 'Address', 'center', 'group_no', 'loan_stage')
    //      ->where('center', $request->center_selector)
    //      ->where('group_no', $request->group_selector)
    //      ->get();
    //   }
    //   else
    //   {
    //    $data = Loan::
    //      select('CustomerName', 'Gender', 'Address', 'City', 'PostalCode', 'Country')
    //      ->get();
    //   }
    //   return ()->of($data)->make(true);
    //  }
        // // borrowers count
        // $borrowersCount = count(Borrower::all());
        // $borrowersCount++;

        // // get branches
        // $branches = Branch::all();
        // // get centers
        // $centers = Center::all();

//       return view('Borrower.borrowerindex');
        // $apprasails = Appraisal::all();
// -----------------------------------------------------------------------------
        $verified = 'Verified';

        $borrowers = Borrower::all();
        $apprasails = Appraisal::all();
        $loans = Loan::where('status',$verified)->paginate(1000);
        return view('Approving.indexApproving',compact('loans'))
            ->with('i',(request()->input('page',1)-1)*5);
// -----------------------------------------------------------------------------


            // $loans = Loan::
            //                 select('branch','center','group_no')
            //                     ->groupBy('branch','center','group_no')
            //                     ->get()->paginate(10);
            //  return view('Approving.indexApproving',compact('loans'))
            //  ->with('i',(request()->input('page',1)-1)*5);



    }
    // public function show($id)
    // {

    //     $apprasails = Appraisal::where('borrower_no', $id)->first();
    //     return view('Approving.apprasialApproving',compact('apprasails'));
    // }
    // public function show($id)
    // {

    //     $borrower = Borrower::where('borrower_no', $id)->first();
    //     return view('Approving.borrowerApproving',compact('borrower'));
    // }
    public function show($id)
    {

        try{
            $earnings = BorrowerEarning::where('borrower_id', $id)->get();
            $expenses = BorrowerExpense::where('borrower_id', $id)->get();
            $incomes = BorrowerMainEarning::where('borrower_id', $id)->get();
            $otherLoans = BorrowerOtherLoan::where('borrower_id', $id)->get();


            $loan = Loan::where('id', $id)->first();
            $apprasails = Appraisal::where('nic', $loan->nic)->first();
            $borrower = Borrower::where('borrower_no', $loan->borrower_no)->first();
            if($apprasails == null || $loan ==null){
                return redirect()->back()->with('error', 'This Borrower has no all documents');

            }else{
                return view('Approving.borrowerApproving',compact('loan','apprasails','borrower','earnings','expenses','incomes','otherLoans'));

            }
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'This Borrower has no all documents');
        }


    }

    public function test($id)
    {

        // $pending = 'pending';
        $borrower= Loan::where('id',$id)->where('status','Verified')->first();
        $borrower['status']='Approved';
        $borrower->save();
//        $borrowers = Borrower::all();
//        $apprasails = Appraisal::all();
        // $loans = Loan::where('status',$pending)->paginate(10);
//        return view('Approving.indexApproving',compact('loans'))/
//            ->with('i',(request()->input('page',1)-1)*5);
//        return redirect('/home');
        return redirect()->route('approving.index');
    }
    public function destroy($id)
    {
        $loans = Loan::where('id', $id)->first();
        return view('Approving.rejectApproving',compact('loans'));
    }
    public function store(Request $request,$id)
    {
         $add=$request ;
        $comment = Loan::where('id',$id)->first();
        $comment ['comment']= $add;
        $comment->save();
        $borrower= Loan::where('borrower_no',$id)->where('status','pending')->first();
        $borrower['status']='Rejected';
        $borrower->save();
        // $request->validate([ 'comment' => 'required']);
        // Loan::create($request->all());
        // $pending = 'Pending';
        // $borrowers = Borrower::all();
        // $apprasails = Appraisal::all();
        // $loans = Loan::where('status',$pending)->paginate(10);
        // return view('Approving.indexApproving',compact('loans'))
        //     ->with('i',(request()->input('page',1)-1)*5);
                return redirect()->route('approving.index');

    }

    public function seclectorB(Request $request ){
        // $select = $request;
        $select = Borrower::where('branch',$request['id'])->select('borrower_no')->get();
        // $selectFind = Loan::where('')
        $idd = Loan::whereIn('borrower_no',$select)->get();
        // $is = json_encode($select);
        return response()->json($idd);

    }
     public function seclectorC(Request $request){
        // $select = $request;
        $select = Borrower::where('center',$request['id'])->select('borrower_no')->get();
        // $selectFind = Loan::where('')
        $idd = Loan::whereIn('borrower_no',$select)->get();
        // $is = json_encode($select);
        return response()->json($idd);

    }

    public function seclectorG(Request $request){
        // $select = $request;
        $select = Borrower::where('group_no',$request['id'])->select('borrower_no')->get();
        // $selectFind = Loan::where('')
        $idd = Loan::whereIn('borrower_no',$select)->get();
        // $is = json_encode($select);
        return response()->json($idd);

    }
}
