<?php

namespace App\Http\Controllers;

use App\CompanyExpences;
use PhpOffice\PhpWord\TemplateProcessor;

class PrintController extends Controller
{
    public function exportVoucher($voucher_id)
    {

        $voucherData = CompanyExpences::where('voucher_id', $voucher_id)->get();
        $invoiceCount = count($voucherData);

        $voucher = new TemplateProcessor('word-template/Voucher.docx');

        $digit = new NumberFormatter("en", NumberFormatter::SPELLOUT);
        $amount_spell = $digit->format($voucherData->expense_amount);
        $amount_spells = ucwords($amount_spell);

        $voucher->setValue('branch', $voucherData->branch);
        $voucher->setValue('vocher_no', $voucherData->id);
        $voucher->setValue('date', $voucherData->expense_date);
        $voucher->setValue('amount_spell', $amount_spells);
        // $voucher->setValue('cheque_no', $voucherData->cheque_no);
        // $voucher->setValue('cheque_date', $voucherData->cheque_date);
         // $voucher->setValue('bank', $voucherData->bank);

        if ($invoiceCount == 5) {
            $voucher->setValue('amount_1', $voucherData[0]['amount']);
            $voucher->setValue('description_1', $voucherData[0]['description']);
            $voucher->setValue('ledger_account_1', $voucherData[0]['ledger_account']);

            $voucher->setValue('amount_2', $voucherData[1]['amount']);
            $voucher->setValue('description_2', $voucherData[1]['description']);
            $voucher->setValue('ledger_account_2', $voucherData[1]['ledger_account']);

            $voucher->setValue('amount_3', $voucherData[2]['amount']);
            $voucher->setValue('description_3', $voucherData[2]['description']);
            $voucher->setValue('ledger_account_3', $voucherData[2]['ledger_account']);

            $voucher->setValue('amount_4', $voucherData[3]['amount']);
            $voucher->setValue('description_4', $voucherData[3]['description']);
            $voucher->setValue('ledger_account_4', $voucherData[3]['ledger_account']);

            $voucher->setValue('amount_5', $voucherData[4]['amount']);
            $voucher->setValue('description_5', $voucherData[4]['description']);
            $voucher->setValue('ledger_account_5', $voucherData[4]['ledger_account']);

        }else if ($invoiceCount == 4){
            $voucher->setValue('amount_1', $voucherData[0]['amount']);
            $voucher->setValue('description_1', $voucherData[0]['description']);
            $voucher->setValue('ledger_account_1', $voucherData[0]['ledger_account']);

            $voucher->setValue('amount_2', $voucherData[1]['amount']);
            $voucher->setValue('description_2', $voucherData[1]['description']);
            $voucher->setValue('ledger_account_2', $voucherData[1]['ledger_account']);

            $voucher->setValue('amount_3', $voucherData[2]['amount']);
            $voucher->setValue('description_3', $voucherData[2]['description']);
            $voucher->setValue('ledger_account_3', $voucherData[2]['ledger_account']);

            $voucher->setValue('amount_4', $voucherData[3]['amount']);
            $voucher->setValue('description_4', $voucherData[3]['description']);
            $voucher->setValue('ledger_account_4', $voucherData[3]['ledger_account']);

            $voucher->setValue('amount_5', '');
            $voucher->setValue('description_5', '');
            $voucher->setValue('ledger_account_5', '');

        }else if ($invoiceCount == 3){
            $voucher->setValue('amount_1', $voucherData[0]['amount']);
            $voucher->setValue('description_1', $voucherData[0]['description']);
            $voucher->setValue('ledger_account_1', $voucherData[0]['ledger_account']);

            $voucher->setValue('amount_2', $voucherData[1]['amount']);
            $voucher->setValue('description_2', $voucherData[1]['description']);
            $voucher->setValue('ledger_account_2', $voucherData[1]['ledger_account']);

            $voucher->setValue('amount_3', $voucherData[2]['amount']);
            $voucher->setValue('description_3', $voucherData[2]['description']);
            $voucher->setValue('ledger_account_3', $voucherData[2]['ledger_account']);

            $voucher->setValue('amount_4', '');
            $voucher->setValue('description_4', '');
            $voucher->setValue('ledger_account_4', '');

            $voucher->setValue('amount_5', '');
            $voucher->setValue('description_5', '');
            $voucher->setValue('ledger_account_5', '');

        }else if ($invoiceCount == 2){
            $voucher->setValue('amount_1', $voucherData[0]['amount']);
            $voucher->setValue('description_1', $voucherData[0]['description']);
            $voucher->setValue('ledger_account_1', $voucherData[0]['ledger_account']);

            $voucher->setValue('amount_2', $voucherData[1]['amount']);
            $voucher->setValue('description_2', $voucherData[1]['description']);
            $voucher->setValue('ledger_account_2', $voucherData[1]['ledger_account']);

            $voucher->setValue('amount_3', '');
            $voucher->setValue('description_3', '');
            $voucher->setValue('ledger_account_3', '');

            $voucher->setValue('amount_4', '');
            $voucher->setValue('description_4', '');
            $voucher->setValue('ledger_account_4', '');

            $voucher->setValue('amount_5', '');
            $voucher->setValue('description_5', '');
            $voucher->setValue('ledger_account_5', '');

        }else if ($invoiceCount == 1){
            $voucher->setValue('amount_1', $voucherData[0]['amount']);
            $voucher->setValue('description_1', $voucherData[0]['description']);
            $voucher->setValue('ledger_account_1', $voucherData[0]['ledger_account']);

            $voucher->setValue('amount_2', '');
            $voucher->setValue('description_2', '');
            $voucher->setValue('ledger_account_2', '');

            $voucher->setValue('amount_3', '');
            $voucher->setValue('description_3', '');
            $voucher->setValue('ledger_account_3', '');

            $voucher->setValue('amount_4', '');
            $voucher->setValue('description_4', '');
            $voucher->setValue('ledger_account_4', '');

            $voucher->setValue('amount_5', '');
            $voucher->setValue('description_5', '');
            $voucher->setValue('ledger_account_5', '');

        }

        $fileName = $voucherData->id . " voucher";
        $voucher->saveAs($fileName . '.docx');
        return response()->download($fileName . '.docx')->deleteFileAfterSend(true);

    }
}
