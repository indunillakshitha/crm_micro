<?php

namespace App\Http\Controllers;

use App\OngoingCheck;
use Illuminate\Http\Request;

class OngoingCheckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ongoing_check.index');
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        // return $request;
        OngoingCheck::create($request->all());
    }


    public function show(OngoingCheck $ongoingCheck)
    {
        //
    }

    public function edit(OngoingCheck $ongoingCheck)
    {
        //
    }


    public function update(Request $request, OngoingCheck $ongoingCheck)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OngoingCheck  $ongoingCheck
     * @return \Illuminate\Http\Response
     */
    public function destroy(OngoingCheck $ongoingCheck)
    {
        //
    }
}
