<?php

namespace App\Http\Controllers;
use App\Repayment;
use Auth;
use Illuminate\Http\Request;

class OldRepaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Repayment.Repayments_on_any_date');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function setRepaymentsDate(Request $request){

        $repayment = Repayment::find($request->id);
        // ->update(['status'=>'COLLECTED']);
        $repayment->status = 'COLLECTED';
        $repayment->payed_date_index = $request->paid_date;
        $repayment->payed_amount = $request->today_payment;
        $repayment->save();

        $next_repayment = Repayment::find($request->id+1);
        $next_repayment->due_amount = $repayment->due_amount - $repayment->payed_amount;
        $next_repayment->save();

        if($next_repayment->due_amount <= 0){
            $repayment->status = 'SETTLED';
            $repayment->save();

            $unpayed = Repayment::where('loan_id', $repayment->loan_id)->where('status', 'PENDING')->get();
            foreach($unpayed as $u){
                $u->status = 'COLLECTED';
                $u->payed_date_index = $repayment->payed_date_index;
                $u->save();
            }
        }
        return response()->json($repayment);
    }
}
