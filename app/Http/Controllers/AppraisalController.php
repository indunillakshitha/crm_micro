<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\Appraisal_earning_product;
use App\Appraisal_expenses_product;
use App\AppraisalIngredient;
use App\BorrowerExpense;
use App\BorrowerMainExpense;
use App\Center;
use App\Borrower;
use App\BorrowerEarning;
use App\BorrowerMainEarning;
use App\BorrowerOtherLoan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;


class AppraisalController extends Controller
{

    public function index()
    {
        // $apprasails =Appraisal::all();

        $apprasails = Appraisal::where('created_date_index', Carbon::now()->isoFormat('DDD'))->get();

        return view('Appraisal.appraisalindex', compact('apprasails'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function createAppraisalPage()
    {

        $earnings = Appraisal_earning_product::all();
        $expenses = Appraisal_expenses_product::all();

        $branch = Auth::user()->branch;
        $centers = Center::where('branch_no', $branch)->get();
        $borrowersCount = count(Borrower::all());
        $borrowersCount++;

        return view('Appraisal.createappraisal', compact('earnings', 'expenses', 'branch', 'centers', 'borrowersCount'));
    }

    public function store(Request $request)
    {
        $request->validate([
            // 'borrower_no'=>'required ',
            // 'reqesting_loan_amount'=>'required ',
            // 'loan_purpose'=>'required ',
            // 'husband'=>'required |numeric ',
            // 'son'=>'required |numeric ',
            // 'daughter'=>'required |numeric ',
            // 'guardian'=>'required |numeric ',
            // 'other'=>'required |numeric ',
            // 'total_earn'=>'required |numeric ',
            // 'household_expenses'=>'required |numeric ',
            // 'other_expenses'=>'required |numeric ',
            // 'as_borrower_facility_count'=>'required |numeric ',
            // 'as_borrower_total_amount'=>'required |numeric ',
            // 'as_guarantor_facility_count'=>'required |numeric ',
            // 'as_guarantor_total_amount'=>'required |numeric ',
            // 'total_expenses'=>'required |numeric '
        ]);

        Appraisal::create($request->all());
        $appraisal = Appraisal::orderBy('id', 'desc')->first();
        $appraisal->created_date_index = Carbon::now()->isoFormat('DDD');
        $appraisal->created_by = Auth::user()->name;
        $appraisal->save();

        return redirect("https://cantylivelocations.firebaseapp.com/upload");
    }

    public function getUnitPrice(Request $request)
    {
        $find = Appraisal_earning_product::where('product',  $request['id'])->first();
        //   $unitPrice = $find['unit_price'];
        return response()->json($find);
    }

    public function getExpenses(Request $request)
    {

        $find = Appraisal_expenses_product::where('product', $request->id)->first();
        $buyingPrice = $find->buying_price;
        $ings = $find->ingredient;

        $data = [$buyingPrice, $ings];
        return response()->json($data);
    }

    public function addExpense(Request $request)
    {

        $ing = AppraisalIngredient::where('product', $request->product_name)->where('ingredient', $request->ing)->first();
        $min = $ing->min_price;
        $max = $ing->max_price;

        if ($request->buyingPrice > $min && $request->buyingPrice < $max) {

            $expense = new BorrowerExpense;
            // $expense->borrower_id = $request->borrower_id;
            $expense->nic = $request->nic;
            $expense->product_name = $request->product_name;
            $expense->total_units = $request->total_units;
            $expense->period = $request->period;
            $expense->expenses = $request->expenses;
            $expense->buying_price = $request->buyingPrice;
            $expense->save();

            return response()->json($request);
        } else {
            return response()->json('not_in_range');
        }




        // $find = BorrowerExpense::where('id');
    }

    public function getExpense(Request $request)
    {

        $find = BorrowerExpense::where('nic', $request->nic)->where('visibility', 1)->get();
        // $find = BorrowerExpense::all()->where('borrower_id', $request->borrower_id);

        // $data = [
        //     $proName = $find->proName,
        //     $ingredient = $find->ingredient,
        //     $buying_price = $find->buying_price
        // ];
        return response()->json($find);
    }

    public function getNicAppraisals(Request $request)
    {
        // $borrower = Borrower::where('nic', $request->nic)->first();
        $borrower = Appraisal::where('nic', $request->nic)->where('visibility', 1)->first();

        if ($borrower) {
            return response()->json($borrower);
        } else {
            return response()->json('no borrower');
        }
    }
    public function destroy(Appraisal $appraisal)
    {
        $appraisal->delete();

        return redirect()->route('appraisal.index')
            ->with('success', 'Appraisal deleted successfully');

        // return redirect()->json($appraisal);
    }

    public function delPrevApp(Request $request)
    {

        try {
            Appraisal::where('nic', $request->nic)->update(['visibility' => 0]);
        } catch (Exception $e) {
            return;
        }

        try {
            $earnings = BorrowerEarning::where('nic', $request->nic)->get();
            foreach ($earnings as $e) {
                // $e->delete();
                $e->visibility = 0;
                $e->save();
            }
        } catch (Exception $e) {
            return;
        }

        try {

            $expenses = BorrowerExpense::where('nic', $request->nic)->get();
            foreach ($expenses as $e) {
                $e->visibility = 0;
                $e->save();
            }
        } catch (Exception $e) {
            return;
        }
        try {
            $relays = BorrowerMainEarning::where('nic', $request->nic)->get();
            foreach ($relays as $e) {
                $e->visibility = 0;
                $e->save();
            }
        } catch (Exception $e) {
            return;
        }
        try {
            $loans = BorrowerOtherLoan::where('borrower_id', $request->nic)->get();
            foreach ($loans as $e) {
                $e->visibility = 0;
                $e->save();
            }
        } catch (Exception $e) {
            return;
        }

        return response()->json('records deleted');
    }

    public function agentAppraisal($id,$lat,$lng)
    {
        $earnings = Appraisal_earning_product::all();
        $expenses = Appraisal_expenses_product::all();
        // $location['nic']=$id
        $branch = Auth::user()->branch;
        $centers = Center::where('branch_no', $branch)->get();
        $borrowersCount = count(Borrower::all());
        $borrowersCount++;

        return view('Appraisal.agentAppraisal', compact('earnings', 'expenses', 'branch', 'centers', 'borrowersCount', 'id'));
    }

    public function delEarning(Request $request){
        $e = BorrowerEarning::find($request->id);
        $e->visibility = 0;
        $e->save();
        return response()->json($request);
    }

    public function delExpense(Request $request){
        $e = BorrowerExpense::find($request->id);
        $e->visibility = 0;
        $e->save();
        return response()->json($request);
    }

    public function delRelayIncome(Request $request){
        $e = BorrowerMainEarning::find($request->id);
        $e->visibility = 0;
        $e->save();
        return response()->json($request);
    }

    public function delLoans(Request $request){
        $e = BorrowerOtherLoan::find($request->id);
        $e->visibility = 0;
        $e->save();
        return response()->json($request);
    }

    public function all(){
        $appraisals = Appraisal::all();
        return view('Appraisal.all', compact('appraisals'));

    }
}
