<?php

namespace App\Http\Controllers;

use App\CompanyExpencesCategory;
use Illuminate\Http\Request;

class CompanyExpencesCategoryController extends Controller
{

    public function index()
    {
        return view('company_ex_cat.index');
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        CompanyExpencesCategory::create($request->all());
        return view('company_ex_cat.index');
    }


    public function show(CompanyExpencesCategory $companyExpencesCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyExpencesCategory  $companyExpencesCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyExpencesCategory $companyExpencesCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyExpencesCategory  $companyExpencesCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyExpencesCategory $companyExpencesCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyExpencesCategory  $companyExpencesCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyExpencesCategory $companyExpencesCategory)
    {
        //
    }
}
