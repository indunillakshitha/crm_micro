<?php

namespace App\Http\Controllers;

use App\BorrowerOtherLoan;
use Illuminate\Http\Request;

class BorrowerOtherLoansController extends Controller
{
    public function addLoan(Request $request){

        $loan = new BorrowerOtherLoan;
        $loan->borrower_id = $request->borrower_id  ;
        $loan->bank_name = $request->bank_name  ;
        $loan->total_loan = $request->total_loan  ;
        $loan->monthly_paymentt = $request->monthly_payment  ;
        $loan->save();
        return response()->json($request);
    }

    public function getLoan(Request $request){
        $find = BorrowerOtherLoan::where('borrower_id', $request->borrower_id)->where('visibility', 1)->get();
        return response()->json($find);
    }
}
