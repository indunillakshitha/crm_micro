<?php

namespace App\Http\Controllers\Guarantor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Guarantor;
use App\Borrower;

class GuarantorController extends Controller
{

    public function index()
    {
        $guarantors = Guarantor::orderBy('id')->paginate(5);
        return view('guarantor.indexguarantor', compact('guarantors'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    public function destroy(Guarantor $guarantor)
    {
        $guarantor->delete();

        return redirect()->route('guarantor.index')
            ->with('success', 'guarantor deleted successfully');
    }


    public function store(Request $request)
    {
        $request->validate([
            'guarantor_no' => 'required',
            'guarantor_full_name' => 'required ',
            'guarantor_nic' => 'required',
            'guarantor_birthday' => 'required',
            'guarantor_gender' => 'required',
            'guarantor_address' => 'required',
            'guarantor_mobile_no' => 'required | numeric',
            'guarantor_lp_no' => 'required |numeric ',
            'guarantor_occupation' => 'required',


        ]);

        Guarantor::create($request->all());

        return redirect()->route('guarantor.index')
            ->with('success', 'guarantor created successfully.');

    }


    public function show(Guarantor $guarantor)
    {
        return view('guarantor.showGuarantor', compact('guarantor'));
    }


    public function edit(Guarantor $guarantor)
    {
        return view('guarantor.editGuarantor', compact('guarantor'));
    }


    public function update(Request $request, Guarantor $guarantor)
    {
        $request->validate([
            'guarantor_no' => 'required',
            'guarantor_name' => 'required ',
            'guarantor_nic' => 'required',
            'guarantor_birthday' => 'required',
            'guarantor_gender' => 'required',
            'guarantor_address' => 'required',
            'guarantor_mobile' => 'required | numeric',
            'guarantor_land' => 'required |numeric ',
            'guarantor_occupation' => 'required',

        ]);

        $guarantor->update($request->all());

        return redirect()->route('guarantor.index')
            ->with('success', 'guarantor updated successfully');
    }


    public function create()
    {
        $guarantorCount = count(Guarantor::all());
        if ($guarantorCount) {
            $guarantorCount++;
        } else {
            $guarantorCount = 1;
        }
        return view('guarantor.addGuarantor')->with('guarantorCount', $guarantorCount);
    }

    public function borrowerAsGuarantor(){
        $borrowers = Borrower::all();
        return view('Guarantor.indexGuarantor', compact('borrowers'));
    }

}
