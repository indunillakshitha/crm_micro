<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

use App\Center;
use App\CompanyExpences;
use App\Loan;
use App\Repayment;
use App\CompanyExpencesSummary;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\MockObject\Stub\ReturnReference;

class EmergencyController extends Controller
{
    public function gettotal($id)
    {


        return DB::table('repayments')->where('loan_id', $id)->sum('paid_amount');
    }

    public function getcount($id)
    {
        return DB::table('repayments')->where('loan_id', $id)->count();
    }


    public function report($id)
    {
        $centers = Center::all();
        // foreach($centers as $center){
        $count_1 = Loan::where('center', $id)->where('loan_amount', '10000')->count();
        $count_2 = Loan::where('center', $id)->where('loan_amount', '15000')->count();
        $count_3 = Loan::where('center', $id)->where('loan_amount', '20000')->count();
        // }

        return view('emg.index', compact('count_1', 'count_2', 'count_3'));
    }

    public function dateg()
    {
        $data = DB::table('repayments')->where('loan_id', 606)->get();
        return $date = $data->payment_date;
    }
    public function settle(){

    //    $loans=Loan::all();
    //    foreach($loans as $loan){
    //       if($loan->status=='SETTLED'){
    //           $loan->due=0;
    //           $loan->paid=$loan->loan_amount+$loan->interest;
    //           $loan->save();
    //       }
    //    }
$repayments=Repayment::all();
foreach($repayments as $repayment){
        $category=Category::where('fixed_ammount',$repayment->loan_amount)->first();

        $loanA = Loan::where('id',$repayment->loan_id)->first();
        if(!empty($loanA)){
        $interest_amount=round(($loanA->interest / $loanA->loan_duration),2);
        $repayment->inter=$interest_amount;//interest collection
        $repayment->capital=$repayment->paid_amount-$interest_amount;//capital collection

         if($repayment->installment < $repayment->paid_amount){
                 $repayment->over_paid_amount = $repayment->paid_amount - $repayment->installment ;//over paid amount
         }elseif($repayment->installment > $repayment->paid_amount){
                $repayment->less_paid_amount = $repayment->installment - $repayment->paid_amount;//less paid amount
        }

        $repayment->save();
    }
}

    }

    public function correctCounts()
    {
        $loans = Loan::all();

        foreach ($loans as $loan) {
            $paycount = Repayment::where('loan_id', $loan->id)->count();
            $loan->payments_count = $paycount;
            $loan->save();
        }
    }

    public function setMaturityDate()
    {
        $loans = Loan::all();
        foreach ($loans as $loan) {
            $start_date = Carbon::parse($loan->payment_starting_date);
            $duration = $loan->loan_duration * 7;
            $end_date = $start_date->addDays($duration)->isoFormat('Y-M-D');
            $loan->maturity = $end_date;
            $loan->save();
        }
    }
    public function setNotPaids()
    {
        // $loans = Loan::all();
        $loan = Loan::where('id', 1)->first();
        // foreach($loans as $loan){
        $start_date = Carbon::parse($loan->payment_starting_date);
        $duration = $loan->loan_duration * 7;

        $end_date =  Carbon::parse($loan->payment_starting_date)->addDays($duration)->isoFormat('Y-M-D');

        $start_date_date_index = Carbon::parse($loan->payment_starting_date)->isoFormat('DDD');
        $end_date_index = Carbon::parse($end_date)->isoFormat('DDD');


        for ($i = $start_date_date_index; $i <= $end_date_index; $i = $i + 7) {

            //  return Repayment::where('loan_id',$loan->id)->where('payment_date_index',$i)->first();
            if (!Repayment::where('loan_id', $loan->id)->where('payment_date_index', $i)->first()) {

                // return "sadsa";

                $loan->payments_count = ++$loan->payments_count;
                $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
                $loan->last_payment_month = Carbon::now()->isoFormat('M');

                // $due_amount = $loan->loan_amount - $loan->paid;
                $prev_total_payed = $loan->paid;

                $repayment = new Repayment;
                $repayment->logged_user = Auth::user()->name;
                $repayment->borrower_nic = $loan->nic;
                $repayment->branch = $loan->branch;
                $repayment->center = $loan->center;
                $repayment->loan_id = $loan->id;
                $repayment->loan_amount = $loan->loan_amount;
                $repayment->paid_amount = 0;
                $repayment->due_amount = $loan->due - 0;
                $repayment->total_payed = $prev_total_payed + 0;
                $repayment->installment = $loan->instalment;
                $repayment->payment_date_index = $i;
                return $repayment->payment_date = Carbon::createFromFormat('DDD', $i)->isoFormat('M/D/YYYY');
                $repayment->payment_date_full = Carbon::parse($i)->isoFormat('M/D/YYYY');
                $repayment->installment_no = $loan->payments_count;
                $repayment->status = 'ABSENT'; #warike gatta
                $repayment->type = 'CASH';
                $repayment->method_status = 'TEMP COLLECTION';

                $loan->due = $repayment->due_amount;
                $loan->paid = $repayment->total_payed;

                $category = Category::where('fixed_ammount', $repayment->loan_amount)->first();

                $interest_amount = $repayment->installment * $category->category_rate / 100;
                $repayment->inter = $interest_amount; //interest collection
                $repayment->capital = $repayment->paid_amount - $interest_amount; //capital collection

                if ($repayment->installment < $repayment->paid_amount) {
                    $repayment->over_paid_amount = $repayment->paid_amount - $repayment->installment; //over paid amount
                } elseif ($repayment->installment > $repayment->paid_amount) {
                    $repayment->less_paid_amount = $repayment->installment - $repayment->paid_amount; //less paid amount
                }
                if ($loan->due <= 0) {
                    $loan->status = 'SETTLED';
                    $repayment->status = 'SETTLED';
                }
                if (!Repayment::where('loan_id', $repayment->loan_id)->where('payment_date', $repayment->payment_date)->first()) {
                    $repayment->save();
                    $loan->save();
                }
                $centerTotal = 0;
                $centerRepayments = Repayment::where('center', $loan->center)
                    ->where('payment_date_index', $repayment->payment_date_index)
                    ->get();

                foreach ($centerRepayments as $r) {
                    $centerTotal += $r->paid_amount;
                }


                $loan->maturity = $end_date;
                $loan->save();
                $c = 7;
                $start_date = (new Carbon($start_date))->addDays(7);
            // } else {
            //     return 2;
            // }
            // return $loan->id;
            }
        }
    }


    public function docChargeUpdate(){
       $repayments= Repayment::where('method_status','DOCUMENT')->get();
       foreach($repayments as $repayment){

        $repayment->doc_charges=$repayment->paid_amount;
        $repayment->paid_amount=0;
        $repayment->capital=0;
        $repayment->over_paid_amount=0;
        $repayment->save();
       }

    }

    public function corectExpenceTotals(){
        $expences =CompanyExpencesSummary::where('visibility',1)->get();
        foreach($expences as $expence){
            $total=CompanyExpences::where('visibility',1)->where('voucher_id',$expence->voucher_id)->sum('item_total');
            $expence->invoice_total=$total;
            $expence->save();
        }
    }

    public function re(){
        $repayments=Repayment::all();
        foreach($repayments as $repayment){
            $loan=Loan::where('id',$repayment->loan_id)->first();
            $repayment->borrower_no=$loan->borrower_no;
            $repayment->save();
        }
    }


    public function weekIndexesinrepayments(){
        $repayments=Repayment::all();
        foreach($repayments as $repayment){

        $repayment->week_index=Carbon::parse($repayment->payment_date_full)->isoFormat('W');
        $repayment->save();


        }
    }

    public function totalPayements(){
        return $total =Loan::where('status','Payed')->sum('installment');
    }

    public function correctDocCharges(){
        $repayments=Repayment::where('method_status','DOCUMENT')->get();
        foreach($repayments as $rep){
            $rep->doc_charges=$rep->inter;
           // $rep->paid_amount=0;
            $rep->save();
        }
    }

    // public function correctTotalPayments(){
    //     $repayments=Repayment::all();
    //     foreach($repayments as $rep){
    //         $loan=Loan::where('id',$rep->loan_id)->first();
    //         $loan->
    //     }
    // }

    public function addYearandMonthNumber(){

        $repayments =Repayment::all();
        foreach($repayments as $rep){
            $date = new Carbon( $rep->payment_date );
             $rep->year=$date->year;
             $rep->month=$date->month;
             $rep->save();
        }

    }

    public function bulkLocation(){

    }

    //updating ledger wrongs
    public function correctLedger(){

        $loans=Loan::where('visibility',1)->get();

        foreach($loans as $loan){
            $tot_paid=0;
            $balance=$loan->loan_amount+$loan->interest;

        $repayments=Repayment::where('loan_id',$loan->id)->orderBy('payment_date')->get();
        foreach($repayments as $repayment){
            $repayment->total_payed=$tot_paid+$repayment->paid_amount;
            $repayment->due_amount=$balance-$repayment->paid_amount;
            $repayment->save();
            $tot_paid=$tot_paid+$repayment->paid_amount;
            $balance=$balance-$repayment->paid_amount;
        }
        $loan->paid=$tot_paid;
        $loan->due=$balance;
        $loan->save();



        }

        return response()->json('success');
    }


    public function clearDenominatioons(){
        $repayments= Repayment::all();
        foreach($repayments as $rep){
            $rep->deliminated='0';
            $rep->save();
        }
        return "completed";
    }


   
}
