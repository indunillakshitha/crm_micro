<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangeStaffPasswordController extends Controller
{
   public function index(){
    return view('admin.change_passwoed');
   }
//
   public function store(Request $request){
    $status = Hash::check($request->old_p, auth()->user()->password);
    if($status==true){
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->password)]);
        return redirect()->back()->with('message', 'Password Changed Succesfully');
    }else{
        return redirect()->back()->with('message', 'Current Password is not Matching');
    }
   }
}
