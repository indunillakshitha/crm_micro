<?php

namespace App\Http\Controllers;

use App\CompanyExpencesCategory;
use App\CompanyExpences;
use App\CompanyExpencesSummary;
use Illuminate\Http\Request;
use DB;
class CompantExpencesController extends Controller
{

    public function index()
    {
        $categories = CompanyExpencesCategory::where('new_company_expences.visibility',1)->get();
        $voucher_id = CompanyExpencesSummary::where('new_company_expences.visibility',1)->get();
        $voucher_id = count($voucher_id) + 1;
        // return $voucher_id;
        return view('company_ex.index', compact('categories', 'voucher_id'));
    }


    public function addExpense(Request $request)
    {
        // return response()->json($request);


         $expense = CompanyExpences::create($request->all());

        if($request->hasFile('image')){
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $filenameToStore = $filename.'_'.time().'.'.$extension; //The Timestamp makes each image's name unique
            $path = $request->file('image')->storeAs('public/images',$filenameToStore);

        }
        else{
            $filenameToStore ='noimage.jpg';
        }
        $expense->image = $filenameToStore;
        $expense->save();

        $voucher_id = CompanyExpencesSummary::where('voucher_id', $request->voucher_id)->first();

        if ($voucher_id) {
            $voucher_id->invoice_total += $request->item_total;
            $voucher_id->save();
        } else {

            $new = new CompanyExpencesSummary;
            $new->voucher_id = $request->voucher_id;
            $new->invoice_total = $request->item_total;
            $new->date = $request->date;
            $new->save();

        }

        return response()->json('success');
    }

    public function getExpense(Request $request)
    {
        $data = CompanyExpences::where('visibility', 1)->where('voucher_id', $request->voucher_id)->get();
        return response()->json($data);
    }

    public function deleteExpense(Request $request)
    {
        $record = CompanyExpences::find($request->id);

        $voucher_id = CompanyExpencesSummary::where('voucher_id', $record->voucher_id)->first();
        $voucher_id->invoice_total -= $record->item_total;

        $record->visibility = 0;
        $record->save();
        $voucher_id->save();

        return response()->json($record);
    }

    public function all(){
        // $expences=CompanyExpencesSummary::all();
        // return view('company_ex.main',compact('expences'));
        $expences=CompanyExpencesSummary::leftjoin('new_company_expences','new_company_expences.voucher_id','new_company_expences_summery.voucher_id')
        ->where('new_company_expences.visibility',1)
        ->select(DB::raw("sum(new_company_expences.	item_total) as total"),'new_company_expences.voucher_id','new_company_expences.date','new_company_expences.category'
                    ,'new_company_expences.payee','new_company_expences_summery.status')
        ->groupBy('new_company_expences.voucher_id','new_company_expences.date','new_company_expences.category','new_company_expences.payee','new_company_expences_summery.status')
        ->orderBy('new_company_expences.date')
        ->get();

         return view('company_ex.main',compact('expences'));

    }

    public function edit($id){
        $categories = CompanyExpencesCategory::all();

        $summery=CompanyExpencesSummary::where('voucher_id',$id)->first();
        $expences=CompanyExpences::where('voucher_id',$id)->where('visibility',1)->get();
        return view('company_ex.edit',compact('summery','expences','categories'));
    }
}
