<?php

namespace App\Http\Controllers;

use App\Expenses;

use App\ReferenceExpenses;
use Illuminate\Http\Request;
use App\ExpensesType;


class ReferenceExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $expenses = Expenses::latest()->paginate();
            return view('Expenses.expensesIndex', compact('expenses'))
                ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $refID= $id;
        $expensesTypes = ExpensesType::all();

        return view('Expenses.referenceExpensesAdd',compact('refID','expensesTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ReferenceExpenses::create($request->all());
        return redirect()->route('referenceExpense.index')
                        ->with('success','Expenses created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReferenceExpenses  $referenceExpenses
     * @return \Illuminate\Http\Response
     */
    public function show(ReferenceExpenses $referenceExpenses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReferenceExpenses  $referenceExpenses
     * @return \Illuminate\Http\Response
     */
    public function edit(ReferenceExpenses $referenceExpenses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReferenceExpenses  $referenceExpenses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReferenceExpenses $referenceExpenses)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReferenceExpenses  $referenceExpenses
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReferenceExpenses $referenceExpenses)
    {
        //
    }
}
