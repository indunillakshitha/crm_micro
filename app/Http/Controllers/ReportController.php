<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Borrower;
use App\Center;
use App\Loan;
use App\Appraisal;
use App\Branch;
use App\Category;
use App\Expenses;
use App\UserChart;
use Illuminate\Support\Facades\Auth;
use App\ReportsDetails;
use App\Repayment;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Sabberworm\CSS\CSSList\Document;
use Chartisan\PHP\Chartisan;
use Illuminate\Support\Facades\DB;


class ReportController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $issued_loans =Loan::where('status','Payed')->count();
        $active_loans = Loan::where('status','Payed')->count();
        $setle_loans=Loan::where('status','SETTLED')->count();
        $new_loans=Loan::where('status','Pending')->count();
        $pending_loans=Loan::where('status','Pending')->get()->count();
        return view('Reports.indexReports',compact('issued_loans','active_loans','setle_loans','new_loans','pending_loans'));
    }

// ===========================================Collections Rerports===================================================

    public function collectionsReport()
    {

        $branches = Branch::all();
        $centers = Center::all();
        return view('Reports.collectionReports',compact('centers','branches'));

    }

    public function getdaily(Request $request)
    {
        $branchUp = strtoupper($request->branch);
        $centerUp = strtoupper($request->center);
        if($request->ref == 0){
        $test = Repayment::where('branch',$request->branch)->where('center',$request->center)->where('branch',$branchUp)->where('center',$centerUp)->get();
        return response()->json($test);
        }elseif($request->ref == 2){
            $test1 = $request->day % 7;

            if($test1 == 0){

                $check = $request->day - 7;
                $test = Repayment::whereBetween('payment_date_index',[$check,$request->day])->where('branch',$request->branch)->where('center',$request->center)->get();
                return response()->json($test);
            }else{

                $check = $request->day - $test1;
                $check2 = $request->day + 7 ;
                $test = Repayment::whereBetween('payment_date_index',[$check,$check2])->where('branch',$request->branch)->where('center',$request->center)->where('branch',$branchUp)->where('center',$centerUp)->get();
                return response()->json($test);
            }
        }elseif($request->ref == 3){
            $test1 = $request->day % 30;

            if($test1 == 0){

                $check = $request->day - 30;
                $test = Repayment::whereBetween('payment_date_index',[$check,$request->day])->where('branch',$request->branch)->where('center',$request->center)->where('branch',$branchUp)->where('center',$centerUp)->get();
                return response()->json($test);
            }else{

                $check = $request->day - $test1;
                $check2 = $request->day + 30 ;
                $test = Repayment::whereBetween('payment_date_index',[$check,$check2])->where('branch',$request->branch)->where('center',$request->center)->where('branch',$branchUp)->where('center',$centerUp)->get();
                return response()->json($test);
            }
        }elseif($request->ref == 1){
           $test = Repayment::where('payment_date_index',$request->day)->where('branch',$request->branch)->where('center',$request->center)->where('branch',$branchUp)->where('center',$centerUp)->get();
            return response()->json($test);
        }
    }

    public function getbranch(Request $request)
    {

        $branchUp = strtoupper($request->branch);
        $data =$request->branch;
        $test = Repayment::where('branch',$data)->where('branch',$branchUp)->get();
        return response()->json($test);

    }

    public function getcenter(Request $request)
    {

        $centerUp = strtoupper($request->center);
        $data =$request->center;
        $test = Repayment::where('center',$data)->where('center',$centerUp)->get();
        return response()->json($test);
    }

    public function testing(Request $request)
    {

        $centerUp = strtoupper($request->center);
        $calcu = Repayment::where('center',$request->center)->where('center',$centerUp)->get();
        $cat = Category::all();
        $catcount=Category::all()->count();
        $repcount =Repayment::all()->count();
        $loan1 = Repayment::where('loan_amount','10000')->where('center',$request->center)->where('center',$centerUp)->sum('paid_amount');
        $loacount1 = Repayment::where('loan_amount','10000')->where('center',$request->center)->where('center',$centerUp)->count('paid_amount');

        $loan2 = Repayment::where('loan_amount','15000')->where('center',$request->center)->where('center',$centerUp)->sum('paid_amount');
        $loacount2 = Repayment::where('loan_amount','15000')->where('center',$request->center)->where('center',$centerUp)->count('paid_amount');

        $loan3 = Repayment::where('loan_amount','20000')->where('center',$request->center)->where('center',$centerUp)->sum('paid_amount');
        $loacount3 = Repayment::where('loan_amount','20000')->where('center',$request->center)->where('center',$centerUp)->count('paid_amount');

        $loan4 = Repayment::where('loan_amount','25000')->where('center',$request->center)->where('center',$centerUp)->sum('paid_amount');
        $loacount4 = Repayment::where('loan_amount','25000')->where('center',$request->center)->where('center',$centerUp)->count('paid_amount');

        $loan5 = Repayment::where('loan_amount','15000')->where('center',$request->center)->where('center',$centerUp)->where('installment','1218.75')->sum('paid_amount');
        $loacount5 = Repayment::where('loan_amount','15000')->where('center',$request->center)->where('center',$centerUp)->where('installment','1218.75')->count('paid_amount');



        $tot = Repayment::where('center',$request->center)->where('center',$centerUp)->sum('paid_amount');
        $totloacount = Repayment::where('center',$request->center)->where('center',$centerUp)->count('paid_amount');
        $array = [$totloacount,$tot, $loacount1,$loan1,$loacount2,$loan2,$loacount3,$loan3,$loacount4,$loan4,$loacount5,$loan5];
        return response()->json($array);







    }
//============================================Interest Collections Report=======================================================================

 public function interestCollectionsReport()
    {

        $branches = Branch::all();
        $centers = Center::all();
        return view('Reports.interestCollectionsReports',compact('centers','branches'));

    }

    public function icrgetdaily(Request $request)
    {
        $data =$request->ref;
        return response()->json($data);

    }

    public function icrgetbranch(Request $request)
    {

        $data =$request->branch;
        $test = Repayment::where('branch',$data)->get();

        return response()->json($test);
    }

    public function icrgetcenter(Request $request)
    {

        $data =$request->center;
        $test = Repayment::where('center',$data)->get();
        return response()->json($test);
    }

//============================================Capital Collections Report=======================================================================

    public function capitalCollectionsReport()
    {

        $branches = Branch::all();
        $centers = Center::all();
        return view('Reports.capitalCollectionsReports',compact('centers','branches'));

    }

    public function ccrgetdaily(Request $request)
    {
        $data =$request->ref;
        return response()->json($data);

    }

    public function ccrgetbranch(Request $request)
    {

        $data =$request->branch;
        return response()->json($data);
    }

    public function ccrgetcenter(Request $request)
    {

        $data =$request->center;
        $test = Repayment::where('center',$data)->get();
        return response()->json($test);
    }

//============================================Outstanding Collections Report=======================================================================

    public function outstandingCollectionsReport()
    {

        $branches = Branch::all();
        $centers = Center::all();
        return view('Reports.collectionReports',compact('centers','branches'));

    }

    public function ocrgetdaily(Request $request)
    {
        $data =$request->ref;
        return response()->json($data);

    }

    public function ocrgetbranch(Request $request)
    {

        $data =$request->branch;
        return response()->json($data);
    }

    public function ocrgetcenter(Request $request)
    {

        $data =$request->center;
        return response()->json($data);
    }

//============================================Future Outstanding Collections Report=======================================================================

    public function futureInterestCollectionsReport()
    {

        $branches = Branch::all();
        $centers = Center::all();
        return view('Reports.collectionReports',compact('centers','branches'));

    }

    public function ficrgetdaily(Request $request)
    {
        $data =$request->ref;
        return response()->json($data);

    }

    public function ficrgetbranch(Request $request)
    {

        $data =$request->branch;
        return response()->json($data);
    }

    public function ficrgetcenter(Request $request)
    {

        $data =$request->center;
        return response()->json($data);
    }

//============================================Capital Outstanding Collections Report=======================================================================

    public function capitalOutstandingCollectionsReport()
    {

        $branches = Branch::all();
        $centers = Center::all();
        return view('Reports.collectionReports',compact('centers','branches'));

    }

    public function cocrgetdaily(Request $request)
    {
        $data =$request->ref;
        return response()->json($data);

    }

    public function coicrgetbranch(Request $request)
    {

        $data =$request->branch;
        return response()->json($data);
    }

    public function coicrgetcenter(Request $request)
    {

        $data =$request->center;
        return response()->json($data);
    }

//============================================Total Outstanding Collections Report=======================================================================

    public function totalOutstandingCollectionsReport()
    {

        $branches = Branch::all();
        $centers = Center::all();
        return view('Reports.totalOutstandingCollectionsReport',compact('centers','branches'));

    }

    public function tocrgetdaily(Request $request)
    {
        $data =$request->ref;
        return response()->json($data);

    }

    public function tocrgetbranch(Request $request)
    {

        $data =$request->branch;
        return response()->json($data);
    }

    public function tocrgetcenter(Request $request)
    {

        $data =$request->center;
        return response()->json($data);
    }


    //============================================Arrearse Interest Collections Report=======================================================================

 public function arrearsInterestCollectionsReport()
    {

        $branches = Branch::all();
        $centers = Center::all();
        return view('Reports.arrearsInterestReports',compact('centers','branches'));

    }

    public function aicrgetdaily(Request $request)
    {
        $data =$request->ref;
        return response()->json($data);

    }

    public function aicrgetbranch(Request $request)
    {

        $data =$request->branch;
        return response()->json($data);
    }

    public function aicrgetcenter(Request $request)
    {

        $data =$request->center;
        return response()->json($data);
    }

//============================================Arrearse Capital Collections Report=======================================================================

 public function arrearseCapitalCollectionsReport()
    {

        $branches = Branch::all();
        $centers = Center::all();
        return view('Reports.arrearsCapitalReports',compact('centers','branches'));

    }

    public function accrgetdaily(Request $request)
    {
        $data =$request->ref;
        return response()->json($data);

    }

    public function accrgetbranch(Request $request)
    {

        $data =$request->branch;
        return response()->json($data);
    }

    public function accrgetcenter(Request $request)
    {

        $data =$request->center;
        return response()->json($data);
    }


//============================================Advanced Payments Collections Report=======================================================================

 public function advancedPaymentsCollectionsReport()
    {

        $branches = Branch::all();
        $centers = Center::all();
        return view('Reports.advancePaymentsReports',compact('centers','branches'));

    }

    public function apcrgetdaily(Request $request)
    {
        $data =$request->ref;
        return response()->json($data);

    }

    public function apcrgetbranch(Request $request)
    {

        $data =$request->branch;
        return response()->json($data);
    }

    public function apcrgetcenter(Request $request)
    {

        $data =$request->center;
        return response()->json($data);
    }


// ------------------------------------------DashBoard View-----------------------------------------------------


    public function dashboardView(Request $request)
    {

        $branches = Branch::all();
        $centers = Center::all();
        $time =Carbon::now()->isoFormat('DDD');
        return view('Reports.dashboardView',compact('centers','branches','time'));
    }

     public function details(Request $request)
    {
        $centerUp = strtoupper($request->center);
        $loans = Loan::where('center',$request->center)->where('center',$centerUp)->where('status','Payed')->get();
        return response()->json($loans);




    }

    public function dashboardViewCollection(Request $request)
    {

        $branches = Branch::all();
        $centers = Center::all();
        $time =Carbon::now()->isoFormat('DDD');
        $name = Auth::user()->branch;
        $upName = strtoupper($name);
        $tot = Repayment::where('branch',$name)->where('branch',$upName)->sum('total_payed');
        $totloacount = Repayment::where('branch',$name)->where('branch',$upName)->count('total_payed');


        ReportsDetails::truncate();
        $centersC = Center::select('center_name')->get();
        // $centerUp = strtoupper($request->center);
        // $loans = Loan::where('center',$request->center)->where('center',$centerUp)->where('status','Payed')->get();
        foreach($centersC as $center){
        $nameC = $center->center_name;
        $upNameC = strtoupper($nameC);
        $totC = Repayment::where('center',$nameC)->where('center',$upNameC)->sum('total_payed');
        $totloacountC = Repayment::where('center',$nameC)->where('center',$upNameC)->count('total_payed');
        $details = new ReportsDetails;
        $details->center_name = $center->center_name;
        $details->center_collection = $totC;
        $details->loan_count_collection = $totloacountC;
        $details->save();


        }
        $data = ReportsDetails::all();
        return view('Reports.dashboardCollections',compact('centers','branches','time','totloacount','tot','data'))
        ->with('i', (request()->input('page', 1) - 1) * 5);;
    }

    //  public function detailsCollection(Request $request)
    // {
    //     ReportsDetails::truncate();
    //     $centersC = Center::select('center_name')->get();
    //     // $centerUp = strtoupper($request->center);
    //     // $loans = Loan::where('center',$request->center)->where('center',$centerUp)->where('status','Payed')->get();
    //     foreach($centersC as $center){
    //     $nameC = $center->center_name;
    //     $upNameC = strtoupper($nameC);
    //     $totC = Repayment::where('center',$nameC)->where('center',$upNameC)->sum('total_payed');
    //     $totloacountC = Repayment::where('center',$nameC)->where('center',$upNameC)->count('total_payed');
    //     $details = new ReportsDetails;
    //     $details->center_name = $center->center_name;
    //     $details->center_collection = $totC;
    //     $details->loan_count_collection = $totloacountC;
    //     $details->save();


    //     }
    //     $data = ReportsDetails::get();
    //     // return response()->json($data);
    //     return view('Center.indexCenter', compact('centers'))
    //             ->with('i', (request()->input('page', 1) - 1) * 5);




    // }

    public function centerTotal(){
         $total=DB::table('loans')
        ->select(DB::raw("sum(loans.paid) as paid"),DB::raw("sum(loans.due) as balance"), 'loans.center')
        ->groupBy('loans.center', 'loans.center')
        ->orderBy('loans.id','desc')
        ->get();

    return view('Reports.centertotal',compact('total'));
    }
    public function centerDetails($id){
         $totals=DB::table('loans')
                ->select(DB::raw("sum(loans.paid) as paid"),'loans.borrower_no','loans.loan_amount','loans.payments_count','loans.loan_duration','loans.status',DB::raw("sum(loans.due) as balance"), 'loans.center')
                ->where('loans.center',$id)
                ->groupBy('loans.borrower_no','loans.loan_amount','loans.payments_count','loans.loan_duration','loans.status','loans.center')
                ->orderBy('loans.id')
                ->get();

    return view('Reports.centertotaldetails',compact('totals'));
    }

    public function chartsOne()
    {
// Loans Tot
        $users = Borrower::select('id')->where('id','<',6)->get();
        $loan1= Loan::where('status','Payed')->where('loan_amount','10000')->sum('loan_amount');
        $loan2= Loan::where('status','Payed')->where('loan_amount','15000')->sum('loan_amount');
        $loan3= Loan::where('status','Payed')->where('loan_amount','20000')->sum('loan_amount');
        $loan4= Loan::where('status','Payed')->where('loan_amount','25000')->sum('loan_amount');
        $users = array($loan1,$loan2,$loan3,$loan4);

// Loan Count
        $cloans = array(0,0,0,0,0,0,0,0,0,0,0,0);
        for($i=1;$i<12;$i++){
        $cloan = Loan::where('status','Payed')->whereMonth('payment_starting_date','=',$i)->count('id');
        $i=$i-1;
        $cloans[$i] = $cloan;
        $i=$i+1;
        }

// Expenses
        $exps = array(0,0,0,0,0,0,0,0,0,0,0,0);
        for($i=1;$i<12;$i++){
        $exp = Expenses::select('expense_amount')->whereMonth('expense_date','=',$i)->sum('expense_amount');
        $i=$i-1;
        $exps[$i] = $exp;
        $i=$i+1;
        }

// Collections
        $cols = array(0,0,0,0,0,0,0,0,0,0,0,0);
        for($i=1;$i<12;$i++){
        $col = Repayment::whereMonth('payment_date','=',$i)->sum('paid_amount');
        $i=$i-1;
        $cols[$i] = $col;
        $i=$i+1;
        }

// Collections Capital
        $colsCap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $colInt = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $colAdv = array(0,0,0,0,0,0,0,0,0,0,0,0);

        for($i=1;$i<12;$i++){
            $repays = Repayment::whereMonth('payment_date','=',$i)->get();
            $i=$i-1;
            $capitalCol = 0;
            $interestCol = 0;
            $advanced = 0;
                foreach($repays as $repay){
                    $cap = $repay->paid_amount - $repay->installment;
                    $cat = Loan::select('loan_duration','interest')->where('id',$repay->loan_id)->first();
                    $interest = $cat->interest / $cat->loan_duration;
                    $capital = $repay->installment -  $interest;
                    $int =  $repay->paid_amount - $capital;
                    if($cap >=0){
                        $capitalCol = $capital + $capitalCol;
                        $interestCol = $interest + $interestCol;
                        $advanced = $cap + $advanced;
                    }elseif($repay->paid_amount >= $capital ){
                        $capitalCol = $capital + $capitalCol;
                        $interestCol = $interestCol + $int;

                    }else{
                        $arri = $repay->paid_amount;
                    }
                }
            $colsCap[$i] = round($capitalCol,2);
            $colInt[$i] = round($interestCol,2);
            $colAdv[$i] = round($advanced,2);
            $i=$i+1;
        }

// Arrearse Capital
        $arrCap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $arrInt = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $arrTot = array(0,0,0,0,0,0,0,0,0,0,0,0);

        for($i=1;$i<12;$i++){
            $repayss = Repayment::whereMonth('payment_date','=',$i)->get();
            $i=$i-1;
            $capitalArr = 0;
            $interestArr = 0;
            $totalArr = 0;
                foreach($repayss as $repay){
                    $pay =  $repay->installment - $repay->paid_amount ;
                    $cato = Loan::select('loan_duration','interest')->where('id',$repay->loan_id)->first();
                    $interestt = $cato->interest / $cato->loan_duration;
                    $capitalo = $repay->installment -  $interestt;
                    $interestAmount =  $repay->paid_amount - $capitalo;
                    if($pay >=0){
                        $totalArr = $totalArr + $pay;

                    }else{
                        $arrii = $repay->paid_amount;
                    }
                }
            $arrTot[$i] = round($totalArr,2);

                foreach($repayss as $repay){
                    $payy =  $repay->installment - $repay->paid_amount ;
                    $catt = Loan::select('loan_duration','interest')->where('id',$repay->loan_id)->first();
                    $interesttt = $catt->interest / $catt->loan_duration;
                    $capitall = $repay->installment -  $interesttt;
                    $interestAmount =  $repay->paid_amount - $capitall;
                    if($repay->paid_amount <= $capitall ){
                        $prec = $capitall - $repay->paid_amount;
                        $capitalArr = $capitalArr + $prec;
                        $interestArr = $interestArr + $interesttt;
                    }elseif(($repay->paid_amount < $repay->installment) && ($repay->paid_amount > $capital)){
                        $prei = $repay->paid_amount - $capitall;
                        $preii = $interesttt - $prei;
                        $interestArr = $interestArr + $preii;
                    }else{
                        $arriii = $repay->paid_amount;
                    }

                }
                $arrCap[$i] = round($capitalArr,2);
                $arrInt[$i] = round($interestArr,2);
            $i=$i+1;
        }
// Outstanding
        $outTot = array(0,0,0);
        $out = Loan::where('status','Payed')->sum('due');
        $outTot[0] = $out;




return view ('admin.charts',['Users' => $users,'Cols' => $cols,'Exps' => $exps,'Cloans' => $cloans,'ColCap' => $colsCap,'ColInt' => $colInt,
                              'ColAdv' => $colAdv,'ArrTot' => $arrTot,'ArrCap' => $arrCap,'ArrInt' => $arrInt,'OutStd' => $outTot]);

    }

    public function arrearsCustomerReviewReport(){

        $branches = Branch::all();
        $centers = Center::all();
        return view('Reports.arrearseCustomerReview',compact('centers','branches'));

    }

    public function getBranchCV(Request $request){

        $today_index=Carbon::now()->isoFormat('DDD');
        $tot_ar_cunt = 0;
        $tot_ar = 0;
        $under_pay =0;
        $under_pay_cunt = 0;
        $send = array(0,0,0,0);
        if(!empty($request->branch) && !empty($request->center)){

            $loans = Loan::where('branch',$request->branch)->where('center',$request->center)->where('status','payed')->get();
            foreach($loans as $loan){

                $repayments = Repayment::where('loan_id',$loan->id)->orderBy('payment_date_index','asc')->get();
                $repayments_count = Repayment::where('loan_id',$loan->id)->count('id');
                $loan_start_date = $loan->payment_starting_date_index - 7;
                $at_today_paymnt_cunt1 = $today_index - $loan_start_date;
                $at_today_paymnt_cunt = floor($at_today_paymnt_cunt1 / 7);

                if($at_today_paymnt_cunt < $loan->loan_duration){
                    if($at_today_paymnt_cunt > $repayments_count){
                        $arrearse_cunt = $at_today_paymnt_cunt - $repayments_count ;
                        $arrearse = $arrearse_cunt * $loan->instalment;
                        $tot_ar = $tot_ar + $arrearse;
                        $tot_ar_cunt = $tot_ar_cunt + $arrearse_cunt;
                        $arrearse_cunt = 0;
                        $arrearse = 0;
                    }else{}
                }else{}
                foreach($repayments as $repay){
                    if($repay->less_paid_amount > 0 ){
                        $under_pay = $under_pay + $repay->less_paid_amount;
                        $under_pay_cunt = $under_pay_cunt + 1;
                    }else{}
                }
            }
        }elseif(!empty($request->branch)){

                    $loans = Loan::where('branch',$request->branch)->where('status','payed')->get();
            foreach($loans as $loan){

                $repayments = Repayment::where('loan_id',$loan->id)->orderBy('payment_date_index','asc')->get();
                $repayments_count = Repayment::where('loan_id',$loan->id)->count('id');
                $at_today_paymnt_cunt1 = $today_index - $loan->payment_starting_date_index;
                $at_today_paymnt_cunt = floor($at_today_paymnt_cunt1 / 7);

                if($at_today_paymnt_cunt < $loan->loan_duration){
                    if($at_today_paymnt_cunt > $repayments_count){
                        $arrearse_cunt = $at_today_paymnt_cunt - $repayments_count ;
                        $arrearse = $arrearse_cunt * $loan->instalment;
                        $tot_ar = $tot_ar + $arrearse;
                        $tot_ar_cunt = $tot_ar_cunt + $arrearse_cunt;
                        $arrearse_cunt = 0;
                        $arrearse = 0;
                    }else{}

                }else{}
                foreach($repayments as $repay){
                    if($repay->less_paid_amount > 0 ){
                        $under_pay = $under_pay + $repay->less_paid_amount;
                        $under_pay_cunt = $under_pay_cunt + 1;
                    }else{}
                }

            }

        }else{}
            $send[0] = round($tot_ar,2);
            $send[1] = $tot_ar_cunt;
            $send[2] = round($under_pay,2);
            $send[3] = $under_pay_cunt;




        return response()->json(['Send' => $send]);

    }

    public function weekCustomerReview(){

        $branches = Branch::all();
        $centers = Center::all();
        return view('Reports.weekCustomerReview',compact('centers','branches'));

    }

    public function getWeekBranchCV(Request $request){

        $today_index=Carbon::now()->isoFormat('DDD');
        $today_index_range = $today_index - 8;
        $tot_ar_cunt = 0;
        $tot_ar = 0;
        $under_pay =0;
        $under_pay_cunt = 0;
        $send = array(0,0,0,0);
        if(!empty($request->branch) && !empty($request->center)){

            $loans = Loan::where('branch',$request->branch)->where('center',$request->center)
                        ->where('last_payment_date','<',$today_index)->where('last_payment_date','>',$today_index_range)
                        ->where('status','payed')->get();
            $loans_ars = Loan::where('branch',$request->branch)->where('center',$request->center)
                        ->where('last_payment_date','<',$today_index_range)
                        ->where('status','payed')->get();
            foreach($loans_ars as $loan_ar){

                $tot_ar_cunt = $tot_ar_cunt + 1;
                $tot_ar = $tot_ar + $loan_ar->instalment;
            }

            foreach($loans as $loan){

                $repy = Repayment::where('loan_id',$loan->id)->where('payment_date_index',$loan->last_payment_date)->first();

                if($repy->less_paid_amount != 0){

                $under_pay_cunt = $under_pay_cunt + 1;
                $under_pay = $under_pay + $repy->less_paid_amount;

                }else{}
            }
        }elseif(!empty($request->branch)){

            $loans = Loan::where('branch',$request->branch)->where('last_payment_date','<',$today_index)->where('last_payment_date','>',$today_index_range)
                        ->where('status','payed')->get();
            $loans_ars = Loan::where('branch',$request->branch)
                        ->where('last_payment_date','<',$today_index_range)
                        ->where('status','payed')->get();
            foreach($loans_ars as $loan_ar){

                $tot_ar_cunt = $tot_ar_cunt + 1;
                $tot_ar = $tot_ar + $loan_ar->instalment;
            }

            foreach($loans as $loan){

                $repy = Repayment::where('loan_id',$loan->id)->where('payment_date_index',$loan->last_payment_date)->first();

                if($repy->less_paid_amount != 0){

                $under_pay_cunt = $under_pay_cunt + 1;
                $under_pay = $under_pay + $repy->less_paid_amount;

                }else{}
            }


        }else{}
            $send[0] = round($tot_ar,2);
            $send[1] = $tot_ar_cunt;
            $send[2] = round($under_pay,2);
            $send[3] = $under_pay_cunt;




        return response()->json(['Send' => $send]);

    }

    public function getCustomer(){

        $branches = Branch::all();
        $centers = Center::all();
        return view('Reports.customerReview',compact('centers','branches'));

    }

    public function getCustomerBranchCV(Request $request){

        $reports = DB::table('loans')
                ->leftjoin('borrowers','loans.borrower_no','=','borrowers.borrower_no')
                // ->where('status','payed')
                // ->where('branch',$request->branch)
                ->get();

                return response()->json($reports);
    }

    public function WeekRepayDetalis(Request $request){

        $branchUp = strtoupper($request->branch);
        $centerUp = strtoupper($request->center);
        $Sdate = Carbon::parse($request->day)->startOfWeek()->isoFormat('DDD');
        $Edate = Carbon::parse($request->day)->endOfWeek()->isoFormat('DDD');
        $weekRepy = array(0,0,0,0,0,0,0);
        $set = 0;
        $totWeekDay = 0;
        if(!empty($request->branch) && !empty($request->center)){

        $test = Repayment::whereBetween('payment_date_index',[$Sdate,$Edate])->where('branch',$request->branch)->where('center',$request->center)->get();
        for($i=0; $i<7; $i++){
            $checkDay = $Sdate + $i;
            foreach($test as $reLoan){
                $ln_start = $reLoan->payment_date_index;
                if($ln_start == $checkDay){
                    $set = $reLoan->paid_amount;
                    $totWeekDay = $totWeekDay + $set;

                }else{

                }

        }
        $weekRepy[$i] = $totWeekDay;

        $totWeekDay = 0;
    }
        }elseif(!empty($request->branch)){
            $test = Repayment::whereBetween('payment_date_index',[$Sdate,$Edate])->where('branch',$request->branch)->get();
        for($i=0; $i<7; $i++){
            $checkDay = $Sdate + $i;
            foreach($test as $reLoan){
                $ln_start = $reLoan->payment_date_index;
                if($ln_start == $checkDay){
                    $set = $reLoan->paid_amount;
                    $totWeekDay = $totWeekDay + $set;

                }else{

                }

        }
        $weekRepy[$i] = $totWeekDay;

        $totWeekDay = 0;
    }

        }elseif(!empty($request->center)){
            $test = Repayment::whereBetween('payment_date_index',[$Sdate,$Edate])->where('center',$request->center)->get();
        for($i=0; $i<7; $i++){
            $checkDay = $Sdate + $i;
            foreach($test as $reLoan){
                $ln_start = $reLoan->payment_date_index;
                if($ln_start == $checkDay){
                    $set = $reLoan->paid_amount;
                    $totWeekDay = $totWeekDay + $set;

                }else{

                }

        }
        $weekRepy[$i] = $totWeekDay;

        $totWeekDay = 0;
    }
        }else{
            
        }
        return response()->json(['Week' => $weekRepy]);


    }

// _________________________________For Testing_______________________________________________________
    public function testChart(){

        $branches = Branch::all();
        $centers = Center::all();
        return view('chartsDemo.testChartAjax',compact('centers','branches'));
    }

        public function testCollection(Request $request)
    {
        $ColVal = array();
        $ColLeb = array();
        if(!empty($request->branch) && !empty($request->center)){

            
            for($i =1 ; $i <10; $i++){
                $ColVal[] = $i;
                $ColLeb[] = $i+1;
            }
      

        }elseif(!empty($request->branch)){

            for($i =1 ; $i <10; $i++){
                $ColVal[] = $i+5;
                $ColLeb[] = $i+10;
            }

        }elseif(!empty($request->center)){

            for($i =1 ; $i <10; $i++){
                $ColVal[] = $i+20;
                $ColLeb[] = $i+40;
            }

        }else{

        }
        return response()->json(['CoAdv' => $ColVal,'ClAdv' => $ColLeb]);

    }

// __________________________________________________________________________________________________________________
}
