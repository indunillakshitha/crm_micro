<?php

namespace App\Http\Controllers;

use App\Borrower;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
class api_vueAddressController extends Controller
{
    public function getAndSetAddress($nic){
        $b = Borrower::where('nic', $nic)->first();
        $address = $b->address;
        $name = $b->full_name;

        /*
        // $string = 'The event will take place between ? and ?';
        // $replaced = Str::replaceArray('?', ['8:30', '9:00'], $string);
        */
        $address = str_replace(' ', '_', $address);
        $address = str_replace('/', '-', $address);


        $name = str_replace(' ', '_', $name);
        $name = str_replace('/', '-', $name);

        // return response()->json($address);

        return redirect("https://cantylivelocations.firebaseapp.com/location/$nic/$address/$name");
        // return redirect("http://localhost:8080/location/$nic/$address");
    }
}
