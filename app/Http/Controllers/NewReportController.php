<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Center;
use App\Repayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NewReportController extends Controller
{
    public function centerWiseDue($id){
          $mon = DB::table('shedules')->leftJoin('loans', 'loans.center', 'shedules.center')
        ->where('loans.status', 'Payed')
        ->where('shedules.date',$id)
        ->select(DB::raw("sum(loans.instalment) as sum"),'shedules.center')
        ->groupBy('shedules.center')
        ->get();
          $collection = DB::table('shedules')->leftJoin('loans', 'loans.center', 'shedules.center')
         ->leftJoin('repayments', 'repayments.loan_id', 'loans.id')
        ->where('loans.status', 'Payed')
        ->where('repayments.week_index', Carbon::now()->isoFormat('W'))
        ->where('shedules.date',$id)
        ->select(DB::raw("sum(loans.instalment) as sum"),DB::raw("sum(repayments.paid_amount) as collection"),'shedules.center')
        ->groupBy('shedules.center')
        ->get();

        return view('admin.centerWiseDue',compact('mon','collection'));
    }


    public function newDashboard(){

        $branches = Branch::all();
        $centers = Center::all();
        return view('newreports.dashboard',compact('branches','centers'));
    }


    public function collectionReport(Request $request){
        // return response()->json($request);
        // $request(branch,center,dateFrom,dateTo,select_period_type)
        $from = Carbon::parse($request->dateFrom)->isoFormat('DDD');
        $to = Carbon::parse($request->dateTo)->isoFormat('DDD');
        $data=Repayment::where('center',$request->center)
                       ->where('branch',$request->branch)
                       ->whereBetween('payment_date_index',[$from,$to])
                       ->select(
                           DB::raw("sum(paid_amount) as total"),
                                 DB::raw("sum(capital) as capital"),
                                 DB::raw("sum(inter) as interest"),
                                 'repayments.week_index'
                                 )
                        ->groupBy('repayments.week_index')
                       ->get();
        return response()->json($data);

        $ColVal = array();
        $ColLeb = array();

        $ColVal = $data;
        $ColLeb = $data;

            // for($i =1 ; $i <10; $i++){
            //     $ColVal[] = $i+20;
            //     $ColLeb[] = $i+40;
            // }


        return response()->json(['CoAdv' => $ColVal,'ClAdv' => $ColLeb]);
    }
}
