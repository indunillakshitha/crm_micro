<?php

namespace App\Http\Controllers;

use App\Appraisal_earning_product;
use Illuminate\Http\Request;

class addAppraisalEarningProductController extends Controller
{
    public function index(){
        $apprasailEarningProducts =Appraisal_earning_product::all();

        return view('Appraisal.appraisalearningindex',compact('apprasailEarningProducts'))
            ->with('i',(request()->input('page',1)-1)*5);
    }


    public function store(Request $request){
        $request->validate([
            'product'=>'required',
            'category_name'=>'required',
            'min'=>'required',
            'max'=>'required'
        ]);

        Appraisal_earning_product::create($request->all());

        return redirect()->route('appraisalearning.index')
            ->with('success','Appraisal Earning Product created successfully.');

    }
}
