<?php

namespace App\Http\Controllers;

use App\Borrower;
use App\Branch;
use App\Center;
use App\CompanyExpences;
use App\Loan;
use App\Shedule;
use App\Staff;
use NumberFormatter;
use PhpOffice\PhpWord\TemplateProcessor;

class PrintController extends Controller
{
    public function cheque()
    {

        $branches = Branch::all();
        return view('Cheque.printCheque')->with('branches', $branches);
    }

    public function exportPromissory($id)
    {
        
        $loan = Loan::where('id', $id)->first();
        $borrower = Borrower::find($loan->borrower_no);
        $promissory = new TemplateProcessor('word-template/Promissory.docx');
        $witnessA = Staff::where('branch', $borrower->branch)->where('designation', 'Agent')->first();

        $digit = new NumberFormatter("en", NumberFormatter::SPELLOUT);
        $loan_amount_spell = $digit->format($loan->loan_amount);
        $interest_rate_spell = $digit->format($loan->interest_rate);
        $loan_amount_spells = ucwords($loan_amount_spell);
        $interest_rate_spells = ucwords($interest_rate_spell);

        $promissory->setValue('release_date', $loan->release_date);
        $promissory->setValue('loan_amount', $loan->loan_amount);
        $promissory->setValue('loan_amount_spell', $loan_amount_spells);
        $promissory->setValue('address', $borrower->address);
        $promissory->setValue('full_name', $borrower->full_name);
        $promissory->setValue('nic', $borrower->nic);
        $promissory->setValue('borrower_no', $borrower->borrower_no);
        $promissory->setValue('interest_rate', $loan->interest_rate);
        $promissory->setValue('interest_rate_spell', $interest_rate_spells);
        $promissory->setValue('application_no', $borrower->application_no);

        $promissory->setValue('name_witnessA', $witnessA->name);
        $promissory->setValue('nic_witnessA', $witnessA->nic);
        $promissory->setValue('address_witnessA', $witnessA->address);

        $fileName = $borrower->borrower_no . " Promissory";

        $promissory->saveAs($fileName . '.docx');
        return response()->download($fileName . '.docx')->deleteFileAfterSend(true);
    }

    public function exportAgreement($id)
    {
        $loan = Loan::where('id', $id)->first();
        $borrower = Borrower::find($loan->borrower_no);
        $witnessL = Borrower::where('borrower_no', $loan->witnessC)->first();
        $witnessA = Staff::where('branch', $borrower->branch)->where('designation', 'Agent')->first();

        $agreement = new TemplateProcessor('word-template/Agreement.docx');

        $digit = new NumberFormatter("en", NumberFormatter::SPELLOUT);
        $loan_amount_spell = $digit->format($loan->loan_amount);
        $interest_rate_spell = $digit->format($loan->interest_rate);
        $loan_amount_spells = ucwords($loan_amount_spell);
        $interest_rate_spells = ucwords($interest_rate_spell);
        $releseDate = $loan->release_date;
        $oderDate = explode('-', $releseDate);
        $year = $oderDate[0];
        $month = $oderDate[1];
        $day = $oderDate[2];

        $agreement->setValue('release_date', $releseDate);
        $agreement->setValue('loan_amount', $loan->loan_amount);
        $agreement->setValue('loan_amount_spell', $loan_amount_spells);
        $agreement->setValue('address', $borrower->address);
        $agreement->setValue('full_name', $borrower->full_name);
        $agreement->setValue('nic', $borrower->nic);
        $agreement->setValue('borrower_no', $borrower->borrower_no);
        $agreement->setValue('interest_rate', $loan->interest_rate);
        $agreement->setValue('interest_rate_spell', $interest_rate_spells);
        $agreement->setValue('application_no', $borrower->application_no);
        $agreement->setValue('branch', $borrower->branch);
        $agreement->setValue('loan_duration', $loan->loan_duration);
        $agreement->setValue('instalment', $loan->instalment);
        $agreement->setValue('year', $year);
        $agreement->setValue('month', $month);
        $agreement->setValue('day', $day);

        $agreement->setValue('name_witnessL', $witnessL->full_name);
        $agreement->setValue('nic_witnessL', $witnessL->nic);
        $agreement->setValue('name_witnessA', $witnessA->name);
        $agreement->setValue('nic_witnessA', $witnessA->nic);

        $agreementName = $borrower->borrower_no . " Agreement";
        $agreement->saveAs($agreementName . '.docx');
        return response()->download($agreementName . '.docx')->deleteFileAfterSend(true);
    }

    public function exportGuarantorBond($id)
    {
        $loan = Loan::where('id', $id)->first();
        $borrower = Borrower::where('borrower_no', $loan->borrower_no)->first();
        $allMembers = Borrower::where('branch', $borrower->branch)
            ->where('center', $borrower->center)->where('group_no', $borrower->group_no)->where('status', $borrower->status = 'active')->get();

        $allMembersArray = json_decode($allMembers, true);

        if (($loan->borrower_no) == $allMembersArray[0]["borrower_no"]) {
            unset($allMembersArray[0]);
        } else if (($loan->borrower_no) == $allMembersArray[1]["borrower_no"]) {
            unset($allMembersArray[1]);
        } else if (($loan->borrower_no) == $allMembersArray[2]["borrower_no"]) {
            unset($allMembersArray[2]);
        } else if (($loan->borrower_no) == $allMembersArray[3]["borrower_no"]) {
            unset($allMembersArray[3]);
        } else if (($loan->borrower_no) == $allMembersArray[4]["borrower_no"]) {
            unset($allMembersArray[4]);
        }

        $guarantorsArray = array_values($allMembersArray);

        $count = count($guarantorsArray);

        $guarantorBond = new TemplateProcessor('word-template/GuarantorBond.docx');

        $digit = new NumberFormatter("en", NumberFormatter::SPELLOUT);
        $loan_amount_spell = $digit->format($loan->loan_amount);
        $loan_amount_spells = ucwords($loan_amount_spell);

        if ($count == 4) {

            $name_1 = $guarantorsArray[0]["full_name"];
            $name_2 = $guarantorsArray[1]["full_name"];
            $name_3 = $guarantorsArray[2]["full_name"];
            $name_4 = $guarantorsArray[3]["full_name"];

            $address_1 = $guarantorsArray[0]["address"];
            $address_2 = $guarantorsArray[1]["address"];
            $address_3 = $guarantorsArray[2]["address"];
            $address_4 = $guarantorsArray[3]["address"];

            $nic_1 = $guarantorsArray[0]["nic"];
            $nic_2 = $guarantorsArray[1]["nic"];
            $nic_3 = $guarantorsArray[2]["nic"];
            $nic_4 = $guarantorsArray[3]["nic"];

            $guarantorBond->setValue('name_1', $name_1);
            $guarantorBond->setValue('address_1', $address_1);
            $guarantorBond->setValue('nic_1', $nic_1);

            $guarantorBond->setValue('name_2', $name_2);
            $guarantorBond->setValue('address_2', $address_2);
            $guarantorBond->setValue('nic_2', $nic_2);

            $guarantorBond->setValue('name_3', $name_3);
            $guarantorBond->setValue('address_3', $address_3);
            $guarantorBond->setValue('nic_3', $nic_3);

            $guarantorBond->setValue('name_4', $name_4);
            $guarantorBond->setValue('address_4', $address_4);
            $guarantorBond->setValue('nic_4', $nic_4);
        } else if ($count == 3) {

            $name_1 = $guarantorsArray[0]["full_name"];
            $name_2 = $guarantorsArray[1]["full_name"];
            $name_3 = $guarantorsArray[2]["full_name"];

            $address_1 = $guarantorsArray[0]["address"];
            $address_2 = $guarantorsArray[1]["address"];
            $address_3 = $guarantorsArray[2]["address"];

            $nic_1 = $guarantorsArray[0]["nic"];
            $nic_2 = $guarantorsArray[1]["nic"];
            $nic_3 = $guarantorsArray[2]["nic"];

            $guarantorBond->setValue('name_1', $name_1);
            $guarantorBond->setValue('address_1', $address_1);
            $guarantorBond->setValue('nic_1', $nic_1);

            $guarantorBond->setValue('name_2', $name_2);
            $guarantorBond->setValue('address_2', $address_2);
            $guarantorBond->setValue('nic_2', $nic_2);

            $guarantorBond->setValue('name_3', $name_3);
            $guarantorBond->setValue('address_3', $address_3);
            $guarantorBond->setValue('nic_3', $nic_3);

            $guarantorBond->setValue('name_4', '');
            $guarantorBond->setValue('address_4', '');
            $guarantorBond->setValue('nic_4', '');
        } else if ($count == 2) {

            $name_1 = $guarantorsArray[0]["full_name"];
            $name_2 = $guarantorsArray[1]["full_name"];

            $address_1 = $guarantorsArray[0]["address"];
            $address_2 = $guarantorsArray[1]["address"];

            $nic_1 = $guarantorsArray[0]["nic"];
            $nic_2 = $guarantorsArray[1]["nic"];

            $guarantorBond->setValue('name_1', $name_1);
            $guarantorBond->setValue('address_1', $address_1);
            $guarantorBond->setValue('nic_1', $nic_1);

            $guarantorBond->setValue('name_2', $name_2);
            $guarantorBond->setValue('address_2', $address_2);
            $guarantorBond->setValue('nic_2', $nic_2);

            $guarantorBond->setValue('name_3', '');
            $guarantorBond->setValue('address_3', '');
            $guarantorBond->setValue('nic_3', '');

            $guarantorBond->setValue('name_4', '');
            $guarantorBond->setValue('address_4', '');
            $guarantorBond->setValue('nic_4', '');
        } else if ($count == 1) {

            $name_1 = $guarantorsArray[0]["full_name"];

            $address_1 = $guarantorsArray[0]["address"];

            $nic_1 = $guarantorsArray[0]["nic"];

            $guarantorBond->setValue('name_1', $name_1);
            $guarantorBond->setValue('address_1', $address_1);
            $guarantorBond->setValue('nic_1', $nic_1);

            $guarantorBond->setValue('name_2', '');
            $guarantorBond->setValue('address_2', '');
            $guarantorBond->setValue('nic_2', '');

            $guarantorBond->setValue('name_3', '');
            $guarantorBond->setValue('address_3', '');
            $guarantorBond->setValue('nic_3', '');

            $guarantorBond->setValue('name_4', '');
            $guarantorBond->setValue('address_4', '');
            $guarantorBond->setValue('nic_4', '');
        }

        $guarantorBond->setValue('borrower_no', $loan->borrower_no);
        $guarantorBond->setValue('release_date', $loan->release_date);
        $guarantorBond->setValue('loan_amount', $loan->loan_amount);
        $guarantorBond->setValue('loan_amount_spell', $loan_amount_spells);
        $guarantorBond->setValue('address', $borrower->address);
        $guarantorBond->setValue('full_name', $borrower->full_name);
        $guarantorBond->setValue('nic', $borrower->nic);
        $guarantorBond->setValue('application_no', $borrower->application_no);
        $releseDate = $loan->release_date;
        $oderDate = explode('-', $releseDate);
        $year = $oderDate[0];
        $month = $oderDate[1];
        $day = $oderDate[2];
        $guarantorBond->setValue('year', $year);
        $guarantorBond->setValue('month', $month);
        $guarantorBond->setValue('day', $day);

        $guarantorBondName = $borrower->borrower_no . " Guarantor Bond";
        $guarantorBond->saveAs($guarantorBondName . '.docx');
        return response()->download($guarantorBondName . '.docx')->deleteFileAfterSend(true);
    }

    public function exportRepayments($branchName, $centerName)
    {
        $branch = Branch::where('branch_name', $branchName)->first();
        $center = Center::where('branch_no', $branch->branch_name)->where('center_name', $centerName)->first();
        $group_1 = Borrower::where('branch', $branchName)->where('center', $centerName)->where('group_no', 1)->get();
        $group_2 = Borrower::where('branch', $branchName)->where('center', $centerName)->where('group_no', 2)->get();
        $group_3 = Borrower::where('branch', $branchName)->where('center', $centerName)->where('group_no', 3)->get();
        $group_4 = Borrower::where('branch', $branchName)->where('center', $centerName)->where('group_no', 4)->get();
        $group_5 = Borrower::where('branch', $branchName)->where('center', $centerName)->where('group_no', 5)->get();
        //------------------------------------------------------------------------------------------------------------

        $countGroup_1 = count($group_1);
        $countGroup_2 = count($group_2);
        $countGroup_3 = count($group_3);
        $countGroup_4 = count($group_4);
        $countGroup_5 = count($group_5);

        $repayment = new TemplateProcessor('word-template/Repayments.docx');

        //------------------------- group  1  --------------------------------------------------------

        if ($countGroup_1 == 5) {

            $loan_11 = Loan::where('borrower_no', $group_1[0]['borrower_no'])->first();
            $loan_12 = Loan::where('borrower_no', $group_1[1]['borrower_no'])->first();
            $loan_13 = Loan::where('borrower_no', $group_1[2]['borrower_no'])->first();
            $loan_14 = Loan::where('borrower_no', $group_1[3]['borrower_no'])->first();
            $loan_15 = Loan::where('borrower_no', $group_1[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no11', $group_1[0]['borrower_no']);
            $repayment->setValue('borrower_no12', $group_1[1]['borrower_no']);
            $repayment->setValue('borrower_no13', $group_1[2]['borrower_no']);
            $repayment->setValue('borrower_no14', $group_1[3]['borrower_no']);
            $repayment->setValue('borrower_no15', $group_1[4]['borrower_no']);

            $repayment->setValue('name_11', $group_1[0]['full_name']);
            $repayment->setValue('name_12', $group_1[1]['full_name']);
            $repayment->setValue('name_13', $group_1[2]['full_name']);
            $repayment->setValue('name_14', $group_1[3]['full_name']);
            $repayment->setValue('name_15', $group_1[4]['full_name']);

            $repayment->setValue('mobile_no11', $group_1[0]['mobile_no']);
            $repayment->setValue('mobile_no12', $group_1[1]['mobile_no']);
            $repayment->setValue('mobile_no13', $group_1[2]['mobile_no']);
            $repayment->setValue('mobile_no14', $group_1[3]['mobile_no']);
            $repayment->setValue('mobile_no15', $group_1[4]['mobile_no']);

            $repayment->setValue('loan_amount11', $loan_11->loan_amount);
            $repayment->setValue('loan_amount12', $loan_12->loan_amount);
            $repayment->setValue('loan_amount13', $loan_13->loan_amount);
            $repayment->setValue('loan_amount14', $loan_14->loan_amount);
            $repayment->setValue('loan_amount15', $loan_15->loan_amount);

            $repayment->setValue('loan_due11', $loan_11->due);
            $repayment->setValue('loan_due12', $loan_12->due);
            $repayment->setValue('loan_due13', $loan_13->due);
            $repayment->setValue('loan_due14', $loan_14->due);
            $repayment->setValue('loan_due15', $loan_15->due);

        } else if ($countGroup_1 == 4) {

            $loan_11 = Loan::where('borrower_no', $group_1[0]['borrower_no'])->first();
            $loan_12 = Loan::where('borrower_no', $group_1[1]['borrower_no'])->first();
            $loan_13 = Loan::where('borrower_no', $group_1[2]['borrower_no'])->first();
            $loan_14 = Loan::where('borrower_no', $group_1[3]['borrower_no'])->first();
            // $loan_15 = Loan::where('borrower_no', $group_1[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no11', $group_1[0]['borrower_no']);
            $repayment->setValue('borrower_no12', $group_1[1]['borrower_no']);
            $repayment->setValue('borrower_no13', $group_1[2]['borrower_no']);
            $repayment->setValue('borrower_no14', $group_1[3]['borrower_no']);
            $repayment->setValue('borrower_no15', '');

            $repayment->setValue('name_11', $group_1[0]['full_name']);
            $repayment->setValue('name_12', $group_1[1]['full_name']);
            $repayment->setValue('name_13', $group_1[2]['full_name']);
            $repayment->setValue('name_14', $group_1[3]['full_name']);
            $repayment->setValue('name_15', '');

            $repayment->setValue('mobile_no11', $group_1[0]['mobile_no']);
            $repayment->setValue('mobile_no12', $group_1[1]['mobile_no']);
            $repayment->setValue('mobile_no13', $group_1[2]['mobile_no']);
            $repayment->setValue('mobile_no14', $group_1[3]['mobile_no']);
            $repayment->setValue('mobile_no15', '');

            $repayment->setValue('loan_amount11', $loan_11->loan_amount);
            $repayment->setValue('loan_amount12', $loan_12->loan_amount);
            $repayment->setValue('loan_amount13', $loan_13->loan_amount);
            $repayment->setValue('loan_amount14', $loan_14->loan_amount);
            $repayment->setValue('loan_amount15', '');

            $repayment->setValue('loan_due11', $loan_11->due);
            $repayment->setValue('loan_due12', $loan_12->due);
            $repayment->setValue('loan_due13', $loan_13->due);
            $repayment->setValue('loan_due14', $loan_14->due);
            $repayment->setValue('loan_due15', '');

        } else if ($countGroup_1 == 3) {

            $loan_11 = Loan::where('borrower_no', $group_1[0]['borrower_no'])->first();
            $loan_12 = Loan::where('borrower_no', $group_1[1]['borrower_no'])->first();
            $loan_13 = Loan::where('borrower_no', $group_1[2]['borrower_no'])->first();
            // $loan_14 = Loan::where('borrower_no', $group_1[3]['borrower_no'])->first();
            // $loan_15 = Loan::where('borrower_no', $group_1[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no11', $group_1[0]['borrower_no']);
            $repayment->setValue('borrower_no12', $group_1[1]['borrower_no']);
            $repayment->setValue('borrower_no13', $group_1[2]['borrower_no']);
            $repayment->setValue('borrower_no14', '');
            $repayment->setValue('borrower_no15', '');

            $repayment->setValue('name_11', $group_1[0]['full_name']);
            $repayment->setValue('name_12', $group_1[1]['full_name']);
            $repayment->setValue('name_13', $group_1[2]['full_name']);
            $repayment->setValue('name_14', '');
            $repayment->setValue('name_15', '');

            $repayment->setValue('mobile_no11', $group_1[0]['mobile_no']);
            $repayment->setValue('mobile_no12', $group_1[1]['mobile_no']);
            $repayment->setValue('mobile_no13', $group_1[2]['mobile_no']);
            $repayment->setValue('mobile_no14', '');
            $repayment->setValue('mobile_no15', '');

            $repayment->setValue('loan_amount11', $loan_11->loan_amount);
            $repayment->setValue('loan_amount12', $loan_12->loan_amount);
            $repayment->setValue('loan_amount13', $loan_13->loan_amount);
            $repayment->setValue('loan_amount14', '');
            $repayment->setValue('loan_amount15', '');

            $repayment->setValue('loan_due11', $loan_11->due);
            $repayment->setValue('loan_due12', $loan_12->due);
            $repayment->setValue('loan_due13', $loan_13->due);
            $repayment->setValue('loan_due14', '');
            $repayment->setValue('loan_due15', '');

        } else if ($countGroup_1 == 2) {

            $loan_11 = Loan::where('borrower_no', $group_1[0]['borrower_no'])->first();
            $loan_12 = Loan::where('borrower_no', $group_1[1]['borrower_no'])->first();
            // $loan_13 = Loan::where('borrower_no', $group_1[2]['borrower_no'])->first();
            // $loan_14 = Loan::where('borrower_no', $group_1[3]['borrower_no'])->first();
            // $loan_15 = Loan::where('borrower_no', $group_1[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no11', $group_1[0]['borrower_no']);
            $repayment->setValue('borrower_no12', $group_1[1]['borrower_no']);
            $repayment->setValue('borrower_no13', '');
            $repayment->setValue('borrower_no14', '');
            $repayment->setValue('borrower_no15', '');

            $repayment->setValue('name_11', $group_1[0]['full_name']);
            $repayment->setValue('name_12', $group_1[1]['full_name']);
            $repayment->setValue('name_13', '');
            $repayment->setValue('name_14', '');
            $repayment->setValue('name_15', '');

            $repayment->setValue('mobile_no11', $group_1[0]['mobile_no']);
            $repayment->setValue('mobile_no12', $group_1[1]['mobile_no']);
            $repayment->setValue('mobile_no13', '');
            $repayment->setValue('mobile_no14', '');
            $repayment->setValue('mobile_no15', '');

            $repayment->setValue('loan_amount11', $loan_11->loan_amount);
            $repayment->setValue('loan_amount12', $loan_12->loan_amount);
            $repayment->setValue('loan_amount13', '');
            $repayment->setValue('loan_amount14', '');
            $repayment->setValue('loan_amount15', '');

            $repayment->setValue('loan_due11', $loan_11->due);
            $repayment->setValue('loan_due12', $loan_12->due);
            $repayment->setValue('loan_due13', '');
            $repayment->setValue('loan_due14', '');
            $repayment->setValue('loan_due15', '');

            // $repayment->setValue('loan_amount11',$groupLoan_1[0]['loan_amount']);
        } else if ($countGroup_1 == 0) {

            // $loan_11 = Loan::where('borrower_no', $group_1[0]['borrower_no'])->first();
            // $loan_12 = Loan::where('borrower_no', $group_1[1]['borrower_no'])->first();
            // $loan_13 = Loan::where('borrower_no', $group_1[2]['borrower_no'])->first();
            // $loan_14 = Loan::where('borrower_no', $group_1[3]['borrower_no'])->first();
            // $loan_15 = Loan::where('borrower_no', $group_1[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no11', '');
            $repayment->setValue('borrower_no12', '');
            $repayment->setValue('borrower_no13', '');
            $repayment->setValue('borrower_no14', '');
            $repayment->setValue('borrower_no15', '');

            $repayment->setValue('name_11', '');
            $repayment->setValue('name_12', '');
            $repayment->setValue('name_13', '');
            $repayment->setValue('name_14', '');
            $repayment->setValue('name_15', '');

            $repayment->setValue('mobile_no11', '');
            $repayment->setValue('mobile_no12', '');
            $repayment->setValue('mobile_no13', '');
            $repayment->setValue('mobile_no14', '');
            $repayment->setValue('mobile_no15', '');

            $repayment->setValue('loan_amount11', '');
            $repayment->setValue('loan_amount12', '');
            $repayment->setValue('loan_amount13', '');
            $repayment->setValue('loan_amount14', '');
            $repayment->setValue('loan_amount15', '');

            $repayment->setValue('loan_due11', '');
            $repayment->setValue('loan_due12', '');
            $repayment->setValue('loan_due13', '');
            $repayment->setValue('loan_due14', '');
            $repayment->setValue('loan_due15', '');

            // $repayment->setValue('loan_amount11',$groupLoan_1[0]['loan_amount']);
        }

        //------------------------- group  2  --------------------------------------------------------

        if ($countGroup_2 == 5) {

            $loan_21 = Loan::where('borrower_no', $group_2[0]['borrower_no'])->first();
            $loan_22 = Loan::where('borrower_no', $group_2[1]['borrower_no'])->first();
            $loan_23 = Loan::where('borrower_no', $group_2[2]['borrower_no'])->first();
            $loan_24 = Loan::where('borrower_no', $group_2[3]['borrower_no'])->first();
            $loan_25 = Loan::where('borrower_no', $group_2[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no21', $group_2[0]['borrower_no']);
            $repayment->setValue('borrower_no22', $group_2[1]['borrower_no']);
            $repayment->setValue('borrower_no23', $group_2[2]['borrower_no']);
            $repayment->setValue('borrower_no24', $group_2[3]['borrower_no']);
            $repayment->setValue('borrower_no25', $group_2[4]['borrower_no']);

            $repayment->setValue('name_21', $group_2[0]['full_name']);
            $repayment->setValue('name_22', $group_2[1]['full_name']);
            $repayment->setValue('name_23', $group_2[2]['full_name']);
            $repayment->setValue('name_24', $group_2[3]['full_name']);
            $repayment->setValue('name_25', $group_2[4]['full_name']);

            $repayment->setValue('mobile_no21', $group_2[0]['mobile_no']);
            $repayment->setValue('mobile_no22', $group_2[1]['mobile_no']);
            $repayment->setValue('mobile_no23', $group_2[2]['mobile_no']);
            $repayment->setValue('mobile_no24', $group_2[3]['mobile_no']);
            $repayment->setValue('mobile_no25', $group_2[4]['mobile_no']);

            $repayment->setValue('loan_amount21', $loan_21->loan_amount);
            $repayment->setValue('loan_amount22', $loan_22->loan_amount);
            $repayment->setValue('loan_amount23', $loan_23->loan_amount);
            $repayment->setValue('loan_amount24', $loan_24->loan_amount);
            $repayment->setValue('loan_amount25', $loan_25->loan_amount);

            $repayment->setValue('loan_due21', $loan_21->due);
            $repayment->setValue('loan_due22', $loan_22->due);
            $repayment->setValue('loan_due23', $loan_23->due);
            $repayment->setValue('loan_due24', $loan_24->due);
            $repayment->setValue('loan_due25', $loan_25->due);

        } else if ($countGroup_2 == 4) {

            $loan_21 = Loan::where('borrower_no', $group_2[0]['borrower_no'])->first();
            $loan_22 = Loan::where('borrower_no', $group_2[1]['borrower_no'])->first();
            $loan_23 = Loan::where('borrower_no', $group_2[2]['borrower_no'])->first();
            $loan_24 = Loan::where('borrower_no', $group_2[3]['borrower_no'])->first();
            // $loan_25 = Loan::where('borrower_no', $group_2[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no21', $group_2[0]['borrower_no']);
            $repayment->setValue('borrower_no22', $group_2[1]['borrower_no']);
            $repayment->setValue('borrower_no23', $group_2[2]['borrower_no']);
            $repayment->setValue('borrower_no24', $group_2[3]['borrower_no']);
            $repayment->setValue('borrower_no25', '');

            $repayment->setValue('name_21', $group_2[0]['full_name']);
            $repayment->setValue('name_22', $group_2[1]['full_name']);
            $repayment->setValue('name_23', $group_2[2]['full_name']);
            $repayment->setValue('name_24', $group_2[3]['full_name']);
            $repayment->setValue('name_25', '');

            $repayment->setValue('mobile_no21', $group_2[0]['mobile_no']);
            $repayment->setValue('mobile_no22', $group_2[1]['mobile_no']);
            $repayment->setValue('mobile_no23', $group_2[2]['mobile_no']);
            $repayment->setValue('mobile_no24', $group_2[3]['mobile_no']);
            $repayment->setValue('mobile_no25', '');

            $repayment->setValue('loan_amount21', $loan_21->loan_amount);
            $repayment->setValue('loan_amount22', $loan_22->loan_amount);
            $repayment->setValue('loan_amount23', $loan_23->loan_amount);
            $repayment->setValue('loan_amount24', $loan_24->loan_amount);
            $repayment->setValue('loan_amount25', '');

            $repayment->setValue('loan_due21', $loan_21->due);
            $repayment->setValue('loan_due22', $loan_22->due);
            $repayment->setValue('loan_due23', $loan_23->due);
            $repayment->setValue('loan_due24', $loan_24->due);
            $repayment->setValue('loan_due25', '');

        } else if ($countGroup_2 == 3) {

            $loan_21 = Loan::where('borrower_no', $group_2[0]['borrower_no'])->first();
            $loan_22 = Loan::where('borrower_no', $group_2[1]['borrower_no'])->first();
            $loan_23 = Loan::where('borrower_no', $group_2[2]['borrower_no'])->first();
            // $loan_24 = Loan::where('borrower_no', $group_2[3]['borrower_no'])->first();
            // $loan_25 = Loan::where('borrower_no', $group_2[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no21', $group_2[0]['borrower_no']);
            $repayment->setValue('borrower_no22', $group_2[1]['borrower_no']);
            $repayment->setValue('borrower_no23', $group_2[2]['borrower_no']);
            $repayment->setValue('borrower_no24', '');
            $repayment->setValue('borrower_no25', '');

            $repayment->setValue('name_21', $group_2[0]['full_name']);
            $repayment->setValue('name_22', $group_2[1]['full_name']);
            $repayment->setValue('name_23', $group_2[2]['full_name']);
            $repayment->setValue('name_24', '');
            $repayment->setValue('name_25', '');

            $repayment->setValue('mobile_no21', $group_2[0]['mobile_no']);
            $repayment->setValue('mobile_no22', $group_2[1]['mobile_no']);
            $repayment->setValue('mobile_no23', $group_2[2]['mobile_no']);
            $repayment->setValue('mobile_no24', '');
            $repayment->setValue('mobile_no25', '');

            $repayment->setValue('loan_amount21', $loan_21->loan_amount);
            $repayment->setValue('loan_amount22', $loan_22->loan_amount);
            $repayment->setValue('loan_amount23', $loan_23->loan_amount);
            $repayment->setValue('loan_amount24', '');
            $repayment->setValue('loan_amount25', '');

            $repayment->setValue('loan_due21', $loan_21->due);
            $repayment->setValue('loan_due22', $loan_22->due);
            $repayment->setValue('loan_due23', $loan_23->due);
            $repayment->setValue('loan_due24', '');
            $repayment->setValue('loan_due25', '');

        } else if ($countGroup_2 == 2) {

            $loan_21 = Loan::where('borrower_no', $group_2[0]['borrower_no'])->first();
            $loan_22 = Loan::where('borrower_no', $group_2[1]['borrower_no'])->first();
            // $loan_23 = Loan::where('borrower_no', $group_2[2]['borrower_no'])->first();
            // $loan_24 = Loan::where('borrower_no', $group_2[3]['borrower_no'])->first();
            // $loan_25 = Loan::where('borrower_no', $group_2[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no21', $group_2[0]['borrower_no']);
            $repayment->setValue('borrower_no22', $group_2[1]['borrower_no']);
            $repayment->setValue('borrower_no23', '');
            $repayment->setValue('borrower_no24', '');
            $repayment->setValue('borrower_no25', '');

            $repayment->setValue('name_21', $group_2[0]['full_name']);
            $repayment->setValue('name_22', $group_2[1]['full_name']);
            $repayment->setValue('name_23', '');
            $repayment->setValue('name_24', '');
            $repayment->setValue('name_25', '');

            $repayment->setValue('mobile_no21', $group_2[0]['mobile_no']);
            $repayment->setValue('mobile_no22', $group_2[1]['mobile_no']);
            $repayment->setValue('mobile_no23', '');
            $repayment->setValue('mobile_no24', '');
            $repayment->setValue('mobile_no25', '');

            $repayment->setValue('loan_amount21', $loan_21->loan_amount);
            $repayment->setValue('loan_amount22', $loan_22->loan_amount);
            $repayment->setValue('loan_amount23', '');
            $repayment->setValue('loan_amount24', '');
            $repayment->setValue('loan_amount25', '');

            $repayment->setValue('loan_due21', $loan_21->due);
            $repayment->setValue('loan_due22', $loan_22->due);
            $repayment->setValue('loan_due23', '');
            $repayment->setValue('loan_due24', '');
            $repayment->setValue('loan_due25', '');
        } else if ($countGroup_2 == 0) {

            $repayment->setValue('borrower_no21', '');
            $repayment->setValue('borrower_no22', '');
            $repayment->setValue('borrower_no23', '');
            $repayment->setValue('borrower_no24', '');
            $repayment->setValue('borrower_no25', '');

            $repayment->setValue('name_21', '');
            $repayment->setValue('name_22', '');
            $repayment->setValue('name_23', '');
            $repayment->setValue('name_24', '');
            $repayment->setValue('name_25', '');

            $repayment->setValue('mobile_no21', '');
            $repayment->setValue('mobile_no22', '');
            $repayment->setValue('mobile_no23', '');
            $repayment->setValue('mobile_no24', '');
            $repayment->setValue('mobile_no25', '');

            $repayment->setValue('loan_amount21', '');
            $repayment->setValue('loan_amount22', '');
            $repayment->setValue('loan_amount23', '');
            $repayment->setValue('loan_amount24', '');
            $repayment->setValue('loan_amount25', '');

            $repayment->setValue('loan_due21', '');
            $repayment->setValue('loan_due22', '');
            $repayment->setValue('loan_due23', '');
            $repayment->setValue('loan_due24', '');
            $repayment->setValue('loan_due25', '');
        }

        //------------------------- group  3  --------------------------------------------------------

        if ($countGroup_3 == 5) {

            $loan_31 = Loan::where('borrower_no', $group_3[0]['borrower_no'])->first();
            $loan_32 = Loan::where('borrower_no', $group_3[1]['borrower_no'])->first();
            $loan_33 = Loan::where('borrower_no', $group_3[2]['borrower_no'])->first();
            $loan_34 = Loan::where('borrower_no', $group_3[3]['borrower_no'])->first();
            $loan_35 = Loan::where('borrower_no', $group_3[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no31', $group_3[0]['borrower_no']);
            $repayment->setValue('borrower_no32', $group_3[1]['borrower_no']);
            $repayment->setValue('borrower_no33', $group_3[2]['borrower_no']);
            $repayment->setValue('borrower_no34', $group_3[3]['borrower_no']);
            $repayment->setValue('borrower_no35', $group_3[4]['borrower_no']);

            $repayment->setValue('name_31', $group_3[0]['full_name']);
            $repayment->setValue('name_32', $group_3[1]['full_name']);
            $repayment->setValue('name_33', $group_3[2]['full_name']);
            $repayment->setValue('name_34', $group_3[3]['full_name']);
            $repayment->setValue('name_35', $group_3[4]['full_name']);

            $repayment->setValue('mobile_no31', $group_3[0]['mobile_no']);
            $repayment->setValue('mobile_no32', $group_3[1]['mobile_no']);
            $repayment->setValue('mobile_no33', $group_3[2]['mobile_no']);
            $repayment->setValue('mobile_no34', $group_3[3]['mobile_no']);
            $repayment->setValue('mobile_no35', $group_3[4]['mobile_no']);

            $repayment->setValue('loan_amount31', $loan_31->loan_amount);
            $repayment->setValue('loan_amount32', $loan_32->loan_amount);
            $repayment->setValue('loan_amount33', $loan_33->loan_amount);
            $repayment->setValue('loan_amount34', $loan_34->loan_amount);
            $repayment->setValue('loan_amount35', $loan_35->loan_amount);

            $repayment->setValue('loan_due31', $loan_31->due);
            $repayment->setValue('loan_due32', $loan_32->due);
            $repayment->setValue('loan_due33', $loan_33->due);
            $repayment->setValue('loan_due34', $loan_34->due);
            $repayment->setValue('loan_due35', $loan_35->due);

        } else if ($countGroup_3 == 4) {

            $loan_31 = Loan::where('borrower_no', $group_3[0]['borrower_no'])->first();
            $loan_32 = Loan::where('borrower_no', $group_3[1]['borrower_no'])->first();
            $loan_33 = Loan::where('borrower_no', $group_3[2]['borrower_no'])->first();
            $loan_34 = Loan::where('borrower_no', $group_3[3]['borrower_no'])->first();
            // $loan_35 = Loan::where('borrower_no', $group_3[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no31', $group_3[0]['borrower_no']);
            $repayment->setValue('borrower_no32', $group_3[1]['borrower_no']);
            $repayment->setValue('borrower_no33', $group_3[2]['borrower_no']);
            $repayment->setValue('borrower_no34', $group_3[3]['borrower_no']);
            $repayment->setValue('borrower_no35', '');

            $repayment->setValue('name_31', $group_3[0]['full_name']);
            $repayment->setValue('name_32', $group_3[1]['full_name']);
            $repayment->setValue('name_33', $group_3[2]['full_name']);
            $repayment->setValue('name_34', $group_3[3]['full_name']);
            $repayment->setValue('name_35', '');

            $repayment->setValue('mobile_no31', $group_3[0]['mobile_no']);
            $repayment->setValue('mobile_no32', $group_3[1]['mobile_no']);
            $repayment->setValue('mobile_no33', $group_3[2]['mobile_no']);
            $repayment->setValue('mobile_no34', $group_3[3]['mobile_no']);
            $repayment->setValue('mobile_no35', '');

            $repayment->setValue('loan_amount31', $loan_31->loan_amount);
            $repayment->setValue('loan_amount32', $loan_32->loan_amount);
            $repayment->setValue('loan_amount33', $loan_33->loan_amount);
            $repayment->setValue('loan_amount34', $loan_34->loan_amount);
            $repayment->setValue('loan_amount35', '');

            $repayment->setValue('loan_due31', $loan_31->due);
            $repayment->setValue('loan_due32', $loan_32->due);
            $repayment->setValue('loan_due33', $loan_33->due);
            $repayment->setValue('loan_due34', $loan_34->due);
            $repayment->setValue('loan_due35', '');

        } else if ($countGroup_3 == 3) {

            $loan_31 = Loan::where('borrower_no', $group_3[0]['borrower_no'])->first();
            $loan_32 = Loan::where('borrower_no', $group_3[1]['borrower_no'])->first();
            $loan_33 = Loan::where('borrower_no', $group_3[2]['borrower_no'])->first();
            // $loan_34 = Loan::where('borrower_no', $group_3[3]['borrower_no'])->first();
            // $loan_35 = Loan::where('borrower_no', $group_3[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no31', $group_3[0]['borrower_no']);
            $repayment->setValue('borrower_no32', $group_3[1]['borrower_no']);
            $repayment->setValue('borrower_no33', $group_3[2]['borrower_no']);
            $repayment->setValue('borrower_no34', '');
            $repayment->setValue('borrower_no35', '');

            $repayment->setValue('name_31', $group_3[0]['full_name']);
            $repayment->setValue('name_32', $group_3[1]['full_name']);
            $repayment->setValue('name_33', $group_3[2]['full_name']);
            $repayment->setValue('name_34', '');
            $repayment->setValue('name_35', '');

            $repayment->setValue('mobile_no31', $group_3[0]['mobile_no']);
            $repayment->setValue('mobile_no32', $group_3[1]['mobile_no']);
            $repayment->setValue('mobile_no33', $group_3[2]['mobile_no']);
            $repayment->setValue('mobile_no34', '');
            $repayment->setValue('mobile_no35', '');

            $repayment->setValue('loan_amount31', $loan_31->loan_amount);
            $repayment->setValue('loan_amount32', $loan_32->loan_amount);
            $repayment->setValue('loan_amount33', $loan_33->loan_amount);
            $repayment->setValue('loan_amount34', '');
            $repayment->setValue('loan_amount35', '');

            $repayment->setValue('loan_due31', $loan_31->due);
            $repayment->setValue('loan_due32', $loan_32->due);
            $repayment->setValue('loan_due33', $loan_33->due);
            $repayment->setValue('loan_due34', '');
            $repayment->setValue('loan_due35', '');

        } else if ($countGroup_3 == 2) {

            $loan_31 = Loan::where('borrower_no', $group_3[0]['borrower_no'])->first();
            $loan_32 = Loan::where('borrower_no', $group_3[1]['borrower_no'])->first();
            // $loan_33 = Loan::where('borrower_no', $group_3[2]['borrower_no'])->first();
            // $loan_34 = Loan::where('borrower_no', $group_3[3]['borrower_no'])->first();
            // $loan_35 = Loan::where('borrower_no', $group_3[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no31', $group_3[0]['borrower_no']);
            $repayment->setValue('borrower_no32', $group_3[1]['borrower_no']);
            $repayment->setValue('borrower_no33', '');
            $repayment->setValue('borrower_no34', '');
            $repayment->setValue('borrower_no35', '');

            $repayment->setValue('name_31', $group_3[0]['full_name']);
            $repayment->setValue('name_32', $group_3[1]['full_name']);
            $repayment->setValue('name_33', '');
            $repayment->setValue('name_34', '');
            $repayment->setValue('name_35', '');

            $repayment->setValue('mobile_no31', $group_3[0]['mobile_no']);
            $repayment->setValue('mobile_no32', $group_3[1]['mobile_no']);
            $repayment->setValue('mobile_no33', '');
            $repayment->setValue('mobile_no34', '');
            $repayment->setValue('mobile_no35', '');

            $repayment->setValue('loan_amount31', $loan_31->loan_amount);
            $repayment->setValue('loan_amount32', $loan_32->loan_amount);
            $repayment->setValue('loan_amount33', '');
            $repayment->setValue('loan_amount34', '');
            $repayment->setValue('loan_amount35', '');

            $repayment->setValue('loan_due31', $loan_31->due);
            $repayment->setValue('loan_due32', $loan_32->due);
            $repayment->setValue('loan_due33', '');
            $repayment->setValue('loan_due34', '');
            $repayment->setValue('loan_due35', '');
        } else if ($countGroup_3 == 0) {

            $repayment->setValue('borrower_no31', '');
            $repayment->setValue('borrower_no32', '');
            $repayment->setValue('borrower_no33', '');
            $repayment->setValue('borrower_no34', '');
            $repayment->setValue('borrower_no35', '');

            $repayment->setValue('name_31', '');
            $repayment->setValue('name_32', '');
            $repayment->setValue('name_33', '');
            $repayment->setValue('name_34', '');
            $repayment->setValue('name_35', '');

            $repayment->setValue('mobile_no31', '');
            $repayment->setValue('mobile_no32', '');
            $repayment->setValue('mobile_no33', '');
            $repayment->setValue('mobile_no34', '');
            $repayment->setValue('mobile_no35', '');

            $repayment->setValue('loan_amount31', '');
            $repayment->setValue('loan_amount32', '');
            $repayment->setValue('loan_amount33', '');
            $repayment->setValue('loan_amount34', '');
            $repayment->setValue('loan_amount35', '');

            $repayment->setValue('loan_due31', '');
            $repayment->setValue('loan_due32', '');
            $repayment->setValue('loan_due33', '');
            $repayment->setValue('loan_due34', '');
            $repayment->setValue('loan_due35', '');
        }

        //------------------------- group  4  --------------------------------------------------------

        if ($countGroup_4 == 5) {

            $loan_41 = Loan::where('borrower_no', $group_4[0]['borrower_no'])->first();
            $loan_42 = Loan::where('borrower_no', $group_4[1]['borrower_no'])->first();
            $loan_43 = Loan::where('borrower_no', $group_4[2]['borrower_no'])->first();
            $loan_44 = Loan::where('borrower_no', $group_4[3]['borrower_no'])->first();
            $loan_45 = Loan::where('borrower_no', $group_4[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no41', $group_4[0]['borrower_no']);
            $repayment->setValue('borrower_no42', $group_4[1]['borrower_no']);
            $repayment->setValue('borrower_no43', $group_4[2]['borrower_no']);
            $repayment->setValue('borrower_no44', $group_4[3]['borrower_no']);
            $repayment->setValue('borrower_no45', $group_4[4]['borrower_no']);

            $repayment->setValue('name_41', $group_4[0]['full_name']);
            $repayment->setValue('name_42', $group_4[1]['full_name']);
            $repayment->setValue('name_43', $group_4[2]['full_name']);
            $repayment->setValue('name_44', $group_4[3]['full_name']);
            $repayment->setValue('name_45', $group_4[4]['full_name']);

            $repayment->setValue('mobile_no41', $group_4[0]['mobile_no']);
            $repayment->setValue('mobile_no42', $group_4[1]['mobile_no']);
            $repayment->setValue('mobile_no43', $group_4[2]['mobile_no']);
            $repayment->setValue('mobile_no44', $group_4[3]['mobile_no']);
            $repayment->setValue('mobile_no45', $group_4[4]['mobile_no']);

            $repayment->setValue('loan_amount41', $loan_41->loan_amount);
            $repayment->setValue('loan_amount42', $loan_42->loan_amount);
            $repayment->setValue('loan_amount43', $loan_43->loan_amount);
            $repayment->setValue('loan_amount44', $loan_44->loan_amount);
            $repayment->setValue('loan_amount45', $loan_45->loan_amount);

            $repayment->setValue('loan_due41', $loan_41->due);
            $repayment->setValue('loan_due42', $loan_42->due);
            $repayment->setValue('loan_due43', $loan_43->due);
            $repayment->setValue('loan_due44', $loan_44->due);
            $repayment->setValue('loan_due45', $loan_45->due);

        } else if ($countGroup_4 == 4) {

            $loan_41 = Loan::where('borrower_no', $group_4[0]['borrower_no'])->first();
            $loan_42 = Loan::where('borrower_no', $group_4[1]['borrower_no'])->first();
            $loan_43 = Loan::where('borrower_no', $group_4[2]['borrower_no'])->first();
            $loan_44 = Loan::where('borrower_no', $group_4[3]['borrower_no'])->first();
            // $loan_45 = Loan::where('borrower_no', $group_4[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no41', $group_4[0]['borrower_no']);
            $repayment->setValue('borrower_no42', $group_4[1]['borrower_no']);
            $repayment->setValue('borrower_no43', $group_4[2]['borrower_no']);
            $repayment->setValue('borrower_no44', $group_4[3]['borrower_no']);
            $repayment->setValue('borrower_no45', '');

            $repayment->setValue('name_41', $group_4[0]['full_name']);
            $repayment->setValue('name_42', $group_4[1]['full_name']);
            $repayment->setValue('name_43', $group_4[2]['full_name']);
            $repayment->setValue('name_44', $group_4[3]['full_name']);
            $repayment->setValue('name_45', '');

            $repayment->setValue('mobile_no41', $group_4[0]['mobile_no']);
            $repayment->setValue('mobile_no42', $group_4[1]['mobile_no']);
            $repayment->setValue('mobile_no43', $group_4[2]['mobile_no']);
            $repayment->setValue('mobile_no44', $group_4[3]['mobile_no']);
            $repayment->setValue('mobile_no45', '');

            $repayment->setValue('loan_amount41', $loan_41->loan_amount);
            $repayment->setValue('loan_amount42', $loan_42->loan_amount);
            $repayment->setValue('loan_amount43', $loan_43->loan_amount);
            $repayment->setValue('loan_amount44', $loan_44->loan_amount);
            $repayment->setValue('loan_amount45', '');

            $repayment->setValue('loan_due41', $loan_41->due);
            $repayment->setValue('loan_due42', $loan_42->due);
            $repayment->setValue('loan_due43', $loan_43->due);
            $repayment->setValue('loan_due44', $loan_44->due);
            $repayment->setValue('loan_due45', '');

        } else if ($countGroup_4 == 3) {

            $loan_41 = Loan::where('borrower_no', $group_4[0]['borrower_no'])->first();
            $loan_42 = Loan::where('borrower_no', $group_4[1]['borrower_no'])->first();
            $loan_43 = Loan::where('borrower_no', $group_4[2]['borrower_no'])->first();
            // $loan_44 = Loan::where('borrower_no', $group_4[3]['borrower_no'])->first();
            // $loan_45 = Loan::where('borrower_no', $group_4[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no41', $group_4[0]['borrower_no']);
            $repayment->setValue('borrower_no42', $group_4[1]['borrower_no']);
            $repayment->setValue('borrower_no43', $group_4[2]['borrower_no']);
            $repayment->setValue('borrower_no44', '');
            $repayment->setValue('borrower_no45', '');

            $repayment->setValue('name_41', $group_4[0]['full_name']);
            $repayment->setValue('name_42', $group_4[1]['full_name']);
            $repayment->setValue('name_43', $group_4[2]['full_name']);
            $repayment->setValue('name_44', '');
            $repayment->setValue('name_45', '');

            $repayment->setValue('mobile_no41', $group_4[0]['mobile_no']);
            $repayment->setValue('mobile_no42', $group_4[1]['mobile_no']);
            $repayment->setValue('mobile_no43', $group_4[2]['mobile_no']);
            $repayment->setValue('mobile_no44', '');
            $repayment->setValue('mobile_no45', '');

            $repayment->setValue('loan_amount41', $loan_41->loan_amount);
            $repayment->setValue('loan_amount42', $loan_42->loan_amount);
            $repayment->setValue('loan_amount43', $loan_43->loan_amount);
            $repayment->setValue('loan_amount44', '');
            $repayment->setValue('loan_amount45', '');

            $repayment->setValue('loan_due41', $loan_41->due);
            $repayment->setValue('loan_due42', $loan_42->due);
            $repayment->setValue('loan_due43', $loan_43->due);
            $repayment->setValue('loan_due44', '');
            $repayment->setValue('loan_due45', '');

        } else if ($countGroup_4 == 2) {

            $loan_41 = Loan::where('borrower_no', $group_4[0]['borrower_no'])->first();
            $loan_42 = Loan::where('borrower_no', $group_4[1]['borrower_no'])->first();
            // $loan_43 = Loan::where('borrower_no', $group_4[2]['borrower_no'])->first();
            // $loan_44 = Loan::where('borrower_no', $group_4[3]['borrower_no'])->first();
            // $loan_45 = Loan::where('borrower_no', $group_4[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no41', $group_4[0]['borrower_no']);
            $repayment->setValue('borrower_no42', $group_4[1]['borrower_no']);
            $repayment->setValue('borrower_no43', '');
            $repayment->setValue('borrower_no44', '');
            $repayment->setValue('borrower_no45', '');

            $repayment->setValue('name_41', $group_4[0]['full_name']);
            $repayment->setValue('name_42', $group_4[1]['full_name']);
            $repayment->setValue('name_43', '');
            $repayment->setValue('name_44', '');
            $repayment->setValue('name_45', '');

            $repayment->setValue('mobile_no41', $group_4[0]['mobile_no']);
            $repayment->setValue('mobile_no42', $group_4[1]['mobile_no']);
            $repayment->setValue('mobile_no43', '');
            $repayment->setValue('mobile_no44', '');
            $repayment->setValue('mobile_no45', '');

            $repayment->setValue('loan_amount41', $loan_41->loan_amount);
            $repayment->setValue('loan_amount42', $loan_42->loan_amount);
            $repayment->setValue('loan_amount43', '');
            $repayment->setValue('loan_amount44', '');
            $repayment->setValue('loan_amount45', '');

            $repayment->setValue('loan_due41', $loan_41->due);
            $repayment->setValue('loan_due42', $loan_42->due);
            $repayment->setValue('loan_due43', '');
            $repayment->setValue('loan_due44', '');
            $repayment->setValue('loan_due45', '');

        } else if ($countGroup_4 == 0) {

            $repayment->setValue('borrower_no41', '');
            $repayment->setValue('borrower_no42', '');
            $repayment->setValue('borrower_no43', '');
            $repayment->setValue('borrower_no44', '');
            $repayment->setValue('borrower_no45', '');

            $repayment->setValue('name_41', '');
            $repayment->setValue('name_42', '');
            $repayment->setValue('name_43', '');
            $repayment->setValue('name_44', '');
            $repayment->setValue('name_45', '');

            $repayment->setValue('mobile_no41', '');
            $repayment->setValue('mobile_no42', '');
            $repayment->setValue('mobile_no43', '');
            $repayment->setValue('mobile_no44', '');
            $repayment->setValue('mobile_no45', '');

            $repayment->setValue('loan_amount41', '');
            $repayment->setValue('loan_amount42', '');
            $repayment->setValue('loan_amount43', '');
            $repayment->setValue('loan_amount44', '');
            $repayment->setValue('loan_amount45', '');

            $repayment->setValue('loan_due41', '');
            $repayment->setValue('loan_due42', '');
            $repayment->setValue('loan_due43', '');
            $repayment->setValue('loan_due44', '');
            $repayment->setValue('loan_due45', '');

        }

        //------------------------- group  5  --------------------------------------------------------

        if ($countGroup_5 == 5) {

            $loan_51 = Loan::where('borrower_no', $group_5[0]['borrower_no'])->first();
            $loan_52 = Loan::where('borrower_no', $group_5[1]['borrower_no'])->first();
            $loan_53 = Loan::where('borrower_no', $group_5[2]['borrower_no'])->first();
            $loan_54 = Loan::where('borrower_no', $group_5[3]['borrower_no'])->first();
            $loan_55 = Loan::where('borrower_no', $group_5[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no51', $group_5[0]['borrower_no']);
            $repayment->setValue('borrower_no52', $group_5[1]['borrower_no']);
            $repayment->setValue('borrower_no53', $group_5[2]['borrower_no']);
            $repayment->setValue('borrower_no54', $group_5[3]['borrower_no']);
            $repayment->setValue('borrower_no55', $group_5[4]['borrower_no']);

            $repayment->setValue('name_51', $group_5[0]['full_name']);
            $repayment->setValue('name_52', $group_5[1]['full_name']);
            $repayment->setValue('name_53', $group_5[2]['full_name']);
            $repayment->setValue('name_54', $group_5[3]['full_name']);
            $repayment->setValue('name_55', $group_5[4]['full_name']);

            $repayment->setValue('mobile_no51', $group_5[0]['mobile_no']);
            $repayment->setValue('mobile_no52', $group_5[1]['mobile_no']);
            $repayment->setValue('mobile_no53', $group_5[2]['mobile_no']);
            $repayment->setValue('mobile_no54', $group_5[3]['mobile_no']);
            $repayment->setValue('mobile_no55', $group_5[4]['mobile_no']);

            $repayment->setValue('loan_amount51', $loan_51->loan_amount);
            $repayment->setValue('loan_amount52', $loan_52->loan_amount);
            $repayment->setValue('loan_amount53', $loan_53->loan_amount);
            $repayment->setValue('loan_amount54', $loan_54->loan_amount);
            $repayment->setValue('loan_amount55', $loan_55->loan_amount);

            $repayment->setValue('loan_due51', $loan_51->due);
            $repayment->setValue('loan_due52', $loan_52->due);
            $repayment->setValue('loan_due53', $loan_53->due);
            $repayment->setValue('loan_due54', $loan_54->due);
            $repayment->setValue('loan_due55', $loan_55->due);

        } else if ($countGroup_5 == 4) {

            $loan_51 = Loan::where('borrower_no', $group_5[0]['borrower_no'])->first();
            $loan_52 = Loan::where('borrower_no', $group_5[1]['borrower_no'])->first();
            $loan_53 = Loan::where('borrower_no', $group_5[2]['borrower_no'])->first();
            $loan_54 = Loan::where('borrower_no', $group_5[3]['borrower_no'])->first();
            // $loan_55 = Loan::where('borrower_no', $group_5[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no51', $group_5[0]['borrower_no']);
            $repayment->setValue('borrower_no52', $group_5[1]['borrower_no']);
            $repayment->setValue('borrower_no53', $group_5[2]['borrower_no']);
            $repayment->setValue('borrower_no54', $group_5[3]['borrower_no']);
            $repayment->setValue('borrower_no55', '');

            $repayment->setValue('name_51', $group_5[0]['full_name']);
            $repayment->setValue('name_52', $group_5[1]['full_name']);
            $repayment->setValue('name_53', $group_5[2]['full_name']);
            $repayment->setValue('name_54', $group_5[3]['full_name']);
            $repayment->setValue('name_55', '');

            $repayment->setValue('mobile_no51', $group_5[0]['mobile_no']);
            $repayment->setValue('mobile_no52', $group_5[1]['mobile_no']);
            $repayment->setValue('mobile_no53', $group_5[2]['mobile_no']);
            $repayment->setValue('mobile_no54', $group_5[3]['mobile_no']);
            $repayment->setValue('mobile_no55', '');

            $repayment->setValue('loan_amount51', $loan_51->loan_amount);
            $repayment->setValue('loan_amount52', $loan_52->loan_amount);
            $repayment->setValue('loan_amount53', $loan_53->loan_amount);
            $repayment->setValue('loan_amount54', $loan_54->loan_amount);
            $repayment->setValue('loan_amount55', '');

            $repayment->setValue('loan_due51', $loan_51->due);
            $repayment->setValue('loan_due52', $loan_52->due);
            $repayment->setValue('loan_due53', $loan_53->due);
            $repayment->setValue('loan_due54', $loan_54->due);
            $repayment->setValue('loan_due55', '');

        } else if ($countGroup_5 == 3) {

            $loan_51 = Loan::where('borrower_no', $group_5[0]['borrower_no'])->first();
            $loan_52 = Loan::where('borrower_no', $group_5[1]['borrower_no'])->first();
            $loan_53 = Loan::where('borrower_no', $group_5[2]['borrower_no'])->first();
            // $loan_54 = Loan::where('borrower_no', $group_5[3]['borrower_no'])->first();
            // $loan_55 = Loan::where('borrower_no', $group_5[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no51', $group_5[0]['borrower_no']);
            $repayment->setValue('borrower_no52', $group_5[1]['borrower_no']);
            $repayment->setValue('borrower_no53', $group_5[2]['borrower_no']);
            $repayment->setValue('borrower_no54', '');
            $repayment->setValue('borrower_no55', '');

            $repayment->setValue('name_51', $group_5[0]['full_name']);
            $repayment->setValue('name_52', $group_5[1]['full_name']);
            $repayment->setValue('name_53', $group_5[2]['full_name']);
            $repayment->setValue('name_54', '');
            $repayment->setValue('name_55', '');

            $repayment->setValue('mobile_no51', $group_5[0]['mobile_no']);
            $repayment->setValue('mobile_no52', $group_5[1]['mobile_no']);
            $repayment->setValue('mobile_no53', $group_5[2]['mobile_no']);
            $repayment->setValue('mobile_no54', '');
            $repayment->setValue('mobile_no55', '');

            $repayment->setValue('loan_amount51', $loan_51->loan_amount);
            $repayment->setValue('loan_amount52', $loan_52->loan_amount);
            $repayment->setValue('loan_amount53', $loan_53->loan_amount);
            $repayment->setValue('loan_amount54', '');
            $repayment->setValue('loan_amount55', '');

            $repayment->setValue('loan_due51', $loan_51->due);
            $repayment->setValue('loan_due52', $loan_52->due);
            $repayment->setValue('loan_due53', $loan_53->due);
            $repayment->setValue('loan_due54', '');
            $repayment->setValue('loan_due55', '');

        } else if ($countGroup_5 == 2) {

            $loan_51 = Loan::where('borrower_no', $group_5[0]['borrower_no'])->first();
            $loan_52 = Loan::where('borrower_no', $group_5[1]['borrower_no'])->first();
            // $loan_53 = Loan::where('borrower_no', $group_5[2]['borrower_no'])->first();
            // $loan_54 = Loan::where('borrower_no', $group_5[3]['borrower_no'])->first();
            // $loan_55 = Loan::where('borrower_no', $group_5[4]['borrower_no'])->first();

            $repayment->setValue('borrower_no51', $group_5[0]['borrower_no']);
            $repayment->setValue('borrower_no52', $group_5[1]['borrower_no']);
            $repayment->setValue('borrower_no53', '');
            $repayment->setValue('borrower_no54', '');
            $repayment->setValue('borrower_no55', '');

            $repayment->setValue('name_51', $group_5[0]['full_name']);
            $repayment->setValue('name_52', $group_5[1]['full_name']);
            $repayment->setValue('name_53', '');
            $repayment->setValue('name_54', '');
            $repayment->setValue('name_55', '');

            $repayment->setValue('mobile_no51', $group_5[0]['mobile_no']);
            $repayment->setValue('mobile_no52', $group_5[1]['mobile_no']);
            $repayment->setValue('mobile_no53', '');
            $repayment->setValue('mobile_no54', '');
            $repayment->setValue('mobile_no55', '');

            $repayment->setValue('loan_amount51', $loan_51->loan_amount);
            $repayment->setValue('loan_amount52', $loan_52->loan_amount);
            $repayment->setValue('loan_amount53', '');
            $repayment->setValue('loan_amount54', '');
            $repayment->setValue('loan_amount55', '');

            $repayment->setValue('loan_due51', $loan_51->due);
            $repayment->setValue('loan_due52', $loan_52->due);
            $repayment->setValue('loan_due53', '');
            $repayment->setValue('loan_due54', '');
            $repayment->setValue('loan_due55', '');

        } else if ($countGroup_5 == 0) {

            $repayment->setValue('borrower_no51', '');
            $repayment->setValue('borrower_no52', '');
            $repayment->setValue('borrower_no53', '');
            $repayment->setValue('borrower_no54', '');
            $repayment->setValue('borrower_no55', '');

            $repayment->setValue('name_51', '');
            $repayment->setValue('name_52', '');
            $repayment->setValue('name_53', '');
            $repayment->setValue('name_54', '');
            $repayment->setValue('name_55', '');

            $repayment->setValue('mobile_no51', '');
            $repayment->setValue('mobile_no52', '');
            $repayment->setValue('mobile_no53', '');
            $repayment->setValue('mobile_no54', '');
            $repayment->setValue('mobile_no55', '');

            $repayment->setValue('loan_amount51', '');
            $repayment->setValue('loan_amount52', '');
            $repayment->setValue('loan_amount53', '');
            $repayment->setValue('loan_amount54', '');
            $repayment->setValue('loan_amount55', '');

            $repayment->setValue('loan_due51', '');
            $repayment->setValue('loan_due52', '');
            $repayment->setValue('loan_due53', '');
            $repayment->setValue('loan_due54', '');
            $repayment->setValue('loan_due55', '');

        }

        $repayment->setValue('center_no', $center->center_no);
        $repayment->setValue('branch_name', $branchName);
        $repayment->setValue('center_name', $centerName);

        $fileName = $center->center_name . " Repayments";

        $repayment->saveAs($fileName . '.docx');
        return response()->download($fileName . '.docx')->deleteFileAfterSend(true);
    }

    public function exportDisbursementCenter($branchName, $centerName)
    {
        $branch = Branch::where('branch_name', $branchName)->first();
        $center = Center::where('branch_no', $branch->branch_name)->where('center_name', $centerName)->first();
        $group_1 = Borrower::where('branch', $branchName)->where('center', $centerName)->where('group_no', 1)->get();
        $group_2 = Borrower::where('branch', $branchName)->where('center', $centerName)->where('group_no', 2)->get();
        $group_3 = Borrower::where('branch', $branchName)->where('center', $centerName)->where('group_no', 3)->get();
        $group_4 = Borrower::where('branch', $branchName)->where('center', $centerName)->where('group_no', 4)->get();
        $group_5 = Borrower::where('branch', $branchName)->where('center', $centerName)->where('group_no', 5)->get();

        $countGroup_1 = count($group_1);
        $countGroup_2 = count($group_2);
        $countGroup_3 = count($group_3);
        $countGroup_4 = count($group_4);
        $countGroup_5 = count($group_5);

        $disbursement = new TemplateProcessor('word-template/DisbursementCenter.docx');

        //------------------------- group  1  --------------------------------------------------------

        if ($countGroup_1 == 5) {

            $loan_11 = Loan::where('borrower_no', $group_1[0]['borrower_no'])->first();
            $loan_12 = Loan::where('borrower_no', $group_1[1]['borrower_no'])->first();
            $loan_13 = Loan::where('borrower_no', $group_1[2]['borrower_no'])->first();
            $loan_14 = Loan::where('borrower_no', $group_1[3]['borrower_no'])->first();
            $loan_15 = Loan::where('borrower_no', $group_1[4]['borrower_no'])->first();

            $disbursement->setValue('id_11', $group_1[0]['borrower_no']);
            $disbursement->setValue('id_12', $group_1[1]['borrower_no']);
            $disbursement->setValue('id_13', $group_1[2]['borrower_no']);
            $disbursement->setValue('id_14', $group_1[3]['borrower_no']);
            $disbursement->setValue('id_15', $group_1[4]['borrower_no']);

            $disbursement->setValue('name_11', $group_1[0]['full_name']);
            $disbursement->setValue('name_12', $group_1[1]['full_name']);
            $disbursement->setValue('name_13', $group_1[2]['full_name']);
            $disbursement->setValue('name_14', $group_1[3]['full_name']);
            $disbursement->setValue('name_15', $group_1[4]['full_name']);

            $disbursement->setValue('nic_11', $group_1[0]['nic']);
            $disbursement->setValue('nic_12', $group_1[1]['nic']);
            $disbursement->setValue('nic_13', $group_1[2]['nic']);
            $disbursement->setValue('nic_14', $group_1[3]['nic']);
            $disbursement->setValue('nic_15', $group_1[4]['nic']);

            $disbursement->setValue('loan_amount_11', $loan_11->loan_amount);
            $disbursement->setValue('loan_amount_12', $loan_12->loan_amount);
            $disbursement->setValue('loan_amount_13', $loan_13->loan_amount);
            $disbursement->setValue('loan_amount_14', $loan_14->loan_amount);
            $disbursement->setValue('loan_amount_15', $loan_15->loan_amount);

            $disbursement->setValue('loan_duration_11', $loan_11->loan_duration);
            $disbursement->setValue('loan_duration_12', $loan_12->loan_duration);
            $disbursement->setValue('loan_duration_13', $loan_13->loan_duration);
            $disbursement->setValue('loan_duration_14', $loan_14->loan_duration);
            $disbursement->setValue('loan_duration_15', $loan_15->loan_duration);

            $disbursement->setValue('group_no_11', $loan_11->group_no);
            $disbursement->setValue('group_no_12', $loan_12->group_no);
            $disbursement->setValue('group_no_13', $loan_13->group_no);
            $disbursement->setValue('group_no_14', $loan_14->group_no);
            $disbursement->setValue('group_no_15', $loan_15->group_no);

            $disbursement->setValue('interest_rate_11', $loan_11->interest_rate);
            $disbursement->setValue('interest_rate_12', $loan_12->interest_rate);
            $disbursement->setValue('interest_rate_13', $loan_13->interest_rate);
            $disbursement->setValue('interest_rate_14', $loan_14->interest_rate);
            $disbursement->setValue('interest_rate_15', $loan_15->interest_rate);

            $total_1 = ($loan_11->loan_amount + $loan_12->loan_amount + $loan_13->loan_amount + $loan_14->loan_amount + $loan_15->loan_amount);

        } else if ($countGroup_1 == 4) {

            $loan_11 = Loan::where('borrower_no', $group_1[0]['borrower_no'])->first();
            $loan_12 = Loan::where('borrower_no', $group_1[1]['borrower_no'])->first();
            $loan_13 = Loan::where('borrower_no', $group_1[2]['borrower_no'])->first();
            $loan_14 = Loan::where('borrower_no', $group_1[3]['borrower_no'])->first();

            $disbursement->setValue('id_11', $group_1[0]['borrower_no']);
            $disbursement->setValue('id_12', $group_1[1]['borrower_no']);
            $disbursement->setValue('id_13', $group_1[2]['borrower_no']);
            $disbursement->setValue('id_14', $group_1[3]['borrower_no']);
            $disbursement->setValue('id_15', '');

            $disbursement->setValue('name_11', $group_1[0]['full_name']);
            $disbursement->setValue('name_12', $group_1[1]['full_name']);
            $disbursement->setValue('name_13', $group_1[2]['full_name']);
            $disbursement->setValue('name_14', $group_1[3]['full_name']);
            $disbursement->setValue('name_15', '');

            $disbursement->setValue('nic_11', $group_1[0]['nic']);
            $disbursement->setValue('nic_12', $group_1[1]['nic']);
            $disbursement->setValue('nic_13', $group_1[2]['nic']);
            $disbursement->setValue('nic_14', $group_1[3]['nic']);
            $disbursement->setValue('nic_15', '');

            $disbursement->setValue('loan_amount_11', $loan_11->loan_amount);
            $disbursement->setValue('loan_amount_12', $loan_12->loan_amount);
            $disbursement->setValue('loan_amount_13', $loan_13->loan_amount);
            $disbursement->setValue('loan_amount_14', $loan_14->loan_amount);
            $disbursement->setValue('loan_amount_15', '');

            $disbursement->setValue('loan_duration_11', $loan_11->loan_duration);
            $disbursement->setValue('loan_duration_12', $loan_12->loan_duration);
            $disbursement->setValue('loan_duration_13', $loan_13->loan_duration);
            $disbursement->setValue('loan_duration_14', $loan_14->loan_duration);
            $disbursement->setValue('loan_duration_15', '');

            $disbursement->setValue('group_no_11', $loan_11->group_no);
            $disbursement->setValue('group_no_12', $loan_12->group_no);
            $disbursement->setValue('group_no_13', $loan_13->group_no);
            $disbursement->setValue('group_no_14', $loan_14->group_no);
            $disbursement->setValue('group_no_15', '');

            $disbursement->setValue('interest_rate_11', $loan_11->interest_rate);
            $disbursement->setValue('interest_rate_12', $loan_12->interest_rate);
            $disbursement->setValue('interest_rate_13', $loan_13->interest_rate);
            $disbursement->setValue('interest_rate_14', $loan_14->interest_rate);
            $disbursement->setValue('interest_rate_15', '');

            $total_1 = ($loan_11->loan_amount + $loan_12->loan_amount + $loan_13->loan_amount + $loan_14->loan_amount);

        } else if ($countGroup_1 == 3) {

            $loan_11 = Loan::where('borrower_no', $group_1[0]['borrower_no'])->first();
            $loan_12 = Loan::where('borrower_no', $group_1[1]['borrower_no'])->first();
            $loan_13 = Loan::where('borrower_no', $group_1[2]['borrower_no'])->first();

            $disbursement->setValue('id_11', $group_1[0]['borrower_no']);
            $disbursement->setValue('id_12', $group_1[1]['borrower_no']);
            $disbursement->setValue('id_13', $group_1[2]['borrower_no']);
            $disbursement->setValue('id_14', '');
            $disbursement->setValue('id_15', '');

            $disbursement->setValue('name_11', $group_1[0]['full_name']);
            $disbursement->setValue('name_12', $group_1[1]['full_name']);
            $disbursement->setValue('name_13', $group_1[2]['full_name']);
            $disbursement->setValue('name_14', '');
            $disbursement->setValue('name_15', '');

            $disbursement->setValue('nic_11', $group_1[0]['nic']);
            $disbursement->setValue('nic_12', $group_1[1]['nic']);
            $disbursement->setValue('nic_13', $group_1[2]['nic']);
            $disbursement->setValue('nic_14', '');
            $disbursement->setValue('nic_15', '');

            $disbursement->setValue('loan_amount_11', $loan_11->loan_amount);
            $disbursement->setValue('loan_amount_12', $loan_12->loan_amount);
            $disbursement->setValue('loan_amount_13', $loan_13->loan_amount);
            $disbursement->setValue('loan_amount_14', '');
            $disbursement->setValue('loan_amount_15', '');

            $disbursement->setValue('loan_duration_11', $loan_11->loan_duration);
            $disbursement->setValue('loan_duration_12', $loan_12->loan_duration);
            $disbursement->setValue('loan_duration_13', $loan_13->loan_duration);
            $disbursement->setValue('loan_duration_14', '');
            $disbursement->setValue('loan_duration_15', '');

            $disbursement->setValue('group_no_11', $loan_11->group_no);
            $disbursement->setValue('group_no_12', $loan_12->group_no);
            $disbursement->setValue('group_no_13', $loan_13->group_no);
            $disbursement->setValue('group_no_14', '');
            $disbursement->setValue('group_no_15', '');

            $disbursement->setValue('interest_rate_11', $loan_11->interest_rate);
            $disbursement->setValue('interest_rate_12', $loan_12->interest_rate);
            $disbursement->setValue('interest_rate_13', $loan_13->interest_rate);
            $disbursement->setValue('interest_rate_14', '');
            $disbursement->setValue('interest_rate_15', '');

            $total_1 = ($loan_11->loan_amount + $loan_12->loan_amount + $loan_13->loan_amount);

        } else if ($countGroup_1 == 2) {

            $loan_11 = Loan::where('borrower_no', $group_1[0]['borrower_no'])->first();
            $loan_12 = Loan::where('borrower_no', $group_1[1]['borrower_no'])->first();

            $disbursement->setValue('id_11', $group_1[0]['borrower_no']);
            $disbursement->setValue('id_12', $group_1[1]['borrower_no']);
            $disbursement->setValue('id_13', '');
            $disbursement->setValue('id_14', '');
            $disbursement->setValue('id_15', '');

            $disbursement->setValue('name_11', $group_1[0]['full_name']);
            $disbursement->setValue('name_12', $group_1[1]['full_name']);
            $disbursement->setValue('name_13', '');
            $disbursement->setValue('name_14', '');
            $disbursement->setValue('name_15', '');

            $disbursement->setValue('nic_11', $group_1[0]['nic']);
            $disbursement->setValue('nic_12', $group_1[1]['nic']);
            $disbursement->setValue('nic_13', '');
            $disbursement->setValue('nic_14', '');
            $disbursement->setValue('nic_15', '');

            $disbursement->setValue('loan_amount_11', $loan_11->loan_amount);
            $disbursement->setValue('loan_amount_12', $loan_12->loan_amount);
            $disbursement->setValue('loan_amount_13', '');
            $disbursement->setValue('loan_amount_14', '');
            $disbursement->setValue('loan_amount_15', '');

            $disbursement->setValue('loan_duration_11', $loan_11->loan_duration);
            $disbursement->setValue('loan_duration_12', $loan_12->loan_duration);
            $disbursement->setValue('loan_duration_13', '');
            $disbursement->setValue('loan_duration_14', '');
            $disbursement->setValue('loan_duration_15', '');

            $disbursement->setValue('group_no_11', $loan_11->group_no);
            $disbursement->setValue('group_no_12', $loan_12->group_no);
            $disbursement->setValue('group_no_13', '');
            $disbursement->setValue('group_no_14', '');
            $disbursement->setValue('group_no_15', '');

            $disbursement->setValue('interest_rate_11', $loan_11->interest_rate);
            $disbursement->setValue('interest_rate_12', $loan_12->interest_rate);
            $disbursement->setValue('interest_rate_13', '');
            $disbursement->setValue('interest_rate_14', '');
            $disbursement->setValue('interest_rate_15', '');

            $total_1 = ($loan_11->loan_amount + $loan_12->loan_amount);

        } else if ($countGroup_1 == 1) {
            return redirect()->back()->with('error', 'ONLY ONE BORROWER ADDED IN GROUP 1');
        } else if ($countGroup_1 == 0) {

            $disbursement->setValue('id_11', '');
            $disbursement->setValue('id_12', '');
            $disbursement->setValue('id_13', '');
            $disbursement->setValue('id_14', '');
            $disbursement->setValue('id_15', '');

            $disbursement->setValue('name_11', '');
            $disbursement->setValue('name_12', '');
            $disbursement->setValue('name_13', '');
            $disbursement->setValue('name_14', '');
            $disbursement->setValue('name_15', '');

            $disbursement->setValue('nic_11', '');
            $disbursement->setValue('nic_12', '');
            $disbursement->setValue('nic_13', '');
            $disbursement->setValue('nic_14', '');
            $disbursement->setValue('nic_15', '');

            $disbursement->setValue('loan_amount_11', '');
            $disbursement->setValue('loan_amount_12', '');
            $disbursement->setValue('loan_amount_13', '');
            $disbursement->setValue('loan_amount_14', '');
            $disbursement->setValue('loan_amount_15', '');

            $disbursement->setValue('loan_duration_11', '');
            $disbursement->setValue('loan_duration_12', '');
            $disbursement->setValue('loan_duration_13', '');
            $disbursement->setValue('loan_duration_14', '');
            $disbursement->setValue('loan_duration_15', '');

            $disbursement->setValue('group_no_11', '');
            $disbursement->setValue('group_no_12', '');
            $disbursement->setValue('group_no_13', '');
            $disbursement->setValue('group_no_14', '');
            $disbursement->setValue('group_no_15', '');

            $disbursement->setValue('interest_rate_11', '');
            $disbursement->setValue('interest_rate_12', '');
            $disbursement->setValue('interest_rate_13', '');
            $disbursement->setValue('interest_rate_14', '');
            $disbursement->setValue('interest_rate_15', '');

            $total_1 = 0;

        }

        //------------------------- group  2  --------------------------------------------------------

        if ($countGroup_2 == 5) {

            $loan_21 = Loan::where('borrower_no', $group_2[0]['borrower_no'])->first();
            $loan_22 = Loan::where('borrower_no', $group_2[1]['borrower_no'])->first();
            $loan_23 = Loan::where('borrower_no', $group_2[2]['borrower_no'])->first();
            $loan_24 = Loan::where('borrower_no', $group_2[3]['borrower_no'])->first();
            $loan_25 = Loan::where('borrower_no', $group_2[4]['borrower_no'])->first();

            $disbursement->setValue('id_21', $group_2[0]['borrower_no']);
            $disbursement->setValue('id_22', $group_2[1]['borrower_no']);
            $disbursement->setValue('id_23', $group_2[2]['borrower_no']);
            $disbursement->setValue('id_24', $group_2[3]['borrower_no']);
            $disbursement->setValue('id_25', $group_2[4]['borrower_no']);

            $disbursement->setValue('name_21', $group_2[0]['full_name']);
            $disbursement->setValue('name_22', $group_2[1]['full_name']);
            $disbursement->setValue('name_23', $group_2[2]['full_name']);
            $disbursement->setValue('name_24', $group_2[3]['full_name']);
            $disbursement->setValue('name_25', $group_2[4]['full_name']);

            $disbursement->setValue('nic_21', $group_2[0]['nic']);
            $disbursement->setValue('nic_22', $group_2[1]['nic']);
            $disbursement->setValue('nic_23', $group_2[2]['nic']);
            $disbursement->setValue('nic_24', $group_2[3]['nic']);
            $disbursement->setValue('nic_25', $group_2[4]['nic']);

            $disbursement->setValue('loan_amount_21', $loan_21->loan_amount);
            $disbursement->setValue('loan_amount_22', $loan_22->loan_amount);
            $disbursement->setValue('loan_amount_23', $loan_23->loan_amount);
            $disbursement->setValue('loan_amount_24', $loan_24->loan_amount);
            $disbursement->setValue('loan_amount_25', $loan_25->loan_amount);

            $disbursement->setValue('loan_duration_21', $loan_21->loan_duration);
            $disbursement->setValue('loan_duration_22', $loan_22->loan_duration);
            $disbursement->setValue('loan_duration_23', $loan_23->loan_duration);
            $disbursement->setValue('loan_duration_24', $loan_24->loan_duration);
            $disbursement->setValue('loan_duration_25', $loan_25->loan_duration);

            $disbursement->setValue('group_no_21', $loan_21->group_no);
            $disbursement->setValue('group_no_22', $loan_22->group_no);
            $disbursement->setValue('group_no_23', $loan_23->group_no);
            $disbursement->setValue('group_no_24', $loan_24->group_no);
            $disbursement->setValue('group_no_25', $loan_25->group_no);

            $disbursement->setValue('interest_rate_21', $loan_21->interest_rate);
            $disbursement->setValue('interest_rate_22', $loan_22->interest_rate);
            $disbursement->setValue('interest_rate_23', $loan_23->interest_rate);
            $disbursement->setValue('interest_rate_24', $loan_24->interest_rate);
            $disbursement->setValue('interest_rate_25', $loan_25->interest_rate);

            $total_2 = ($loan_21->loan_amount + $loan_22->loan_amount + $loan_23->loan_amount + $loan_24->loan_amount + $loan_25->loan_amount);

        } else if ($countGroup_2 == 4) {

            $loan_21 = Loan::where('borrower_no', $group_2[0]['borrower_no'])->first();
            $loan_22 = Loan::where('borrower_no', $group_2[1]['borrower_no'])->first();
            $loan_23 = Loan::where('borrower_no', $group_2[2]['borrower_no'])->first();
            $loan_24 = Loan::where('borrower_no', $group_2[3]['borrower_no'])->first();

            $disbursement->setValue('id_21', $group_2[0]['borrower_no']);
            $disbursement->setValue('id_22', $group_2[1]['borrower_no']);
            $disbursement->setValue('id_23', $group_2[2]['borrower_no']);
            $disbursement->setValue('id_24', $group_2[3]['borrower_no']);
            $disbursement->setValue('id_25', '');

            $disbursement->setValue('name_21', $group_2[0]['full_name']);
            $disbursement->setValue('name_22', $group_2[1]['full_name']);
            $disbursement->setValue('name_23', $group_2[2]['full_name']);
            $disbursement->setValue('name_24', $group_2[3]['full_name']);
            $disbursement->setValue('name_25', '');

            $disbursement->setValue('nic_21', $group_2[0]['nic']);
            $disbursement->setValue('nic_22', $group_2[1]['nic']);
            $disbursement->setValue('nic_23', $group_2[2]['nic']);
            $disbursement->setValue('nic_24', $group_2[3]['nic']);
            $disbursement->setValue('nic_25', '');

            $disbursement->setValue('loan_amount_21', $loan_21->loan_amount);
            $disbursement->setValue('loan_amount_22', $loan_22->loan_amount);
            $disbursement->setValue('loan_amount_23', $loan_23->loan_amount);
            $disbursement->setValue('loan_amount_24', $loan_24->loan_amount);
            $disbursement->setValue('loan_amount_25', '');

            $disbursement->setValue('loan_duration_21', $loan_21->loan_duration);
            $disbursement->setValue('loan_duration_22', $loan_22->loan_duration);
            $disbursement->setValue('loan_duration_23', $loan_23->loan_duration);
            $disbursement->setValue('loan_duration_24', $loan_24->loan_duration);
            $disbursement->setValue('loan_duration_25', '');

            $disbursement->setValue('group_no_21', $loan_21->group_no);
            $disbursement->setValue('group_no_22', $loan_22->group_no);
            $disbursement->setValue('group_no_23', $loan_23->group_no);
            $disbursement->setValue('group_no_24', $loan_24->group_no);
            $disbursement->setValue('group_no_25', '');

            $disbursement->setValue('interest_rate_21', $loan_21->interest_rate);
            $disbursement->setValue('interest_rate_22', $loan_22->interest_rate);
            $disbursement->setValue('interest_rate_23', $loan_23->interest_rate);
            $disbursement->setValue('interest_rate_24', $loan_24->interest_rate);
            $disbursement->setValue('interest_rate_25', '');

            $total_2 = ($loan_21->loan_amount + $loan_22->loan_amount + $loan_23->loan_amount + $loan_24->loan_amount);

        } else if ($countGroup_2 == 3) {

            $loan_21 = Loan::where('borrower_no', $group_2[0]['borrower_no'])->first();
            $loan_22 = Loan::where('borrower_no', $group_2[1]['borrower_no'])->first();
            $loan_23 = Loan::where('borrower_no', $group_2[2]['borrower_no'])->first();

            $disbursement->setValue('id_21', $group_2[0]['borrower_no']);
            $disbursement->setValue('id_22', $group_2[1]['borrower_no']);
            $disbursement->setValue('id_23', $group_2[2]['borrower_no']);
            $disbursement->setValue('id_24', '');
            $disbursement->setValue('id_25', '');

            $disbursement->setValue('name_21', $group_2[0]['full_name']);
            $disbursement->setValue('name_22', $group_2[1]['full_name']);
            $disbursement->setValue('name_23', $group_2[2]['full_name']);
            $disbursement->setValue('name_24', '');
            $disbursement->setValue('name_25', '');

            $disbursement->setValue('nic_21', $group_2[0]['nic']);
            $disbursement->setValue('nic_22', $group_2[1]['nic']);
            $disbursement->setValue('nic_23', $group_2[2]['nic']);
            $disbursement->setValue('nic_24', '');
            $disbursement->setValue('nic_25', '');

            $disbursement->setValue('loan_amount_21', $loan_21->loan_amount);
            $disbursement->setValue('loan_amount_22', $loan_22->loan_amount);
            $disbursement->setValue('loan_amount_23', $loan_23->loan_amount);
            $disbursement->setValue('loan_amount_24', '');
            $disbursement->setValue('loan_amount_25', '');

            $disbursement->setValue('loan_duration_21', $loan_21->loan_duration);
            $disbursement->setValue('loan_duration_22', $loan_22->loan_duration);
            $disbursement->setValue('loan_duration_23', $loan_23->loan_duration);
            $disbursement->setValue('loan_duration_24', '');
            $disbursement->setValue('loan_duration_25', '');

            $disbursement->setValue('group_no_21', $loan_21->group_no);
            $disbursement->setValue('group_no_22', $loan_22->group_no);
            $disbursement->setValue('group_no_23', $loan_23->group_no);
            $disbursement->setValue('group_no_24', '');
            $disbursement->setValue('group_no_25', '');

            $disbursement->setValue('interest_rate_21', $loan_21->interest_rate);
            $disbursement->setValue('interest_rate_22', $loan_22->interest_rate);
            $disbursement->setValue('interest_rate_23', $loan_23->interest_rate);
            $disbursement->setValue('interest_rate_24', '');
            $disbursement->setValue('interest_rate_25', '');

            $total_2 = ($loan_21->loan_amount + $loan_22->loan_amount + $loan_23->loan_amount);

        } else if ($countGroup_2 == 2) {

            $loan_21 = Loan::where('borrower_no', $group_2[0]['borrower_no'])->first();
            $loan_22 = Loan::where('borrower_no', $group_2[1]['borrower_no'])->first();

            $disbursement->setValue('id_21', $group_2[0]['borrower_no']);
            $disbursement->setValue('id_22', $group_2[1]['borrower_no']);
            $disbursement->setValue('id_23', '');
            $disbursement->setValue('id_24', '');
            $disbursement->setValue('id_25', '');

            $disbursement->setValue('name_21', $group_2[0]['full_name']);
            $disbursement->setValue('name_22', $group_2[1]['full_name']);
            $disbursement->setValue('name_23', '');
            $disbursement->setValue('name_24', '');
            $disbursement->setValue('name_25', '');

            $disbursement->setValue('nic_21', $group_2[0]['nic']);
            $disbursement->setValue('nic_22', $group_2[1]['nic']);
            $disbursement->setValue('nic_23', '');
            $disbursement->setValue('nic_24', '');
            $disbursement->setValue('nic_25', '');

            $disbursement->setValue('loan_amount_21', $loan_21->loan_amount);
            $disbursement->setValue('loan_amount_22', $loan_22->loan_amount);
            $disbursement->setValue('loan_amount_23', '');
            $disbursement->setValue('loan_amount_24', '');
            $disbursement->setValue('loan_amount_25', '');

            $disbursement->setValue('loan_duration_21', $loan_21->loan_duration);
            $disbursement->setValue('loan_duration_22', $loan_22->loan_duration);
            $disbursement->setValue('loan_duration_23', '');
            $disbursement->setValue('loan_duration_24', '');
            $disbursement->setValue('loan_duration_25', '');

            $disbursement->setValue('group_no_21', $loan_21->group_no);
            $disbursement->setValue('group_no_22', $loan_22->group_no);
            $disbursement->setValue('group_no_23', '');
            $disbursement->setValue('group_no_24', '');
            $disbursement->setValue('group_no_25', '');

            $disbursement->setValue('interest_rate_21', $loan_21->interest_rate);
            $disbursement->setValue('interest_rate_22', $loan_22->interest_rate);
            $disbursement->setValue('interest_rate_23', '');
            $disbursement->setValue('interest_rate_24', '');
            $disbursement->setValue('interest_rate_25', '');

            $total_2 = ($loan_21->loan_amount + $loan_22->loan_amount);

        } else if ($countGroup_2 == 1) {
            return redirect()->back()->with('error', 'ONLY ONE BORROWER ADDED IN GROUP 2');
        } else if ($countGroup_2 == 0) {

            $disbursement->setValue('id_21', '');
            $disbursement->setValue('id_22', '');
            $disbursement->setValue('id_23', '');
            $disbursement->setValue('id_24', '');
            $disbursement->setValue('id_25', '');

            $disbursement->setValue('name_21', '');
            $disbursement->setValue('name_22', '');
            $disbursement->setValue('name_23', '');
            $disbursement->setValue('name_24', '');
            $disbursement->setValue('name_25', '');

            $disbursement->setValue('nic_21', '');
            $disbursement->setValue('nic_22', '');
            $disbursement->setValue('nic_23', '');
            $disbursement->setValue('nic_24', '');
            $disbursement->setValue('nic_25', '');

            $disbursement->setValue('loan_amount_21', '');
            $disbursement->setValue('loan_amount_22', '');
            $disbursement->setValue('loan_amount_23', '');
            $disbursement->setValue('loan_amount_24', '');
            $disbursement->setValue('loan_amount_25', '');

            $disbursement->setValue('loan_duration_21', '');
            $disbursement->setValue('loan_duration_22', '');
            $disbursement->setValue('loan_duration_23', '');
            $disbursement->setValue('loan_duration_24', '');
            $disbursement->setValue('loan_duration_25', '');

            $disbursement->setValue('group_no_21', '');
            $disbursement->setValue('group_no_22', '');
            $disbursement->setValue('group_no_23', '');
            $disbursement->setValue('group_no_24', '');
            $disbursement->setValue('group_no_25', '');

            $disbursement->setValue('interest_rate_21', '');
            $disbursement->setValue('interest_rate_22', '');
            $disbursement->setValue('interest_rate_23', '');
            $disbursement->setValue('interest_rate_24', '');
            $disbursement->setValue('interest_rate_25', '');

            $total_2 = 0;

        }

        //------------------------- group  3  --------------------------------------------------------

        if ($countGroup_3 == 5) {

            $loan_31 = Loan::where('borrower_no', $group_3[0]['borrower_no'])->first();
            $loan_32 = Loan::where('borrower_no', $group_3[1]['borrower_no'])->first();
            $loan_33 = Loan::where('borrower_no', $group_3[2]['borrower_no'])->first();
            $loan_34 = Loan::where('borrower_no', $group_3[3]['borrower_no'])->first();
            $loan_35 = Loan::where('borrower_no', $group_3[4]['borrower_no'])->first();

            $disbursement->setValue('id_31', $group_3[0]['borrower_no']);
            $disbursement->setValue('id_32', $group_3[1]['borrower_no']);
            $disbursement->setValue('id_33', $group_3[2]['borrower_no']);
            $disbursement->setValue('id_34', $group_3[3]['borrower_no']);
            $disbursement->setValue('id_35', $group_3[4]['borrower_no']);

            $disbursement->setValue('name_31', $group_3[0]['full_name']);
            $disbursement->setValue('name_32', $group_3[1]['full_name']);
            $disbursement->setValue('name_33', $group_3[2]['full_name']);
            $disbursement->setValue('name_34', $group_3[3]['full_name']);
            $disbursement->setValue('name_35', $group_3[4]['full_name']);

            $disbursement->setValue('nic_31', $group_3[0]['nic']);
            $disbursement->setValue('nic_32', $group_3[1]['nic']);
            $disbursement->setValue('nic_33', $group_3[2]['nic']);
            $disbursement->setValue('nic_34', $group_3[3]['nic']);
            $disbursement->setValue('nic_35', $group_3[4]['nic']);

            $disbursement->setValue('loan_amount_31', $loan_31->loan_amount);
            $disbursement->setValue('loan_amount_32', $loan_32->loan_amount);
            $disbursement->setValue('loan_amount_33', $loan_33->loan_amount);
            $disbursement->setValue('loan_amount_34', $loan_34->loan_amount);
            $disbursement->setValue('loan_amount_35', $loan_35->loan_amount);

            $disbursement->setValue('loan_duration_31', $loan_31->loan_duration);
            $disbursement->setValue('loan_duration_32', $loan_32->loan_duration);
            $disbursement->setValue('loan_duration_33', $loan_33->loan_duration);
            $disbursement->setValue('loan_duration_34', $loan_34->loan_duration);
            $disbursement->setValue('loan_duration_35', $loan_35->loan_duration);

            $disbursement->setValue('group_no_31', $loan_31->group_no);
            $disbursement->setValue('group_no_32', $loan_32->group_no);
            $disbursement->setValue('group_no_33', $loan_33->group_no);
            $disbursement->setValue('group_no_34', $loan_34->group_no);
            $disbursement->setValue('group_no_35', $loan_35->group_no);

            $disbursement->setValue('interest_rate_31', $loan_31->interest_rate);
            $disbursement->setValue('interest_rate_32', $loan_32->interest_rate);
            $disbursement->setValue('interest_rate_33', $loan_33->interest_rate);
            $disbursement->setValue('interest_rate_34', $loan_34->interest_rate);
            $disbursement->setValue('interest_rate_35', $loan_35->interest_rate);

            $total_3 = ($loan_31->loan_amount + $loan_32->loan_amount + $loan_33->loan_amount + $loan_34->loan_amount + $loan_35->loan_amount);

        } else if ($countGroup_3 == 4) {

            $loan_31 = Loan::where('borrower_no', $group_3[0]['borrower_no'])->first();
            $loan_32 = Loan::where('borrower_no', $group_3[1]['borrower_no'])->first();
            $loan_33 = Loan::where('borrower_no', $group_3[2]['borrower_no'])->first();
            $loan_34 = Loan::where('borrower_no', $group_3[3]['borrower_no'])->first();

            $disbursement->setValue('id_31', $group_3[0]['borrower_no']);
            $disbursement->setValue('id_32', $group_3[1]['borrower_no']);
            $disbursement->setValue('id_33', $group_3[2]['borrower_no']);
            $disbursement->setValue('id_34', $group_3[3]['borrower_no']);
            $disbursement->setValue('id_35', '');

            $disbursement->setValue('name_31', $group_3[0]['full_name']);
            $disbursement->setValue('name_32', $group_3[1]['full_name']);
            $disbursement->setValue('name_33', $group_3[2]['full_name']);
            $disbursement->setValue('name_34', $group_3[3]['full_name']);
            $disbursement->setValue('name_35', '');

            $disbursement->setValue('nic_31', $group_3[0]['nic']);
            $disbursement->setValue('nic_32', $group_3[1]['nic']);
            $disbursement->setValue('nic_33', $group_3[2]['nic']);
            $disbursement->setValue('nic_34', $group_3[3]['nic']);
            $disbursement->setValue('nic_35', '');

            $disbursement->setValue('loan_amount_31', $loan_31->loan_amount);
            $disbursement->setValue('loan_amount_32', $loan_32->loan_amount);
            $disbursement->setValue('loan_amount_33', $loan_33->loan_amount);
            $disbursement->setValue('loan_amount_34', $loan_34->loan_amount);
            $disbursement->setValue('loan_amount_35', '');

            $disbursement->setValue('loan_duration_31', $loan_31->loan_duration);
            $disbursement->setValue('loan_duration_32', $loan_32->loan_duration);
            $disbursement->setValue('loan_duration_33', $loan_33->loan_duration);
            $disbursement->setValue('loan_duration_34', $loan_34->loan_duration);
            $disbursement->setValue('loan_duration_35', '');

            $disbursement->setValue('group_no_31', $loan_31->group_no);
            $disbursement->setValue('group_no_32', $loan_32->group_no);
            $disbursement->setValue('group_no_33', $loan_33->group_no);
            $disbursement->setValue('group_no_34', $loan_34->group_no);
            $disbursement->setValue('group_no_35', '');

            $disbursement->setValue('interest_rate_31', $loan_31->interest_rate);
            $disbursement->setValue('interest_rate_32', $loan_32->interest_rate);
            $disbursement->setValue('interest_rate_33', $loan_33->interest_rate);
            $disbursement->setValue('interest_rate_34', $loan_34->interest_rate);
            $disbursement->setValue('interest_rate_35', '');

            $total_3 = ($loan_31->loan_amount + $loan_32->loan_amount + $loan_33->loan_amount + $loan_34->loan_amount);

        } else if ($countGroup_3 == 3) {

            $loan_31 = Loan::where('borrower_no', $group_3[0]['borrower_no'])->first();
            $loan_32 = Loan::where('borrower_no', $group_3[1]['borrower_no'])->first();
            $loan_33 = Loan::where('borrower_no', $group_3[2]['borrower_no'])->first();

            $disbursement->setValue('id_31', $group_3[0]['borrower_no']);
            $disbursement->setValue('id_32', $group_3[1]['borrower_no']);
            $disbursement->setValue('id_33', $group_3[2]['borrower_no']);
            $disbursement->setValue('id_34', '');
            $disbursement->setValue('id_35', '');

            $disbursement->setValue('name_31', $group_3[0]['full_name']);
            $disbursement->setValue('name_32', $group_3[1]['full_name']);
            $disbursement->setValue('name_33', $group_3[2]['full_name']);
            $disbursement->setValue('name_34', '');
            $disbursement->setValue('name_35', '');

            $disbursement->setValue('nic_31', $group_3[0]['nic']);
            $disbursement->setValue('nic_32', $group_3[1]['nic']);
            $disbursement->setValue('nic_33', $group_3[2]['nic']);
            $disbursement->setValue('nic_34', '');
            $disbursement->setValue('nic_35', '');

            $disbursement->setValue('loan_amount_31', $loan_31->loan_amount);
            $disbursement->setValue('loan_amount_32', $loan_32->loan_amount);
            $disbursement->setValue('loan_amount_33', $loan_33->loan_amount);
            $disbursement->setValue('loan_amount_34', '');
            $disbursement->setValue('loan_amount_35', '');

            $disbursement->setValue('loan_duration_31', $loan_31->loan_duration);
            $disbursement->setValue('loan_duration_32', $loan_32->loan_duration);
            $disbursement->setValue('loan_duration_33', $loan_33->loan_duration);
            $disbursement->setValue('loan_duration_34', '');
            $disbursement->setValue('loan_duration_35', '');

            $disbursement->setValue('group_no_31', $loan_31->group_no);
            $disbursement->setValue('group_no_32', $loan_32->group_no);
            $disbursement->setValue('group_no_33', $loan_33->group_no);
            $disbursement->setValue('group_no_34', '');
            $disbursement->setValue('group_no_35', '');

            $disbursement->setValue('interest_rate_31', $loan_31->interest_rate);
            $disbursement->setValue('interest_rate_32', $loan_32->interest_rate);
            $disbursement->setValue('interest_rate_33', $loan_33->interest_rate);
            $disbursement->setValue('interest_rate_34', '');
            $disbursement->setValue('interest_rate_35', '');

            $total_3 = ($loan_31->loan_amount + $loan_32->loan_amount + $loan_33->loan_amount);

        } else if ($countGroup_3 == 2) {

            $loan_31 = Loan::where('borrower_no', $group_3[0]['borrower_no'])->first();
            $loan_32 = Loan::where('borrower_no', $group_3[1]['borrower_no'])->first();

            $disbursement->setValue('id_31', $group_3[0]['borrower_no']);
            $disbursement->setValue('id_32', $group_3[1]['borrower_no']);
            $disbursement->setValue('id_33', '');
            $disbursement->setValue('id_34', '');
            $disbursement->setValue('id_35', '');

            $disbursement->setValue('name_31', $group_3[0]['full_name']);
            $disbursement->setValue('name_32', $group_3[1]['full_name']);
            $disbursement->setValue('name_33', '');
            $disbursement->setValue('name_34', '');
            $disbursement->setValue('name_35', '');

            $disbursement->setValue('nic_31', $group_3[0]['nic']);
            $disbursement->setValue('nic_32', $group_3[1]['nic']);
            $disbursement->setValue('nic_33', '');
            $disbursement->setValue('nic_34', '');
            $disbursement->setValue('nic_35', '');

            $disbursement->setValue('loan_amount_31', $loan_31->loan_amount);
            $disbursement->setValue('loan_amount_32', $loan_32->loan_amount);
            $disbursement->setValue('loan_amount_33', '');
            $disbursement->setValue('loan_amount_34', '');
            $disbursement->setValue('loan_amount_35', '');

            $disbursement->setValue('loan_duration_31', $loan_31->loan_duration);
            $disbursement->setValue('loan_duration_32', $loan_32->loan_duration);
            $disbursement->setValue('loan_duration_33', '');
            $disbursement->setValue('loan_duration_34', '');
            $disbursement->setValue('loan_duration_35', '');

            $disbursement->setValue('group_no_31', $loan_31->group_no);
            $disbursement->setValue('group_no_32', $loan_32->group_no);
            $disbursement->setValue('group_no_33', '');
            $disbursement->setValue('group_no_34', '');
            $disbursement->setValue('group_no_35', '');

            $disbursement->setValue('interest_rate_31', $loan_31->interest_rate);
            $disbursement->setValue('interest_rate_32', $loan_32->interest_rate);
            $disbursement->setValue('interest_rate_33', '');
            $disbursement->setValue('interest_rate_34', '');
            $disbursement->setValue('interest_rate_35', '');

            $total_3 = ($loan_31->loan_amount + $loan_32->loan_amount);

        } else if ($countGroup_3 == 1) {
            return redirect()->back()->with('error', 'ONLY ONE BORROWER ADDED IN GROUP 3');
        } else if ($countGroup_3 == 0) {

            $disbursement->setValue('id_31', '');
            $disbursement->setValue('id_32', '');
            $disbursement->setValue('id_33', '');
            $disbursement->setValue('id_34', '');
            $disbursement->setValue('id_35', '');

            $disbursement->setValue('name_31', '');
            $disbursement->setValue('name_32', '');
            $disbursement->setValue('name_33', '');
            $disbursement->setValue('name_34', '');
            $disbursement->setValue('name_35', '');

            $disbursement->setValue('nic_31', '');
            $disbursement->setValue('nic_32', '');
            $disbursement->setValue('nic_33', '');
            $disbursement->setValue('nic_34', '');
            $disbursement->setValue('nic_35', '');

            $disbursement->setValue('loan_amount_31', '');
            $disbursement->setValue('loan_amount_32', '');
            $disbursement->setValue('loan_amount_33', '');
            $disbursement->setValue('loan_amount_34', '');
            $disbursement->setValue('loan_amount_35', '');

            $disbursement->setValue('loan_duration_31', '');
            $disbursement->setValue('loan_duration_32', '');
            $disbursement->setValue('loan_duration_33', '');
            $disbursement->setValue('loan_duration_34', '');
            $disbursement->setValue('loan_duration_35', '');

            $disbursement->setValue('group_no_31', '');
            $disbursement->setValue('group_no_32', '');
            $disbursement->setValue('group_no_33', '');
            $disbursement->setValue('group_no_34', '');
            $disbursement->setValue('group_no_35', '');

            $disbursement->setValue('interest_rate_31', '');
            $disbursement->setValue('interest_rate_32', '');
            $disbursement->setValue('interest_rate_33', '');
            $disbursement->setValue('interest_rate_34', '');
            $disbursement->setValue('interest_rate_35', '');

            $total_3 = 0;

        }

        //------------------------- group  4  --------------------------------------------------------

        if ($countGroup_4 == 5) {

            $loan_41 = Loan::where('borrower_no', $group_4[0]['borrower_no'])->first();
            $loan_42 = Loan::where('borrower_no', $group_4[1]['borrower_no'])->first();
            $loan_43 = Loan::where('borrower_no', $group_4[2]['borrower_no'])->first();
            $loan_44 = Loan::where('borrower_no', $group_4[3]['borrower_no'])->first();
            $loan_45 = Loan::where('borrower_no', $group_4[4]['borrower_no'])->first();

            $disbursement->setValue('id_41', $group_4[0]['borrower_no']);
            $disbursement->setValue('id_42', $group_4[1]['borrower_no']);
            $disbursement->setValue('id_43', $group_4[2]['borrower_no']);
            $disbursement->setValue('id_44', $group_4[3]['borrower_no']);
            $disbursement->setValue('id_45', $group_4[4]['borrower_no']);

            $disbursement->setValue('name_41', $group_4[0]['full_name']);
            $disbursement->setValue('name_42', $group_4[1]['full_name']);
            $disbursement->setValue('name_43', $group_4[2]['full_name']);
            $disbursement->setValue('name_44', $group_4[3]['full_name']);
            $disbursement->setValue('name_45', $group_4[4]['full_name']);

            $disbursement->setValue('nic_41', $group_4[0]['nic']);
            $disbursement->setValue('nic_42', $group_4[1]['nic']);
            $disbursement->setValue('nic_43', $group_4[2]['nic']);
            $disbursement->setValue('nic_44', $group_4[3]['nic']);
            $disbursement->setValue('nic_45', $group_4[4]['nic']);

            $disbursement->setValue('loan_amount_41', $loan_41->loan_amount);
            $disbursement->setValue('loan_amount_42', $loan_42->loan_amount);
            $disbursement->setValue('loan_amount_43', $loan_43->loan_amount);
            $disbursement->setValue('loan_amount_44', $loan_44->loan_amount);
            $disbursement->setValue('loan_amount_45', $loan_45->loan_amount);

            $disbursement->setValue('loan_duration_41', $loan_41->loan_duration);
            $disbursement->setValue('loan_duration_42', $loan_42->loan_duration);
            $disbursement->setValue('loan_duration_43', $loan_43->loan_duration);
            $disbursement->setValue('loan_duration_44', $loan_44->loan_duration);
            $disbursement->setValue('loan_duration_45', $loan_45->loan_duration);

            $disbursement->setValue('group_no_41', $loan_41->group_no);
            $disbursement->setValue('group_no_42', $loan_42->group_no);
            $disbursement->setValue('group_no_43', $loan_43->group_no);
            $disbursement->setValue('group_no_44', $loan_44->group_no);
            $disbursement->setValue('group_no_45', $loan_45->group_no);

            $disbursement->setValue('interest_rate_41', $loan_41->interest_rate);
            $disbursement->setValue('interest_rate_42', $loan_42->interest_rate);
            $disbursement->setValue('interest_rate_43', $loan_43->interest_rate);
            $disbursement->setValue('interest_rate_44', $loan_44->interest_rate);
            $disbursement->setValue('interest_rate_45', $loan_45->interest_rate);

            $total_4 = ($loan_41->loan_amount + $loan_42->loan_amount + $loan_43->loan_amount + $loan_44->loan_amount + $loan_45->loan_amount);

        } else if ($countGroup_4 == 4) {

            $loan_41 = Loan::where('borrower_no', $group_4[0]['borrower_no'])->first();
            $loan_42 = Loan::where('borrower_no', $group_4[1]['borrower_no'])->first();
            $loan_43 = Loan::where('borrower_no', $group_4[2]['borrower_no'])->first();
            $loan_44 = Loan::where('borrower_no', $group_4[3]['borrower_no'])->first();

            $disbursement->setValue('id_41', $group_4[0]['borrower_no']);
            $disbursement->setValue('id_42', $group_4[1]['borrower_no']);
            $disbursement->setValue('id_43', $group_4[2]['borrower_no']);
            $disbursement->setValue('id_44', $group_4[3]['borrower_no']);
            $disbursement->setValue('id_45', '');

            $disbursement->setValue('name_41', $group_4[0]['full_name']);
            $disbursement->setValue('name_42', $group_4[1]['full_name']);
            $disbursement->setValue('name_43', $group_4[2]['full_name']);
            $disbursement->setValue('name_44', $group_4[3]['full_name']);
            $disbursement->setValue('name_45', '');

            $disbursement->setValue('nic_41', $group_4[0]['nic']);
            $disbursement->setValue('nic_42', $group_4[1]['nic']);
            $disbursement->setValue('nic_43', $group_4[2]['nic']);
            $disbursement->setValue('nic_44', $group_4[3]['nic']);
            $disbursement->setValue('nic_45', '');

            $disbursement->setValue('loan_amount_41', $loan_41->loan_amount);
            $disbursement->setValue('loan_amount_42', $loan_42->loan_amount);
            $disbursement->setValue('loan_amount_43', $loan_43->loan_amount);
            $disbursement->setValue('loan_amount_44', $loan_44->loan_amount);
            $disbursement->setValue('loan_amount_45', '');

            $disbursement->setValue('loan_duration_41', $loan_41->loan_duration);
            $disbursement->setValue('loan_duration_42', $loan_42->loan_duration);
            $disbursement->setValue('loan_duration_43', $loan_43->loan_duration);
            $disbursement->setValue('loan_duration_44', $loan_44->loan_duration);
            $disbursement->setValue('loan_duration_45', '');

            $disbursement->setValue('group_no_41', $loan_41->group_no);
            $disbursement->setValue('group_no_42', $loan_42->group_no);
            $disbursement->setValue('group_no_43', $loan_43->group_no);
            $disbursement->setValue('group_no_44', $loan_44->group_no);
            $disbursement->setValue('group_no_45', '');

            $disbursement->setValue('interest_rate_41', $loan_41->interest_rate);
            $disbursement->setValue('interest_rate_42', $loan_42->interest_rate);
            $disbursement->setValue('interest_rate_43', $loan_43->interest_rate);
            $disbursement->setValue('interest_rate_44', $loan_44->interest_rate);
            $disbursement->setValue('interest_rate_45', '');

            $total_4 = ($loan_41->loan_amount + $loan_42->loan_amount + $loan_43->loan_amount + $loan_44->loan_amount);

        } else if ($countGroup_4 == 3) {

            $loan_41 = Loan::where('borrower_no', $group_4[0]['borrower_no'])->first();
            $loan_42 = Loan::where('borrower_no', $group_4[1]['borrower_no'])->first();
            $loan_43 = Loan::where('borrower_no', $group_4[2]['borrower_no'])->first();

            $disbursement->setValue('id_41', $group_4[0]['borrower_no']);
            $disbursement->setValue('id_42', $group_4[1]['borrower_no']);
            $disbursement->setValue('id_43', $group_4[2]['borrower_no']);
            $disbursement->setValue('id_44', '');
            $disbursement->setValue('id_45', '');

            $disbursement->setValue('name_41', $group_4[0]['full_name']);
            $disbursement->setValue('name_42', $group_4[1]['full_name']);
            $disbursement->setValue('name_43', $group_4[2]['full_name']);
            $disbursement->setValue('name_44', '');
            $disbursement->setValue('name_45', '');

            $disbursement->setValue('nic_41', $group_4[0]['nic']);
            $disbursement->setValue('nic_42', $group_4[1]['nic']);
            $disbursement->setValue('nic_43', $group_4[2]['nic']);
            $disbursement->setValue('nic_44', '');
            $disbursement->setValue('nic_45', '');

            $disbursement->setValue('loan_amount_41', $loan_41->loan_amount);
            $disbursement->setValue('loan_amount_42', $loan_42->loan_amount);
            $disbursement->setValue('loan_amount_43', $loan_43->loan_amount);
            $disbursement->setValue('loan_amount_44', '');
            $disbursement->setValue('loan_amount_45', '');

            $disbursement->setValue('loan_duration_41', $loan_41->loan_duration);
            $disbursement->setValue('loan_duration_42', $loan_42->loan_duration);
            $disbursement->setValue('loan_duration_43', $loan_43->loan_duration);
            $disbursement->setValue('loan_duration_44', '');
            $disbursement->setValue('loan_duration_45', '');

            $disbursement->setValue('group_no_41', $loan_41->group_no);
            $disbursement->setValue('group_no_42', $loan_42->group_no);
            $disbursement->setValue('group_no_43', $loan_43->group_no);
            $disbursement->setValue('group_no_44', '');
            $disbursement->setValue('group_no_45', '');

            $disbursement->setValue('interest_rate_41', $loan_41->interest_rate);
            $disbursement->setValue('interest_rate_42', $loan_42->interest_rate);
            $disbursement->setValue('interest_rate_43', $loan_43->interest_rate);
            $disbursement->setValue('interest_rate_44', '');
            $disbursement->setValue('interest_rate_45', '');

            $total_4 = ($loan_41->loan_amount + $loan_42->loan_amount + $loan_43->loan_amount);

        } else if ($countGroup_4 == 2) {

            $loan_41 = Loan::where('borrower_no', $group_4[0]['borrower_no'])->first();
            $loan_42 = Loan::where('borrower_no', $group_4[1]['borrower_no'])->first();

            $disbursement->setValue('id_41', $group_4[0]['borrower_no']);
            $disbursement->setValue('id_42', $group_4[1]['borrower_no']);
            $disbursement->setValue('id_43', '');
            $disbursement->setValue('id_44', '');
            $disbursement->setValue('id_45', '');

            $disbursement->setValue('name_41', $group_4[0]['full_name']);
            $disbursement->setValue('name_42', $group_4[1]['full_name']);
            $disbursement->setValue('name_43', '');
            $disbursement->setValue('name_44', '');
            $disbursement->setValue('name_45', '');

            $disbursement->setValue('nic_41', $group_4[0]['nic']);
            $disbursement->setValue('nic_42', $group_4[1]['nic']);
            $disbursement->setValue('nic_43', '');
            $disbursement->setValue('nic_44', '');
            $disbursement->setValue('nic_45', '');

            $disbursement->setValue('loan_amount_41', $loan_41->loan_amount);
            $disbursement->setValue('loan_amount_42', $loan_42->loan_amount);
            $disbursement->setValue('loan_amount_43', '');
            $disbursement->setValue('loan_amount_44', '');
            $disbursement->setValue('loan_amount_45', '');

            $disbursement->setValue('loan_duration_41', $loan_41->loan_duration);
            $disbursement->setValue('loan_duration_42', $loan_42->loan_duration);
            $disbursement->setValue('loan_duration_43', '');
            $disbursement->setValue('loan_duration_44', '');
            $disbursement->setValue('loan_duration_45', '');

            $disbursement->setValue('group_no_41', $loan_41->group_no);
            $disbursement->setValue('group_no_42', $loan_42->group_no);
            $disbursement->setValue('group_no_43', '');
            $disbursement->setValue('group_no_44', '');
            $disbursement->setValue('group_no_45', '');

            $disbursement->setValue('interest_rate_41', $loan_41->interest_rate);
            $disbursement->setValue('interest_rate_42', $loan_42->interest_rate);
            $disbursement->setValue('interest_rate_43', '');
            $disbursement->setValue('interest_rate_44', '');
            $disbursement->setValue('interest_rate_45', '');

            $total_4 = ($loan_41->loan_amount + $loan_42->loan_amount);

        } else if ($countGroup_4 == 1) {
            return redirect()->back()->with('error', 'ONLY ONE BORROWER ADDED IN GROUP 4');
        } else if ($countGroup_4 == 0) {

            $disbursement->setValue('id_41', '');
            $disbursement->setValue('id_42', '');
            $disbursement->setValue('id_43', '');
            $disbursement->setValue('id_44', '');
            $disbursement->setValue('id_45', '');

            $disbursement->setValue('name_41', '');
            $disbursement->setValue('name_42', '');
            $disbursement->setValue('name_43', '');
            $disbursement->setValue('name_44', '');
            $disbursement->setValue('name_45', '');

            $disbursement->setValue('nic_41', '');
            $disbursement->setValue('nic_42', '');
            $disbursement->setValue('nic_43', '');
            $disbursement->setValue('nic_44', '');
            $disbursement->setValue('nic_45', '');

            $disbursement->setValue('loan_amount_41', '');
            $disbursement->setValue('loan_amount_42', '');
            $disbursement->setValue('loan_amount_43', '');
            $disbursement->setValue('loan_amount_44', '');
            $disbursement->setValue('loan_amount_45', '');

            $disbursement->setValue('loan_duration_41', '');
            $disbursement->setValue('loan_duration_42', '');
            $disbursement->setValue('loan_duration_43', '');
            $disbursement->setValue('loan_duration_44', '');
            $disbursement->setValue('loan_duration_45', '');

            $disbursement->setValue('group_no_41', '');
            $disbursement->setValue('group_no_42', '');
            $disbursement->setValue('group_no_43', '');
            $disbursement->setValue('group_no_44', '');
            $disbursement->setValue('group_no_45', '');

            $disbursement->setValue('interest_rate_41', '');
            $disbursement->setValue('interest_rate_42', '');
            $disbursement->setValue('interest_rate_43', '');
            $disbursement->setValue('interest_rate_44', '');
            $disbursement->setValue('interest_rate_45', '');

            $total_4 = 0;

        }

        //------------------------- group  5  --------------------------------------------------------

        if ($countGroup_5 == 5) {

            $loan_51 = Loan::where('borrower_no', $group_5[0]['borrower_no'])->first();
            $loan_52 = Loan::where('borrower_no', $group_5[1]['borrower_no'])->first();
            $loan_53 = Loan::where('borrower_no', $group_5[2]['borrower_no'])->first();
            $loan_54 = Loan::where('borrower_no', $group_5[3]['borrower_no'])->first();
            $loan_55 = Loan::where('borrower_no', $group_5[4]['borrower_no'])->first();

            $disbursement->setValue('id_51', $group_5[0]['borrower_no']);
            $disbursement->setValue('id_52', $group_5[1]['borrower_no']);
            $disbursement->setValue('id_53', $group_5[2]['borrower_no']);
            $disbursement->setValue('id_54', $group_5[3]['borrower_no']);
            $disbursement->setValue('id_55', $group_5[4]['borrower_no']);

            $disbursement->setValue('name_51', $group_5[0]['full_name']);
            $disbursement->setValue('name_52', $group_5[1]['full_name']);
            $disbursement->setValue('name_53', $group_5[2]['full_name']);
            $disbursement->setValue('name_54', $group_5[3]['full_name']);
            $disbursement->setValue('name_55', $group_5[4]['full_name']);

            $disbursement->setValue('nic_51', $group_5[0]['nic']);
            $disbursement->setValue('nic_52', $group_5[1]['nic']);
            $disbursement->setValue('nic_53', $group_5[2]['nic']);
            $disbursement->setValue('nic_54', $group_5[3]['nic']);
            $disbursement->setValue('nic_55', $group_5[4]['nic']);

            $disbursement->setValue('loan_amount_51', $loan_51->loan_amount);
            $disbursement->setValue('loan_amount_52', $loan_52->loan_amount);
            $disbursement->setValue('loan_amount_53', $loan_53->loan_amount);
            $disbursement->setValue('loan_amount_54', $loan_54->loan_amount);
            $disbursement->setValue('loan_amount_55', $loan_55->loan_amount);

            $disbursement->setValue('loan_duration_51', $loan_51->loan_duration);
            $disbursement->setValue('loan_duration_52', $loan_52->loan_duration);
            $disbursement->setValue('loan_duration_53', $loan_53->loan_duration);
            $disbursement->setValue('loan_duration_54', $loan_54->loan_duration);
            $disbursement->setValue('loan_duration_55', $loan_55->loan_duration);

            $disbursement->setValue('group_no_51', $loan_51->group_no);
            $disbursement->setValue('group_no_52', $loan_52->group_no);
            $disbursement->setValue('group_no_53', $loan_53->group_no);
            $disbursement->setValue('group_no_54', $loan_54->group_no);
            $disbursement->setValue('group_no_55', $loan_55->group_no);

            $disbursement->setValue('interest_rate_51', $loan_51->interest_rate);
            $disbursement->setValue('interest_rate_52', $loan_52->interest_rate);
            $disbursement->setValue('interest_rate_53', $loan_53->interest_rate);
            $disbursement->setValue('interest_rate_54', $loan_54->interest_rate);
            $disbursement->setValue('interest_rate_55', $loan_55->interest_rate);

            $total_5 = ($loan_51->loan_amount + $loan_52->loan_amount + $loan_53->loan_amount + $loan_54->loan_amount + $loan_55->loan_amount);

        } else if ($countGroup_5 == 4) {

            $loan_51 = Loan::where('borrower_no', $group_5[0]['borrower_no'])->first();
            $loan_52 = Loan::where('borrower_no', $group_5[1]['borrower_no'])->first();
            $loan_53 = Loan::where('borrower_no', $group_5[2]['borrower_no'])->first();
            $loan_54 = Loan::where('borrower_no', $group_5[3]['borrower_no'])->first();

            $disbursement->setValue('id_51', $group_5[0]['borrower_no']);
            $disbursement->setValue('id_52', $group_5[1]['borrower_no']);
            $disbursement->setValue('id_53', $group_5[2]['borrower_no']);
            $disbursement->setValue('id_54', $group_5[3]['borrower_no']);
            $disbursement->setValue('id_55', '');

            $disbursement->setValue('name_51', $group_5[0]['full_name']);
            $disbursement->setValue('name_52', $group_5[1]['full_name']);
            $disbursement->setValue('name_53', $group_5[2]['full_name']);
            $disbursement->setValue('name_54', $group_5[3]['full_name']);
            $disbursement->setValue('name_55', '');

            $disbursement->setValue('nic_51', $group_5[0]['nic']);
            $disbursement->setValue('nic_52', $group_5[1]['nic']);
            $disbursement->setValue('nic_53', $group_5[2]['nic']);
            $disbursement->setValue('nic_54', $group_5[3]['nic']);
            $disbursement->setValue('nic_55', '');

            $disbursement->setValue('loan_amount_51', $loan_51->loan_amount);
            $disbursement->setValue('loan_amount_52', $loan_52->loan_amount);
            $disbursement->setValue('loan_amount_53', $loan_53->loan_amount);
            $disbursement->setValue('loan_amount_54', $loan_54->loan_amount);
            $disbursement->setValue('loan_amount_55', '');

            $disbursement->setValue('loan_duration_51', $loan_51->loan_duration);
            $disbursement->setValue('loan_duration_52', $loan_52->loan_duration);
            $disbursement->setValue('loan_duration_53', $loan_53->loan_duration);
            $disbursement->setValue('loan_duration_54', $loan_54->loan_duration);
            $disbursement->setValue('loan_duration_55', '');

            $disbursement->setValue('group_no_51', $loan_51->group_no);
            $disbursement->setValue('group_no_52', $loan_52->group_no);
            $disbursement->setValue('group_no_53', $loan_53->group_no);
            $disbursement->setValue('group_no_54', $loan_54->group_no);
            $disbursement->setValue('group_no_55', '');

            $disbursement->setValue('interest_rate_51', $loan_51->interest_rate);
            $disbursement->setValue('interest_rate_52', $loan_52->interest_rate);
            $disbursement->setValue('interest_rate_53', $loan_53->interest_rate);
            $disbursement->setValue('interest_rate_54', $loan_54->interest_rate);
            $disbursement->setValue('interest_rate_55', '');

            $total_5 = ($loan_51->loan_amount + $loan_52->loan_amount + $loan_53->loan_amount + $loan_54->loan_amount);

        } else if ($countGroup_5 == 3) {

            $loan_51 = Loan::where('borrower_no', $group_5[0]['borrower_no'])->first();
            $loan_52 = Loan::where('borrower_no', $group_5[1]['borrower_no'])->first();
            $loan_53 = Loan::where('borrower_no', $group_5[2]['borrower_no'])->first();

            $disbursement->setValue('id_51', $group_5[0]['borrower_no']);
            $disbursement->setValue('id_52', $group_5[1]['borrower_no']);
            $disbursement->setValue('id_53', $group_5[2]['borrower_no']);
            $disbursement->setValue('id_54', '');
            $disbursement->setValue('id_55', '');

            $disbursement->setValue('name_51', $group_5[0]['full_name']);
            $disbursement->setValue('name_52', $group_5[1]['full_name']);
            $disbursement->setValue('name_53', $group_5[2]['full_name']);
            $disbursement->setValue('name_54', '');
            $disbursement->setValue('name_55', '');

            $disbursement->setValue('nic_51', $group_5[0]['nic']);
            $disbursement->setValue('nic_52', $group_5[1]['nic']);
            $disbursement->setValue('nic_53', $group_5[2]['nic']);
            $disbursement->setValue('nic_54', '');
            $disbursement->setValue('nic_55', '');

            $disbursement->setValue('loan_amount_51', $loan_51->loan_amount);
            $disbursement->setValue('loan_amount_52', $loan_52->loan_amount);
            $disbursement->setValue('loan_amount_53', $loan_53->loan_amount);
            $disbursement->setValue('loan_amount_54', '');
            $disbursement->setValue('loan_amount_55', '');

            $disbursement->setValue('loan_duration_51', $loan_51->loan_duration);
            $disbursement->setValue('loan_duration_52', $loan_52->loan_duration);
            $disbursement->setValue('loan_duration_53', $loan_53->loan_duration);
            $disbursement->setValue('loan_duration_54', '');
            $disbursement->setValue('loan_duration_55', '');

            $disbursement->setValue('group_no_51', $loan_51->group_no);
            $disbursement->setValue('group_no_52', $loan_52->group_no);
            $disbursement->setValue('group_no_53', $loan_53->group_no);
            $disbursement->setValue('group_no_54', '');
            $disbursement->setValue('group_no_55', '');

            $disbursement->setValue('interest_rate_51', $loan_51->interest_rate);
            $disbursement->setValue('interest_rate_52', $loan_52->interest_rate);
            $disbursement->setValue('interest_rate_53', $loan_53->interest_rate);
            $disbursement->setValue('interest_rate_54', '');
            $disbursement->setValue('interest_rate_55', '');

            $total_5 = ($loan_51->loan_amount + $loan_52->loan_amount + $loan_53->loan_amount);

        } else if ($countGroup_5 == 2) {

            $loan_51 = Loan::where('borrower_no', $group_5[0]['borrower_no'])->first();
            $loan_52 = Loan::where('borrower_no', $group_5[1]['borrower_no'])->first();

            $disbursement->setValue('id_51', $group_5[0]['borrower_no']);
            $disbursement->setValue('id_52', $group_5[1]['borrower_no']);
            $disbursement->setValue('id_53', '');
            $disbursement->setValue('id_54', '');
            $disbursement->setValue('id_55', '');

            $disbursement->setValue('name_51', $group_5[0]['full_name']);
            $disbursement->setValue('name_52', $group_5[1]['full_name']);
            $disbursement->setValue('name_53', '');
            $disbursement->setValue('name_54', '');
            $disbursement->setValue('name_55', '');

            $disbursement->setValue('nic_51', $group_5[0]['nic']);
            $disbursement->setValue('nic_52', $group_5[1]['nic']);
            $disbursement->setValue('nic_53', '');
            $disbursement->setValue('nic_54', '');
            $disbursement->setValue('nic_55', '');

            $disbursement->setValue('loan_amount_51', $loan_51->loan_amount);
            $disbursement->setValue('loan_amount_52', $loan_52->loan_amount);
            $disbursement->setValue('loan_amount_53', '');
            $disbursement->setValue('loan_amount_54', '');
            $disbursement->setValue('loan_amount_55', '');

            $disbursement->setValue('loan_duration_51', $loan_51->loan_duration);
            $disbursement->setValue('loan_duration_52', $loan_52->loan_duration);
            $disbursement->setValue('loan_duration_53', '');
            $disbursement->setValue('loan_duration_54', '');
            $disbursement->setValue('loan_duration_55', '');

            $disbursement->setValue('group_no_51', $loan_51->group_no);
            $disbursement->setValue('group_no_52', $loan_52->group_no);
            $disbursement->setValue('group_no_53', '');
            $disbursement->setValue('group_no_54', '');
            $disbursement->setValue('group_no_55', '');

            $disbursement->setValue('interest_rate_51', $loan_51->interest_rate);
            $disbursement->setValue('interest_rate_52', $loan_52->interest_rate);
            $disbursement->setValue('interest_rate_53', '');
            $disbursement->setValue('interest_rate_54', '');
            $disbursement->setValue('interest_rate_55', '');

            $total_5 = ($loan_51->loan_amount + $loan_52->loan_amount);

        } else if ($countGroup_5 == 1) {
            return redirect()->back()->with('error', 'ONLY ONE BORROWER ADDED IN GROUP 5');
        } else if ($countGroup_5 == 0) {

            $disbursement->setValue('id_51', '');
            $disbursement->setValue('id_52', '');
            $disbursement->setValue('id_53', '');
            $disbursement->setValue('id_54', '');
            $disbursement->setValue('id_55', '');

            $disbursement->setValue('name_51', '');
            $disbursement->setValue('name_52', '');
            $disbursement->setValue('name_53', '');
            $disbursement->setValue('name_54', '');
            $disbursement->setValue('name_55', '');

            $disbursement->setValue('nic_51', '');
            $disbursement->setValue('nic_52', '');
            $disbursement->setValue('nic_53', '');
            $disbursement->setValue('nic_54', '');
            $disbursement->setValue('nic_55', '');

            $disbursement->setValue('loan_amount_51', '');
            $disbursement->setValue('loan_amount_52', '');
            $disbursement->setValue('loan_amount_53', '');
            $disbursement->setValue('loan_amount_54', '');
            $disbursement->setValue('loan_amount_55', '');

            $disbursement->setValue('loan_duration_51', '');
            $disbursement->setValue('loan_duration_52', '');
            $disbursement->setValue('loan_duration_53', '');
            $disbursement->setValue('loan_duration_54', '');
            $disbursement->setValue('loan_duration_55', '');

            $disbursement->setValue('group_no_51', '');
            $disbursement->setValue('group_no_52', '');
            $disbursement->setValue('group_no_53', '');
            $disbursement->setValue('group_no_54', '');
            $disbursement->setValue('group_no_55', '');

            $disbursement->setValue('interest_rate_51', '');
            $disbursement->setValue('interest_rate_52', '');
            $disbursement->setValue('interest_rate_53', '');
            $disbursement->setValue('interest_rate_54', '');
            $disbursement->setValue('interest_rate_55', '');

            $total_5 = 0;

        }

        $disbursement->setValue('center_no', $center->center_no);
        $disbursement->setValue('branch', $branchName);
        $disbursement->setValue('center', $centerName);
        $releaseDate = Loan::where('borrower_no', $group_1[0]['borrower_no'])->first();
        $disbursement->setValue('release_date', $releaseDate->release_date);
        $total = ($total_1 + $total_2 + $total_3 + $total_4 + $total_5);
        $disbursement->setValue('total', $total);

        $fileName = $center->center_name . " disbursement";

        $disbursement->saveAs($fileName . '.docx');
        return response()->download($fileName . '.docx')->deleteFileAfterSend(true);
    }

    public function exportDisbursementGroup($id)
    {
        $loan = Loan::where('id', $id)->first();
        $borrower = Borrower::where('borrower_no', $loan->borrower_no)->first();
        $guarantors = Borrower::where('branch', $borrower->branch)
            ->where('center', $borrower->center)->where('group_no', $borrower->group_no)->get();

        $center = Center::where('branch_no', $borrower->branch)->where('center_name', $borrower->center)->first();
        $center_no = $center->center_no;

        $count = count($guarantors);

        $guarantorsArray = json_decode($guarantors, true);
        $loanArray = json_decode($loan, true);
        $allLoans = Loan::where('branch', $borrower->branch)->where('center', $borrower->center)->where('group_no', $guarantorsArray[0]["group_no"])->get();

        $disbursement = new TemplateProcessor('word-template/DisbursementGroup.docx');
        if ($count == 5) {

            $loanAmount1 = $allLoans[0]["loan_amount"];
            $loanAmount2 = $allLoans[1]["loan_amount"];
            $loanAmount3 = $allLoans[2]["loan_amount"];
            $loanAmount4 = $allLoans[3]["loan_amount"];
            $loanAmount5 = $allLoans[4]["loan_amount"];

            $interestRate1 = $allLoans[0]["interest_rate"];
            $interestRate2 = $allLoans[1]["interest_rate"];
            $interestRate3 = $allLoans[2]["interest_rate"];
            $interestRate4 = $allLoans[3]["interest_rate"];
            $interestRate5 = $allLoans[4]["interest_rate"];

            $loanDuration1 = $allLoans[0]["loan_duration"];
            $loanDuration2 = $allLoans[1]["loan_duration"];
            $loanDuration3 = $allLoans[2]["loan_duration"];
            $loanDuration4 = $allLoans[3]["loan_duration"];
            $loanDuration5 = $allLoans[4]["loan_duration"];

            $disbursement->setValue('branch', $guarantorsArray[0]["branch"]);
            $disbursement->setValue('center', $guarantorsArray[0]["center"]);
            $disbursement->setValue('release_date', $loanArray["release_date"]);

            $disbursement->setValue('name_1', $guarantorsArray[0]["full_name"]);
            $disbursement->setValue('id_1', $guarantorsArray[0]["borrower_no"]);
            $disbursement->setValue('nic_1', $guarantorsArray[0]["nic"]);
            $disbursement->setValue('interest_rate_1', $interestRate1);
            $disbursement->setValue('loan_amount_1', $loanAmount1);
            $disbursement->setValue('loan_duration_1', $loanDuration1);
            $disbursement->setValue('group_no_1', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_2', $guarantorsArray[1]["full_name"]);
            $disbursement->setValue('id_2', $guarantorsArray[1]["borrower_no"]);
            $disbursement->setValue('nic_2', $guarantorsArray[1]["nic"]);
            $disbursement->setValue('interest_rate_2', $interestRate2);
            $disbursement->setValue('loan_amount_2', $loanAmount2);
            $disbursement->setValue('loan_duration_2', $loanDuration2);
            $disbursement->setValue('group_no_2', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_3', $guarantorsArray[2]["full_name"]);
            $disbursement->setValue('id_3', $guarantorsArray[2]["borrower_no"]);
            $disbursement->setValue('nic_3', $guarantorsArray[2]["nic"]);
            $disbursement->setValue('interest_rate_3', $interestRate3);
            $disbursement->setValue('loan_amount_3', $loanAmount3);
            $disbursement->setValue('loan_duration_3', $loanDuration3);
            $disbursement->setValue('group_no_3', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_4', $guarantorsArray[3]["full_name"]);
            $disbursement->setValue('id_4', $guarantorsArray[3]["borrower_no"]);
            $disbursement->setValue('nic_4', $guarantorsArray[3]["nic"]);
            $disbursement->setValue('interest_rate_4', $interestRate4);
            $disbursement->setValue('loan_amount_4', $loanAmount4);
            $disbursement->setValue('loan_duration_4', $loanDuration4);
            $disbursement->setValue('group_no_4', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_5', $guarantorsArray[4]["full_name"]);
            $disbursement->setValue('id_5', $guarantorsArray[4]["borrower_no"]);
            $disbursement->setValue('nic_5', $guarantorsArray[4]["nic"]);
            $disbursement->setValue('interest_rate_5', $interestRate5);
            $disbursement->setValue('loan_amount_5', $loanAmount5);
            $disbursement->setValue('loan_duration_5', $loanDuration5);
            $disbursement->setValue('group_no_5', $guarantorsArray[0]["group_no"]);
            $sum = ($loanAmount1 + $loanAmount2 + $loanAmount3 + $loanAmount4 + $loanAmount5);
        } else if ($count == 4) {

            $loanAmount1 = $allLoans[0]["loan_amount"];
            $loanAmount2 = $allLoans[1]["loan_amount"];
            $loanAmount3 = $allLoans[2]["loan_amount"];
            $loanAmount4 = $allLoans[3]["loan_amount"];

            $interestRate1 = $allLoans[0]["interest_rate"];
            $interestRate2 = $allLoans[1]["interest_rate"];
            $interestRate3 = $allLoans[2]["interest_rate"];
            $interestRate4 = $allLoans[3]["interest_rate"];

            $loanDuration1 = $allLoans[0]["loan_duration"];
            $loanDuration2 = $allLoans[1]["loan_duration"];
            $loanDuration3 = $allLoans[2]["loan_duration"];
            $loanDuration4 = $allLoans[3]["loan_duration"];

            $disbursement->setValue('branch', $guarantorsArray[0]["branch"]);
            $disbursement->setValue('center', $guarantorsArray[0]["center"]);
            $disbursement->setValue('release_date', $loanArray["release_date"]);

            $disbursement->setValue('name_1', $guarantorsArray[0]["full_name"]);
            $disbursement->setValue('id_1', $guarantorsArray[0]["borrower_no"]);
            $disbursement->setValue('nic_1', $guarantorsArray[0]["nic"]);
            $disbursement->setValue('interest_rate_1', $interestRate1);
            $disbursement->setValue('loan_amount_1', $loanAmount1);
            $disbursement->setValue('loan_duration_1', $loanDuration1);
            $disbursement->setValue('group_no_1', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_2', $guarantorsArray[1]["full_name"]);
            $disbursement->setValue('id_2', $guarantorsArray[1]["borrower_no"]);
            $disbursement->setValue('nic_2', $guarantorsArray[1]["nic"]);
            $disbursement->setValue('interest_rate_2', $interestRate2);
            $disbursement->setValue('loan_amount_2', $loanAmount2);
            $disbursement->setValue('loan_duration_2', $loanDuration2);
            $disbursement->setValue('group_no_2', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_3', $guarantorsArray[2]["full_name"]);
            $disbursement->setValue('id_3', $guarantorsArray[2]["borrower_no"]);
            $disbursement->setValue('nic_3', $guarantorsArray[2]["nic"]);
            $disbursement->setValue('interest_rate_3', $interestRate3);
            $disbursement->setValue('loan_amount_3', $loanAmount3);
            $disbursement->setValue('loan_duration_3', $loanDuration3);
            $disbursement->setValue('group_no_3', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_4', $guarantorsArray[3]["full_name"]);
            $disbursement->setValue('id_4', $guarantorsArray[3]["borrower_no"]);
            $disbursement->setValue('nic_4', $guarantorsArray[3]["nic"]);
            $disbursement->setValue('interest_rate_4', $interestRate4);
            $disbursement->setValue('loan_amount_4', $loanAmount4);
            $disbursement->setValue('loan_duration_4', $loanDuration4);
            $disbursement->setValue('group_no_4', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_5', '');
            $disbursement->setValue('id_5', '');
            $disbursement->setValue('nic_5', '');
            $disbursement->setValue('interest_rate_5', '');
            $disbursement->setValue('loan_amount_5', '');
            $disbursement->setValue('loan_duration_5', '');
            $disbursement->setValue('group_no_5', '');
            $sum = ($loanAmount1 + $loanAmount2 + $loanAmount3 + $loanAmount4);
        } else if ($count == 3) {

            $loanAmount1 = $allLoans[0]["loan_amount"];
            $loanAmount2 = $allLoans[1]["loan_amount"];
            $loanAmount3 = $allLoans[2]["loan_amount"];

            $interestRate1 = $allLoans[0]["interest_rate"];
            $interestRate2 = $allLoans[1]["interest_rate"];
            $interestRate3 = $allLoans[2]["interest_rate"];

            $loanDuration1 = $allLoans[0]["loan_duration"];
            $loanDuration2 = $allLoans[1]["loan_duration"];
            $loanDuration3 = $allLoans[2]["loan_duration"];

            $disbursement->setValue('branch', $guarantorsArray[0]["branch"]);
            $disbursement->setValue('center', $guarantorsArray[0]["center"]);
            $disbursement->setValue('release_date', $loanArray["release_date"]);

            $disbursement->setValue('name_1', $guarantorsArray[0]["full_name"]);
            $disbursement->setValue('id_1', $guarantorsArray[0]["borrower_no"]);
            $disbursement->setValue('nic_1', $guarantorsArray[0]["nic"]);
            $disbursement->setValue('interest_rate_1', $interestRate1);
            $disbursement->setValue('loan_amount_1', $loanAmount1);
            $disbursement->setValue('loan_duration_1', $loanDuration1);
            $disbursement->setValue('group_no_1', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_2', $guarantorsArray[1]["full_name"]);
            $disbursement->setValue('id_2', $guarantorsArray[1]["borrower_no"]);
            $disbursement->setValue('nic_2', $guarantorsArray[1]["nic"]);
            $disbursement->setValue('interest_rate_2', $interestRate2);
            $disbursement->setValue('loan_amount_2', $loanAmount2);
            $disbursement->setValue('loan_duration_2', $loanDuration2);
            $disbursement->setValue('group_no_2', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_3', $guarantorsArray[2]["full_name"]);
            $disbursement->setValue('id_3', $guarantorsArray[2]["borrower_no"]);
            $disbursement->setValue('nic_3', $guarantorsArray[2]["nic"]);
            $disbursement->setValue('interest_rate_3', $interestRate3);
            $disbursement->setValue('loan_amount_3', $loanAmount3);
            $disbursement->setValue('loan_duration_3', $loanDuration3);
            $disbursement->setValue('group_no_3', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_4', '');
            $disbursement->setValue('id_4', '');
            $disbursement->setValue('nic_4', '');
            $disbursement->setValue('interest_rate_4', '');
            $disbursement->setValue('loan_amount_4', '');
            $disbursement->setValue('loan_duration_4', '');
            $disbursement->setValue('group_no_4', '');

            $disbursement->setValue('name_5', '');
            $disbursement->setValue('id_5', '');
            $disbursement->setValue('nic_5', '');
            $disbursement->setValue('interest_rate_5', '');
            $disbursement->setValue('loan_amount_5', '');
            $disbursement->setValue('loan_duration_5', '');
            $disbursement->setValue('group_no_5', '');
            $sum = ($loanAmount1 + $loanAmount2 + $loanAmount3);
        } else if ($count == 2) {

            $loanAmount1 = $allLoans[0]["loan_amount"];
            $loanAmount2 = $allLoans[1]["loan_amount"];

            $interestRate1 = $allLoans[0]["interest_rate"];
            $interestRate2 = $allLoans[1]["interest_rate"];

            $loanDuration1 = $allLoans[0]["loan_duration"];
            $loanDuration2 = $allLoans[1]["loan_duration"];

            $disbursement->setValue('branch', $guarantorsArray[0]["branch"]);
            $disbursement->setValue('center', $guarantorsArray[0]["center"]);
            $disbursement->setValue('release_date', $loanArray["release_date"]);

            $disbursement->setValue('name_1', $guarantorsArray[0]["full_name"]);
            $disbursement->setValue('id_1', $guarantorsArray[0]["borrower_no"]);
            $disbursement->setValue('nic_1', $guarantorsArray[0]["nic"]);
            $disbursement->setValue('interest_rate_1', $interestRate1);
            $disbursement->setValue('loan_amount_1', $loanAmount1);
            $disbursement->setValue('loan_duration_1', $loanDuration1);
            $disbursement->setValue('group_no_1', $guarantorsArray[0]["group_no"]);
            $disbursement->setValue('name_2', $guarantorsArray[1]["full_name"]);
            $disbursement->setValue('id_2', $guarantorsArray[1]["borrower_no"]);
            $disbursement->setValue('nic_2', $guarantorsArray[1]["nic"]);
            $disbursement->setValue('interest_rate_2', $interestRate2);
            $disbursement->setValue('loan_amount_2', $loanAmount2);
            $disbursement->setValue('loan_duration_2', $loanDuration2);
            $disbursement->setValue('group_no_2', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_3', '');
            $disbursement->setValue('id_3', '');
            $disbursement->setValue('nic_3', '');
            $disbursement->setValue('interest_rate_3', '');
            $disbursement->setValue('loan_amount_3', '');
            $disbursement->setValue('loan_duration_3', '');
            $disbursement->setValue('group_no_3', '');

            $disbursement->setValue('name_4', '');
            $disbursement->setValue('id_4', '');
            $disbursement->setValue('nic_4', '');
            $disbursement->setValue('interest_rate_4', '');
            $disbursement->setValue('loan_amount_4', '');
            $disbursement->setValue('loan_duration_4', '');
            $disbursement->setValue('group_no_4', '');

            $disbursement->setValue('name_5', '');
            $disbursement->setValue('id_5', '');
            $disbursement->setValue('nic_5', '');
            $disbursement->setValue('interest_rate_5', '');
            $disbursement->setValue('loan_amount_5', '');
            $disbursement->setValue('loan_duration_5', '');
            $disbursement->setValue('group_no_5', '');
            $sum = ($loanAmount1 + $loanAmount2);
        } else if ($count == 1) {
            return redirect()->back()->with('error', 'ONLY ONE BORROWER ADDED');
        }

        $disbursement->setValue('total', $sum);
        $disbursement->setValue('center_no', $center_no);

        $disbursementName = $loan->borrower_no . " Disbursement";
        $disbursement->saveAs($disbursementName . '.docx');
        return response()->download($disbursementName . '.docx')->deleteFileAfterSend(true);
    }

    public function exportVoucher($voucher_id)
    {

        $voucherData = CompanyExpences::where('voucher_id', $voucher_id)->where('visibility',1)->get();
        $invoiceCount = count($voucherData);

        $voucher = new TemplateProcessor('word-template/Voucher.docx');

        $digit = new NumberFormatter("en", NumberFormatter::SPELLOUT);

        $voucher->setValue('branch', $voucherData[0]['branch']);
        $voucher->setValue('vocher_no', $voucherData[0]['voucher_id']);
        $voucher->setValue('date', $voucherData[0]['date']);
        $voucher->setValue('payee', $voucherData[0]['payee']);
        $voucher->setValue('remarks', $voucherData[0]['remarks']);
        $voucher->setValue('cheque_no', '');
        $voucher->setValue('cheque_date', '');
        $voucher->setValue('bank', '');

        if ($invoiceCount == 5) {
            $voucher->setValue('amount_1', $voucherData[0]['item_total']);
            $voucher->setValue('description_1', $voucherData[0]['description']);
            $voucher->setValue('ledger_account_1', $voucherData[0]['ledger_account']);
            $voucher->setValue('qnty_1', $voucherData[0]['quantity']);
            $voucher->setValue('qnty_2', $voucherData[1]['quantity']);
            $voucher->setValue('qnty_3', $voucherData[2]['quantity']);
            $voucher->setValue('qnty_4', $voucherData[3]['quantity']);
            $voucher->setValue('qnty_5', $voucherData[4]['quantity']);

            $voucher->setValue('amount_2', $voucherData[1]['item_total']);
            $voucher->setValue('description_2', $voucherData[1]['description']);
            $voucher->setValue('ledger_account_2', $voucherData[1]['ledger_account']);




            $voucher->setValue('amount_3', $voucherData[2]['item_total']);
            $voucher->setValue('description_3', $voucherData[2]['description']);
            $voucher->setValue('ledger_account_3', $voucherData[2]['ledger_account']);

            $voucher->setValue('amount_4', $voucherData[3]['item_total']);
            $voucher->setValue('description_4', $voucherData[3]['description']);
            $voucher->setValue('ledger_account_4', $voucherData[3]['ledger_account']);

            $voucher->setValue('amount_5', $voucherData[4]['item_total']);
            $voucher->setValue('description_5', $voucherData[4]['description']);
            $voucher->setValue('ledger_account_5', $voucherData[4]['ledger_account']);
            $total = ($voucherData[0]['amount'] + $voucherData[1]['amount'] + $voucherData[2]['amount'] + $voucherData[3]['amount'] + $voucherData[4]['amount']);

        } else if ($invoiceCount == 4) {
            $voucher->setValue('amount_1', $voucherData[0]['item_total']);
            $voucher->setValue('description_1', $voucherData[0]['description']);
            $voucher->setValue('ledger_account_1', $voucherData[0]['ledger_account']);

            $voucher->setValue('amount_2', $voucherData[1]['item_total']);
            $voucher->setValue('description_2', $voucherData[1]['description']);
            $voucher->setValue('ledger_account_2', $voucherData[1]['ledger_account']);

            $voucher->setValue('amount_3', $voucherData[2]['item_total']);
            $voucher->setValue('description_3', $voucherData[2]['description']);
            $voucher->setValue('ledger_account_3', $voucherData[2]['ledger_account']);

            $voucher->setValue('amount_4', $voucherData[3]['item_total']);
            $voucher->setValue('description_4', $voucherData[3]['description']);
            $voucher->setValue('ledger_account_4', $voucherData[3]['ledger_account']);

            $voucher->setValue('qnty_1', $voucherData[0]['quantity']);
            $voucher->setValue('qnty_2', $voucherData[1]['quantity']);
            $voucher->setValue('qnty_3', $voucherData[2]['quantity']);
            $voucher->setValue('qnty_4', $voucherData[3]['quantity']);

            $voucher->setValue('amount_5', '');
            $voucher->setValue('description_5', '');
            $voucher->setValue('ledger_account_5', '');
            $total = ($voucherData[0]['item_total'] + $voucherData[1]['item_total'] + $voucherData[2]['item_total'] + $voucherData[3]['item_total']);

        } else if ($invoiceCount == 3) {
            $voucher->setValue('amount_1', $voucherData[0]['item_total']);
            $voucher->setValue('description_1', $voucherData[0]['description']);
            $voucher->setValue('ledger_account_1', $voucherData[0]['ledger_account']);

            $voucher->setValue('qnty_1', $voucherData[0]['quantity']);
            $voucher->setValue('qnty_2', $voucherData[1]['quantity']);
            $voucher->setValue('qnty_3', $voucherData[2]['quantity']);

            $voucher->setValue('amount_2', $voucherData[1]['item_total']);
            $voucher->setValue('description_2', $voucherData[1]['description']);
            $voucher->setValue('ledger_account_2', $voucherData[1]['ledger_account']);

            $voucher->setValue('amount_3', $voucherData[2]['item_total']);
            $voucher->setValue('description_3', $voucherData[2]['description']);
            $voucher->setValue('ledger_account_3', $voucherData[2]['ledger_account']);

            $voucher->setValue('amount_4', '');
            $voucher->setValue('description_4', '');
            $voucher->setValue('ledger_account_4', '');

            $voucher->setValue('amount_5', '');
            $voucher->setValue('description_5', '');
            $voucher->setValue('ledger_account_5', '');
            $total = ($voucherData[0]['item_total'] + $voucherData[1]['item_total'] + $voucherData[2]['amoitem_totalunt']);

        } else if ($invoiceCount == 2) {
            $voucher->setValue('amount_1', $voucherData[0]['item_total']);
            $voucher->setValue('description_1', $voucherData[0]['description']);
            $voucher->setValue('ledger_account_1', $voucherData[0]['ledger_account']);

            $voucher->setValue('qnty_1', $voucherData[0]['quantity']);
            $voucher->setValue('qnty_2', $voucherData[1]['quantity']);


            $voucher->setValue('amount_2', $voucherData[1]['item_total']);
            $voucher->setValue('description_2', $voucherData[1]['description']);
            $voucher->setValue('ledger_account_2', $voucherData[1]['ledger_account']);

            $voucher->setValue('amount_3', '');
            $voucher->setValue('description_3', '');
            $voucher->setValue('ledger_account_3', '');

            $voucher->setValue('amount_4', '');
            $voucher->setValue('description_4', '');
            $voucher->setValue('ledger_account_4', '');

            $voucher->setValue('amount_5', '');
            $voucher->setValue('description_5', '');
            $voucher->setValue('ledger_account_5', '');
            $total = ($voucherData[0]['amount'] + $voucherData[1]['item_total']);

        } else if ($invoiceCount == 1) {
            $voucher->setValue('amount_1', $voucherData[0]['item_total']);
            $voucher->setValue('description_1', $voucherData[0]['description']);
            $voucher->setValue('ledger_account_1', $voucherData[0]['ledger_account']);

            $voucher->setValue('qnty_1', $voucherData[0]['quantity']);


            $voucher->setValue('amount_2', '-');
            $voucher->setValue('description_2', '-');
            $voucher->setValue('ledger_account_2', '-');

            $voucher->setValue('amount_3', '-');
            $voucher->setValue('description_3', '-');
            $voucher->setValue('ledger_account_3', '-');

            $voucher->setValue('amount_4', '-');
            $voucher->setValue('description_4', '-');
            $voucher->setValue('ledger_account_4', '-');

            $voucher->setValue('amount_5', '-');
            $voucher->setValue('description_5', '-');
            $voucher->setValue('ledger_account_5', '-');
            $total = ($voucherData[0]['amount']);

        }

        $amount_spell = $digit->format($total);
        $amount_spells = ucwords($amount_spell);
        $voucher->setValue('amount_spell', $amount_spells);

        $fileName = $voucher_id . " voucher";
        $voucher->saveAs($fileName . '.docx');
        return response()->download($fileName . '.docx')->deleteFileAfterSend(true);

    }

}
