<?php

namespace App\Http\Controllers;

use App\Investment;
use Illuminate\Http\Request;
use App\Investor;
use Illuminate\Support\Facades\Auth;

class InvestmentController extends Controller
{

    public function index()
    {
        $investments=Investment::leftjoin('investors','investors.id','investments.invested_by')
        ->get();
        return view('investments.index',compact('investments'));
    }


    public function create()
    {
        $investores=Investor::all();
        return view('investments.create',compact('investores'));
    }


    public function store(Request $request)
    {
        $investment = $request->all();
        $investment['created_by']=Auth::user()->id;
        $investment['created_branch']=Auth::user()->branch;
        Investment::create($investment);
        return redirect()->route('investments.index')->with('success','Successfully Invested');
    }


    public function show(Investment $investment)
    {
        //
    }


    public function edit(Investment $investment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Investment  $investment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Investment $investment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Investment  $investment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Investment $investment)
    {
        //
    }
}
