<?php

namespace App\Http\Controllers;

use App\CompanyExpencesSummary;
use App\CompantExpences;
use App\CompanyExpences;
use Illuminate\Http\Request;
use DB;
class PendingCompanyExpencesController extends Controller
{
    public function index(){

        $pendings=CompanyExpencesSummary::leftjoin('new_company_expences','new_company_expences.voucher_id','new_company_expences_summery.voucher_id')
        ->where('new_company_expences_summery.status','=','PENDING')
        ->select(DB::raw("sum(new_company_expences.	item_total) as total"),'new_company_expences.voucher_id','new_company_expences.date','new_company_expences.category'
        ,'new_company_expences.payee')
        ->groupBy('new_company_expences.voucher_id','new_company_expences.date','new_company_expences.category','new_company_expences.payee')
        ->orderBy('new_company_expences.date')
        ->get();


        return view('Pendingapprovals.CompanyExpences.index',compact( 'pendings'));
    }

    public function show($id){
            $invoice = CompanyExpencesSummary::where('voucher_id',$id)->first();

             $details = CompanyExpences::where('voucher_id',$invoice->voucher_id)->where('visibility',1)->get();
        return view('Pendingapprovals.CompanyExpences.show', compact('invoice','details'));
    }
    public function approve($id){
        $invoice = CompanyExpencesSummary::where('id',$id)->update(['status'=>'APPROVED']);

        return redirect()->route('verifycomex.index');
    }
}
