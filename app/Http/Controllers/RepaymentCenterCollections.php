<?php

namespace App\Http\Controllers;

use App\Center;
use App\Loan;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RepaymentCenterCollections extends Controller
{
    public function getGroup(Request $request)
    {

        // $groups = $
        // return response()->json($request);
        $centers = Center::where('branch_no',Auth::user()->branch )->get();
        try {
            $loans = Loan::where('branch', Auth::user()->branch)
                ->where('center', $request->center)
                ->where('group_no', $request->group)
                ->where('status', 'Payed')
                ->get();

            if(count($loans) > 0){
                return view('Repayments.repayment_index', compact('loans', 'centers'));
            } else {
                return view('Repayments.repayment_index', compact('centers', 'loans'))->with('error', 'There are no pending loans');
                // return 345;
            }


            // return $loans;

        } catch (Exception $e) {
            // $loans = null;
            // return view('Repayments.repayment_index', compact('centers'))->with('success', 'There are no pending loans');
            return 123;
        }

        // return 123;
    }
}
