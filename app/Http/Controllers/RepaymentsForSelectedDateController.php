<?php

namespace App\Http\Controllers;

use App\Loan;
use App\RepaymentsForSelectedDate;
use Illuminate\Http\Request;
use App\Repayment;
use Auth;
use Illuminate\Support\Carbon;



class RepaymentsForSelectedDateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('Repayment.repaymentf_for_selected_date');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RepaymentsForSelectedDate  $repaymentsForSelectedDate
     * @return \Illuminate\Http\Response
     */
    public function show(RepaymentsForSelectedDate $repaymentsForSelectedDate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RepaymentsForSelectedDate  $repaymentsForSelectedDate
     * @return \Illuminate\Http\Response
     */
    public function edit(RepaymentsForSelectedDate $repaymentsForSelectedDate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RepaymentsForSelectedDate  $repaymentsForSelectedDate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RepaymentsForSelectedDate $repaymentsForSelectedDate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RepaymentsForSelectedDate  $repaymentsForSelectedDate
     * @return \Illuminate\Http\Response
     */
    public function destroy(RepaymentsForSelectedDate $repaymentsForSelectedDate)
    {
        //
    }

    public function getRepaymentsDate(Request $request){
          $repayments = Repayment::where('date_index',$request->day)
          ->where('branch', Auth::user()->branch)
          ->where('status','PENDING')->get();


        return response()->json($repayments);
    }

    public function setRepaymentsDate(Request $request){

        $repayment = Repayment::find($request->id);
        // ->update(['status'=>'COLLECTED']);
        $repayment->status = 'COLLECTED';
        $repayment->payed_date_index = Carbon::now()->isoFormat('DDD');
        $repayment->payed_amount = $request->today_payment;
        $repayment->save();

        $next_repayment = Repayment::find($request->id+1);
        $next_repayment->due_amount = $repayment->due_amount - $repayment->payed_amount;
        $next_repayment->save();

        if($next_repayment->due_amount <= 0){
            $repayment->status = 'SETTLED';
            $repayment->save();

            $unpayed = Repayment::where('loan_id', $repayment->loan_id)->where('status', 'PENDING')->get();
            foreach($unpayed as $u){
                $u->status = 'COLLECTED';
                $u->payed_date_index = $repayment->payed_date_index;
                $u->save();
            }
        }
        // return response()->json($repayment);

        $loan_id = $repayment->loan_id;
        $loan = Loan::find($loan_id);
        $branch = $loan->branch;
        $center = $loan->center;

        // return redirect('/home');
        return redirect('/repayments/'.$branch.'/'.$center);
    }
}
