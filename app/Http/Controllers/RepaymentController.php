<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Center;
use App\Loan;
use App\Repayment;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class RepaymentController extends Controller
{

    public function index()
    {
        // // $repayments = Repayment::all();
        // $date_index=Carbon::now()->isoFormat('DDD');
        // // return $date_index;
        // $repayments = Repayment::where('date_index',$date_index)
        // ->where('branch', Auth::user()->branch)
        // ->where('status','PENDING')->get();
        // // return $date_index;
        // return view('Repayments.repayment_index',compact('repayments'))
        //     ->with('i',(request()->input('page',1)-1)*5);
        $centers = Center::where('branch_no', Auth::user()->branch)->get();
        try {
            $loans = Loan::where('branch', Auth::user()->branch)
                ->where('status', 'Payed')->get();

            return view('Repayments.repayment_index', compact('loans', 'centers'));
        } catch (Exception $e) {
            return view('Repayments.repayment_index', compact('centers'))->with('success', 'There are no pending loans');
        }
    }


    public function collected(Request $request)
    {
        // return response()->json($id);

        $loan = Loan::find($request->id);
        $loan->payments_count = ++$loan->payments_count;
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_payment_month = Carbon::now()->isoFormat('M');

        // $due_amount = $loan->loan_amount - $loan->paid;
        $prev_total_payed = $loan->paid;

        $repayment = new Repayment;
        $repayment->logged_user = Auth::user()->name;
        $repayment->borrower_nic = $loan->nic;
        $repayment->branch = $loan->branch;
        $repayment->center = $loan->center;
        $repayment->loan_id = $loan->id;
        $repayment->loan_amount = $loan->loan_amount;
        $repayment->due_amount = $loan->due - $request->paying_amount;
        $repayment->total_payed = $prev_total_payed + $request->paying_amount;
        $repayment->installment = $loan->instalment;
        $repayment->payment_date_index = Carbon::now()->isoFormat('DDD');
        $repayment->payment_date = Carbon::now()->isoFormat('dddd');
        $repayment->payment_date_full = Carbon::now()->isoFormat('M/D/YYYY');
        $repayment->installment_no = $loan->payments_count;
        $repayment->status = 'COLLECTED'; #warike gatta
        $repayment->type = 'CASH';
        $repayment->method_status = 'CENTER COLLECTION';

        $loan->due = $repayment->due_amount;
        $loan->paid = $repayment->total_payed;

        if ($loan->due <= 0) {
            $loan->status = 'SETTLED';
            $repayment->status = 'SETTLED';
        }

        $repayment->save();
        $loan->save();

        return redirect()->route('repayment.index');
    }

    public function doc()
    {
        $centers = Center::where('branch_no', Auth::user()->branch)->get();

        return view('Repayments.repayment_docs', compact('centers'));
    }

    public function officeCollection()
    {
        $loans = Loan::where('branch', Auth::user()->branch)
            ->where('status', 'Payed')->get();

        return view('Repayments.office_collection', compact('loans'));
    }

    public function officeCollect(Request $request){
        $loan = Loan::find($request->id);
        $loan->payments_count = ++$loan->payments_count;
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_payment_month = Carbon::now()->isoFormat('m');

        $prev_total_payed = $loan->paid;

        $repayment = new Repayment;
        $repayment->logged_user = Auth::user()->name;
        $repayment->borrower_nic = $loan->nic;
        $repayment->branch = $loan->branch;
        $repayment->center = $loan->center;
        $repayment->loan_id = $loan->id;
        $repayment->loan_amount = $loan->loan_amount;
        $repayment->due_amount = $loan->due - $request->paying_amount;
        $repayment->total_payed = $prev_total_payed + $request->paying_amount;
        $repayment->installment = $loan->instalment;
        $repayment->payment_date_index = Carbon::now()->isoFormat('DDD');
        $repayment->payment_date = Carbon::now()->isoFormat('dddd');
        $repayment->payment_date_full = Carbon::now()->isoFormat('M/D/YYYY');
        $repayment->installment_no = $loan->payments_count;
        $repayment->status = 'COLLECTED'; #warike gatta
        $repayment->type = 'CASH';
        $repayment->method_status = 'OFFICE COLLECTION';

        $loan->due = $repayment->due_amount;
        $loan->paid = $repayment->total_payed;

        if ($loan->due <= 0) {
            $loan->status = 'SETTLED';
            $repayment->status = 'SETTLED';
        }

        $repayment->save();
        $loan->save();

        return redirect('/repayments/officecollection');
    }

    public function bulkCollection(){
        $loans = Loan::where('branch', Auth::user()->branch)
            ->where('status', 'Payed')->get();

    $centers = Center::where('branch_no', Auth::user()->branch)->get();

        return view('Repayments.bulk_collection', compact('loans', 'centers'));
    }

    public function bulkCollect(Request $request){

        // return $request;

        $loan = Loan::find($request->id);
        $loan->payments_count = ++$loan->payments_count;
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_payment_month = Carbon::now()->isoFormat('m');

        $prev_total_payed = $loan->paid;

        $repayment = new Repayment;
        $repayment->logged_user = Auth::user()->name;
        $repayment->borrower_nic = $loan->nic;
        $repayment->branch = $loan->branch;
        $repayment->center = $loan->center;
        $repayment->loan_id = $loan->id;
        $repayment->loan_amount = $loan->loan_amount;
        $repayment->due_amount = $loan->due - $request->paying_amount;
        $repayment->total_payed = $prev_total_payed + $request->paying_amount;
        $repayment->installment = $loan->instalment;
        $repayment->payment_date_index = $request->payment_date_index;
        $repayment->payment_date = $request->payment_date;
        $repayment->payment_date_full = Carbon::parse($request->date)->isoFormat('M/D/YYYY');
        $repayment->installment_no = $loan->payments_count;
        $repayment->status = 'COLLECTED'; #warike gatta
        $repayment->type = 'CASH';
        $repayment->method_status = 'OFFICE COLLECTION';

        $loan->due = $repayment->due_amount;
        $loan->paid = $repayment->total_payed;

        if ($loan->due <= 0) {
            $loan->status = 'SETTLED';
            $repayment->status = 'SETTLED';
        }

        $repayment->save();
        $loan->save();

        return redirect('/repayments/bulkcollection');
    }

    public function getPayed(Request $request){
        return response()->json($request);
    }

    public function edit(){
        $centers = Center::where('branch_no', Auth::user()->branch)->get();
        $repayments = [];
        // $repayments_count = 0;
        // $total_center_payments = 0 ;
        // $total_group_payments = 0 ;
        $total_payments = 0;
        return view('Repayments.edit_repayments', compact('centers', 'repayments', 'total_payments'));
    }

    public function getByDate(Request $request){

        #DON'T DELETE - truncated get by date original function

        $repayments = Repayment::where('branch', Auth::user()->branch)
        ->where('center', $request->center)
        ->where('payment_date_index',Carbon::parse($request->date)->isoFormat('DDD'))
        ->get();

        $total_payments = 0 ;
        foreach($repayments as $r){
            $total_payments += $r->paid_amount;
        }


        $centers = Center::where('branch_no', Auth::user()->branch)->get();
        // $repayments = Repayment::where('branch', Auth::user()->branch)
        // ->where('center', $request->center)
        // ->where('group_no',$request->group)
        // ->get();
        // $repayments_count = count($repayments);

        // $centers_for_cal_total_payments = Repayment::where('branch', Auth::user()->branch)
        // ->where('center', $request->center)
        // ->get();
        // $total_center_payments = 0;
        // foreach($centers_for_cal_total_payments as $c){
        //     $total_center_payments += $c->paid_amount;
        // }

        // $total_group_payments = 0;
        // foreach($repayments as $r){
        //     $total_group_payments += $r->paid_amount;
        // }

        return view('Repayments.edit_repayments', compact('centers', 'repayments', 'total_payments'));
    }

    public function editSingleRepayment($id){
        $repayment = Repayment::find($id);

        return view('Repayments.edit_single_repayment', compact('repayment'));
    }
    public function updateSingleRepayment(Request $request, $id){
        $repayment = Repayment::find($id);
        $repayment->paid_amount = $request->paid_amount;
        $repayment->payment_date_index = Carbon::parse($request->date)->isoFormat('DDD');
        $repayment->payment_date = Carbon::parse($request->date)->isoFormat('dddd');
        $repayment->payment_date_full = $request->date;
        $repayment->save();

        return redirect('/repayments/edit');
    }
}
