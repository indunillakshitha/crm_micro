<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Loan;
use App\Appraisal;
use App\Borrower;
use App\Approving;
use App\Center;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ApprovedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $approved = 'Approved';

        $borrowers = Borrower::all();
        $apprasails = Appraisal::all();
        $centers = Center::where('branch_no', Auth::user()->branch)->get();
        $loans = Loan::where('status',$approved)
        ->orderBy('created_at', 'desc')
        ->where('loans.branch',Auth::user()->branch)
        ->paginate(1000);
        return view('Approved.indexApproved',compact('loans', 'centers'))
            ->with('i',(request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Loan $id)
    {
        // return view('Approved.paperCharge',compact('loan'));
        // $data = $id;
        // return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loan $loan)
    {
        $request->validate([
            'fee'  => 'required',

        ]);

        $loan->update($request->all());

        return redirect()->route('approved.index')
            ->with('success', 'Charges upadted successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function issue($id)
    {
        $loan = Loan::find($id);
        $loan->issued_date = Carbon::now()->isoFormat('dddd');
        $loan->issued_date_index = Carbon::now()->isoFormat('DDD');
        $loan->save();

        $borrower= Loan::where('id',$id)->where('status','Approved')->first();


        // ->first()
        // ->update('status','issued');
        $borrower['status']='Issued';
        $borrower->save();
        return redirect()->route('approved.index');
    }

    public function pprCharg(Request $request, $id)
    {

         $request->validate([
            'fees'  => 'required',

        ]);

        $edit= Loan::where('id',$id)->first();
        $edit->update($request->all());


        // $charg=$request;

        // $edit['fees']=$charg;
        // $edit->save();
        return redirect()->route('approved.index');
        // return response()->json($edit);
    }

        public function pprChrgView( $id)
    {
        $loan= Loan::where('id',$id)->first();

        return view('Approved.paperCharge',compact('loan'));
        //  $data = $loan;
        // return response()->json($data);
    }
}
