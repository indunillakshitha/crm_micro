<?php

namespace App\Http\Controllers;

use App\Designation;
use App\Investor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvestorController extends Controller
{

    public function index()
    {
        $investors=Investor::all();
        return view('investors.index',compact('investors'));
    }


    public function create()
    {
         $designnations =Designation::all();
        return view('investors.create',compact('designnations'));
    }


    public function store(Request $request)
    {
        $investor = $request->all();
        $investor['created_by']=Auth::user()->id;
        $investor['created_branch']=Auth::user()->branch;
        Investor::create($investor);
        return redirect()->route('investors.index')->with('success','Successfully Added');
    }


    public function show(Investor $investor)
    {
        //
    }


    public function edit(Investor $investor)
    {
        //
    }


    public function update(Request $request, Investor $investor)
    {
        //
    }


    public function destroy(Investor $investor)
    {
        //
    }
}
