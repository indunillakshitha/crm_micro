<?php

namespace App\Http\Controllers\Center;

use App\Borrower;
use App\Branch;
use App\Center;
use App\CenterLeader;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;


class CenterLeadersController extends Controller
{
    public function index()
    {


        $branches = Branch::all();
        $centers = Center::all();
         $leader=DB::table('center_leaders')
        ->leftjoin('borrowers','borrowers.borrower_no','=','center_leaders.borrower_id')
        ->select('borrowers.*')
	->where('center_leaders.branch',Auth::user()->branch)
        ->get();
         $leaders=json_decode($leader,true);
        $borrowers = Borrower::latest()->paginate(100);
        return view('Center.centerLeadersIndex',compact( 'branches','centers','leaders'));

//        return view('Center.centerLeadersIndex');
    }
    public function store(Request $request){

        $borrower=Borrower::where('borrower_no',$request->borrower_no)->first();
	if(CenterLeader::where('center',$borrower->center)->first()){
$center_leader=CenterLeader::where('center',$borrower->center)->first();
$center_leader->borrower_id=$borrower->borrower_no;
        $center_leader->branch=$borrower->branch;
        $center_leader->center=$borrower->center;
        $center_leader->save();
        // CenterLeader::create($center_leader->all());
        Borrower::where('borrower_no',$request->borrower_no)->update(['remarks'=>'Center Leader']);
 return redirect()->route('centerleader.index')
            ->with('success','Center Leader Updated Successfully');
}else{
        $center_leader=new CenterLeader();
        $center_leader->borrower_id=$borrower->borrower_no;
        $center_leader->branch=$borrower->branch;
        $center_leader->center=$borrower->center;
        $center_leader->save();
        // CenterLeader::create($center_leader->all());
        Borrower::where('borrower_no',$request->borrower_no)->update(['remarks'=>'Center Leader']);

        return redirect()->route('centerleader.index')
            ->with('success','Leader added successfully.');
}
    }
    public function destroy($id){
        Borrower::where('id',$id)->update(['remarks'=>'NONE']);


        return redirect()->route('centerleader.index')
            ->with('success','Leader added successfully.');

    }
    public function show(Request $request){
        return redirect()->route('centerleader.index')
            ->with('abort','This Option Still not available');

    }
    public function edit($id){
        // return redirect()->route('centerleader.index')
        //     ->with('abort','This Option Still not available');
        $data = $id;
        $leader = CenterLeader::where('id',$data)->first();
        return view('Center.editCenterLeader',compact('leader'));
        // return response()->json($leader);
    }

    public function update($id, CenterLeader $leader)
    {


        $leader->update($request->all());

        return redirect()->route('centerleader.index')
                        ->with('success','Center Leader updated successfully');
    }


}
