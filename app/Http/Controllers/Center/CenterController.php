<?php

namespace App\Http\Controllers\Center;

use App\Center;
use App\Branch;
use App\Http\Controllers\Controller;
use App\PostalCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CenterController extends Controller
{
    // public function __construct(){
    //     $super_user = $this->middleware('access_level_1');
    // }
    // const $super_user = $this->middleware('super_user');

    public function index()
    {

        // if(Auth::user()->access_level == 'level_10' || Auth::user()->access_level == 'level_1'){

            $centers = Center::latest()->paginate(10);
            return view('Center.indexCenter', compact('centers'))
                ->with('i', (request()->input('page', 1) - 1) * 5);
        // } else{
        //     return redirect('/home');
        // }

    }
    public function addcenter()
    {
        return view('admin.addCenter');
    }

    public function create()
    {
        $centerCount = count(Center::where('branch_no',Auth::user()->branch)->get());
        if ($centerCount) {
            $centerCount++;
        } else {
            $centerCount = 1;
        }

        $branches = Branch::all();
        $postals = PostalCode::orderBy('city')->get();

        return view('Center.addCenter', compact('centerCount', 'branches', 'postals'));
        // ->with('centerCount', $centerCount);
    }

    public function store(Request $request)
    {
        $request->validate([

            'center_name'  => 'required ',
            'center_no'  => 'required',
            'branch_no'  => 'required',
            'center_address'  => 'required',
            'center_province'  => 'required',
            'center_city'  => 'required',
            'center_postal'  => 'numeric',
            'center_mobile'  => ' numeric ',
            // 'center_land'  => 'required | numeric' ,
            // 'center_email'  => 'required | email',

        ]);

        Center::create($request->all());

        return redirect()->route('center.index')
            ->with('success', 'Center created successfully.');
    }

    public function show(Center $center)
    {
        return view('Center.showCenter', compact('center'));
    }


    public function edit(Center $center)
    {
        return view('Center.editCenter', compact('center'));
    }

    public function update(Request $request, Center $center)
    {
        $request->validate([
            'center_name'  => 'required',
            'center_no'  => 'required',
            'branch_no'  => 'required',
            'center_address'  => 'required',
            'center_province'  => 'required',
            'center_city'  => 'required',
            'center_postal'  => 'required',
            'center_mobile'  => 'required',
            'center_land'  => 'required',
            'day' =>  'required'
        ]);

        $center->update($request->all());

        return redirect()->route('center.index')
            ->with('success', 'Center updated successfully');
    }


    public function destroy(Center $center)
    {
        $center->delete();

        return redirect()->route('center.index')
            ->with('success', 'Center deleted successfully');
    }
}
