<?php

namespace App\Http\Controllers;

use App\Repayment;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
class ArrearsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date_index=Carbon::now()->isoFormat('DDD')+1;
        $repayments = Repayment::where('date_index',$date_index)
        ->where('status','PENDING')->paginate(10);
        return view('Arrears.indexArrears',compact('repayments'))
            ->with('i',(request()->input('page',1)-1)*5);
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

  
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
