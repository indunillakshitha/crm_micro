<?php

namespace App\Http\Controllers\Witness;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Witness;
use App\CenterLeader;

class WitnessController extends Controller
{

    public function index()
    {
        $witnesses = Witness::orderBy('id')->paginate(5);
        return view('Witness.indexWitness',compact('witnesses'))
                ->with('i',(request()->input('page',1)-1)*5);
    }


    public function destroy(Witness $witness)
    {
        $witness->delete();

        return redirect()->route('witness.index')
                        ->with('success','Witness deleted successfully');    }


    public function store(Request $request)
    {
        $request->validate([

            'witness_name' => 'required ',
            'witness_nic' => 'required',
            'witness_address' => 'required',
            'witness_mobile' => 'required | numeric',
            'witness_land' => 'required |numeric ',


        ]);

        Witness::create($request->all());

        return redirect()->route('witness.index')
                        ->with('success','Witness created successfully.');

    }



    public function show(Witness $witness)
    {
        return view('Witness.showWitness',compact('witness'));
    }



    public function edit(Witness $witness)
    {
        return view('Witness.editWitness',compact('witness'));
    }



    public function update(Request $request, Witness $witness)
    {
        $request->validate([
            'witness_name' => 'required ',
            'witness_nic' => 'required',
            'witness_address' => 'required',
            'witness_mobile' => 'required | numeric',
            'witness_land' => 'required |numeric ',

        ]);

        $witness->update($request->all());

        return redirect()->route('witness.index')
                        ->with('success','Witness updated successfully');
    }



    public function create()
    {
        $witnessCount = count(Witness::all());
        if($witnessCount){
            $witnessCount++;
        } else{
            $witnessCount = 1;
        }
        return view('Witness.addWitness')->with('witnessCount', $witnessCount);
    }

    public function addWitness(Request $request){

        $witness=CenterLeader::where('name',$request->witness_name)->first();

        $newWitness = new Witness;
        $newWitness->borrower_no = $request->borrower_no;
        $newWitness->witness_name = $witness->name ;
        $newWitness->witness_nic = $witness->nic ;
        $newWitness->witness_address = $witness->address ;
        $newWitness->witness_mobile = $witness->mobile ;
        $newWitness->witness_type = $witness->type ;
        $newWitness->save();
        return response()->json($newWitness);
        // return response()->json($witness->name);
    }


}
