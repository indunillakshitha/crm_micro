<?php

namespace App\Http\Controllers;

use App\Appraisal_expenses_product;
use Illuminate\Http\Request;

class AppraisalExpensesProductsController extends Controller
{
    public function index(){

        $products = Appraisal_expenses_product::all();
        return view('Appraisal/ExpensesProducts', compact('products'));
    }

    public function store(Request $request){

        Appraisal_expenses_product::create($request->all());
        return redirect('/appraisalproducts');
    }


}
