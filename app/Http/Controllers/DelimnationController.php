<?php

namespace App\Http\Controllers;

use App\DeliminationReason;
use Illuminate\Support\Facades\Auth;

use App\Delimnation;
use App\DenominationCenterTotal;
use App\DenominationDepositeVariance;
use App\User;
use Illuminate\Http\Request;
use App\Repayment;
use App\Shedule;
use App\SlipDetail;
use Carbon\Carbon;
use CreateDenominationCenterTotalsTable;
use DB;
use Illuminate\Support\Facades\DB as FacadesDB;

class DelimnationController extends Controller
{
    public function index()
    {
        $users = User::where('branch', Auth::user()->branch)
            // ->where('access_level','agent')
            ->get();
        return view('delimnation.index', (['users' => $users]));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {

        // return $request;
        $center_total = new DenominationCenterTotal;
        $center_total->branch = Auth::user()->branch;
        $center_total->agent_id = Auth::user()->id;
        $center_total->total_cash_collected = $request->total_to_be;
        $center_total->total_cash_available = $request->total_got;
        $center_total->varians_avail_n_collected = $request->total_to_be - $request->total_got;
        $center_total->comment_for_vNc = $request->remarks;
        $center_total->date = $request->date;
        $center_total->user = $request->user;
        $center_total->date_index =  $index = Carbon::parse($request->date)->isoFormat('DDD');;
        $center_total->save();

        $data = $request;
        $data['created_by'] = Auth::user()->name;
        $del = Delimnation::create($data->all());
        // DB::table('repayments')->where('logged_user',$request->agent)->where('deliminated',0)->update(['deliminated'=>1]);
        $del->agent = Auth::user()->id;
        $del->denomination_center_totals_id = $center_total->id;
        $del->save();

        // -------------------------------------- DELIMINATION REASONS------------------------------
        $del_reason = DeliminationReason::create($request->all());
        $del_reason->delimination_id = $del->id;
        $del_reason->save();


        //-------------------------------------------------------------change repayment status

        $index = Carbon::parse($request->date)->isoFormat('DDD');

        // return $first_date_index = Repayment::where('deliminated',0)
        // ->where('year','2020')
        // ->orderBy('payment_date_index')
        // ->first()->payment_date;
           $first_date_index = Shedule::leftjoin('repayments', 'repayments.center', 'shedules.center')
        // ->where('shedules.excecutive_id', $request->user)
        ->where('repayments.deliminated', '0')
        ->where('repayments.year','2020')
        ->orderBy('payment_date_index')
        ->first()->payment_date_index;

        //  $repayments = Repayment::leftjoin('shedules', 'repayments.center', 'shedules.center')
        // ->where('shedules.excecutive_id', $request->user)
        // ->where('repayments.deliminated', '0')
        // ->where('repayments.payment_date_index', $first_date_index)
        // ->where('repayments.year','2020')
        // ->select('repayments.*')
        // // ->orderBy('repayments.centers')
        // ->get();
        $repayments = Repayment::where('branch', Auth::user()->branch)
        ->leftjoin('centers', 'centers.center_name', 'repayments.center')
        ->where('repayments.deliminated', '0')
        ->where('repayments.payment_date_index', $first_date_index)
        // ->where('repayments.logged_user', $request->user)
        ->where('repayments.year','2020')
        ->select('repayments.*')
        ->orderBy('centers.id')
        ->get();
            foreach($repayments as $rep){
                $rep->denominate_id=$center_total->id;
                $rep->deliminated='1';
                $rep->save();
            }
            // ->update(['denominate_id' => $center_total->id, 'deliminated' => 1]);

        //-------------------------------------------------------------change repayment status end

        return response()->json(['Delimination Added', $del]);

        // return view('delimnation.print' );
        // return view('welcome');
    }


    public function show(Delimnation $delimnation)
    {
        //
    }

    public function edit(Delimnation $delimnation)
    {
        //
    }


    public function update(Request $request, Delimnation $delimnation)
    {
        //
    }


    public function destroy(Delimnation $delimnation)
    {
        //
    }

    public function getTotal(Request $request)
    {
        // return $request;
        $index = Carbon::parse($request->date)->isoFormat('DDD');

         $first_date_index = Shedule::leftjoin('repayments', 'repayments.center', 'shedules.center')
        // ->where('shedules.excecutive_id', $request->user)
        ->where('repayments.deliminated', '0')
        ->where('repayments.year','2020')
        ->orderBy('payment_date_index')
        ->first()->payment_date_index;

        $repayments = Repayment::where('branch', Auth::user()->branch)
            ->leftjoin('centers', 'centers.center_name', 'repayments.center')
            ->where('repayments.deliminated', '0')
            ->where('repayments.payment_date_index', $first_date_index)
            // ->where('repayments.logged_user', $request->user)
            ->where('repayments.year','2020')
            ->select(
                DB::raw("sum(paid_amount) as total"),
                DB::raw("sum(doc_charges) as total_doc"),
                DB::raw("sum(arrears) as total_arrears"),
                DB::raw("sum(not_paid_collection) as total_not_paid"),
                'center',
                'centers.center_no','repayments.payment_date'
            )
            ->groupBy('center', 'centers.center_no','repayments.payment_date')
            ->orderBy('centers.id')
            ->get();

//use this to get executive wise-----------------------------------------------------------------
        // $centers = DB::table('shedules')
        // ->leftjoin('repayments', 'repayments.center', 'shedules.center')
        // ->where('shedules.excecutive_id', $request->user)
        // ->where('repayments.deliminated', 0)
        // ->where('repayments.payment_date_index', $first_date_index)
        //     ->where('repayments.year','2020')
        //     ->select(
        //         DB::raw("sum(repayments.paid_amount) as total"),
        //         DB::raw("sum(repayments.doc_charges) as total_doc"),
        //         DB::raw("sum(repayments.arrears) as total_arrears"),
        //         DB::raw("sum(repayments.not_paid_collection) as total_not_paid"),
        //         'repayments.center',
        //         'repayments.payment_date'
        //     )
        //     ->groupBy('repayments.center','repayments.payment_date')
        //     ->get();

        // $repayments_arr = [];
        // foreach($centers as $center){
        //     array_push($repayments_arr, Repayment::where('center', $center));
        // }

        // return $repayments_arr;
//use this to get executive wise-----------------------------------------------------------------

        return response()->json($repayments);

    }

    // return response()->json($first_date_index);

    public function saveDelimnation(Request $request)
    {

        $data = $request;
        $data['crated_by'] = Auth::user()->name;
    }

    public function print()
    {

        $pending = DB::table('denomination_center_totals')->where('total_cash_deposited', '>', 0)->get();
        return view('delimnation.printindex', compact('pending'));
    }

    public function deposites()
    {
        $pending = DB::table('denomination_center_totals')->where('total_cash_deposited', 0)->get();
        return view('delimnation.deposites_index', compact('pending'));
    }
    public function printd($id)
    {
         $denomination_main = DB::table('denomination_center_totals')->where('id', $id)->get();
        $notes = DB::table('delimnation')->where('denomination_center_totals_id', $id)->get();
        $variance_reasons = DB::table('denomination_deposite_variances')->where('denomination_center_totals_id', $id)->get();

        $repayments = Repayment::where('branch', Auth::user()->branch)
            ->leftjoin('centers', 'centers.center_name', 'repayments.center')
            ->where('repayments.denominate_id', $id)
            ->select(
                DB::raw("sum(paid_amount) as total"),
                DB::raw("sum(doc_charges) as total_doc"),
                DB::raw("sum(arrears) as total_arrears"),
                DB::raw("sum(not_paid_collection) as total_not_paid"),
                'center',
                'centers.center_no'
            )
            ->groupBy('center', 'centers.center_no')
            ->orderBy('centers.id')
            ->get();
        return view('delimnation.print', compact('denomination_main', 'notes', 'variance_reasons', 'repayments'));
    }


    public function deposite($id)
    {
        $details = DB::table('denomination_center_totals')->where('id', $id)->get();
        return view('delimnation.deposite', compact('details', 'id'));
    }

    public function depositeAdd(Request $request)
    {
        // return response()->json($request);
        $cen_tot = DenominationCenterTotal::where('id', $request->id)->first();
        // return response()->json($cen_tot);

        $cen_tot->total_cash_deposited = $request->available;
        $cen_tot->varians_avail_n_deposited = $cen_tot->total_cash_available-$cen_tot->total_cash_deposited;
        $cen_tot->save();

        $depo_variance = DenominationDepositeVariance::create($request->all());
        $depo_variance->denomination_center_totals_id = $cen_tot->id;
        $depo_variance->save();
        return response()->json($request);

    }


    public function printDe($id){
          $dcenter_total=DB::table('denomination_center_totals')->where('id',$id)->get();
           $repayments = Repayment::where('branch', Auth::user()->branch)
        ->leftjoin('centers','centers.center_name','repayments.center')
        // ->where('repayments.deliminated','0')
        ->where('repayments.payment_date_index', $dcenter_total[0]->date_index)
        ->where('repayments.logged_user',$dcenter_total[0]->user)
        ->select(DB::raw("sum(paid_amount) as total"),
                 DB::raw("sum(doc_charges) as total_doc"),
                DB::raw("sum(arrears) as total_arrears"),
                DB::raw("sum(not_paid_collection) as total_not_paid"),
                'center','centers.center_no')
        ->groupBy('center','centers.center_no')
        ->orderBy('centers.id')
        ->get();

        // return $repayments;

             $cash_structure=DB::table('delimnation')->where('denomination_center_totals_id',$id)->get();
             $deposite=DB::table('denomination_deposite_variances')->where('denomination_center_totals_id',$id)->get();
        return view('delimnation.print',compact('repayments','dcenter_total','cash_structure','deposite'));
    }

    public function add_slip(Request $request){
        // return $request;
        SlipDetail::create($request->all());

        $total = 0;

        foreach(SlipDetail::where('denomination_id', $request->denomination_id)->get() as $slip){
            $total += $slip->slip_amount;
        }


        return response()->json($total);
    }
}
