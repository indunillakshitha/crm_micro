<?php

namespace App\Http\Controllers;

use App\Appraisal_prduct_cateory;
use App\Appraisal_expenses_product;
use Illuminate\Http\Request;

class addAppraisalProductCategoryController extends Controller
{
    public function store(Request $request){

        $request->validate([
            'category_name'=>'required'
        ]);

        // Appraisal_prduct_cateory::create($request->all());

        // return redirect()->route('appraisalcategory.index')
        //     ->with('success','Borrower created successfully.');
        Appraisal_expenses_product::create($request->all());

        return redirect('/home');

    }

    public function index(){
        $categories =Appraisal_prduct_cateory::all();

        return view('Appraisal.appraisalearningcategoryindex',compact('categories'))
            ->with('i',(request()->input('page',1)-1)*5);
    }
}
