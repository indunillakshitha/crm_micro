<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Center;
use App\Staff;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Image;
class StaffController extends Controller
{

    public function index()
    {


//       return view('Borrower.borrowerindex');
        $staffs = Staff::latest()->paginate(100);
        return view('Staff.staff_index',compact('staffs'))
            ->with('i',(request()->input('page',1)-1)*5);

    }


    public function create()
    {
        $branches = Branch::all();
        return view('Staff.staff_add', compact('branches'));
    }


    public function store(Request $request)
    {


        // Staff::create($request->all());
        // return view('Staff.staff_add');


        // $this->validate($request,[
        //     'img_nic' => 'image|nullable' //1.999mb
        // ]);

        // nic
        //Handle File Upload
        if($request->hasFile('img_nic')){
            //Get filename with extension
            $filenameWithExt = $request->file('img_nic')->getClientOriginalName();

            //Below makes sure many uploads with same filename from many users won't be a problem

            //Get just filename
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            //Get just ext
            $extension = $request->file('img_nic')->getClientOriginalExtension();
            //Filename to store
            $nicfilenameToStore = $filename.'_'.time().'.'.$extension; //The Timestamp makes each image's name unique
            //Upload Image
            $path = $request->file('img_nic')->storeAs('public/images',$nicfilenameToStore);

        }
        else{
            $nicfilenameToStore ='noimage.jpg';
        }

        // bday certificate
        if($request->hasFile('img_birth_certificate')){
            $filenameWithExt = $request->file('img_birth_certificate')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('img_birth_certificate')->getClientOriginalExtension();
            $bdayfilenameToStore = $filename.'_'.time().'.'.$extension; //The Timestamp makes each image's name unique
            $path = $request->file('img_birth_certificate')->storeAs('public/images',$bdayfilenameToStore);

        }
        else{
            $bdayfilenameToStore ='noimage.jpg';
        }

        // grame
        if($request->hasFile('img_gr_cer')){
            $filenameWithExt = $request->file('img_gr_cer')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('img_gr_cer')->getClientOriginalExtension();
            $gramefilenameToStore = $filename.'_'.time().'.'.$extension; //The Timestamp makes each image's name unique
            $path = $request->file('img_gr_cer')->storeAs('public/images',$gramefilenameToStore);

        }
        else{
            $gramefilenameToStore ='noimage.jpg';
        }

        // police
        if($request->hasFile('img_police_cer')){
            $filenameWithExt = $request->file('img_police_cer')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('img_police_cer')->getClientOriginalExtension();
            $policefilenameToStore = $filename.'_'.time().'.'.$extension; //The Timestamp makes each image's name unique
            $path = $request->file('img_police_cer')->storeAs('public/images',$policefilenameToStore);

        }
        else{
            $policefilenameToStore ='noimage.jpg';
        }

        // married
        if($request->hasFile('img_married_cer')){
            $filenameWithExt = $request->file('img_married_cer')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('img_married_cer')->getClientOriginalExtension();
            $marriedfilenameToStore = $filename.'_'.time().'.'.$extension; //The Timestamp makes each image's name unique
            $path = $request->file('img_married_cer')->storeAs('public/images',$marriedfilenameToStore);

        }
        else{
            $marriedfilenameToStore ='noimage.jpg';
        }

        // edu 1
        if($request->hasFile('img_edu_1_cer')){
            $filenameWithExt = $request->file('img_edu_1_cer')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('img_edu_1_cer')->getClientOriginalExtension();
            $edu1filenameToStore = $filename.'_'.time().'.'.$extension; //The Timestamp makes each image's name unique
            $path = $request->file('img_edu_1_cer')->storeAs('public/images',$edu1filenameToStore);

        }
        else{
            $edu1filenameToStore ='noimage.jpg';
        }

        // edu 2
        if($request->hasFile('img_edu_2_cer')){
            $filenameWithExt = $request->file('img_edu_2_cer')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('img_edu_2_cer')->getClientOriginalExtension();
            $edu2filenameToStore = $filename.'_'.time().'.'.$extension; //The Timestamp makes each image's name unique
            $path = $request->file('img_edu_2_cer')->storeAs('public/images',$edu2filenameToStore);

        }
        else{
            $edu2filenameToStore ='noimage.jpg';
        }

        // edu 3
        if($request->hasFile('img_edu_3_cer')){
            $filenameWithExt = $request->file('img_edu_3_cer')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('img_edu_3_cer')->getClientOriginalExtension();
            $edu3filenameToStore = $filename.'_'.time().'.'.$extension; //The Timestamp makes each image's name unique
            $path = $request->file('img_edu_3_cer')->storeAs('public/images',$edu3filenameToStore);

        }
        else{
            $edu3filenameToStore ='noimage.jpg';
        }

         $staff = new Staff;
        //  $post->title =  $request->input('title');
         $staff->branch = $request->branch;
        //  $staff->center = $request->center;
         $staff->name = $request->name;
         $staff->designation = $request->designation;
         $staff->nic = $request->nic;
         $staff->address = $request->address;
         $staff->mobile = $request->mobile;
         $staff->mail = $request->mail;
         $staff->relational_contact = $request->relational_contact;
         $staff->applying_name = $request->applying_name;
         $staff->edu_qual = $request->edu_qual;
         $staff->pro_qual = $request->pro_qual;
         $staff->pro_exp = $request->pro_exp;
         $staff->civil_status = $request->civil_status;
         $staff->img_nic = $nicfilenameToStore;
         $staff->img_nic = $nicfilenameToStore;
         $staff->img_birth_certificate = $bdayfilenameToStore;
         $staff->img_gr_cer = $gramefilenameToStore;
         $staff->img_police_cer = $policefilenameToStore;
         $staff->img_married_cer = $marriedfilenameToStore;
         $staff->img_edu_1_cer = $edu1filenameToStore;
         $staff->img_edu_2_cer = $edu2filenameToStore;
         $staff->img_edu_3_cer = $edu3filenameToStore;
         $staff->save();

         $user = new User();
         $user->name = $staff->name;
         if($request->designation=='Director'){
            $user->access_level='super_user';
         }else if($request->designation=='Field Executive'){
            $user->access_level='agent';
         }else if($request->designation=='Branch Manager'){
            $user->access_level='branch_manager';
         }else if($request->designation=='Account Head' ){
            $user->access_level='accountant';
         }else{
            $user->access_level=$request->designation;
         }
        //  $user->access_level=$request->designation;

         $user->phone = $staff->mobile;
         $user->branch = $staff->branch;
         $user->email = $staff->mail;
         $user->password = Hash::make('Abcd@1234');
         $user->save();

         return redirect()->back()->with('message', 'Member Added');
    }


    public function show(Staff $staff)
    {
        //
    }


    public function edit(Staff $staff)
    {
        return view('Staff.editStaff', compact('staff'));
    }


    public function update(Request $request, Staff $staff)
    {
        {
            $request->validate([
                'branch' => 'required',
                'name'=> 'required',
                'designation'=> 'required',
                'nic'=> 'required',
                'address'=> 'required',
                'mobile'=> 'required',
                'mail'=> 'required',
                'edu_qual'=> 'required',
                'pro_qual'=> 'required',
                'pro_exp'=> 'required',
                'civil_status'=> 'required',
                'img_nic'=> 'required',
                'img_birth_certificate'=> 'required',
                'img_gr_cer'=> 'required',
                'img_police_cer'=> 'required',
                'img_married_cer'=> 'required',
                'img_edu_1_cer'=> 'required',
                'img_edu_2_cer'=> 'required',
                'img_edu_3_cer'=> 'required',
                'applying_name'=> 'required',
                'relational_contact'=> 'required',
            ]);

            $staff->update($request->all());

            return redirect()->route('staff.index')
                ->with('success', 'Staff updated successfully');
        }    }


    public function destroy(Staff $staff)
    {
        //
    }

    public function hange_password(Request $request){

    }

    public function my_profile(){
        $user=User::find(auth()->user());
        $user_json=json_decode($user,true);
        $staff_member=Staff::where('mail',$user_json[0]["email"])->first();
        // return $staff_member ;
        return view('admin.my_profile',compact('staff_member'));
    }
}
