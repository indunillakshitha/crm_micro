<?php

namespace App\Http\Controllers;

use App\Borrower;
use App\Loan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SosController extends Controller
{
    public function removeSettlements(){
        $loans= Loan::where('visibility',1)->where('branch',Auth::user()->branch)->get();
        return view('sos.settlement',compact('loans'));
    }
    public function removeSettlementsAsReq(Request $request){
        Loan::where('id',$request->id)->update(['status'=>$request->status]);
        $stringmsg='Successfully updated to '.$request->status;
       return response()->json(['success'=>$stringmsg]);
    }
    public function deactivateUsers(){
        $loans= Borrower::where('visibility',1)->where('branch',Auth::user()->branch)->get();
        return view('sos.deactivatemembers',compact('loans'));
    }
    public function deactivateUsersAsReq(Request $request){

        Borrower::where('id',$request->id)->update(['is_deactivated'=>$request->status,'visibility'=>'0']);
        $stringmsg = 'Success';

        return response()->json(['success'=>$stringmsg]);
    }
}
