<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\Borrower;
use App\Branch;
use App\Center;
use App\CompanyExpences;
use App\Loan;
use App\Repayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{  public function __construct()
    {

        date_default_timezone_set('Asia/Colombo');

    }

    public function index()
    {
        $issued_loans = Loan::where('status', 'Payed')->where('branch', Auth::user()->branch)->count();
        $active_loans = Loan::where('status', 'Payed')->where('branch', Auth::user()->branch)->count();
        $setle_loans = Loan::where('status', 'SETTLED')->where('branch', Auth::user()->branch)->count();
        $new_loans = Loan::where('status', 'Pending')->where('branch', Auth::user()->branch)->count();
        $pending_loans = Loan::where('status', 'Pending')->where('branch', Auth::user()->branch)->get()->count();
        $borrower_count = Borrower::where('branch', Auth::user()->branch)->count();
        $apprasails = Appraisal::where('created_date_index', Carbon::now()->isoFormat('DDD'))->count();
        $loan_count = Loan::where('branch', Auth::user()->branch)->count();
        $centers = Center::where('branch_no', Auth::user()->branch)->count();
        $loans_to_pay = Loan::where('branch', Auth::user()->branch)->where('status', 'Approved')->get()->count();
        $name = Auth::user()->branch;
        $upName = strtoupper($name);
        $branches = Branch::all();
        $centers = Center::all();

        $loans = Loan::where('branch', $name)->where('branch', $upName)->get();
        $loansSum = Loan::where('branch', $name)->where('branch', $upName)->sum('paid');
        $recentDateIndex = Carbon::now()->isoFormat('DDD');
        $tot = Loan::where('branch', Auth::user()->branch)->sum('paid');
        $totloacount = Repayment::where('branch', $name)->where('branch', $upName)->count('total_payed');

        // $loansSum= number_format( $loansSu , 2 , '.' , ',' );
        foreach ($loans as $loan) {
            $paymentStartedDateIndex = $loan->payment_starting_date_index;

            $interDate = $recentDateIndex - $paymentStartedDateIndex;

            $shouldInstalments = (int) ($interDate / 7);

            $didPayments = $loan->payments_count;

            $arrearsInstalments = $shouldInstalments - $didPayments;

            if (($arrearsInstalments <= 0)) {

                $instalmentOfPer = $loan->instalment;

                $didTotalPayments = $instalmentOfPer * $loan->payments_count;

                $shouldTotalPayments = $shouldInstalments * $loan->instalment;

                $totalAdvancedPayment = $didTotalPayments - $shouldTotalPayments;

                $totalAdvancedPayment += $totalAdvancedPayment;

            } else if ($arrearsInstalments > 0) {

                $instalmentOfPer = $loan->instalment;

                $didTotalPayments = $instalmentOfPer * $loan->payments_count;

                $shouldTotalPayments = $shouldInstalments * $loan->instalment;

                $totalArrearse = $shouldTotalPayments - $didTotalPayments;

                $totalArrearse += $totalArrearse;

            } else {
                echo ("Error");
            }
        }

//    ----------------------------------------------------------------------------------------------------------------------------------------------------

// // Loans Tot
        //         $users = Borrower::select('id')->where('id','<',6)->get();
        //         $loan1= Loan::where('status','Payed')->where('loan_amount','10000')->sum('loan_amount');
        //         $loan2= Loan::where('status','Payed')->where('loan_amount','15000')->sum('loan_amount');
        //         $loan3= Loan::where('status','Payed')->where('loan_amount','20000')->sum('loan_amount');
        //         $loan4= Loan::where('status','Payed')->where('loan_amount','25000')->sum('loan_amount');
        //         $users = array($loan1,$loan2,$loan3,$loan4);

// // Loan Count
        //         $cloans = array(0,0,0,0,0,0,0,0,0,0,0,0);
        //         for($i=1;$i<12;$i++){
        //         $cloan = Loan::where('status','Payed')->whereMonth('payment_starting_date','=',$i)->count('id');
        //         $i=$i-1;
        //         $cloans[$i] = $cloan;
        //         $i=$i+1;
        //         }

// // Expenses
        //         $exps = array(0,0,0,0,0,0,0,0,0,0,0,0);
        //         for($i=1;$i<12;$i++){
        //         $expss = CompanyExpences::whereMonth('date','=',$i)->get();
        //         $i=$i-1;
        //         $expAs = 0;
        //         foreach($expss as $exps){

//             $pExexpAs = $exps->amount * $exps->quantity;
        //             $expAs = $expAs + $pExexpAs;
        //                 }
        //         $exps[$i] = round($expAs,2);
        //         $i=$i+1;
        //         }

// // Collections
        //         $cols = array(0,0,0,0,0,0,0,0,0,0,0,0);
        //         for($i=1;$i<12;$i++){
        //         $col = Repayment::whereMonth('payment_date','=',$i)->sum('paid_amount');
        //         $i=$i-1;
        //         $cols[$i] = $col;
        //         $i=$i+1;
        //         }

// // Collections Capital
        //         $colsCap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        //         $colInt = array(0,0,0,0,0,0,0,0,0,0,0,0);
        //         $colAdv = array(0,0,0,0,0,0,0,0,0,0,0,0);

//         for($i=1;$i<12;$i++){
        //             $repays = Repayment::whereMonth('payment_date','=',$i)->get();
        //             $i=$i-1;
        //             $capitalCol = 0;
        //             $interestCol = 0;
        //             $advanced = 0;
        //                 foreach($repays as $repay){
        //                     $cap = $repay->paid_amount - $repay->installment;
        //                     $cat = Loan::select('loan_duration','interest')->where('id',$repay->loan_id)->first();
        //                     if(!empty($cat)){
        //                     //     echo $cat;
        //                     //     echo $repay->loan_id;
        //                     // }else{
        //                     $interestx = $cat->interest / $cat->loan_duration;
        //                     $capital = $repay->installment -  $interestx;
        //                     $int =  $repay->paid_amount - $capital;
        //                     if($cap >=0){
        //                         $capitalCol = $capital + $capitalCol;
        //                         $interestCol = $interestx + $interestCol;
        //                         $advanced = $cap + $advanced;
        //                     }elseif($repay->paid_amount >= $capital ){
        //                         $capitalCol = $capital + $capitalCol;
        //                         $interestCol = $interestCol + $int;

//                     }else{
        //                         $arri = $repay->paid_amount;
        //                     }
        //                 }
        //                 }
        //             $colsCap[$i] = round($capitalCol,2);
        //             $colInt[$i] = round($interestCol,2);
        //             $colAdv[$i] = round($advanced,2);
        //             $i=$i+1;
        //         }

// // Arrearse Capital
        //         $arrCap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        //         $arrInt = array(0,0,0,0,0,0,0,0,0,0,0,0);
        //         $arrTot = array(0,0,0,0,0,0,0,0,0,0,0,0);

//         for($i=1;$i<12;$i++){
        //             $repayss = Repayment::whereMonth('payment_date','=',$i)->get();
        //             $i=$i-1;
        //             $capitalArr = 0;
        //             $interestArr = 0;
        //             $totalArr = 0;
        //                 foreach($repayss as $repay){
        //                     $pay =  $repay->installment - $repay->paid_amount ;
        //                     $cato = Loan::select('loan_duration','interest')->where('id',$repay->loan_id)->first();
        //                     if(!empty($cato)){
        //                     $interestt = $cato->interest / $cato->loan_duration;
        //                     $capitalo = $repay->installment -  $interestt;
        //                     $interestAmount =  $repay->paid_amount - $capitalo;
        //                     if($pay >=0){
        //                         $totalArr = $totalArr + $pay;

//                     }else{
        //                         $arrii = $repay->paid_amount;
        //                     }
        //                 }
        //                 }
        //             $arrTot[$i] = round($totalArr,2);

//                 foreach($repayss as $repay){
        //                     $payy =  $repay->installment - $repay->paid_amount ;
        //                     $catt = Loan::select('loan_duration','interest')->where('id',$repay->loan_id)->first();
        //                     if(!empty($catt)){
        //                     $interesttt = $catt->interest / $catt->loan_duration;
        //                     $capitall = $repay->installment -  $interesttt;
        //                     $interestAmount =  $repay->paid_amount - $capitall;
        //                     if($repay->paid_amount <= $capitall ){
        //                         $prec = $capitall - $repay->paid_amount;
        //                         $capitalArr = $capitalArr + $prec;
        //                         $interestArr = $interestArr + $interesttt;
        //                     }elseif(($repay->paid_amount < $repay->installment) && ($repay->paid_amount > $capitall)){
        //                         $prei = $repay->paid_amount - $capitall;
        //                         $preii = $interesttt - $prei;
        //                         $interestArr = $interestArr + $preii;
        //                     }else{
        //                         $arriii = $repay->paid_amount;
        //                     }
        //                 }

//                 }
        //                 $arrCap[$i] = round($capitalArr,2);
        //                 $arrInt[$i] = round($interestArr,2);
        //             $i=$i+1;
        //         }

// // Outstanding
        //         $outTot = array(0,0,0);
        //         $out = Loan::where('status','Payed')->sum('due');
        //         $outTot[0] = $out;

// Profit and Lost
        $proNlost = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        $cuTot = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        for ($i = 1; $i < 12; $i++) {
            // $intT = Repayment::whereMonth('payment_date','=',$i)->get();
            // $expT = CompanyExpences::whereMonth('date','=',$i)->where('visibility',1)->get();
            // $pprT = Loan::whereMonth('payment_starting_date','=',$i)->where('fees','>',0)->get();
            $intT = Repayment::whereMonth('payment_date', '=', $i)->sum('inter');
            $expT = CompanyExpences::whereMonth('date', '=', $i)->where('visibility', 1)->sum('item_total');
            $pprT = Loan::whereMonth('payment_starting_date', '=', $i)->where('fees', '>', 0)->sum('fees');

            $i = $i - 1;
            // $intA = 0;
            // $expA = 0;
            // $pprA = 0;
            //     foreach($intT as $int){

            //     $intA = $intA + $int->inter;

            //     }

            //     foreach($expT as $exp){

            //         $expA = $expA + $exp->item_total;
            //     }

            //     foreach($pprT as $ppr){

            //         $pprA = $pprA + $ppr->fees;
            //     }

            // $proNlost[$i] = round(($intA - ($expA + $pprA)),2);
            $proNlost[$i] = round((($intT + $pprT) - $expT), 2);
            if ($i == 0) {
                $cuTot[$i] = $proNlost[$i];
            } else {
                $x = $i - 1;
                $cuTot[$i] = $cuTot[$x] + $proNlost[$i];
            }
            $i = $i + 1;
        }

        $mon = DB::table('shedules')->leftJoin('loans', 'loans.center', 'shedules.center')
            ->where('loans.status', 'Payed')
            ->where('shedules.date', 'monday')
 ->where('loans.visibility', '1')
            ->sum('loans.instalment');

        $tue = DB::table('shedules')->leftJoin('loans', 'loans.center', 'shedules.center')
            ->where('loans.status', 'Payed')
            ->where('shedules.date', 'tuesday')
 ->where('loans.visibility', '1')
            ->sum('loans.instalment');

        $wed = DB::table('shedules')->leftJoin('loans', 'loans.center', 'shedules.center')
            ->where('loans.status', 'Payed')
            ->where('shedules.date', 'wednesday')
 ->where('loans.visibility', '1')
            ->sum('loans.instalment');

        $thu = DB::table('shedules')->leftJoin('loans', 'loans.center', 'shedules.center')
            ->where('loans.status', 'Payed')
            ->where('shedules.date', 'thursday')
 ->where('loans.visibility', '1')
            ->sum('loans.instalment');

        $fri = DB::table('shedules')->leftJoin('loans', 'loans.center', 'shedules.center')
            ->where('loans.status', 'Payed')
            ->where('shedules.date', 'friday')
 ->where('loans.visibility', '1')
            ->sum('loans.instalment');

        $sat = DB::table('shedules')->leftJoin('loans', 'loans.center', 'shedules.center')
            ->where('loans.status', 'Payed')
            ->where('shedules.date', 'saturday')
 ->where('loans.visibility', '1')
            ->sum('loans.instalment');

        $sun = DB::table('shedules')->leftJoin('loans', 'loans.center', 'shedules.center')
            ->where('loans.status', 'Payed')
            ->where('shedules.date', 'sunday')
 ->where('loans.visibility', '1')
            ->sum('loans.instalment');

$mon_payments = DB::table('shedules')
        // ->leftJoin('loans', 'loans.center', 'shedules.center')
        ->leftJoin('repayments', 'repayments.center', 'shedules.center')
           ->where('repayments.week_index', Carbon::now()->isoFormat('W'))
            ->whereBetween('repayments.payment_date_index', [Carbon::now()->startOfWeek()->isoFormat('DDD'),Carbon::now()->endOfWeek()->isoFormat('DDD')])
            ->where('repayments.branch', Auth::user()->branch)
            ->where('shedules.date', 'monday')
            ->sum('repayments.paid_amount');

        $tue_payments = DB::table('shedules')
        // ->leftJoin('loans', 'loans.center', 'shedules.center')
        ->leftJoin('repayments', 'repayments.center', 'shedules.center')
            //->where('repayments.week_index', Carbon::now()->isoFormat('W'))
//  ->whereBetween('repayments.created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
->whereBetween('repayments.payment_date_index', [Carbon::now()->startOfWeek()->isoFormat('DDD'),Carbon::now()->endOfWeek()->isoFormat('DDD')])

 ->where('repayments.branch', Auth::user()->branch)
            ->where('shedules.date', 'tuesday')
            ->sum('repayments.paid_amount');


        $wed_payments = DB::table('shedules')
        // ->leftJoin('loans', 'loans.center', 'shedules.center')
        ->leftJoin('repayments', 'repayments.center', 'shedules.center')
          //  ->where('repayments.week_index', Carbon::now()->isoFormat('W'))
//  ->whereBetween('repayments.created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
->whereBetween('repayments.payment_date_index', [Carbon::now()->startOfWeek()->isoFormat('DDD'),Carbon::now()->endOfWeek()->isoFormat('DDD')])

 ->where('repayments.branch', Auth::user()->branch)
            ->where('shedules.date', 'wednesday')
            ->sum('repayments.paid_amount');


        $thu_payments = DB::table('shedules')
        // ->leftJoin('loans', 'loans.center', 'shedules.center')
        ->leftJoin('repayments', 'repayments.center', 'shedules.center')
            //->where('repayments.week_index', Carbon::now()->isoFormat('W'))
//  ->whereBetween('repayments.created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
->whereBetween('repayments.payment_date_index', [Carbon::now()->startOfWeek()->isoFormat('DDD'),Carbon::now()->endOfWeek()->isoFormat('DDD')])

 ->where('repayments.branch', Auth::user()->branch)
            ->where('shedules.date', 'thursday')
            ->sum('repayments.paid_amount');


        $fri_payments = DB::table('shedules')
        // ->leftJoin('loans', 'loans.center', 'shedules.center')
        ->leftJoin('repayments', 'repayments.center', 'shedules.center')
          //  ->where('repayments.week_index', Carbon::now()->isoFormat('W'))
//  ->whereBetween('repayments.created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
->whereBetween('repayments.payment_date_index', [Carbon::now()->startOfWeek()->isoFormat('DDD'),Carbon::now()->endOfWeek()->isoFormat('DDD')])

 ->where('repayments.branch', Auth::user()->branch)
            ->where('shedules.date', 'friday')
            ->sum('repayments.paid_amount');


        $sat_payments = DB::table('shedules')
        // ->leftJoin('loans', 'loans.center', 'shedules.center')
        ->leftJoin('repayments', 'repayments.center', 'shedules.center')
            //->where('repayments.week_index', Carbon::now()->isoFormat('W'))
//  ->whereBetween('repayments.created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
->whereBetween('repayments.payment_date_index', [Carbon::now()->startOfWeek()->isoFormat('DDD'),Carbon::now()->endOfWeek()->isoFormat('DDD')])

 ->where('repayments.branch', Auth::user()->branch)
            ->where('shedules.date', 'saturday')
            ->sum('repayments.paid_amount');


        $sun_payments = DB::table('shedules')
        // ->leftJoin('loans', 'loans.center', 'shedules.center')
        ->leftJoin('repayments', 'repayments.center', 'shedules.center')
           // ->where('repayments.week_index', Carbon::now()->isoFormat('W'))
//  ->whereBetween('repayments.created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
->whereBetween('repayments.payment_date_index', [Carbon::now()->startOfWeek()->isoFormat('DDD'),Carbon::now()->endOfWeek()->isoFormat('DDD')])

 ->where('repayments.branch', Auth::user()->branch)
            ->where('shedules.date', 'sunday')
            ->sum('repayments.paid_amount');

   $sun_all = DB::table('loans')
            ->where('status', 'Payed')
 ->where('visibility', '1')
 ->where('branch', Auth::user()->branch)
            ->sum('instalment');


            $loans=Loan::where('status','Payed')
                        ->where('visibility',1)
                        ->where('branch', Auth::user()->branch)
                        ->get();
            $total_loan_amount=0;
           $total_returned=0;
           $total_capital=0;
           $total_interest=0;
           $came_capital=0;
           $came_interest=0;
            foreach($loans as $loan){
                $total_loan_amount=$total_loan_amount+$loan->loan_amount+$loan->interest;
                $total_returned=$total_returned+$loan->paid;
                $total_capital=$total_capital+$loan->loan_amount;
                $total_interest=$total_interest+$loan->interest;
                $repayments=Repayment::where('loan_id',$loan->id)->get();
                foreach($repayments as $repaymet){
                    $came_capital=$came_capital+$repaymet->capital;
                    $came_interest=$came_interest+$repaymet->inter;
                }
            }
            $total_outstanding=$total_loan_amount-$total_returned;
            $total_capital_out=$total_capital-$came_capital;
            $total_inter_out=$total_interest-$came_interest;



    // $total_loan_amount_=Loan
        // return $mon;

        // $weekRepy = array(0, 0, 0, 0, 0, 0, 0);
        // $set = 0;
        // $totWeekDay = 0;
        // $Tdate = Carbon::now()->startOfWeek()->isoFormat('DDD');
        // $reLoans = Loan::where('status', 'Payed')->where('branch', Auth::user()->branch)->get();
        // for ($i = 0; $i < 7; $i++) {
        //     $checkDay = $Tdate + $i;
        //     foreach ($reLoans as $reLoan) {
        //         $ln_start = $reLoan->payment_starting_date_index - 7;
        //         $paymnt = ($checkDay - $ln_start) % 7;
        //         if ($paymnt == 0) {
        //             $set = $reLoan->instalment;
        //             $totWeekDay = $totWeekDay + $set;

        //         } else {

        //         }

        //     }
        //     $weekRepy[$i] = $totWeekDay;

        //     $totWeekDay = 0;

        // }
        // $mon = $weekRepy[0];
        // $tue = $weekRepy[1];
        // $wen = $weekRepy[1];
        // $thu = $weekRepy[3];
        // $fri = $weekRepy[4];
        // $sat = $weekRepy[5];
        // $sun = $weekRepy[6];

// return view ('admin.charts',['Users' => $users],['Cols' => $cols],['Exps' => $exps]);
        //
        //  return view('admin.dashboard',compact('centers','branches','borrower_count','loan_count','loans_to_pay','centers',
        //         'apprasails','issued_loans','active_loans','setle_loans','new_loans','pending_loans','totalArrearse','totalAdvancedPayment',
        //         'loans','loansSum','tot','totloacount'),['Users' => $users,'Cols' => $cols,'Exps' => $exps,'Cloans' => $cloans,'ColCap' => $colsCap,'ColInt' => $colInt,
        //                               'ColAdv' => $colAdv,'ArrTot' => $arrTot,'ArrCap' => $arrCap,'ArrInt' => $arrInt,'OutStd' => $outTot,'PronLost' => $proNlost,'CuTot' => $cuTot]);

//     }
        return view('admin.dashboard', compact('sun', 'sat', 'fri', 'thu', 'wed', 'tue', 'mon', 'centers', 'branches', 'borrower_count', 'loan_count', 'loans_to_pay', 'centers',
            'apprasails', 'issued_loans', 'active_loans', 'setle_loans', 'new_loans', 'pending_loans', 'totalArrearse', 'totalAdvancedPayment',
            'loans', 'loansSum', 'tot', 'totloacount','mon_payments',
            'tue_payments',
            'wed_payments',
            'thu_payments',
            'fri_payments',
            'total_capital_out',
            'total_inter_out',
'sun_all',
'total_outstanding',
            'sat_payments','sun_payments'), ['PronLost' => $proNlost, 'CuTot' => $cuTot]);

    }

    public function getCloChart(Request $request)
    {
        $ColVal = array();
        $ColLeb = array();
        if (!empty($request->branch) && !empty($request->center)) {

            $dateFrom = $request->dateFrom;
            $dateTo = $request->dateTo;
            $collection = Repayment::select(DB::raw('SUM(paid_amount) AS sum'), DB::raw('WEEK(payment_date) AS weeknumber'))
                ->where('branch', $request->branch)
                ->where('center', $request->center)
                ->whereBetween('payment_date', [$dateFrom, $dateTo])
                ->groupBy('weeknumber')
                ->get();
            foreach ($collection as $col) {

                $ColVal[] = round($col->sum, 2);
                $ColLeb[] = round($col->weeknumber, 2);

            }

        } elseif (!empty($request->branch)) {

            $dateFrom = $request->dateFrom;
            $dateTo = $request->dateTo;
            $collection = Repayment::select(DB::raw('SUM(paid_amount) AS sum'), DB::raw('WEEK(payment_date) AS weeknumber'))
                ->where('branch', $request->branch)
                ->whereBetween('payment_date', [$dateFrom, $dateTo])
                ->groupBy('weeknumber')
                ->get();
            foreach ($collection as $col) {

                $ColVal[] = round($col->sum, 2);
                $ColLeb[] = round($col->weeknumber, 2);

            }

        } elseif (!empty($request->center)) {

            $dateFrom = $request->dateFrom;
            $dateTo = $request->dateTo;
            $collection = Repayment::select(DB::raw('SUM(paid_amount) AS sum'), DB::raw('WEEK(payment_date) AS weeknumber'))
                ->where('center', $request->center)
                ->whereBetween('payment_date', [$dateFrom, $dateTo])
                ->groupBy('weeknumber')
                ->get();
            foreach ($collection as $col) {

                $ColVal[] = round($col->sum, 2);
                $ColLeb[] = round($col->weeknumber, 2);

            }

        } else {

        }

        // return response()->json(['CoAdv' => $arrCap,'ClAdv' => $arrCapl]);
        return response()->json(['CoAdv' => $ColVal, 'ClAdv' => $ColLeb]);

    }

    public function getArrChart(Request $request)
    {
        $ArrVal = array();
        $ArrLeb = array();
        if (!empty($request->branch) && !empty($request->center)) {

            $dateFrom = $request->dateFrom;
            $dateTo = $request->dateTo;
            $collection = Repayment::select(DB::raw('SUM(paid_amount) AS sum'), DB::raw('SUM(installment) AS rsum'), DB::raw('WEEK(payment_date) AS weeknumber'))
                ->where('branch', $request->branch)
                ->where('center', $request->center)
                ->whereBetween('payment_date', [$dateFrom, $dateTo])
                ->groupBy('weeknumber')
                ->get();
            foreach ($collection as $col) {

                $check = $col->rsum - $col->sum;
                if ($check >= 0) {
                    $ArrVal[] = round($check, 2);
                } else {

                }
                $ArrLeb[] = round($col->weeknumber, 2);

            }

        } elseif (!empty($request->branch)) {

            $dateFrom = $request->dateFrom;
            $dateTo = $request->dateTo;
            $collection = Repayment::select(DB::raw('SUM(paid_amount) AS sum'), DB::raw('SUM(installment) AS rsum'), DB::raw('WEEK(payment_date) AS weeknumber'))
                ->where('branch', $request->branch)
                ->whereBetween('payment_date', [$dateFrom, $dateTo])
                ->groupBy('weeknumber')
                ->get();
            // echo ($collection);
            foreach ($collection as $col) {

                $check = $col->rsum - $col->sum;
                if ($check >= 0) {
                    $ArrVal[] = round($check, 2);
                } else {

                }
                $ArrLeb[] = round($col->weeknumber, 2);

            }

        } elseif (!empty($request->center)) {

            $dateFrom = $request->dateFrom;
            $dateTo = $request->dateTo;
            $collection = Repayment::select(DB::raw('SUM(paid_amount) AS sum'), DB::raw('SUM(installment) AS rsum'), DB::raw('WEEK(payment_date) AS weeknumber'))
                ->where('center', $request->center)
                ->whereBetween('payment_date', [$dateFrom, $dateTo])
                ->groupBy('weeknumber')
                ->get();
            foreach ($collection as $col) {

                $check = $col->rsum - $col->sum;
                if ($check >= 0) {
                    $ArrVal[] = round($check, 2);
                } else {

                }
                $ArrLeb[] = round($col->weeknumber, 2);

            }

        } else {

        }

        // return response()->json(['CoAdv' => $arrCap,'ClAdv' => $arrCapl]);
        return response()->json(['ArAdv' => $ArrVal, 'AlAdv' => $ArrLeb]);
    }

    public function chrtNreport()
    {
        $issued_loans = Loan::where('status', 'Payed')->where('branch', Auth::user()->branch)->where('visibility','1')->count();
        $active_loans = Loan::where('status', 'Payed')->where('branch', Auth::user()->branch)->where('visibility','1')->count();
        $setle_loans = Loan::where('status', 'SETTLED')->where('branch', Auth::user()->branch)->where('visibility','1')->count();
        $new_loans = Loan::where('status', 'Pending')->where('branch', Auth::user()->branch)->where('visibility','1')->count();
        $pending_loans = Loan::where('status', 'Pending')->where('branch', Auth::user()->branch)->where('visibility','1')->get()->count();
        $borrower_count = Borrower::where('branch', Auth::user()->branch)->count();
        $apprasails = Appraisal::where('created_date_index', Carbon::now()->isoFormat('DDD'))->count();
        $loan_count = Loan::where('branch', Auth::user()->branch)->count();
        $centers = Center::where('branch_no', Auth::user()->branch)->count();
        $loans_to_pay = Loan::where('branch', Auth::user()->branch)->where('status', 'Approved')->get()->count();
        $name = Auth::user()->branch;
        $upName = strtoupper($name);
        $branches = Branch::all();
        $centers = Center::all();

        $loans = Loan::where('branch', $name)->where('branch', $upName)->get();
        $loansSum = Loan::where('branch', $name)->where('branch', $upName)->sum('paid');
        $recentDateIndex = Carbon::now()->isoFormat('DDD');
        $tot = Loan::where('branch', Auth::user()->branch)->sum('paid');
        $totloacount = Repayment::where('branch', $name)->where('branch', $upName)->count('total_payed');

        // $loansSum= number_format( $loansSu , 2 , '.' , ',' );
        foreach ($loans as $loan) {
            $paymentStartedDateIndex = $loan->payment_starting_date_index;

            $interDate = $recentDateIndex - $paymentStartedDateIndex;

            $shouldInstalments = (int) ($interDate / 7);

            $didPayments = $loan->payments_count;

            $arrearsInstalments = $shouldInstalments - $didPayments;

            if (($arrearsInstalments <= 0)) {

                $instalmentOfPer = $loan->instalment;

                $didTotalPayments = $instalmentOfPer * $loan->payments_count;

                $shouldTotalPayments = $shouldInstalments * $loan->instalment;

                $totalAdvancedPayment = $didTotalPayments - $shouldTotalPayments;

                $totalAdvancedPayment += $totalAdvancedPayment;

            } else if ($arrearsInstalments > 0) {

                $instalmentOfPer = $loan->instalment;

                $didTotalPayments = $instalmentOfPer * $loan->payments_count;

                $shouldTotalPayments = $shouldInstalments * $loan->instalment;

                $totalArrearse = $shouldTotalPayments - $didTotalPayments;

                $totalArrearse += $totalArrearse;

            } else {
                echo ("Error");
            }
        }

//    ----------------------------------------------------------------------------------------------------------------------------------------------------

// Loans Tot
        $users = Borrower::select('id')->where('id', '<', 6)->get();
        $loan1 = Loan::where('status', 'Payed')->where('loan_amount', '10000')->sum('loan_amount');
        $loan2 = Loan::where('status', 'Payed')->where('loan_amount', '15000')->sum('loan_amount');
        $loan3 = Loan::where('status', 'Payed')->where('loan_amount', '20000')->sum('loan_amount');
        $loan4 = Loan::where('status', 'Payed')->where('loan_amount', '25000')->sum('loan_amount');
        $users = array($loan1, $loan2, $loan3, $loan4);

// Loan Count
        $cloans = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        for ($i = 1; $i < 12; $i++) {
            $cloan = Loan::where('status', 'Payed')->whereMonth('payment_starting_date', '=', $i)->count('id');
            $i = $i - 1;
            $cloans[$i] = $cloan;
            $i = $i + 1;
        }

// Expenses
        $exps = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        for ($i = 1; $i < 12; $i++) {
            $expT = CompanyExpences::whereMonth('date', '=', $i)->where('visibility', 1)->sum('item_total');
            $i = $i - 1;

            $exps[$i] = round($expT, 2);
            $i = $i + 1;
        }

// Collections
        $cols = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        for ($i = 1; $i < 12; $i++) {
            $col = Repayment::whereMonth('payment_date', '=', $i)->sum('paid_amount');
            $i = $i - 1;
            $cols[$i] = $col;
            $i = $i + 1;
        }

// Collections Capital
        $colsCap = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        $colInt = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        $colAdv = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        for ($i = 1; $i < 12; $i++) {
            $repays = Repayment::whereMonth('payment_date', '=', $i)->get();
            $i = $i - 1;
            $capitalCol = 0;
            $interestCol = 0;
            $advanced = 0;
            foreach ($repays as $repay) {
                $cap = $repay->paid_amount - $repay->installment;
                $cat = Loan::select('loan_duration', 'interest')->where('id', $repay->loan_id)->first();
                if (!empty($cat)) {
                    //     echo $cat;
                    //     echo $repay->loan_id;
                    // }else{
                    $interestx = $cat->interest / $cat->loan_duration;
                    $capital = $repay->installment - $interestx;
                    $int = $repay->paid_amount - $capital;
                    if ($cap >= 0) {
                        $capitalCol = $capital + $capitalCol;
                        $interestCol = $interestx + $interestCol;
                        $advanced = $cap + $advanced;
                    } elseif ($repay->paid_amount >= $capital) {
                        $capitalCol = $capital + $capitalCol;
                        $interestCol = $interestCol + $int;

                    } else {
                        $arri = $repay->paid_amount;
                    }
                }
            }
            $colsCap[$i] = round($capitalCol, 2);
            $colInt[$i] = round($interestCol, 2);
            $colAdv[$i] = round($advanced, 2);
            $i = $i + 1;
        }

// Arrearse Capital
        $arrCap = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        $arrInt = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        $arrTot = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        for ($i = 1; $i < 12; $i++) {
            $repayss = Repayment::whereMonth('payment_date', '=', $i)->get();
            $i = $i - 1;
            $capitalArr = 0;
            $interestArr = 0;
            $totalArr = 0;
            foreach ($repayss as $repay) {
                $pay = $repay->installment - $repay->paid_amount;
                $cato = Loan::select('loan_duration', 'interest')->where('id', $repay->loan_id)->first();
                if (!empty($cato)) {
                    $interestt = $cato->interest / $cato->loan_duration;
                    $capitalo = $repay->installment - $interestt;
                    $interestAmount = $repay->paid_amount - $capitalo;
                    if ($pay >= 0) {
                        $totalArr = $totalArr + $pay;

                    } else {
                        $arrii = $repay->paid_amount;
                    }
                }
            }
            $arrTot[$i] = round($totalArr, 2);

            foreach ($repayss as $repay) {
                $payy = $repay->installment - $repay->paid_amount;
                $catt = Loan::select('loan_duration', 'interest')->where('id', $repay->loan_id)->first();
                if (!empty($catt)) {
                    $interesttt = $catt->interest / $catt->loan_duration;
                    $capitall = $repay->installment - $interesttt;
                    $interestAmount = $repay->paid_amount - $capitall;
                    if ($repay->paid_amount <= $capitall) {
                        $prec = $capitall - $repay->paid_amount;
                        $capitalArr = $capitalArr + $prec;
                        $interestArr = $interestArr + $interesttt;
                    } elseif (($repay->paid_amount < $repay->installment) && ($repay->paid_amount > $capitall)) {
                        $prei = $repay->paid_amount - $capitall;
                        $preii = $interesttt - $prei;
                        $interestArr = $interestArr + $preii;
                    } else {
                        $arriii = $repay->paid_amount;
                    }
                }

            }
            $arrCap[$i] = round($capitalArr, 2);
            $arrInt[$i] = round($interestArr, 2);
            $i = $i + 1;
        }

// Outstanding
        $outTot = array(0, 0, 0);
        $out = Loan::where('status', 'Payed')->sum('due');
        $outTot[0] = $out;

// Profit and Lost
        $proNlost = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        $cuTot = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        for ($i = 1; $i < 12; $i++) {
            // $intT = Repayment::whereMonth('payment_date','=',$i)->get();
            // $expT = CompanyExpences::whereMonth('date','=',$i)->where('visibility',1)->get();
            // $pprT = Loan::whereMonth('payment_starting_date','=',$i)->where('fees','>',0)->get();
            $intT = Repayment::whereMonth('payment_date', '=', $i)->sum('inter');
            $expT = CompanyExpences::whereMonth('date', '=', $i)->where('visibility', 1)->sum('item_total');
            $pprT = Loan::whereMonth('payment_starting_date', '=', $i)->where('fees', '>', 0)->sum('fees');
            $i = $i - 1;
            // $intA = 0;
            // $expA = 0;
            // $pprA = 0;
            //     foreach($intT as $int){

            //     $intA = $intA + $int->inter;

            //     }

            //     foreach($expT as $exp){

            //         $expA = $expA + $exp->item_total;
            //     }

            //     foreach($pprT as $ppr){

            //         $pprA = $pprA + $ppr->fees;
            //     }

            // $proNlost[$i] = round(($intA - ($expA + $pprA)),2);
            $proNlost[$i] = round((($intT + $pprT) - $expT), 2);
            if ($i == 0) {
                $cuTot[$i] = $proNlost[$i];
            } else {
                $x = $i - 1;
                $cuTot[$i] = $cuTot[$x] + $proNlost[$i];
            }
            $i = $i + 1;
        }

// return view ('admin.charts',['Users' => $users],['Cols' => $cols],['Exps' => $exps]);
        //
        return view('admin.chartsNreports', compact('centers', 'branches', 'borrower_count', 'loan_count', 'loans_to_pay', 'centers',
            'apprasails', 'issued_loans', 'active_loans', 'setle_loans', 'new_loans', 'pending_loans', 'totalArrearse', 'totalAdvancedPayment',
            'loans', 'loansSum', 'tot', 'totloacount','mon_payments',
            'tue_payments',
            'wed_payments',
            'thu_payments',
            'fri_payments',
            'sat_payments',
            'sun_payments'), ['Users' => $users, 'Cols' => $cols, 'Exps' => $exps, 'Cloans' => $cloans, 'ColCap' => $colsCap, 'ColInt' => $colInt,
            'ColAdv' => $colAdv, 'ArrTot' => $arrTot, 'ArrCap' => $arrCap, 'ArrInt' => $arrInt, 'OutStd' => $outTot, 'PronLost' => $proNlost, 'CuTot' => $cuTot]);

    }

    public function branchExp(Request $request)
    {
        $exps = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        if ($request->branch == 'Kesbewa') {
            $char = 'KEB';
        } else {
            $char = 'ALU';
        }
        for ($i = 1; $i < 12; $i++) {
            $expT = CompanyExpences::where('voucher_id', 'LIKE', $char . '%')->whereMonth('date', '=', $i)->where('visibility', 1)->sum('item_total');
            $i = $i - 1;

            $exps[$i] = round($expT, 2);
            $i = $i + 1;
        }
        return response()->json(['Exp' => $exps]);
        // return response()->json($request);

    }
}
// $q = DB::table('repayments')
// ->select(DB::raw("SUM(repayments.paid_amount) AS paid"))
// ->whereBetween('payment_date', [$dateFrom, $dateTo])->get()->groupBy(function($q) {
// return Carbon::parse($q->payment_date)->format('W');
// });
