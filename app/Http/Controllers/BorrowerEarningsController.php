<?php

namespace App\Http\Controllers;
use App\BorrowerEarning;
use App\BorrowerMainEarning;
use Illuminate\Http\Request;
use App\Appraisal_earning_product;

class BorrowerEarningsController extends Controller
{
    public function addEarning(Request $request){

        $product = Appraisal_earning_product::where('product', $request->product_name)->first();
        $min = $product->min;
        $max = $product->max;

        if($request->unit_price > $min && $request->unit_price < $max){
            $earning = new BorrowerEarning;
            // $earning->borrower_id = $request->borrower_id;
            $earning->nic = $request->nic;
            $earning->product_name = $request->product_name;
            $earning->period = $request->period;
            $earning->total_units = $request->total_units;
            $earning->earnings = $request->earnings;
            $earning->unit_price = $request->unit_price;
            $earning->save();

            return response()->json($request);
        } else {
            return response()->json('not_in_range');
        }

    }

    public function getEarning(Request $request){
        $find = BorrowerEarning::where('nic', $request->nic)->where('visibility', 1)->get();

        return response()->json($find);
    }

    public function addRelationshipIncome(Request $request){
        $income = new BorrowerMainEarning;
        // $income->borrower_id = $request->borrower_id;
        $income->nic = $request->nic;
        $income->income_relationship = $request->income_relationship;
        $income->income = $request->income;
        $income->job_type = $request->job_type;
        $income->salary = $request->salary;
        $income->save();

        return response()->json($income);

    }

    public function getRelationshipIncome(Request $request){
        $find = BorrowerMainEarning::where('nic', $request->nic)->where('visibility', 1)->get();

        return response()->json($find);
    }
}
