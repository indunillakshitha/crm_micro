<?php

namespace App\Http\Controllers;

use App\Borrower;
use App\Branch;
use App\Center;
use App\Witness;
use App\Loan;
use App\Staff;
use PhpOffice\PhpWord\TemplateProcessor;
use NumberFormatter;

class PrintController extends Controller
{
    public function cheque()
    {

        $branches = Branch::all();
        return view('Cheque.printCheque')->with('branches', $branches);
    }


    public function exportPromissory($id)
    {
        $loan = Loan::where('id', $id)->first();
        $borrower = Borrower::find($loan->borrower_no);
        $promissory = new TemplateProcessor('word-template/Promissory.docx');
        $witnessA = Staff::where('branch', $borrower->branch)->where('center', $borrower->center)->where('designation', 'Agent')->first();

        $digit = new NumberFormatter("en", NumberFormatter::SPELLOUT);
        $loan_amount_spell = $digit->format($loan->loan_amount);
        $interest_rate_spell = $digit->format($loan->interest_rate);

        $promissory->setValue('release_date', $loan->release_date);
        $promissory->setValue('loan_amount', $loan->loan_amount);
        $promissory->setValue('loan_amount_spell', $loan_amount_spell);
        $promissory->setValue('address', $borrower->address);
        $promissory->setValue('full_name', $borrower->full_name);
        $promissory->setValue('nic', $borrower->nic);
        $promissory->setValue('interest_rate', $loan->interest_rate);
        $promissory->setValue('interest_rate_spell', $interest_rate_spell);
        $promissory->setValue('application_no', $borrower->application_no);

        $promissory->setValue('name_witnessA', $witnessA->name);
        $promissory->setValue('nic_witnessA', $witnessA->nic);
        $promissory->setValue('address_witnessA', $witnessA->address);




        $fileName = $borrower->borrower_no . " Promissory";

        $promissory->saveAs($fileName . '.docx');
        return response()->download($fileName . '.docx')->deleteFileAfterSend(true);
    }

    public function exportAgreement($id)
    {
        $loan = Loan::where('id', $id)->first();
        $borrower = Borrower::find($loan->borrower_no);
        // $witnessL = Borrower::where('center', $borrower->center)->where('remarks', 'Center Leader')->first();
        $witnessL = Borrower::where('borrower_no', $loan->witnessC)->first();
        // $witnessL = $loan->witnessC;
        $witnessA = Staff::where('branch', $borrower->branch)->where('center', $borrower->center)->where('designation', 'Agent')->first();

        $agreement = new TemplateProcessor('word-template/Agreement.docx');

        $digit = new NumberFormatter("en", NumberFormatter::SPELLOUT);
        $loan_amount_spell = $digit->format($loan->loan_amount);
        $interest_rate_spell = $digit->format($loan->interest_rate);
        $releseDate = $loan->release_date;
        $oderDate=explode('-',$releseDate);
        $year=$oderDate[0];
        $month=$oderDate[1];
        $day=$oderDate[2];

        $agreement->setValue('release_date', $releseDate);
        $agreement->setValue('loan_amount', $loan->loan_amount);
        $agreement->setValue('loan_amount_spell', $loan_amount_spell);
        $agreement->setValue('address', $borrower->address);
        $agreement->setValue('full_name', $borrower->full_name);
        $agreement->setValue('nic', $borrower->nic);
        $agreement->setValue('interest_rate', $loan->interest_rate);
        $agreement->setValue('interest_rate_spell', $interest_rate_spell);
        $agreement->setValue('application_no', $borrower->application_no);
        $agreement->setValue('branch', $borrower->branch);
        $agreement->setValue('loan_duration', $loan->loan_duration);
        $agreement->setValue('instalment', $loan->instalment);
        $agreement->setValue('year', $year);
        $agreement->setValue('month', $month);
        $agreement->setValue('day', $day);



        $agreement->setValue('name_witnessL', $witnessL->full_name);
        $agreement->setValue('nic_witnessL', $witnessL->nic);
        $agreement->setValue('name_witnessA', $witnessA->name);
        $agreement->setValue('nic_witnessA', $witnessA->nic);

        $agreementName = $borrower->borrower_no . " Agreement";
        $agreement->saveAs($agreementName . '.docx');
        return response()->download($agreementName . '.docx')->deleteFileAfterSend(true);
    }

    public function exportDisbursement($id)
    {
        $loan = Loan::where('id', $id)->first();
        $borrower = Borrower::where('borrower_no', $loan->borrower_no)->first();
        $guarantors = Borrower::where('branch', $borrower->branch)
            ->where('center', $borrower->center)->where('group_no', $borrower->group_no)->get();

        $center=Center::where('branch_no',$borrower->branch)->where('center_name',$borrower->center)->first();
        $center_no=$center->center_no;

        $count = count($guarantors);

        $guarantorsArray = json_decode($guarantors, true);
        $loanArray = json_decode($loan, true);
        $allLoans = Loan::where('group_no', $guarantorsArray[0]["group_no"])->get();

        $disbursement = new TemplateProcessor('word-template/Disbursement.docx');
        if ($count == 5) {

            $loanAmount1 = $allLoans[0]["loan_amount"];
            $loanAmount2 = $allLoans[1]["loan_amount"];
            $loanAmount3 = $allLoans[2]["loan_amount"];
            $loanAmount4 = $allLoans[3]["loan_amount"];
            $loanAmount5 = $allLoans[4]["loan_amount"];

            $interestRate1 = $allLoans[0]["interest_rate"];
            $interestRate2 = $allLoans[1]["interest_rate"];
            $interestRate3 = $allLoans[2]["interest_rate"];
            $interestRate4 = $allLoans[3]["interest_rate"];
            $interestRate5 = $allLoans[4]["interest_rate"];

            $loanDuration1 = $allLoans[0]["loan_duration"];
            $loanDuration2 = $allLoans[1]["loan_duration"];
            $loanDuration3 = $allLoans[2]["loan_duration"];
            $loanDuration4 = $allLoans[3]["loan_duration"];
            $loanDuration5 = $allLoans[4]["loan_duration"];

            $disbursement->setValue('branch', $guarantorsArray[0]["branch"]);
            $disbursement->setValue('center', $guarantorsArray[0]["center"]);
            $disbursement->setValue('release_date', $loanArray["release_date"]);

            $disbursement->setValue('name_1', $guarantorsArray[0]["full_name"]);
            $disbursement->setValue('id_1', $guarantorsArray[0]["borrower_no"]);
            $disbursement->setValue('nic_1', $guarantorsArray[0]["nic"]);
            $disbursement->setValue('interest_rate_1', $interestRate1);
            $disbursement->setValue('loan_amount_1', $loanAmount1);
            $disbursement->setValue('loan_duration_1', $loanDuration1);
            $disbursement->setValue('group_no_1', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_2', $guarantorsArray[1]["full_name"]);
            $disbursement->setValue('id_2', $guarantorsArray[1]["borrower_no"]);
            $disbursement->setValue('nic_2', $guarantorsArray[1]["nic"]);
            $disbursement->setValue('interest_rate_2', $interestRate2);
            $disbursement->setValue('loan_amount_2', $loanAmount2);
            $disbursement->setValue('loan_duration_2', $loanDuration2);
            $disbursement->setValue('group_no_2', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_3', $guarantorsArray[2]["full_name"]);
            $disbursement->setValue('id_3', $guarantorsArray[2]["borrower_no"]);
            $disbursement->setValue('nic_3', $guarantorsArray[2]["nic"]);
            $disbursement->setValue('interest_rate_3', $interestRate3);
            $disbursement->setValue('loan_amount_3', $loanAmount3);
            $disbursement->setValue('loan_duration_3', $loanDuration3);
            $disbursement->setValue('group_no_3', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_4', $guarantorsArray[3]["full_name"]);
            $disbursement->setValue('id_4', $guarantorsArray[3]["borrower_no"]);
            $disbursement->setValue('nic_4', $guarantorsArray[3]["nic"]);
            $disbursement->setValue('interest_rate_4', $interestRate4);
            $disbursement->setValue('loan_amount_4', $loanAmount4);
            $disbursement->setValue('loan_duration_4', $loanDuration4);
            $disbursement->setValue('group_no_4', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_5', $guarantorsArray[4]["full_name"]);
            $disbursement->setValue('id_5', $guarantorsArray[4]["borrower_no"]);
            $disbursement->setValue('nic_5', $guarantorsArray[4]["nic"]);
            $disbursement->setValue('interest_rate_5', $interestRate5);
            $disbursement->setValue('loan_amount_5', $loanAmount5);
            $disbursement->setValue('loan_duration_5', $loanDuration5);
            $disbursement->setValue('group_no_5', $guarantorsArray[0]["group_no"]);
            $sum=($loanAmount1+$loanAmount2+$loanAmount3+$loanAmount4+$loanAmount5);
        } else if ($count == 4) {

            $loanAmount1 = $allLoans[0]["loan_amount"];
            $loanAmount2 = $allLoans[1]["loan_amount"];
            $loanAmount3 = $allLoans[2]["loan_amount"];
            $loanAmount4 = $allLoans[3]["loan_amount"];

            $interestRate1 = $allLoans[0]["interest_rate"];
            $interestRate2 = $allLoans[1]["interest_rate"];
            $interestRate3 = $allLoans[2]["interest_rate"];
            $interestRate4 = $allLoans[3]["interest_rate"];

            $loanDuration1 = $allLoans[0]["loan_duration"];
            $loanDuration2 = $allLoans[1]["loan_duration"];
            $loanDuration3 = $allLoans[2]["loan_duration"];
            $loanDuration4 = $allLoans[3]["loan_duration"];

            $disbursement->setValue('branch', $guarantorsArray[0]["branch"]);
            $disbursement->setValue('center', $guarantorsArray[0]["center"]);
            $disbursement->setValue('release_date', $loanArray["release_date"]);

            $disbursement->setValue('name_1', $guarantorsArray[0]["full_name"]);
            $disbursement->setValue('id_1', $guarantorsArray[0]["borrower_no"]);
            $disbursement->setValue('nic_1', $guarantorsArray[0]["nic"]);
            $disbursement->setValue('interest_rate_1', $interestRate1);
            $disbursement->setValue('loan_amount_1', $loanAmount1);
            $disbursement->setValue('loan_duration_1', $loanDuration1);
            $disbursement->setValue('group_no_1', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_2', $guarantorsArray[1]["full_name"]);
            $disbursement->setValue('id_2', $guarantorsArray[1]["borrower_no"]);
            $disbursement->setValue('nic_2', $guarantorsArray[1]["nic"]);
            $disbursement->setValue('interest_rate_2', $interestRate2);
            $disbursement->setValue('loan_amount_2', $loanAmount2);
            $disbursement->setValue('loan_duration_2', $loanDuration2);
            $disbursement->setValue('group_no_2', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_3', $guarantorsArray[2]["full_name"]);
            $disbursement->setValue('id_3', $guarantorsArray[2]["borrower_no"]);
            $disbursement->setValue('nic_3', $guarantorsArray[2]["nic"]);
            $disbursement->setValue('interest_rate_3', $interestRate3);
            $disbursement->setValue('loan_amount_3', $loanAmount3);
            $disbursement->setValue('loan_duration_3', $loanDuration3);
            $disbursement->setValue('group_no_3', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_4', $guarantorsArray[3]["full_name"]);
            $disbursement->setValue('id_4', $guarantorsArray[3]["borrower_no"]);
            $disbursement->setValue('nic_4', $guarantorsArray[3]["nic"]);
            $disbursement->setValue('interest_rate_4', $interestRate4);
            $disbursement->setValue('loan_amount_4', $loanAmount4);
            $disbursement->setValue('loan_duration_4', $loanDuration4);
            $disbursement->setValue('group_no_4', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_5', '');
            $disbursement->setValue('id_5', '');
            $disbursement->setValue('nic_5', '');
            $disbursement->setValue('interest_rate_5', '');
            $disbursement->setValue('loan_amount_5', '');
            $disbursement->setValue('loan_duration_5', '');
            $disbursement->setValue('group_no_5', '');
            $sum=($loanAmount1+$loanAmount2+$loanAmount3+$loanAmount4);
        } else if ($count == 3) {

            $loanAmount1 = $allLoans[0]["loan_amount"];
            $loanAmount2 = $allLoans[1]["loan_amount"];
            $loanAmount3 = $allLoans[2]["loan_amount"];

            $interestRate1 = $allLoans[0]["interest_rate"];
            $interestRate2 = $allLoans[1]["interest_rate"];
            $interestRate3 = $allLoans[2]["interest_rate"];

            $loanDuration1 = $allLoans[0]["loan_duration"];
            $loanDuration2 = $allLoans[1]["loan_duration"];
            $loanDuration3 = $allLoans[2]["loan_duration"];

            $disbursement->setValue('branch', $guarantorsArray[0]["branch"]);
            $disbursement->setValue('center', $guarantorsArray[0]["center"]);
            $disbursement->setValue('release_date', $loanArray["release_date"]);

            $disbursement->setValue('name_1', $guarantorsArray[0]["full_name"]);
            $disbursement->setValue('id_1', $guarantorsArray[0]["borrower_no"]);
            $disbursement->setValue('nic_1', $guarantorsArray[0]["nic"]);
            $disbursement->setValue('interest_rate_1', $interestRate1);
            $disbursement->setValue('loan_amount_1', $loanAmount1);
            $disbursement->setValue('loan_duration_1', $loanDuration1);
            $disbursement->setValue('group_no_1', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_2', $guarantorsArray[1]["full_name"]);
            $disbursement->setValue('id_2', $guarantorsArray[1]["borrower_no"]);
            $disbursement->setValue('nic_2', $guarantorsArray[1]["nic"]);
            $disbursement->setValue('interest_rate_2', $interestRate2);
            $disbursement->setValue('loan_amount_2', $loanAmount2);
            $disbursement->setValue('loan_duration_2', $loanDuration2);
            $disbursement->setValue('group_no_2', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_3', $guarantorsArray[2]["full_name"]);
            $disbursement->setValue('id_3', $guarantorsArray[2]["borrower_no"]);
            $disbursement->setValue('nic_3', $guarantorsArray[2]["nic"]);
            $disbursement->setValue('interest_rate_3', $interestRate3);
            $disbursement->setValue('loan_amount_3', $loanAmount3);
            $disbursement->setValue('loan_duration_3', $loanDuration3);
            $disbursement->setValue('group_no_3', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_4', '');
            $disbursement->setValue('id_4', '');
            $disbursement->setValue('nic_4', '');
            $disbursement->setValue('interest_rate_4', '');
            $disbursement->setValue('loan_amount_4', '');
            $disbursement->setValue('loan_duration_4', '');
            $disbursement->setValue('group_no_4', '');

            $disbursement->setValue('name_5', '');
            $disbursement->setValue('id_5', '');
            $disbursement->setValue('nic_5', '');
            $disbursement->setValue('interest_rate_5', '');
            $disbursement->setValue('loan_amount_5', '');
            $disbursement->setValue('loan_duration_5', '');
            $disbursement->setValue('group_no_5', '');
            $sum=($loanAmount1+$loanAmount2+$loanAmount3);
        } else if ($count == 2) {

            $loanAmount1 = $allLoans[0]["loan_amount"];
            $loanAmount2 = $allLoans[1]["loan_amount"];

            $interestRate1 = $allLoans[0]["interest_rate"];
            $interestRate2 = $allLoans[1]["interest_rate"];

            $loanDuration1 = $allLoans[0]["loan_duration"];
            $loanDuration2 = $allLoans[1]["loan_duration"];

            $disbursement->setValue('branch', $guarantorsArray[0]["branch"]);
            $disbursement->setValue('center', $guarantorsArray[0]["center"]);
            $disbursement->setValue('release_date', $loanArray["release_date"]);

            $disbursement->setValue('name_1', $guarantorsArray[0]["full_name"]);
            $disbursement->setValue('id_1', $guarantorsArray[0]["borrower_no"]);
            $disbursement->setValue('nic_1', $guarantorsArray[0]["nic"]);
            $disbursement->setValue('interest_rate_1', $interestRate1);
            $disbursement->setValue('loan_amount_1', $loanAmount1);
            $disbursement->setValue('loan_duration_1', $loanDuration1);
            $disbursement->setValue('group_no_1', $guarantorsArray[0]["group_no"]);
            $disbursement->setValue('name_2', $guarantorsArray[1]["full_name"]);
            $disbursement->setValue('id_2', $guarantorsArray[1]["borrower_no"]);
            $disbursement->setValue('nic_2', $guarantorsArray[1]["nic"]);
            $disbursement->setValue('interest_rate_2', $interestRate2);
            $disbursement->setValue('loan_amount_2', $loanAmount2);
            $disbursement->setValue('loan_duration_2', $loanDuration2);
            $disbursement->setValue('group_no_2', $guarantorsArray[0]["group_no"]);

            $disbursement->setValue('name_3', '');
            $disbursement->setValue('id_3', '');
            $disbursement->setValue('nic_3', '');
            $disbursement->setValue('interest_rate_3', '');
            $disbursement->setValue('loan_amount_3', '');
            $disbursement->setValue('loan_duration_3', '');
            $disbursement->setValue('group_no_3', '');

            $disbursement->setValue('name_4', '');
            $disbursement->setValue('id_4', '');
            $disbursement->setValue('nic_4', '');
            $disbursement->setValue('interest_rate_4', '');
            $disbursement->setValue('loan_amount_4', '');
            $disbursement->setValue('loan_duration_4', '');
            $disbursement->setValue('group_no_4', '');

            $disbursement->setValue('name_5', '');
            $disbursement->setValue('id_5', '');
            $disbursement->setValue('nic_5', '');
            $disbursement->setValue('interest_rate_5', '');
            $disbursement->setValue('loan_amount_5', '');
            $disbursement->setValue('loan_duration_5', '');
            $disbursement->setValue('group_no_5', '');
            $sum=($loanAmount1+$loanAmount2);
        }

        $disbursement->setValue('total', $sum);
        $disbursement->setValue('center_no', $center_no);


        $disbursementName = $loan->borrower_no . " Disbursement";
        $disbursement->saveAs($disbursementName . '.docx');
        return response()->download($disbursementName . '.docx')->deleteFileAfterSend(true);
    }

    public function exportGuarantorBond($id)
    {
        $loan = Loan::where('id', $id)->first();
        $borrower = Borrower::where('borrower_no', $loan->borrower_no)->first();
        $allMembers = Borrower::where('branch', $borrower->branch)
            ->where('center', $borrower->center)->where('group_no', $borrower->group_no)->where('status',$borrower->status='active')->get();


        $allMembersArray = json_decode($allMembers, true);

        if (($loan->borrower_no) == $allMembersArray[0]["borrower_no"]) {
            unset($allMembersArray[0]);
        } else if (($loan->borrower_no) == $allMembersArray[1]["borrower_no"]) {
            unset($allMembersArray[1]);
        } else if (($loan->borrower_no) == $allMembersArray[2]["borrower_no"]) {
            unset($allMembersArray[2]);
        } else if (($loan->borrower_no) == $allMembersArray[3]["borrower_no"]) {
            unset($allMembersArray[3]);
        }

        $guarantorsArray = array_values($allMembersArray);

        $count = count($guarantorsArray);

        $guarantorBond = new TemplateProcessor('word-template/GuarantorBond.docx');

        $digit = new NumberFormatter("en", NumberFormatter::SPELLOUT);
        $loan_amount_spell = $digit->format($loan->loan_amount);

        if ($count == 4) {

            $name_1 = $guarantorsArray[0]["full_name"];
            $name_2 = $guarantorsArray[1]["full_name"];
            $name_3 = $guarantorsArray[2]["full_name"];
            $name_4 = $guarantorsArray[3]["full_name"];

            $address_1 = $guarantorsArray[0]["address"];
            $address_2 = $guarantorsArray[1]["address"];
            $address_3 = $guarantorsArray[2]["address"];
            $address_4 = $guarantorsArray[3]["address"];

            $nic_1 = $guarantorsArray[0]["nic"];
            $nic_2 = $guarantorsArray[1]["nic"];
            $nic_3 = $guarantorsArray[2]["nic"];
            $nic_4 = $guarantorsArray[3]["nic"];

            $guarantorBond->setValue('name_1', $name_1);
            $guarantorBond->setValue('address_1', $address_1);
            $guarantorBond->setValue('nic_1', $nic_1);

            $guarantorBond->setValue('name_2', $name_2);
            $guarantorBond->setValue('address_2', $address_2);
            $guarantorBond->setValue('nic_2', $nic_2);

            $guarantorBond->setValue('name_3', $name_3);
            $guarantorBond->setValue('address_3', $address_3);
            $guarantorBond->setValue('nic_3', $nic_3);

            $guarantorBond->setValue('name_4', $name_4);
            $guarantorBond->setValue('address_4', $address_4);
            $guarantorBond->setValue('nic_4', $nic_4);
        } else if ($count == 3) {

            $name_1 = $guarantorsArray[0]["full_name"];
            $name_2 = $guarantorsArray[1]["full_name"];
            $name_3 = $guarantorsArray[2]["full_name"];

            $address_1 = $guarantorsArray[0]["address"];
            $address_2 = $guarantorsArray[1]["address"];
            $address_3 = $guarantorsArray[2]["address"];

            $nic_1 = $guarantorsArray[0]["nic"];
            $nic_2 = $guarantorsArray[1]["nic"];
            $nic_3 = $guarantorsArray[2]["nic"];

            $guarantorBond->setValue('name_1', $name_1);
            $guarantorBond->setValue('address_1', $address_1);
            $guarantorBond->setValue('nic_1', $nic_1);

            $guarantorBond->setValue('name_2', $name_2);
            $guarantorBond->setValue('address_2', $address_2);
            $guarantorBond->setValue('nic_2', $nic_2);

            $guarantorBond->setValue('name_3', $name_3);
            $guarantorBond->setValue('address_3', $address_3);
            $guarantorBond->setValue('nic_3', $nic_3);

            $guarantorBond->setValue('name_4', '');
            $guarantorBond->setValue('address_4', '');
            $guarantorBond->setValue('nic_4', '');
        } else if ($count == 2) {

            $name_1 = $guarantorsArray[0]["full_name"];
            $name_2 = $guarantorsArray[1]["full_name"];

            $address_1 = $guarantorsArray[0]["address"];
            $address_2 = $guarantorsArray[1]["address"];

            $nic_1 = $guarantorsArray[0]["nic"];
            $nic_2 = $guarantorsArray[1]["nic"];

            $guarantorBond->setValue('name_1', $name_1);
            $guarantorBond->setValue('address_1', $address_1);
            $guarantorBond->setValue('nic_1', $nic_1);

            $guarantorBond->setValue('name_2', $name_2);
            $guarantorBond->setValue('address_2', $address_2);
            $guarantorBond->setValue('nic_2', $nic_2);

            $guarantorBond->setValue('name_3', '');
            $guarantorBond->setValue('address_3', '');
            $guarantorBond->setValue('nic_3', '');

            $guarantorBond->setValue('name_4', '');
            $guarantorBond->setValue('address_4', '');
            $guarantorBond->setValue('nic_4', '');
        } else if ($count == 1) {

            $name_1 = $guarantorsArray[0]["full_name"];

            $address_1 = $guarantorsArray[0]["address"];

            $nic_1 = $guarantorsArray[0]["nic"];

            $guarantorBond->setValue('name_1', $name_1);
            $guarantorBond->setValue('address_1', $address_1);
            $guarantorBond->setValue('nic_1', $nic_1);


            $guarantorBond->setValue('name_2', '');
            $guarantorBond->setValue('address_2', '');
            $guarantorBond->setValue('nic_2', '');


            $guarantorBond->setValue('name_3', '');
            $guarantorBond->setValue('address_3', '');
            $guarantorBond->setValue('nic_3', '');


            $guarantorBond->setValue('name_4', '');
            $guarantorBond->setValue('address_4', '');
            $guarantorBond->setValue('nic_4', '');
        }

        $guarantorBond->setValue('borrower_no', $loan->borrower_no);
        $guarantorBond->setValue('release_date', $loan->release_date);
        $guarantorBond->setValue('loan_amount', $loan->loan_amount);
        $guarantorBond->setValue('loan_amount_spell', $loan_amount_spell);
        $guarantorBond->setValue('address', $borrower->address);
        $guarantorBond->setValue('full_name', $borrower->full_name);
        $guarantorBond->setValue('nic', $borrower->nic);
        $guarantorBond->setValue('application_no', $borrower->application_no);

        $guarantorBondName = $borrower->borrower_no . " Guarantor Bond";
        $guarantorBond->saveAs($guarantorBondName . '.docx');
        return response()->download($guarantorBondName . '.docx')->deleteFileAfterSend(true);
    }
}
