<?php

namespace App\Http\Controllers;

use App\Repayment;
use Illuminate\Http\Request;

class DueController extends Controller
{
    public function index(){
         $due=Repayment::where('paid_amount',0)->get();
        return view('due.index',compact('due'));
    }
}
