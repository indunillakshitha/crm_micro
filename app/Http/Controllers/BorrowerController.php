<?php

namespace App\Http\Controllers;

use App\Borrower;
use App\Branch;
use App\Center;
use App\CenterLeader;
use App\Loan;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB as FacadesDB;

class BorrowerController extends Controller
{

    public function index()
    {

        // borrowers count
        $borrowersCount = count(Borrower::all());
        $borrowersCount++;

        // get branches
        $branches = Branch::where('branch_name',Auth::user()->branch)->get();
        // get centers
        $centers = Center::all();

        //       return view('Borrower.borrowerindex');
        $borrowers = Borrower::latest()->paginate(1000);
        return view('Borrower.borrowerindex', compact('borrowers', 'borrowersCount', 'branches', 'centers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
    }

    public function all()
    {
        $borrowers = Borrower::where('visibility',1)->get();
        return view('Borrower.borrowersindex', compact('borrowers'));
    }

    public function store(Request $request)
    {

        $request->validate([

            // 'application_no'  => 'required | numeric',
            'branch' => 'required',
            // 'branch_no' => 'required',
            'center' => 'required',
            'group_no'  => 'required',
            'borrower_no'  => 'required',
            'full_name' => 'required ',
            'nic' => 'required',
            'birthday' => 'required',
            'gender' => 'required',
            'address' => 'required',
            // 'lp_no' => 'required | numeric',
            'mobile_no' => 'required | numeric',
            // 'email' => 'required ',
            // 'occupation' => 'required',
            // 'requested_amount' => 'required | numeric',
            // 'duration_weeks' => 'required | numeric',
            // 'loan_stage' => 'required'
        ]);

        Borrower::create($request->all());

        if ($request->remarks == 'Center Leader') {
            $leader = new CenterLeader;
            $leader->borrower_id = $request->borrower_no;
            $leader->branch = $request->branch;
            $leader->center = $request->center;
            $leader->save();
        }

        return redirect()->route('borrower.index')
            ->with('success', 'Borrower created successfully.');
    }


    public function show($id)
    {
        $req = $id;
        $borrower = Borrower::where('borrower_no', $req)->first();
        return view('Borrower.viewborrower', compact('borrower'));
    }


    public function edit($id)
    {
        $req = $id;
        $borrower = Borrower::where('borrower_no', $req)->first();
        return view('Borrower.editborrower', compact('borrower'));
    }


    public function update(Request $request, Borrower $borrower)
    {

        $borrower->update($request->all());
        return redirect()->route('borrower.index')
            ->with('success', 'Borrower updated successfully');
    }

    public function destroy(Borrower $borrower)
    {
        $borrower->delete();

        return redirect()->route('borrower.index')
            ->with('success', 'Borrower deleted successfully');
    }

    public function getCenterName(Request $request)
    {
        $center = Center::where('center_name', $request->name)->first();
        $center = $center->center_no;
        return response()->json($center);
    }

    public function getRightCenters(Request $request)
    {
        // $branch = Branch::where('branch_name', $request->branch)->first();
        $centers = Center::where('branch_no', $request->branch)->get();
        return response()->json($centers);
    }

    public function getNIC(request $request)
    {
        $nic = Borrower::where('nic', $request->nic)->where('is_deactivated',0)->first();

        if ($nic) {
            return response()->json('NIC already registered');
        } else {
            return response()->json('NIC available');
        }
    }

    public function getBorrowersCount(Request $request)
    {
        $borrowers = Borrower::where('branch', $request->branch)
            ->where('center', $request->center)
            // ->where('group_no', $request->group)
            ->get();


        return response()->json($borrowers);
    }
    public function getborrowerscountofgroup(Request $request)
    {

        $groupCount = Borrower::where('branch', $request->branch)
            ->where('center', $request->center)
            ->where('group_no', $request->group)
            ->where('visibility',1)
            ->get();

        return response()->json($groupCount);
    }
    public function view($id)
    {
        $loan = Loan::where('id', $id)->first();
        $borrower = Borrower::where('borrower_no', $loan->borrower_no)->first();
        return view('Loans.showLoan', compact('loan', 'borrower'));
    }

    public function addBorrowerAjax(Request $request)
    {

        Borrower::create($request->all());

        if ($request->remarks == 'Center Leader') {
            $leader = new CenterLeader;
            $leader->borrower_id = $request->borrower_no;
            $leader->branch = $request->branch;
            $leader->center = $request->center;
            $leader->save();
        }
        return response()->json('borrower_added');
    }


    public function capital()
    {
        // $borrowers=Borrower::all();
        // foreach($borrowers as $borrowe)
        // Borrower::where('status','active')->update(['full_name'=>ucwords($borrowe->full_name)]);

        $bs = DB::table('borrowers')->get();
        foreach ($bs as $b) {
            $name = $b->address;
            $lc = strtolower($name);
            $uppercased = ucwords($lc);
            // $b->full_name=$uppercased;
            // $b->save();

            Borrower::where('id', $b->id)->update(['address' => $uppercased]);
        }

        return $uppercased;
    }

    public function addBorrowerDeleteBorrower($branch, $center, $group)
    {
        $borrowers = Borrower::where('branch', $branch)
            ->where('center', $center)
            ->where('group_no', $group)
            ->where('visibility', 1)
            ->get();
        return view('Borrower.deleteGroupBorrower', compact('borrowers'));
    }

    public function deleteBorrowerChangeVisibility($id){
        $borrower = Borrower::where('id', $id)->first();

        $status =Loan::where('borrower_no',$borrower->borrower_no)->where('status','!=','SETTLED')->get();
        // return count($status);
        if(count($status) == 0){

            $borrower->visibility = 0;
            $borrower->save();
            return redirect()->back();
        } else{
            return redirect()->back()->with('error', 'Sorry this borrower has incomplete');

        }

    }

    public function checkGuardianNIC(Request $request){
        $nic = Borrower::where('lp_no', $request->nic)->first();

        if ($nic) {
            return response()->json('NIC already registered');
        } else {
            return response()->json('NIC available');
        }
    }
}
