<?php

namespace App\Http\Controllers;

use App\Center;
use App\Shedule;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SheduleController extends Controller
{

    public function index()
    {
        $shedules =Shedule::all();
        return view('shedule.index',compact('shedules'));
    }


    public function create()
    {
        $users= User::where('access_level','agent')->get();
        $centers=Center::where('branch_no',Auth::user()->branch)->get();
        return view('shedule.create',compact('users','centers'));
    }


    public function store(Request $request)
    {
        $shedule=new Shedule();
        $shedule['date']=$request->date;
        $shedule['excecutive_id']=$request->excecutive_id;
        $shedule['center_id']=$request->center_id;
        $user=User::where('id',$request->excecutive_id)->first();
        $shedule['excecutive']=$user->name;
        $center=Center::where('center_no',$request->center_id)->first();
        $shedule['center']=$center->center_name;
        if(Shedule::where('date',$request->date)->where('center_id',$request->center_id)->first()){
            return redirect()->route('shedule.create')->with('error','Already Added');
        }else{
            $shedule->save();
            return redirect()->route('shedule.create')->with('success','Added Succcessfully');
        }

    }


    public function show(Shedule $shedule)
    {
        //
    }


    public function edit(Shedule $shedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shedule  $shedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shedule $shedule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shedule  $shedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shedule $shedule)
    {
        //
    }
}
