<?php

namespace App\Http\Controllers;

use App\Category;
use App\Loan;
use App\Repayment;
use App\Shedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NewRepaymentController extends Controller
{
    public function __construct()
    {

        date_default_timezone_set('Asia/Colombo');

    }
    public function newRepayments(){

         $today=Carbon::now()->isoFormat('dddd');
         $centers_not_completed= Shedule::where('excecutive_id',Auth::user()->id)
                        ->where('collected_loan_count','>',0)
                        ->get();
        if(count($centers_not_completed)>0){
            $centers=$centers_not_completed;
        }else{
          $centers= Shedule::where('excecutive_id',Auth::user()->id)
                        ->where('collected_loan_count',0)
                        ->where('date',$today)
                        ->where('last_visited_date_index','!=',Carbon::now()->isoFormat('DDD'))
                        ->get();
        }
        return view('new_repayments.center_collection',compact('centers'));
    }

    public function newRepaymentGetLoans(Request $request)
    {

        $center= Shedule::where('excecutive_id',Auth::user()->id)
                        ->where('center',$request->center)
                        ->first();
        $today_index=Carbon::now()->isoFormat('DDD');
        $data = DB::table('loans')
            ->leftjoin('borrowers', 'borrowers.borrower_no', 'loans.borrower_no')
            ->select('borrowers.*', 'loans.*')
            ->where('loans.center', $request->center)
            ->where('loans.status', 'Payed')
            ->where('loans.last_paid_date','!=',$today_index)
            ->orderBy('borrowers.borrower_no')
            ->get();
        $center->collected_loan_count=count($data);
        $center->count_to_collect=count($data);
        $center->save();
        // return $data;
        return response()->json($data);
    }


    public function newRepaymentCollect(Request $request)
    {
        $today_index=Carbon::now()->isoFormat('DDD');
        $loan = Loan::find($request->id);
        $loan->payments_count = ++$loan->payments_count;
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_paid_date = $today_index;

        // $due_amount = $loan->loan_amount - $loan->paid;
        $prev_total_payed = $loan->paid;

        $repayment = new Repayment();
        $repayment->logged_user = Auth::user()->name;
        $repayment->borrower_nic = $loan->nic;
        $repayment->branch = $loan->branch;
        $repayment->center = $loan->center;
        $repayment->loan_id = $loan->id;
        $repayment->loan_amount = $loan->loan_amount;
        $repayment->paid_amount = $request->payment;
        $repayment->due_amount = $loan->due - $request->payment;
        $repayment->total_payed = $prev_total_payed + $request->payment;
        $repayment->installment = $loan->instalment;
        $repayment->payment_date_index = Carbon::now()->isoFormat('DDD');
        $repayment->payment_date = Carbon::now()->isoFormat('YYYY-MM-DD');
        $repayment->payment_date_full = Carbon::now()->isoFormat('M/D/YYYY');
        $repayment->year=Carbon::now()->year;
        $repayment->month=Carbon::now()->month;
        $repayment->installment_no = $loan->payments_count;
        $repayment->status = 'COLLECTED'; #warike gatta
        $repayment->type = 'CASH';
        $repayment->method_status = 'CENTER COLLECTION';



        if($request->payment==0){
            $loan->arrears+=$loan->instalment;
            $repayment->arrears=$loan->arrears;
        }

        $loan->due = $repayment->due_amount;
        $loan->paid = $repayment->total_payed;

        $category=Category::where('fixed_ammount',$repayment->loan_amount)->first();


        $interest_amount=$repayment->installment * $category->category_rate/100;
        $repayment->inter=$interest_amount;//interest collection
        $repayment->capital=$repayment->paid_amount-$interest_amount;//capital collection

         if($repayment->installment < $repayment->paid_amount){
                 $repayment->over_paid_amount = $repayment->paid_amount - $repayment->installment ;//over paid amount
         }elseif($repayment->installment > $repayment->paid_amount){
                $repayment->less_paid_amount = $repayment->installment - $repayment->paid_amount;//less paid amount
        }

        $shedule=Shedule::where('excecutive_id',Auth::user()->id)
                ->where('center',$repayment->center)
                ->first();
        $shedule->collected_loan_count-=1;
        $shedule->last_visited_date_index=$today_index;
        $shedule->last_visited_date=Carbon::now();


        if ($loan->due <= 0) {
            $loan->status = 'SETTLED';
            $repayment->status = 'SETTLED';
        }

        if (!Repayment::where('loan_id', $repayment->loan_id)->where('payment_date', $repayment->payment_date)->first()) {
            $repayment->save();
            $loan->save();
            $shedule->save();

        }


        $centerTotal = 0;
        $centerRepayments = Repayment::where('center', $loan->center)
            ->where('payment_date_index', $repayment->payment_date_index)
            ->get();

        foreach ($centerRepayments as $r) {
            $centerTotal += $r->paid_amount;
        }


        return response()->json($centerTotal);
    }

    public function newRepaymentNotCollect(Request $request)
    {
        $today_index=Carbon::now()->isoFormat('DDD');
        $loan = Loan::find($request->id);
        $loan->payments_count = ++$loan->payments_count;
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_paid_date = $today_index;

        // $due_amount = $loan->loan_amount - $loan->paid;
        $prev_total_payed = $loan->paid;

        $repayment = new Repayment();
        $repayment->logged_user = Auth::user()->name;
        $repayment->borrower_nic = $loan->nic;
        $repayment->branch = $loan->branch;
        $repayment->center = $loan->center;
        $repayment->loan_id = $loan->id;
        $repayment->loan_amount = $loan->loan_amount;
        $repayment->paid_amount = 0;
        $repayment->due_amount = $loan->due ;
        $repayment->total_payed = $prev_total_payed + 0;
        $repayment->installment = $loan->instalment;
        $repayment->payment_date_index = Carbon::now()->isoFormat('DDD');
        $repayment->payment_date = Carbon::now()->isoFormat('YYYY-M-D');
        $repayment->payment_date_full = Carbon::now()->isoFormat('M/D/YYYY');
        $repayment->year=Carbon::now()->year;
        $repayment->month=Carbon::now()->month;
        $repayment->installment_no = $loan->payments_count;
        $repayment->status = 'COLLECTED'; #warike gatta
        $repayment->type = 'CASH';
        $repayment->method_status = 'CENTER COLLECTION';
        $repayment->borrower_no = $loan->borrower_no;
        $repayment->week_index = Carbon::now()->isoFormat('W');


            $loan->arrears+=$loan->instalment;
            $repayment->arrears=$loan->arrears;

        $loan->due = $repayment->due_amount;
        $loan->paid = $repayment->total_payed;

        $category=Category::where('fixed_ammount',$repayment->loan_amount)->first();


        $interest_amount=$repayment->installment * $category->category_rate/100;
        $repayment->inter=$interest_amount;//interest collection
        $repayment->capital=$repayment->paid_amount-$interest_amount;//capital collection

         if($repayment->installment < $repayment->paid_amount){
                 $repayment->over_paid_amount = $repayment->paid_amount - $repayment->installment ;//over paid amount
         }elseif($repayment->installment > $repayment->paid_amount){
                $repayment->less_paid_amount = $repayment->installment - $repayment->paid_amount;//less paid amount
        }

        $shedule=Shedule::where('excecutive_id',Auth::user()->id)
                ->where('center',$repayment->center)
                ->first();
        $shedule->collected_loan_count-=1;
        $shedule->last_visited_date_index=$today_index;
        $shedule->last_visited_date=Carbon::now();


        if ($loan->due <= 0) {
            $loan->status = 'SETTLED';
            $repayment->status = 'SETTLED';
        }

        if (!Repayment::where('loan_id', $repayment->loan_id)->where('payment_date', $repayment->payment_date)->first()) {
            $repayment->save();
            $loan->save();
            $shedule->save();

        }


        $centerTotal = 0;
        $centerRepayments = Repayment::where('center', $loan->center)
            ->where('payment_date_index', $repayment->payment_date_index)
            ->get();

        foreach ($centerRepayments as $r) {
            $centerTotal += $r->paid_amount;
        }


        return response()->json($centerTotal);
    }
    public function TempNotCollect(Request $request)
    {

        $today_index=Carbon::now()->isoFormat('DDD');
        $loan = Loan::find($request->id);
        $loan->payments_count = ++$loan->payments_count;
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_paid_date = $today_index;


        // $loan->last_paid_date = $today_index;

        // $due_amount = $loan->loan_amount - $loan->paid;
        $prev_total_payed = $loan->paid;

        $repayment = new Repayment();
        $repayment->logged_user = Auth::user()->name;
        $repayment->borrower_nic = $loan->nic;
        $repayment->branch = $loan->branch;
        $repayment->center = $loan->center;
        $repayment->loan_id = $loan->id;
        $repayment->loan_amount = $loan->loan_amount;
        $repayment->paid_amount = 0;
        $repayment->due_amount = $loan->due - 0;
        $repayment->total_payed = $prev_total_payed + 0;
        $repayment->installment = $loan->instalment;
        $repayment->payment_date_index = Carbon::now()->isoFormat('DDD');
        $repayment->payment_date = Carbon::now()->isoFormat('YYYY-MM-DD');
        $repayment->payment_date_full = Carbon::now()->isoFormat('M/D/YYYY');
        $repayment->year=Carbon::now()->year;
        $repayment->month=Carbon::now()->month;
        $repayment->installment_no = $loan->payments_count;
        $repayment->status = 'COLLECTED'; #warike gatta
        $repayment->type = 'CASH';
        $repayment->method_status = 'NOT PAID';
        $repayment->week_index = Carbon::now()->isoFormat('W');



        $loan->arrears+=$loan->instalment;
        $repayment->arrears=$loan->arrears;

        $loan->due = $repayment->due_amount;
        $loan->paid = $repayment->total_payed;

        $category=Category::where('fixed_ammount',$repayment->loan_amount)->first();


        $interest_amount=$repayment->installment * $category->category_rate/100;
        $repayment->inter=$interest_amount;//interest collection
        $repayment->capital=$repayment->paid_amount-$interest_amount;//capital collection

         if($repayment->installment < $repayment->paid_amount){
                 $repayment->over_paid_amount = $repayment->paid_amount - $repayment->installment ;//over paid amount
         }elseif($repayment->installment > $repayment->paid_amount){
                $repayment->less_paid_amount = $repayment->installment - $repayment->paid_amount;//less paid amount
        }

        // $shedule=Shedule::where('excecutive_id',Auth::user()->id)
        //         ->where('center',$repayment->center)
        //         ->first();
        // $shedule->collected_loan_count-=1;
        // $shedule->last_visited_date_index=$today_index;
        // $shedule->last_visited_date=Carbon::now();


        // if ($loan->due <= 0) {
        //     $loan->status = 'SETTLED';
        //     $repayment->status = 'SETTLED';
        // }

        if (!Repayment::where('loan_id', $repayment->loan_id)
        ->where('payment_date', $repayment->payment_date)
        ->where('year', Carbon::now()->year)
        ->first()) {
            $repayment->save();
            $loan->save();
            // $shedule->save();

        }

        return response()->json("not paid set");
    }


    public function resetCenters(){
       $shedules =Shedule::where('excecutive_id',Auth::user()->id)
                        ->where('collected_loan_count','>',0)
                        ->where('collected_loan_count','==','count_to_collect')
                        ->update(['collected_loan_count'=>'0','last_visited_date_index'=>'0']);
         return redirect()->back();
    }

    public function npReport(){
        $not_paids = Repayment::where('method_status','NOT PAID')
                                ->whereBetween('repayments.created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
                                ->where('repayments.branch', Auth::user()->branch)
                                ->select(DB::raw("sum(installment) as total_due"),DB::raw("count(installment) as npc"),'repayments.center')
                                ->groupBy('repayments.center')
                                ->get();

        return view('new_repayments.not_paids',compact('not_paids'));
    }
    public function npCount(){
        return $not_paids = Repayment::where('method_status','NOT PAID') ->where('repayments.branch', Auth::user()->branch)
                                ->count();

    }

    public function notPaidCustomers(Request $request){
        $today_index=Carbon::now()->isoFormat('DDD');
        $data = DB::table('loans')
            ->leftjoin('borrowers', 'borrowers.borrower_no', 'loans.borrower_no')
            ->select('borrowers.*', 'loans.*')
            ->where('loans.center', $request->center)
            ->where('loans.status', 'Payed')
            ->where('loans.last_paid_date','!=',$today_index)
            ->orderBy('borrowers.borrower_no')
            ->get();
        return response()->json($data);
    }
    public function setNotPaids(Request $request){
        $today_index=Carbon::now()->isoFormat('DDD');
        $data = DB::table('loans')
            ->leftjoin('borrowers', 'borrowers.borrower_no', 'loans.borrower_no')
            ->select('borrowers.*', 'loans.*')
            ->where('loans.center', $request->center)
            ->where('loans.status', 'Payed')
            ->where('loans.last_paid_date','!=',$today_index)
            ->get();
        foreach($data as $dat){
            $today_index=Carbon::now()->isoFormat('DDD');
            $loan = Loan::find($dat->id);
            // $loan->payments_count = ++$loan->payments_count;
            $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
            $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
            // $loan->last_paid_date = $today_index;

            // $due_amount = $loan->loan_amount - $loan->paid;
            $prev_total_payed = $loan->paid;

            $repayment = new Repayment();
            $repayment->logged_user = Auth::user()->name;
            $repayment->borrower_nic = $loan->nic;
            $repayment->branch = $loan->branch;
            $repayment->center = $loan->center;
            $repayment->loan_id = $loan->id;
            $repayment->loan_amount = $loan->loan_amount;
            $repayment->paid_amount = 0;
            $repayment->due_amount = $loan->due - 0;
            $repayment->total_payed = $prev_total_payed + 0;
            $repayment->installment = $loan->instalment;
            $repayment->payment_date_index = Carbon::now()->isoFormat('DDD');
            $repayment->payment_date = Carbon::now()->isoFormat('YYYY-MM-DD');
            $repayment->payment_date_full = Carbon::now()->isoFormat('M/D/YYYY');
            $repayment->year=Carbon::now()->year;
            $repayment->month=Carbon::now()->month;
            $repayment->installment_no = $loan->payments_count;
            $repayment->status = 'COLLECTED'; #warike gatta
            $repayment->type = 'CASH';
            $repayment->method_status = 'NOT PAID';
            $repayment->week_index = Carbon::now()->isoFormat('W');



            $loan->arrears+=$loan->instalment;
            $repayment->arrears=$loan->arrears;

            $loan->due = $repayment->due_amount;
            $loan->paid = $repayment->total_payed;

            $category=Category::where('fixed_ammount',$repayment->loan_amount)->first();


            $interest_amount=$repayment->installment * $category->category_rate/100;
            $repayment->inter=$interest_amount;//interest collection
            $repayment->capital=$repayment->paid_amount-$interest_amount;//capital collection

             if($repayment->installment < $repayment->paid_amount){
                     $repayment->over_paid_amount = $repayment->paid_amount - $repayment->installment ;//over paid amount
             }elseif($repayment->installment > $repayment->paid_amount){
                    $repayment->less_paid_amount = $repayment->installment - $repayment->paid_amount;//less paid amount
            }

            $shedule=Shedule::where('excecutive_id',Auth::user()->id)
                    ->where('center',$repayment->center)
                    ->first();
            $shedule->collected_loan_count-=1;
            $shedule->last_visited_date_index=$today_index;
            $shedule->last_visited_date=Carbon::now();


            // if ($loan->due <= 0) {
            //     $loan->status = 'SETTLED';
            //     $repayment->status = 'SETTLED';
            // }

            if (!Repayment::where('loan_id', $repayment->loan_id)
            ->where('payment_date', $repayment->payment_date)
            ->where('year', Carbon::now()->year)
            ->first()) {
                $repayment->save();
                $loan->save();
                $shedule->save();

            }


            // $centerTotal = 0;
            // $centerRepayments = Repayment::where('center', $loan->center)
            //     ->where('payment_date_index', $repayment->payment_date_index)
            //     ->get();
        }
        return response()->json($data);
    }


    public function notPaidMore($id){


     $not_paids = Repayment::where('method_status','NOT PAID')
                // ->leftjoin('loans','loans.id','repayments.loan_id')
                ->whereBetween('repayments.created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
                ->where('repayments.branch', Auth::user()->branch)
                ->where('repayments.center', $id)
                ->select('repayments.*')
                // ->select(DB::raw("sum(installment) as total_due"),DB::raw("count(installment) as npc"),'repayments.center')
                // ->groupBy('repayments.center')
                ->get()
                // ->count()
                ;

        $export_arr =[];
    foreach($not_paids as $not_paid){

         $loan=Loan::where('id',$not_paid->loan_id)->first();
         $start_date = Carbon::parse($loan->payment_starting_date);
         $end_date = Carbon::now()->isoFormat('YYYY-MM-DD');
         $date_difference = $start_date->diffInDays($end_date);
         $payment_count_to_be=number_format((int) $date_difference/7, 0);
         $payment_to_be=$payment_count_to_be* $loan->instalment;
         $payment_available=$loan->paid;
         $arreas=0;
         $over_paid=0;
         if($payment_to_be > $payment_available){
            $arreas=$payment_to_be - $payment_available;
            $not_paid_count=number_format((int) $arreas/$loan->instalment,0);
         }else{
            $over_paid= $payment_available-$payment_to_be ;
         }



         array_push($export_arr, [
             $loan,
             $arreas,
             $over_paid,
             $not_paid_count,
         ]);

    }

    // $export_arr=json_encode($export_arr);
    return view('new_repayments.not_paid_see_more',compact('export_arr'));

    // return $export_arr;

    }
}
