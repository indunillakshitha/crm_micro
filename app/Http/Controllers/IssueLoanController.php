<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loan;
use App\Appraisal;
use App\Borrower;
use App\Approving;
use App\Center;
use App\LoanPaymentDetails;
use App\Repayment;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class IssueLoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $issued = 'Issued';

        $borrowers = Borrower::all();
        $apprasails = Appraisal::all();
         $loans = Loan::where('status', $issued)
                        ->orderBy('created_at', 'desc')
                        ->groupBy('loans.center','loans.release_date')
                        ->where('loans.branch',Auth::user()->branch)
                        ->where('loans.visibility','1')
                        ->select('loans.center','loans.release_date',DB::raw("count(loan_amount) as loan_count"))
                        ->get();
        return view('IssueLoan.indexIssueLoan', compact('loans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function paymentMethod()
    {
        return view('IssueLoan.PaymentMethods');
    }

    public function singleIssue($id)
    {

        //  $loan = Loan::find($id);
          $pending_loans=Loan::where('status','Payed')->where('center',$id)->get();
          $pendings=Loan::where('status','Issued')
          ->where('center',$id)
          ->get();

          $arr = [];
          foreach($pendings as $p){
              if(Loan::where('status', 'Payed')->where('borrower_no', $p->borrower_no)){
                  $payed = Loan::where('status', 'Payed')->where('borrower_no', $p->borrower_no)->get();

                  foreach($payed as $paid){
                      array_push($arr, $paid);
                    }
                }
            }
    if(count($arr)>0){
            // return $arr;
            return view('Loans.settleForIssue',compact('arr'));
        }
        else{
            // $amount = $loan->loan_amount;
            $pendings=Loan::where('status','Issued')
            ->where('center',$id)
            ->get();
             $amount=0;
             foreach($pendings as $pending){
                 $amount+=$pending->loan_amount;
             }

            return view('IssueLoan.singlePaymentMethods', compact('pendings','amount'));
        }
    }

    public function groupIssue($id)
    {
        // $group = Loan::where('group_no', $id)->where('status', 'Issued')->get();
        // $groupNo = $id;
        $loan = Loan::find($id);
        $group = Loan::where('branch', $loan->branch)
            ->where('center', $loan->center)
            ->where('group_no', $loan->group_no)
            ->get();

        $amount = 0;
        foreach ($group as $member) {
            $amount += $member->loan_amount;
        }

        return view('IssueLoan.groupPaymentMethods', compact('amount', 'loan'));
    }

    public function issueCenter()
    {
        $branch = Auth::user()->branch;
        $centers = Center::where('branch_no', $branch)->get();
        return view('IssueLoan.issueCenter', compact('branch', 'centers'));
    }

    public function markAsPayedSingle(Request $request, $id)
    {
         $request;
        // $loan = Loan::find($id);
$loans=Loan::where('center',$id)->where('status','Issued')->get();
foreach($loans as $loan){


        $loan->status = 'Payed';
        $loan->payment_starting_date = $request->due;
        $loan->payment_cycle_date = Carbon::parse($request->due)->isoFormat('ddd');

        $loan->payment_starting_date_index = Carbon::now()->isoFormat('DDD');

        $duration = $loan->loan_duration;
        $loan->due_date = $loan->payment_starting_date_index + (7 * $duration);

             if ($loan->due_date > 335) {
                $loan->due_month = "12";
            } else if ($loan->due_date > 305) {
                $loan->due_month = "11";
            } else if ($loan->due_date > 274) {
                $loan->due_month = "10";
            } else if ($loan->due_date > 244) {
                $loan->due_month = "09";
            } else if ($loan->due_date > 213) {
                $loan->due_month = "08";
            } else if ($loan->due_date > 182) {
                $loan->due_month = "07";
            } else if ($loan->due_date > 152) {
                $loan->due_month = "06";
            } else if ($loan->due_date > 121) {
                $loan->due_month = "05";
            } else if ($loan->due_date > 91) {
                $loan->due_month = "04";
            } else if ($loan->due_date > 60) {
                $loan->due_month = "03";
            } else if ($loan->due_date < 32) {
                $loan->due_month = "01";
            } else if ($loan->due_date > 31) {
                $loan->due_month = "02";
            }
        $loan->save();

        $borrower = Borrower::where('borrower_no', $loan->borrower_no)->first();
        $borrower->n_of_loans = ++$borrower->n_of_loans;
        $borrower->save();
        $repayment = new Repayment;
        $repayment->logged_user = Auth::user()->name;
        $repayment->borrower_nic = $loan->nic;
        $repayment->branch = $loan->branch;
        $repayment->center = $loan->center;
        $repayment->loan_id = $loan->id;
        $repayment->loan_amount = $loan->loan_amount;
        $repayment->payment_date_index = Carbon::parse($request->doc_date)->isoFormat('DDD');
        $repayment->payment_date = $request->doc_date;
        $repayment->payment_date_full = Carbon::parse($request->doc_date)->isoFormat('M/D/YYYY');
        $repayment->status = 'COLLECTED'; #warike gatta
        $repayment->type = 'CASH';
        $repayment->method_status = 'DOCUMENT';

        $loan->fees = $loan->loan_amount *3/100;
        $repayment->doc_charges = $loan->fees;

        $centerTotal = 0;
        $centerRepayments = Repayment::where('center', $loan->center)
            ->where('payment_date_index', $repayment->payment_date_index)
            ->where('method_status','DOCUMENT')
            ->get();

        foreach ($centerRepayments as $r) {
            $centerTotal += $r->paid_amount;
        }

            $repayment->save();
            $loan->save();

        LoanPaymentDetails::create($request->all());
    }
        return redirect('/issued');
    }

    public function markAsPayedGroup(Request $request, $id)
    {
        // $group = Loan::where('group_no', $id)->where('status', 'Issued')->get();
        $loan = Loan::find($id);
        $group = Loan::where('branch', $loan->branch)
            ->where('center', $loan->center)
            ->where('group_no', $loan->group_no)
            ->where('status', 'Issued')
            ->get();

        foreach ($group as $member) {
            $member->status = 'Payed';
            $member->payment_starting_date = $request->due;
            $member->payment_starting_date_index = Carbon::now()->isoFormat('DDD');
            $member->payment_cycle_date = Carbon::now()->isoFormat('dddd');
            $member->save();

            // $duration_type = $loan->duration_period;
            // $duration = $loan->loan_duration;

            // // $i = 0 ;
            // $date_index = $request->date_index;

            // $loan_amount = $loan->loan_amount+$loan->interest;
            // $installment = $loan->instalment;
            // // $due_amount = $loan_amount-$installment;
            // $due_amount = $loan_amount;
            // // $today_payment = $installment;

            // for($i = 0 ; $i < $duration; $i++){

            //     if ($date_index > 335) {
            //         $month = "12";
            //     } else if ($date_index > 305) {
            //         $month = "11";
            //     } else if ($date_index > 274) {
            //         $month = "10";
            //     } else if ($date_index > 244) {
            //         $month = "09";
            //     } else if ($date_index > 213) {
            //         $month = "08";
            //     } else if ($date_index > 182) {
            //         $month = "07";
            //     } else if ($date_index > 152) {
            //         $month = "06";
            //     } else if ($date_index > 121) {
            //         $month = "05";
            //     } else if ($date_index > 91) {
            //         $month = "04";
            //     } else if ($date_index > 60) {
            //         $month = "03";
            //     } else if ($date_index < 32) {
            //         $month = "01";
            //     } else if ($date_index > 31) {
            //         $month = "02";
            //     }

            //     $repayment = new Repayment;
            //     $repayment->loan_id = $member->id;
            //     $repayment->date_index = $date_index;
            //     $repayment->month = $month;
            //     $repayment->loan_amount = $loan_amount;
            //     $repayment->due_amount = $due_amount;
            //     $repayment->today_payment =$installment;
            //     $repayment->branch = $member->branch;
            //     $repayment->status ='PENDING';
            //     $repayment->save();

            //     if($duration_type == 'Weeks'){
            //         $date_index += 7;
            //     }

            //     $due_amount -= $installment;
            //     // $today_payment += $installment;

            // }
            $borrower = Borrower::where('borrower_no', $member->borrower_no)->first();
            $borrower->n_of_loans = ++$borrower->n_of_loans;
            $borrower->save();
        }

        LoanPaymentDetails::create($request->all());

        return redirect('/issued');
    }

    public function markAsPayedCenter(Request $request, $id)
    {
        $loan = Loan::find($id);
        $center = Loan::where('branch', $loan->branch)
            ->where('center', $loan->center)
            ->where('status', 'Issued')
            ->get();

        foreach ($center as $member) {
            $member->status = 'Payed';
            $member->due_date = $request->due;
            $member->save();

            // $duration_type = $loan->duration_period;
            // $duration = $loan->loan_duration;

            // // $i = 0 ;
            // $date_index = $request->date_index;

            // $loan_amount = $loan->loan_amount+$loan->interest;
            // $installment = $loan->instalment;
            // // $due_amount = $loan_amount-$installment;
            // $due_amount = $loan_amount;
            // // $today_payment = $installment;

            // for($i = 0 ; $i < $duration; $i++){

            //     if ($date_index > 335) {
            //         $month = "12";
            //     } else if ($date_index > 305) {
            //         $month = "11";
            //     } else if ($date_index > 274) {
            //         $month = "10";
            //     } else if ($date_index > 244) {
            //         $month = "09";
            //     } else if ($date_index > 213) {
            //         $month = "08";
            //     } else if ($date_index > 182) {
            //         $month = "07";
            //     } else if ($date_index > 152) {
            //         $month = "06";
            //     } else if ($date_index > 121) {
            //         $month = "05";
            //     } else if ($date_index > 91) {
            //         $month = "04";
            //     } else if ($date_index > 60) {
            //         $month = "03";
            //     } else if ($date_index < 32) {
            //         $month = "01";
            //     } else if ($date_index > 31) {
            //         $month = "02";
            //     }

            //     $repayment = new Repayment;
            //     $repayment->loan_id = $member->id;
            //     $repayment->date_index = $date_index;
            //     $repayment->month = $month;
            //     $repayment->loan_amount = $loan_amount;
            //     $repayment->due_amount = $due_amount;
            //     $repayment->today_payment =$installment;
            //     $repayment->branch = $member->branch;
            //     $repayment->status ='PENDING';
            //     $repayment->save();

            //     if($duration_type == 'Weeks'){
            //         $date_index += 7;
            //     }

            //     $due_amount -= $installment;
            //     // $today_payment += $installment;

            // }
            $borrower = Borrower::where('borrower_no', $member->borrower_no)->first();
            $borrower->n_of_loans = ++$borrower->n_of_loans;
            $borrower->save();
        }

        // LoanPaymentDetails::create($request->all());

        return redirect('/issuedcenter');
    }
}
