<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\Repayment;
use App\Shedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AgentDailySummeryController extends Controller
{
   public function index(){
    $apprasails = Appraisal::where('created_date_index', Carbon::now()->isoFormat('DDD'))->count();

    $centers_to_visit= Shedule::where('excecutive_id',Auth::user()->id)
            ->where('collected_loan_count',0)
            ->where('date',Carbon::now()->isoFormat('dddd'))
            ->where('last_visited_date_index','!=',Carbon::now()->isoFormat('DDD'))
            ->get();
    $centers_completed= Shedule::where('excecutive_id',Auth::user()->id)
            ->where('collected_loan_count',0)
            ->where('date',Carbon::now()->isoFormat('dddd'))
            ->where('last_visited_date_index',Carbon::now()->isoFormat('DDD'))
            ->get();
    $total_notpaids=Repayment::where('method_status','NOT PAID')->where('logged_user',Auth::user()->id)->sum('paid_amount');
    return view('agent.summery',compact('apprasails','centers_to_visit','centers_completed','total_notpaids'));
   }
}
