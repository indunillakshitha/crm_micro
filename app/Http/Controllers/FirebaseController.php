<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Kreait\Firebase;

use Kreait\Firebase\Factory;

use Kreait\Firebase\ServiceAccount;

use Kreait\Firebase\Database;
use Kreait\Firebase\Firestore;

class FirebaseController extends Controller
{
    // public function __construct(Firestore $firestore)
    // {
    //     $this->firestore = $firestore;
    // }
    public function index(){

		// $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/cantyapp-a3d23-firebase-adminsdk-xa58v-add90e9f1a.json');
		$factory  	 = (new Factory)

                        ->withServiceAccount(__DIR__.'/cantyapp-a3d23-firebase-adminsdk-xa58v-add90e9f1a.json')
                        ->withDatabaseUri('https://cantyapp-a3d23.firebaseio.com');
        $firestore = app('firebase.firestore');
        //  $firestore=$factory->createFirestore();
        // $firestore = $factory->createFirestore();
		// $database 		= $firebase->getDatabase();

		// $newPost 		  = $database

		//                     ->getReference('blog/posts')

		//                     ->push(['title' => 'Post title','body' => 'This should probably be longer.']);

		// echo"<pre>";

        // $firestore = app('firebase.firestore');
        $database = $firestore->database();

        return $database;

	}
}
