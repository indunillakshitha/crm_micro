<?php

namespace App\Http\Controllers\Loans;

use App\Appraisal;
use App\Http\Controllers\Controller;
use App\Loan;
use App\Borrower;
use App\BorrowerEarning;
use App\BorrowerExpense;
use App\BorrowerMainEarning;
use App\BorrowerOtherLoan;
use App\Category;
use App\Witness;
use App\CenterLeader;
use App\Center;
use App\Branch;
use App\Staff;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Sabberworm\CSS\Value\Size;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $loans = Loan::where('visibility',1)->get();
        return view('Loans.indexLoan', compact('loans'))
            // ->with('i', (request()->input('page', 1) - 1) * 5)
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $loanCount = count(Loan::all());
        // if($loanCount){
        //     $loanCount++;
        // } else{
        //     $loanCount = 1;
        // }

        // return view('Loans.addLoan')->with('loanCount', $loanCount);
        $branches = Branch::all();
        $borrowers = Borrower::all();
        $categories = Category::all();
        $centerLeaders = CenterLeader::all();
        $loans = Loan::all();
        return view('Loans.addLoan', compact('borrowers', 'categories', 'loans', 'centerLeaders', 'branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([


        //     'borrower_no' => 'required',
        //     'loan_stage' => 'required',
        //     'loan_amount' => 'required',
        //     'release_date' => 'required',
        //     'loan_duration' => 'required',
        //     'duration_period' => 'required',
        //     // 'repayment_cycle'=> 'required',
        //     // 'number_of_repayments'=> 'required',
        //     'interest' => 'required',
        //     'instalment' => 'required',
        //     'loan_purpose' => 'required',
        //     'interest_rate' => 'required',
        // ]);

        // $request->nic = 123;

        Loan::create($request->all());

        //auto selecting witness-----------------------------------
        $borrower = Borrower::find($request->borrower_no);

        $branch = $borrower->branch;
        $center = $borrower->center;

        $centerLeader = CenterLeader::where('branch', $branch)
            ->where('center', $center)
            ->first();

        if ($centerLeader->borrower_id != $request->borrower_no) {
            // return response()->json($centerLeader);
            $center_leader = $centerLeader->borrower_id;
        } else {
            $others = Borrower::where('branch', $branch)
                ->where('center', $center)
                ->where('borrower_no', '!=', $request->borrower_no)
                ->where('status', 'active')
                ->first();
            $center_leader = $others->borrower_no;
        }


        //agent------------------------------------------------------
        $agent = Staff::where('branch', $branch)
            ->where('designation', 'Agent')
            ->first();




        $loan = Loan::orderBy('id', 'desc')->first();
        $borrower = Borrower::where('borrower_no', $loan->borrower_no)->first();

        $monthly_installment = $request->instalment * 4;
        $total_earnings = 0;
        $total_expenses = 0;

        $self_emp_earnings = BorrowerEarning::where('visibility', 1)
            ->where('nic', $borrower->nic)
            ->get();
        foreach ($self_emp_earnings as $s) {
            $total_earnings += $s->earnings;
        }

        $rela_earnings = BorrowerMainEarning::where('visibility', 1)
            ->where('nic', $borrower->nic)
            ->get();
        foreach ($rela_earnings as $s) {
            $total_earnings += $s->salary;
        }

        $self_emp_expenses = BorrowerExpense::where('visibility', 1)
            ->where('nic', $borrower->nic)
            ->get();
        foreach ($self_emp_expenses as $s) {
            $total_expenses += $s->expenses;
        }

        $other_loans = BorrowerOtherLoan::where('visibility', 1)
            ->where('borrower_id', $borrower->nic)
            ->get();
        foreach ($other_loans as $s) {
            $total_expenses += $s->monthly_paymentt;
        }

        // return $total_earnings;

        if ($total_earnings > 0) {
            $final_ratio = ($total_expenses + $monthly_installment) / $total_earnings;
        } else {
            $final_ratio = 0;
        }
        // return $final_ratio;

        $appraisal = Appraisal::where('visibility', 1)
            ->where('nic', $borrower->nic)
            ->first();
        if ($appraisal) {

            $appraisal->final_ratio = $final_ratio;
            $appraisal->save();
        }

        $loan->nic = $borrower->nic;
        $loan->witnessC = $center_leader;
        $loan->witnessA = $agent->name;
        $loan->due = $loan->loan_amount+$loan->interest;
        $loan->save();

        // return redirect()->route('loan.index')
        //     ->with('success', 'loan created successfully.');
        return response()->json('loan_added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Loan $id)
    {
        $loan = Loan::where('id', $id)->first();
        $borrower = Borrower::where('borrower_no', $loan->borrower_no)->first();
        return view('Loans.showLoan', compact('loan', 'borrower'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Loan $loan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $loan = Loan::find($id);
        if($loan->status=='Payed'){
            return redirect('loan')->with('error','SETTLE වී නොමැති බැවින් REMOVE කල නොහැක');

        }else{
            $loan->visibility=0;
            $loan->save();
            return redirect('loan')->with('success','Loan Removed');
        }
    }
    public function rate(Request $request)
    {
        // return response()->json($request);


        // $rate=Category::select('category_rate')->get();
        // return response()->json($rate);

        // $new = $request;
        // return response()->json($new) ;
        // return Category::select('category_rate')->where('category_type',$request->id)->get();
        $new = Category::where('category_type', $request['id'])->first();
        // $rate=$new['category_rate'];

        try {
            $loans = Loan::where('borrower_no', $request->borrower_no)
                ->where('status','!=','SETTLED')
                ->get();
            // return response()->json($loans);
            // return response()->json(count($loans));

            // foreach ($loans) {
                if (count($loans)>0) {
                    $ongoingLoans = 'Incomplete';
                } else {
                    $ongoingLoans = 'Complete';
                }
            // }
            // return response()->json($request->borrower_no);
            return response()->json([$new, $ongoingLoans]);
        } catch (\Exception $e) {
            $ongoingLoans = 'Complete';
            return response()->json([$new, $ongoingLoans]);
        }
    }


    public function addWitness(Request $request)
    {
        $witness = new Witness;
        $witness->borrower_no = $request->borrower_no;
        $witness->witness_name = $request->witness_name;
        $witness->witness_nic = $request->witness_nic;
        $witness->witness_address = $request->witness_address;
        $witness->witness_type = $request->witness_type;
        $witness->save();

        return response()->json($witness);
    }


    public function getWitness(Request $request)
    {
        $find = Witness::where('borrower_no', $request->borrower_no)->get();

        return response()->json($find);
    }

    public function addBCG(Request $request)
    {


        $takeBCG = Borrower::where('borrower_no', $request['id'])->select('branch', 'center', 'group_no')->first();


        return response()->json($takeBCG);
    }

    public function witness(Request $request)
    {

        //        $center = $request->id;
        //        $type = $request->type;
        //        $takeLeaders = CenterLeader::where('center_no',$center)->where('type',$type)->select('id','name','nic','address','mobile')->get();
        //         return response()->json($takeLeaders);

        $borrower = Borrower::find($request->borrowerNo);
        $leaders = CenterLeader::where('branch', $borrower->branch)->where('center_no', $borrower->center)
            ->where('type', $request->type)->get();
        return response()->json($leaders);
    }
    public function take(Request $request)
    {

        $center = $request->id;
        $wid = $request->type;
        $take = CenterLeader::where('center_no', $center)->where('id', $wid)->first();
        return response()->json($take);
    }

    public function getCenters(Request $request)
    {
        // return 123;
        $centers = Center::where('branch_no', $request->branch)->get();
        return response()->json($centers);
    }

    public function getBorrowers(Request $request)
    {
        $borrowers = Borrower::where('branch', $request->branch)
            ->where('center', $request->center)
            ->where('group_no', $request->group)
            ->where('visibility','1')
            ->get();
        return response()->json($borrowers);
    }
    public function getBorrowersForRep(Request $request)
    {
        $borrowers = Borrower::where('branch', $request->branch)
            ->where('center', $request->center)
            ->where('group_no', $request->group)
            ->get();
        return response()->json($borrowers);
    }
    public function getBorrowersbynic(Request $request)
    {

        $borrowers = Borrower::where('nic', $request->nic)->where('visibility',1)
            ->get();
        return response()->json($borrowers);
    }

    public function getWitnessC(Request $request)
    {
        $borrower = Borrower::find($request->borrower_no);

        $branch = $borrower->branch;
        $center = $borrower->center;

        $centerLeader = CenterLeader::where('branch', $branch)
            ->where('center', $center)
            ->first();

        if ($centerLeader->borrower_id != $request->borrower_no) {
            return response()->json($centerLeader);
        } else {
            $others = Borrower::where('branch', $branch)
                ->where('center', $center)
                ->where('borrower_no', '!=', $request->borrower_no)
                ->get();

            return response()->json($others);
        }
    }

    public function getWitnessA(Request $request)
    {
        $borrower = Borrower::find($request->borrower_no);

        $branch = $borrower->branch;
        $center = $borrower->center;

        $agents = Staff::where('branch', $branch)
            // ->where('center', $center)
            ->where('designation', 'Agent')
            ->get();

        return response()->json($agents);
    }

    public function getLoanStage(Request $request)
    {
        $borrower = Borrower::find($request->borrower_no);

        // $loans = Loan::where('borrower_no', $request->borrower_no)->get();

        // foreach($loans as $loan){
        //     if($loan->status != 'Completed'){
        //         $ongoingLoans = 'Incomplete';
        //     }else {
        //         $ongoingLoans = 'Complete';
        //     }
        // }



        // return response()->json([$borrower, $ongoingLoans]);
        return response()->json($borrower);
    }

    public function checkCenterLeader(Request $request)
    {
        // try{
        $leader = CenterLeader::where('branch', $request->branch)->where('center', $request->center)->get();
        return response()->json($leader);

        // } catch(Exception $e){
        //     return response()->json('no_center_leader');
        // }

    }
}
