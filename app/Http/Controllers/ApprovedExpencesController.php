<?php

namespace App\Http\Controllers;

use App\CompanyExpencesSummary;
use Illuminate\Http\Request;
use DB;
class ApprovedExpencesController extends Controller
{
    public function index(){

        // $approveds=DB::table('new_company_expences_summery')
        // ->leftjoin('new_company_expences','new_company_expences.voucher_id','new_company_expences.voucher_id')
        // ->where('new_company_expences_summery.status','=','APPROVED')
        // ->select(DB::raw("sum(new_company_expences.	item_total) as total"),'new_company_expences_summery.voucher_id','new_company_expences.date','new_company_expences.category')
        // ->groupBy('new_company_expences.voucher_id','new_company_expences_summery.voucher_id','new_company_expences.date','new_company_expences.category')
        // ->orderBy('new_company_expences_summery.id')
        // ->get();
        $approveds=CompanyExpencesSummary::leftjoin('new_company_expences','new_company_expences.voucher_id','new_company_expences_summery.voucher_id')
        ->where('new_company_expences_summery.status','=','APPROVED')
        ->where('new_company_expences.visibility',1)
        ->select(DB::raw("sum(new_company_expences.	item_total) as total"),'new_company_expences.voucher_id','new_company_expences.date','new_company_expences.category'
                    ,'new_company_expences.payee')
        ->groupBy('new_company_expences.voucher_id','new_company_expences.date','new_company_expences.category','new_company_expences.payee')
        ->orderBy('new_company_expences.date')
        ->get();



        return view('Pendingapprovals.Approved.index',compact( 'approveds'));
    }

    public function issue($id){
        $voucher=DB::table('new_company_expences_summery')
        ->where('id','=',$id)
        ->first();

        return view('Pendingapprovals.CompanyExpences.approve',compact( 'voucher'));
    }
}
