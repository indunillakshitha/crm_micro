<?php

namespace App\Http\Controllers;

use App\Appraisal_expenses_product;
use Illuminate\Http\Request;

class addAppraisalExpensesProductController extends Controller
{

    public function index(){


        return view('Appraisal.addingredients');
    }

    public  function addAppraisalExpensesProduct(Request $request){
        // $request->validate([
        //     'product'=>'required',
        //     'ingredient'=>'required',
        //     'buying_price'=>'required'
        // ]);

        Appraisal_expenses_product::create($request->all());
        return view('Appraisal.addingredients');
    }
}
