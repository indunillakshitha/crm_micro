<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Loan;
use App\Appraisal;
use App\Borrower;
use App\Approving;
use DB;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redirect;

class VerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $pending = 'Pending';

        $borrowers = Borrower::all();
        $apprasails = Appraisal::all();
        $loans = Loan::where('status',$pending)->where('visibility',1)->get();
        return view('Verification.indexVerification',compact('loans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {



        // echo '<a href="https://cantylivelocations.firebaseapp.com/location/" target="_blank"></a>';

        return redirect("https://cantylivelocations.firebaseapp.com/location/");
        // return redirect('<script>window.open("load.php","_blank")</script>');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Loan::where('id',$id)->update(['visibility'=>0]);
        return redirect()->route('verify.index');
    }
     public function test($id)
    {

        $borrower= Loan::where('id',$id)->where('status','Pending')->first();
         $apprasail=Appraisal::where('nic',$borrower->nic)->where('visibility',1)->count();

         if($apprasail >0){

             $borrower['status']='Verified';
             $borrower->save();
             return redirect()->route('verify.index')->with('success','Verify කිරීම සාර්තකයි');
            }
            return redirect()->route('verify.index')->with('error','Appraisal එකක් නොමැති බැවින් Verify කිරීම අසාර්තකයි ');

    }
}
