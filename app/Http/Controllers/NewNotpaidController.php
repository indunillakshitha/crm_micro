<?php

namespace App\Http\Controllers;

use App\ActivityLog;
use App\Category;
use App\Center;
use App\Loan;
use App\Repayment;
use App\Shedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NewNotpaidController extends Controller
{

    public function __construct()
    {

        date_default_timezone_set('Asia/Colombo');

    }
    public function newRepayments(){

        $centers= Shedule::where('excecutive_id',Auth::user()->id)
                    ->get();
        return view('new_notpaids.index',compact('centers'));
    }

    public function newRepaymentGetLoans(Request $request)
    {
        $week_index = Carbon::now()->isoFormat('W');

        $data = DB::table('loans')
            ->leftjoin('borrowers', 'borrowers.borrower_no', 'loans.borrower_no')
            ->leftjoin('repayments', 'repayments.loan_id', 'loans.id')
            ->select('borrowers.*', 'loans.*')
            ->where('loans.center', $request->center)
            ->where('loans.status', 'Payed')
            ->where('repayments.method_status', 'NOT PAID')
            ->where('repayments.week_index',$week_index)
            ->orderBy('borrowers.borrower_no')
            ->get();
        return response()->json($data);
    }


    public function newRepaymentCollect(Request $request)
    {
        $today_index=Carbon::now()->isoFormat('DDD');
        $loan = Loan::find($request->id);
        $loan->payments_count = ++$loan->payments_count;
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_paid_date = $today_index;

        // $due_amount = $loan->loan_amount - $loan->paid;
        $prev_total_payed = $loan->paid;

         $repayment =  Repayment::where('loan_id',$loan->id)->where('method_status', 'NOT PAID')->first();
        $repayment->logged_user = Auth::user()->name;
        $repayment->borrower_nic = $loan->nic;
        $repayment->branch = $loan->branch;
        $repayment->center = $loan->center;
        $repayment->loan_id = $loan->id;
        $repayment->loan_amount = $loan->loan_amount;
        $repayment->paid_amount = $request->payment;
        $repayment->due_amount = $loan->due - $request->payment;
        $repayment->total_payed = $prev_total_payed + $request->payment;
        $repayment->installment = $loan->instalment;
        $repayment->payment_date_index = Carbon::now()->isoFormat('DDD');
        $repayment->payment_date = Carbon::now()->isoFormat('YYYY-MM-DD');
        $repayment->payment_date_full = Carbon::now()->isoFormat('M/D/YYYY');
        $repayment->year=Carbon::now()->year;
        $repayment->month=Carbon::now()->month;
        $repayment->installment_no = $loan->payments_count;
        $repayment->status = 'COLLECTED'; #warike gatta
        $repayment->type = 'CASH';
        $repayment->method_status = 'CENTER COLLECTION';
        $repayment->mode = 'GET NP MODE';



        if($request->payment==0){
            $loan->arrears+=$loan->instalment;
            $repayment->arrears=$loan->arrears;
        }

        $loan->due = $repayment->due_amount;
        $loan->paid = $repayment->total_payed;

        $category=Category::where('fixed_ammount',$repayment->loan_amount)->first();


        $interest_amount=$repayment->installment * $category->category_rate/100;
        $repayment->inter=$interest_amount;//interest collection
        $repayment->capital=$repayment->paid_amount-$interest_amount;//capital collection

         if($repayment->installment < $repayment->paid_amount){
                 $repayment->over_paid_amount = $repayment->paid_amount - $repayment->installment ;//over paid amount
         }elseif($repayment->installment > $repayment->paid_amount){
                $repayment->less_paid_amount = $repayment->installment - $repayment->paid_amount;//less paid amount
        }

        $shedule=Shedule::where('excecutive_id',Auth::user()->id)
                ->where('center',$repayment->center)
                ->first();
        $shedule->collected_loan_count-=1;
        $shedule->last_visited_date_index=$today_index;
        $shedule->last_visited_date=Carbon::now();


        if ($loan->due <= 0) {
            $loan->status = 'SETTLED';
            $repayment->status = 'SETTLED';
        }

        if (!Repayment::where('loan_id', $repayment->loan_id)
                        ->where('payment_date', $repayment->payment_date)
                        ->where('method_status','CENTER COLLECTION')
                        // ->orWhere('method_status','TEMP COLLECTION')
                        ->first()) {
            $repayment->save();
            $loan->save();
            $shedule->save();

        }


        $centerTotal = 0;
        $centerRepayments = Repayment::where('center', $loan->center)
            ->where('payment_date_index', $repayment->payment_date_index)
            ->get();

        foreach ($centerRepayments as $r) {
            $centerTotal += $r->paid_amount;
        }

        $log=new ActivityLog();
        //$borrwer,$loan,$activity,$amount
        $log->log($loan->borrower_no,$loan->id,'Get not paid collection',$repayment->paid_amount);
        return response()->json($centerTotal);
    }

    public function newRepaymentNotCollect(Request $request)
    {
        $today_index=Carbon::now()->isoFormat('DDD');
        $loan = Loan::find($request->id);
        $loan->payments_count = ++$loan->payments_count;
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_paid_date = $today_index;

        // $due_amount = $loan->loan_amount - $loan->paid;
        $prev_total_payed = $loan->paid;

        $repayment = new Repayment();
        $repayment->logged_user = Auth::user()->name;
        $repayment->borrower_nic = $loan->nic;
        $repayment->branch = $loan->branch;
        $repayment->center = $loan->center;
        $repayment->loan_id = $loan->id;
        $repayment->loan_amount = $loan->loan_amount;
        $repayment->paid_amount = 0;
        $repayment->due_amount = $loan->due ;
        $repayment->total_payed = $prev_total_payed + 0;
        $repayment->installment = $loan->instalment;
        $repayment->payment_date_index = Carbon::now()->isoFormat('DDD');
        $repayment->payment_date = Carbon::now()->isoFormat('YYYY-M-D');
        $repayment->payment_date_full = Carbon::now()->isoFormat('M/D/YYYY');
        $repayment->installment_no = $loan->payments_count;
        $repayment->status = 'COLLECTED'; #warike gatta
        $repayment->type = 'CASH';
        $repayment->method_status = 'CENTER COLLECTION';


        $loan->arrears+=$loan->instalment;
        $repayment->arrears=$loan->arrears;

        $loan->due = $repayment->due_amount;
        $loan->paid = $repayment->total_payed;

        $category=Category::where('fixed_ammount',$repayment->loan_amount)->first();


        $interest_amount=$repayment->installment * $category->category_rate/100;
        $repayment->inter=$interest_amount;//interest collection
        $repayment->capital=$repayment->paid_amount-$interest_amount;//capital collection

         if($repayment->installment < $repayment->paid_amount){
                 $repayment->over_paid_amount = $repayment->paid_amount - $repayment->installment ;//over paid amount
         }elseif($repayment->installment > $repayment->paid_amount){
                $repayment->less_paid_amount = $repayment->installment - $repayment->paid_amount;//less paid amount
        }

        $shedule=Shedule::where('excecutive_id',Auth::user()->id)
                ->where('center',$repayment->center)
                ->first();
        $shedule->collected_loan_count-=1;
        $shedule->last_visited_date_index=$today_index;
        $shedule->last_visited_date=Carbon::now();


        if ($loan->due <= 0) {
            $loan->status = 'SETTLED';
            $repayment->status = 'SETTLED';
        }

        if (!Repayment::where('loan_id', $repayment->loan_id)->where('payment_date', $repayment->payment_date)->first()) {
            $repayment->save();
            $loan->save();
            $shedule->save();

        }


        $centerTotal = 0;
        $centerRepayments = Repayment::where('center', $loan->center)
            ->where('payment_date_index', $repayment->payment_date_index)
            ->get();

        foreach ($centerRepayments as $r) {
            $centerTotal += $r->paid_amount;
        }


        return response()->json($centerTotal);
    }
}
