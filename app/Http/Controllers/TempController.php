<?php

namespace App\Http\Controllers;

use App\Center;
use App\Loan;
use App\Branch;
use App\Borrower;
use App\Category;
use App\Repayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TempController extends Controller
{
  public function __construct()
    {

        date_default_timezone_set('Asia/Colombo');

    }
    public function issueIndex()
    {
        $centers = Center::where('branch_no', Auth::user()->branch)->get();
        return view('Temp.issueIndex', compact('centers'));
    }
    public function collectIndex()
    {
        $centers = Center::where('branch_no', Auth::user()->branch)->get();
        return view('Repayments.collectfromcenter', compact('centers'));
    }
 public function centerTotaltIndex()
    {
        $repayments = [];
        $centers = Center::where('branch_no', Auth::user()->branch)->get();
        return view('Temp.centertotal', compact('centers', 'repayments'));
    }
 public function viewCenterTotal(Request $request)
    {
       //$index=Carbon::parse($request->date)->isoFormat('DDD');

 //$repayments = DB::table('repayments')
           // ->select( 'paid_amount','center',DB::raw("sum(paid_amount) as total"))
          //  ->where('branch', Auth::user()->branch)
	   // ->where('method_status','!=','DOCUMENT')
	   // ->where('payment_date_index','<',$index)
           // ->groupBy('center','paid_amount')
           // ->get();


        //return response()->json($repayments);
$index = Carbon::parse($request->date)->isoFormat('DDD');

      $repayments = Repayment::where('branch', Auth::user()->branch)
                    ->leftjoin('centers','centers.center_name','repayments.center')
        ->where('method_status', '!=', 'DOCUMENT')
        ->where('payment_date_index', '<=', $index+1)
	//->where('payment_date_index', '>', '0')
        ->select(DB::raw("sum(paid_amount) as total"),'center','centers.center_no')
        ->groupBy('center','centers.center_no')
      ->orderBy('centers.id')
        ->get();


        return response()->json($repayments);
    }
    public function repaymentIndex()
    {
        $centers = Center::where('branch_no', Auth::user()->branch)->get();
        return view('Temp.repaymentIndex', compact('centers'));
    }
    public function viewtIndex()
    {
        $repayments = [];
        $centers = Center::where('branch_no', Auth::user()->branch)->get();
        return view('Temp.repaymentView', compact('centers', 'repayments'));
    }

    public function totalByDateIndex(){
        $repayments = [];
        $centers = Center::where('branch_no', Auth::user()->branch)->get();
        return view('Temp.totalByDate', compact('centers', 'repayments'));
    }

    public function getLoans(Request $request)
    {
        // $loans = Loan::where('center', $request->center)
        //     ->where('status', 'Issued')
        //     ->get();
        $loans = DB::table('loans')->leftjoin('borrowers', 'borrowers.borrower_no', 'loans.borrower_no')
            ->select('borrowers.*', 'loans.*')
            ->where('loans.center', $request->center)
            ->where('loans.status', 'Issued')
            // ->where('visibility', 1)
            ->get();

        return response()->json($loans);
    }
    public function repaymentGetLoans(Request $request)
    {

        $data = DB::table('loans')
            ->leftjoin('borrowers', 'borrowers.borrower_no', 'loans.borrower_no')
            ->select('borrowers.*', 'loans.*')
            ->where('loans.center', $request->center)
            ->where('loans.status', 'Payed')
            ->orderBy('borrowers.borrower_no')
            ->get();

        // return $data;
        return response()->json($data);
    }

    public function viewGetLoans(Request $request)
    {
        // return response()->json($request);
        // $repayments = Repayment::where('center', $request->center)->where('payment_date', $request->date)->get();
        $repayments = DB::table('repayments')
            ->leftjoin('loans', 'loans.id', 'repayments.loan_id')
            ->leftjoin('borrowers', 'borrowers.borrower_no', 'loans.borrower_no')
            ->select('borrowers.*', 'repayments.*')
            ->where('repayments.center', $request->center)
            ->where('repayments.payment_date', $request->date)
	  ->where('repayments.method_status','!=','DOCUMENT')
            ->orderBy('borrowers.borrower_no')
            ->get();


        // $centers = Center::where('branch_no', Auth::user()->branch)->get();
        // return view('Temp.repaymentView', compact('centers', 'repayments'));
        return response()->json($repayments);
    }

    public function totalByDateGetRepayments(Request $request) {
        // $data = Repayment::se('center', $request->center)->get();
        $data = DB::table('repayments')
        ->select(DB::raw("sum(repayments.paid_amount) as sum"), DB::raw("count(repayments.paid_amount) as count"), 'repayments.payment_date', 'repayments.center')
        ->groupBy('repayments.payment_date', 'repayments.center')
        ->where('repayments.center', $request->center)
	    ->where('repayments.method_status','!=','DOCUMENT')
        ->orderBy('repayments.payment_date_index','desc')
        ->orderBy('repayments.year','desc')
        ->get();
        return response()->json($data);
    }

    public function issueLoans(Request $request)
    {
        $loan = Loan::find($request->id);
        $loan->release_date = $request->loan_date;
        $loan->status = 'Payed';
        $loan->payment_starting_date = $request->due_start_date;
        $loan->payment_starting_date_index =  Carbon::parse($request->due_start_date)->isoFormat('DDD');
        $loan->save();
        return response()->json($loan);
    }

    public function repaymentCollect(Request $request)
    {

        $loan = Loan::find($request->id);
        $loan->payments_count = ++$loan->payments_count;
        $loan->last_payment_date = Carbon::now()->isoFormat('DDD');
        $loan->last_payment_month = Carbon::now()->isoFormat('M');

        // $due_amount = $loan->loan_amount - $loan->paid;
        $prev_total_payed = $loan->paid;

        $repayment = new Repayment;
        $repayment->logged_user = Auth::user()->name;
        $repayment->borrower_nic = $loan->nic;
        $repayment->branch = $loan->branch;
        $repayment->center = $loan->center;
        $repayment->loan_id = $loan->id;
        $repayment->loan_amount = $loan->loan_amount;
        $repayment->paid_amount = $request->payment;

        $repayment->installment = $loan->instalment;
        $repayment->payment_date_index = Carbon::parse($request->date)->isoFormat('DDD');
        $repayment->payment_date = $request->date;
        $repayment->payment_date_full = Carbon::parse($request->date)->isoFormat('M/D/YYYY');
        $repayment->installment_no = $loan->payments_count;
        $repayment->status = 'COLLECTED'; #warike gatta
        $repayment->type = 'CASH';
        $repayment->method_status = 'TEMP COLLECTION';

        $loan->due = $repayment->due_amount;
        $loan->paid = $repayment->total_payed;
//if delete old payment and enter new
if(Repayment::where('loan_id',$repayment->loan_id)->where('payment_date_index','>',$repayment->payment_date_index)->get()){

    $last_repayment=Repayment::where('loan_id',$repayment->loan_id)->orderBy('payment_date_index','desc')->first();
    $repayment->due_amount = $last_repayment->due_amount - $request->payment;
    $repayment->total_payed = $last_repayment->total_payed + $request->payment;
    $repayments=Repayment::where('loan_id',$repayment->loan_id)->where('payment_date_index','>',$repayment->payment_date_index)->get();

    return response()->json($repayments);
    foreach($repayments as $rep){
        $rep->total_payed +=$repayment->paid_amount;
        $rep->due_amount -=$repayment->paid_amount;
        $rep->save();
        return response()->json("enwa");
    }
}else{
    $repayment->due_amount = $loan->due - $request->payment;
    $repayment->total_payed = $prev_total_payed + $request->payment;
    return response()->json("panna");
}

        $category=Category::where('fixed_ammount',$repayment->loan_amount)->first();


        $interest_amount=$repayment->installment * $category->category_rate/100;
        $repayment->inter=$interest_amount;//interest collection
        $repayment->capital=$repayment->paid_amount-$interest_amount;//capital collection

         if($repayment->installment < $repayment->paid_amount){
                 $repayment->over_paid_amount = $repayment->paid_amount - $repayment->installment ;//over paid amount
         }elseif($repayment->installment > $repayment->paid_amount){
                $repayment->less_paid_amount = $repayment->installment - $repayment->paid_amount;//less paid amount
        }




        if ($loan->due <= 0) {
            $loan->status = 'SETTLED';
            $repayment->status = 'SETTLED';
        }

        if (!Repayment::where('loan_id', $repayment->loan_id)->where('payment_date', $repayment->payment_date)->first()) {
            $repayment->save();
            $loan->save();
            return response()->json($repayment->loan_id);


        }


        $centerTotal = 0;
        $centerRepayments = Repayment::where('center', $loan->center)
            ->where('payment_date_index', $repayment->payment_date_index)
            ->get();

        foreach ($centerRepayments as $r) {
            $centerTotal += $r->paid_amount;
        }


        return response()->json($centerTotal);
    }

    //deleteing loans in repayments
    public function viewDeleteLoans(Request $request)
    {
        // return response()->json($request);
        $repayment = Repayment::find($request->id);
        $repayment->delete();
        if($repayment->method_status != 'DOCUMENT'){
        $loan = Loan::find($repayment->loan_id);
        $loan->due += $request->paid_amount;
        $loan->paid -= $request->paid_amount;
        $loan->status ='Payed';
        $loan->payments_count += $loan->payments_count;
        $loan->save();

        $repayments=Repayment::where('loan_id',$repayment->loan_id)->where('payment_date_index','>',$repayment->payment_date_index)->get();
        foreach($repayments as $rep){
            $rep->total_payed -=$repayment->paid_amount;
            $rep->due_amount +=$repayment->paid_amount;
            $rep->save();
        }
}
        return response()->json($repayment);
    }

    public function docchargeIndex(){
        $centers = Center::where('branch_no', Auth::user()->branch)->get();
        return view('Repayments.doccharges', compact('centers'));
    }
    public function repaymentGetLoansForDocs(Request $request)
    {

        //  return $loans=Loan::where('fees',NULL)
        //                 ->update(['fees'=>0]);
        $data = DB::table('loans')
            ->leftjoin('borrowers', 'borrowers.borrower_no', 'loans.borrower_no')
            ->select('borrowers.*','borrowers.nic as nicc', 'loans.*')
            ->where('loans.center', $request->center)
            ->Where('loans.fees', 0)
            ->Where('loans.visibility','1')
            ->orderBy('borrowers.borrower_no')
            ->get();

        // return $data;
        return response()->json($data);
    }
    public function docChargeCollect(Request $request)
    {


        $loan = Loan::find($request->id);

        $repayment = new Repayment;
        $repayment->logged_user = Auth::user()->name;
        $repayment->borrower_nic = $loan->nic;
        $repayment->branch = $loan->branch;
        $repayment->center = $loan->center;
        $repayment->loan_id = $loan->id;
        $repayment->loan_amount = $loan->loan_amount;
        $repayment->paid_amount = 0;
        $repayment->doc_charges = $request->payment;
        $repayment->payment_date_index = Carbon::parse($request->date)->isoFormat('DDD');
        $repayment->payment_date = $request->date;
        $repayment->payment_date_full = Carbon::parse($request->date)->isoFormat('M/D/YYYY');
        $repayment->status = 'COLLECTED'; #warike gatta
        $repayment->type = 'CASH';
        $repayment->method_status = 'DOCUMENT';

        $loan->fees = $request->payment;

        $centerTotal = 0;
        $centerRepayments = Repayment::where('center', $loan->center)
            ->where('payment_date_index', $repayment->payment_date_index)
            ->where('method_status','DOCUMENT')
            ->get();

        foreach ($centerRepayments as $r) {
            $centerTotal += $r->paid_amount;
        }

            $repayment->save();
            $loan->save();

        return response()->json($centerTotal);
    }

    public function CenterSelectedDate(Request $request){

        $index = Carbon::parse($request->date)->isoFormat('DDD');

    //   $repayments = Repayment::where('branch', Auth::user()->branch)
    //     ->leftjoin('centers','centers.center_name','repayments.center')
    //     ->where('method_status', '!=', 'DOCUMENT')
    //     ->where('payment_date_index', $index)
    //     ->select(DB::raw("sum(paid_amount) as total"),'center','centers.center_no')
    //     ->groupBy('center','centers.center_no')
    //     ->orderBy('centers.id')
    //     ->get();

      $repayments = Repayment::where('branch', Auth::user()->branch)
        ->leftjoin('centers','centers.center_name','repayments.center')
        // ->where('method_status', '!=', 'DOCUMENT')
        ->where('payment_date_index', $index)
        ->select(DB::raw("sum(paid_amount) as total"),
        DB::raw("sum(doc_charges) as total_doc"),
        DB::raw("sum(arrears) as total_arrears"),
        DB::raw("sum(not_paid_collection) as total_not_paid"),
                'center','centers.center_no')
        ->groupBy('center','centers.center_no')
        ->orderBy('centers.id')
        ->get();






        return response()->json($repayments);
    }
    public function CenterSelectedDateEx(Request $request){

        $index = Carbon::parse($request->date)->isoFormat('DDD');


      $repayments = Repayment::where('branch', Auth::user()->branch)
        ->leftjoin('centers','centers.center_name','repayments.center')
        // ->where('method_status', '!=', 'DOCUMENT')
        ->where('payment_date_index', $index)
        ->where('logged_user', $request->center)
        ->select(DB::raw("sum(paid_amount) as total"),
        DB::raw("sum(doc_charges) as total_doc"),
        DB::raw("sum(arrears) as total_arrears"),
        DB::raw("sum(not_paid_collection) as total_not_paid"),
                'center','centers.center_no')
        ->groupBy('center','centers.center_no')
        ->orderBy('centers.id')
        ->get();






        return response()->json($repayments);
    }
    public function CenterSelectedDateDoc(Request $request){

        $index = Carbon::parse($request->date)->isoFormat('DDD');



      $repayments = Repayment::where('branch', Auth::user()->branch)
        ->leftjoin('centers','centers.center_name','repayments.center')
        ->where('method_status', 'DOCUMENT')
        ->where('payment_date_index', $index)
        ->select(DB::raw("sum(paid_amount) as total"),'center','centers.center_no')
        ->groupBy('center','centers.center_no')
        ->orderBy('centers.id')
        ->get();

        return response()->json($repayments);
    }
 public function totalOfADay(){
        return view('Temp.centertotalofaday');
    }
 public function totalOfADayEx(){
        return view('Temp.centertotalofadayex');
    }

    public function ledger(){
        $branches=Branch::all();
        return view('ledger.index',compact('branches'));
    }

    public function loans(Request $request){
        $loans= Loan::where('borrower_no',$request->id)->get();
        return response()->json($loans);
    }
    public function getLedger(Request $request){
        $loan= Loan::where('id',$request->id)->first();
        $borrower=Borrower::where('borrower_no',$loan->borrower_no)->first();
        $repayments=Repayment::where('loan_id',$request->id)->where('method_status','!=','DOCUMENT')->get();


        return response()->json(['loan'=>$loan,'borrower'=>$borrower,'repayments'=>$repayments]);
    }

    public function docchargeDeleteIndex(){

        $repayments = [];
        $centers = Center::where('branch_no', Auth::user()->branch)->get();
        return view('Temp.documentIndex', compact('centers', 'repayments'));
    }

    public function viewGetDocumentCharges(Request $request){
        $repayments = DB::table('repayments')
            ->leftjoin('loans', 'loans.id', 'repayments.loan_id')
            ->leftjoin('borrowers', 'borrowers.borrower_no', 'loans.borrower_no')
            ->select('borrowers.*', 'repayments.*','loans.borrower_no as bn')
            ->where('repayments.center', $request->center)
            ->where('repayments.payment_date', $request->date)
	       ->where('repayments.method_status','DOCUMENT')
            ->orderBy('borrowers.borrower_no')
            ->get();


        return response()->json($repayments);
    }

    public function viewDeleteDocumntCharges(Request $request)
    {
        // return response()->json($request);
        $repayment = Repayment::find($request->id);
        $repayment->delete();
        if($repayment->method_status == 'DOCUMENT'){
        $loan = Loan::find($repayment->loan_id);
        $loan->fees =0;
        $loan->save();


}
        return response()->json($repayment);
    }
}
