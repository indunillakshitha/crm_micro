<?php

namespace App\Http\Controllers;

use App\Loan;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;

class ExportController extends Controller
{
    public function exportLoans(){
    	$users = \App\Loan::where('visibility',1)->get();

	$csvExporter = new \Laracsv\Export();

    return $csvExporter->build($users, ['borrower_no', 'branch', 'center','group_no','loan_amount','release_date','interest_rate',
                        'interest','instalment','due','paid','status','due_date','payment_starting_date'])->download();
    }
    public function exportBorrowers(){
    	$users = \App\Borrower::where('visibility',1)->get();

	$csvExporter = new \Laracsv\Export();

    return $csvExporter->build($users, ['borrower_no', 'branch', 'center','group_no','full_name','nic','birthday',
                        'address','mobile_no'])->download();
    }
    public function exportRepayments(){
    	$users = \App\Repayment::all();

	$csvExporter = new \Laracsv\Export();

    return $csvExporter->build($users, ['borrower_nic', 'borrower_id', 'center','group_no','loan_amount','branch','due_amount',
                        'total_payed','installment','paid_amount','loan_date','	payment_date_full','method_status','capital',
                        'inter','over_paid_amount','less_paid_amount','doc_charges','borrower_no'])->download();
    }


    public function index(){
        return view('exports.index');
    }

    public function locMiss(){
         $users = \App\Borrower::leftjoin('locations','locations.nic','borrowers.nic')
                                ->select( 'borrowers.address', 'borrowers.borrower_no', 'borrowers.branch','borrowers.center',
                                         'borrowers.group_no','borrowers.full_name','borrowers.nic','locations.lat','locations.lng')
                                // ->where('locations')
                                ->get();

        $csvExporter = new \Laracsv\Export();

        return $csvExporter->build($users, ['borrower_no', 'branch', 'center','group_no','full_name','nic','lat','lng',
                            'address'])->download();
    }
}
