<?php

namespace App\Http\Controllers;

use App\AppraisalIngredient;
use Illuminate\Http\Request;

class AppraisalIngredientsController extends Controller
{
    public function addIngredient(Request $request){
        AppraisalIngredient::create($request->all());

        return redirect ('/appraisalingredients');
    }

    public function getIngs(Request $request){
        $find = AppraisalIngredient::where('product', $request->product)->get();

        return response()->json($find);
    }

    public function getIngsPrice(Request $request){
        $find = AppraisalIngredient::where('ingredient', $request->ing)->where('product', $request->product)->get();

        return response()->json($find);
    }
}
