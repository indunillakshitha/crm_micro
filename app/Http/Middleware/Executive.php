<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Executive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->usertype == 'executive'){
            return $next($request);
        }else{
            return redirect ('/123bumblebee')->with('error', 'Not Authorized');
        }
    }
}
