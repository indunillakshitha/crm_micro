<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SuperUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(
            Auth::user()->access_level == 'super_user' 
        ){
            return $next($request);
        } else{
            return redirect('/home')->with('error', 'Not Authorized sup');
        }

    }
}
