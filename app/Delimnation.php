<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delimnation extends Model
{
  protected $table='delimnation';
  public $timestamps = false;

  protected $fillable=['agent','5000','1000','500','100','50','10','created_at', 'updated_at','created_by','total_to_be','total_got','remarks','denomination_center_totals_id'];
}
