<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appraisal extends Model
{
    protected $table = 'appraisals';
    protected $fillable = [ 'borrower_id',
        'husband',
        'son',
        'daughter',
        'guardian',
        'other',
        'total_earn',
        'household_expenses',
        'other_expenses',
        'as_borrower_facility_count',
        'as_borrower_total_amount',
        'as_guarantor_facility_count',
        'as_guarantor_total_amount',
        'total_expenses',
        'sum',
        'reqesting_loan_amount',
        'loan_purpose',
        'budget_ratio',
        'self_employee_total',
        'self_employee_expenses',
        'nic',
        'center',
        'created_by',
        'created_date_index',
        'budget_ratio_self_employee',
        'budget_ratio_with_participation',
        'budget_ratio_with_salary',
        'foods',
        'hire_purchasing',
        'accomadation',
        'health',
        'education',
        'repayment',
        'water_bill',
        'electricity_bill',
        'rent',
        'tax',
        'leasing',
        'festival',
        'final_ratio',
       ];
    //    protected $primaryKey = 'borrower_id';
       public $incrementing = false;
       protected $keyType = 'string';
}
