<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DenominationDepositeVariance extends Model
{
    protected $fillable = [
        'denomination_center_totals_id',
        'atm_rejection',
        'payments',
        'loan_disbursment',
        '',
        '',
        '',
        '',
        '',
        '',
    ];
}
