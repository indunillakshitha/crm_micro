<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shedule extends Model
{
    protected $fillable =[
        'date',
        'excecutive',
        'excecutive_id',
        'center',
        'center_id',
        'created_at',
        'updated_at',
    ];
}
