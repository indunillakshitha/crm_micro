<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $fillable=[

            'branch',
            'name',
            'designation',
            'nic',
            'address',
            'mobile',
            'mail',
            'edu_qual',
            'pro_qual',
            'pro_exp',
            'civil_status',
            'img_nic',
            'img_birth_certificate',
            'img_gr_cer',
            'img_police_cer',
            'img_married_cer',
            'img_edu_1_cer',
            'img_edu_2_cer',
            'img_edu_3_cer',
            'applying_name',
            'relational_contact',
    ];
}
