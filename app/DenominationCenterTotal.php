<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DenominationCenterTotal extends Model
{
    protected $fillable =[
        'branch',
        'agent_id',
        'total_cash_collected',
        'total_cash_available',
        'varians_avail_n_collected',
        'comment_for_vNc',
        'total_cash_deposited',
        'varians_avail_n_deposited',
        '',
        '',
        '',
        '',
    ];
}
