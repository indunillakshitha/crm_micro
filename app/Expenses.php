<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenses extends Model
{
    protected $table = 'expenses';
    protected $fillable =[
        'branch',
        'expense_type',
        'expense_amount',
        'expense_date',
        'link_to_loan',
        'ledger_account',
        'bank',
        'cheque_no',
        'cheque_date',
        'files',

    ];
}
