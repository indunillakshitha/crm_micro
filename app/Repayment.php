<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repayment extends Model
{
    protected $table='repayments';
    protected $fillable =[
        'status',
        'branch',
        'center',
        'payed_date_index',
        'payment_date',
        'payed_amount',
        'type',
        'payment_date_full',
        'over_paid_amount',
        'less_paid_amount',
        'capital',
        'inter',
        'denominate_id',
        'deliminated'
    ];
}
