<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferenceExpenses extends Model
{
    protected $table = 'referenxe_expenses';
    protected $fillable =[
        'ref_id',
        'branch',
        'expense_type',
        'expense_amount',
        'expense_date',
        'link_to_loan',
        'ledger_account',
        'bank',
        'cheque_no',
        'cheque_date',
        'files',

    ];
}
