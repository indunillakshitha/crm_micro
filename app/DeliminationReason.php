<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliminationReason extends Model
{
    protected $fillable = [
        'delimination_id',
        'id_center',
        'id_type',
    ];
}
