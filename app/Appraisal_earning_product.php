<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appraisal_earning_product extends Model
{
    protected $table = 'appraisal_earning_products';
    protected $fillable = [
        'product',
        'category_name',
        'unit_price',
        'min',
        'max'
    ];
}
