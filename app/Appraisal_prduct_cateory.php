<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appraisal_prduct_cateory extends Model
{
    protected $table = 'appraisal_product_categories';
    protected $fillable = [ 'category_name'];
}

