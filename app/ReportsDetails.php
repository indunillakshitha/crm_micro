<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportsDetails extends Model
{
    protected $table = 'reports_details';
    protected $fillable = [
        'center_name',
        'center_collection',
        'loan_count_collection'


    ];
}
