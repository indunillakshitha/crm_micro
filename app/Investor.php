<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investor extends Model
{
    protected $fillable=['name','nic','address','designation','join_date','created_at','updated_at','created_branch','created_by','title'];
}
