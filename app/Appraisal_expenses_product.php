<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appraisal_expenses_product extends Model
{
    protected $table = 'appraisal_expenses_products';
    protected $fillable = [
        'product',
        'ingredient',
        'buying_price',
    ];
}
