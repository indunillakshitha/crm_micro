<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Witness extends Model
{
    protected $table = 'witnesses';
    protected $fillable = [
        'witness_name',
        'witness_nic',
        'witness_address',
        'witness_mobile',
        'witness_land',
    ];

}
