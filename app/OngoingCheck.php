<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OngoingCheck extends Model
{
    protected $table ='ongoing_check';
    protected $fillable = [
        'current_no	'
    ];

}
