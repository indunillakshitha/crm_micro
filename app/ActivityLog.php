<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ActivityLog extends Model
{
    protected $fillable=['user','borrower_id','loan_id','date_time','activity','amount'];
    public function log($borrwer,$loan,$activity,$amount){

        $log['user']=Auth::user()->id;
        $log['borrower_id']=$borrwer;
        $log['loan_id']=$loan;
        $log['date_time']=Carbon::now();
        $log['activity']=$activity;
        $log['amount']=$amount;
        ActivityLog::create($log);
       }
}
