<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpensesType extends Model
{
    protected $table = 'expenses_type';
    protected $fillable =[
        'expense_type',
        'comment',
        

    ];
}
