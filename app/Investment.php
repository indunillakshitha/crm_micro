<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{
   protected $fillable=[
    'amount',
    'start_date',
    'period',
    'rate',
    'created_by',
    'created_branch',
    'invested_by',
   ];
}
