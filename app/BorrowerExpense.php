<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerExpense extends Model
{
    protected $fillable = [
        //ajax fills
        'borrower_id',
        'product_id',
        'period',
        'total_units',
        'expenses'
    ];
}
