<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanPaymentDetails extends Model
{
    protected $fillable = [
        'borrower_no',
        'group_no',
        'issueType',
        'paymentType',
        'bank_name',
        'branch_name',
        'acc_no',
        'cheque_no',
    ];
}
