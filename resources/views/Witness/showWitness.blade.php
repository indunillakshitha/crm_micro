@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')


<div class="">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2> Show Witness</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('witness.index') }}"> Back</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Branch Name:</strong>
                        <input type="text" name="witness_name" disabled class="form-control" placeholder=" {{ $witness->witness_name }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Branch No:</strong>

                        <input type="text" name="witness_no" disabled class="form-control" placeholder="{{ $witness->witness_nic }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Address:</strong>

                        <input type="text" name="witness_address" disabled class="form-control" placeholder="{{ $witness->witness_address }}">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Mobile Number:</strong>

                        <input type="text" name="witness_mobile" disabled class="form-control" placeholder="  {{ $witness->witness_mobile }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Telephone Number:</strong>

                        <input type="text" name="witness_land" disabled class="form-control" placeholder="  {{ $witness->witness_land }}">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection