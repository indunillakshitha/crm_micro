@extends('layouts.master')
@section('title')

Canty International
@endsection
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="pull-left">
                        <h2>Witnesses</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('witness.create') }}"> Add Witness</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

{{-- @if ($message = Session::get('success'))--}}
{{-- <div class="alert alert-success">--}}
{{-- <p>{{ $message }}</p>--}}
{{-- </div>--}}
{{-- @endif--}}
<div class="card" style="width: 100%;">
    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>NIC</th>
                <th> Address</th>
                <th> Mobile</th>
                <th> Land Phone</th>


                <th width="200px">Action</th>
            </tr>
            @foreach ($witnesses as $witness)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $witness->witness_name }}</td>
                <td>{{ $witness->witness_nic }}</td>
                <td>{{ $witness->witness_address }}</td>

                <td>{{ $witness->witness_mobile }}</td>
                <td>{{ $witness->witness_land }}</td>

                <td width="10px">
                    <form href="{{ route('witness.destroy',$witness->id) }}" method="POST">

                        <a class="btn btn-info" href="{{ route('witness.show',$witness->id) }}">Show</a>

                        <a class="btn btn-primary" href="{{ route('witness.edit',$witness->id) }}">Edit</a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger">Delete</button>

                    </form>
                </td>

            </tr>
            @endforeach
        </table>
    </div>
</div>
{!! $witnesses->links() !!}

@endsection