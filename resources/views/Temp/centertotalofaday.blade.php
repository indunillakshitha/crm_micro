@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')

<div class="" >
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">




                    </div>
                    <div class="pull-right">

                        <form id="form" method="post">

                            <input required type="date" name="date" id="date" oninput="console.log(this.value)">

                            <a onclick="get_loans()" class="btn btn-success" data-toggle="modal"
                                data-target="#exampleModal">Get Total
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <button onclick="print()" class="btn btn-primary">Print</button>
        <div id="printDiv">

        <table id="loans_table" class="table table-bordered" id="loans_table">
            <tr>
                <th>Center No</th>
                <th>Center</th>
                <th>Collection</th>
                <th>Not Paid </th>
                <th>Document Charges</th>
                <th>Total</th>

            </tr>
        </table>
    </div>
    </div>
    {{-- <div class="card">
        <table id="doc_table" class="table table-bordered" id="doc_table">
            <tr>
                <th>Center No</th>
                <th>Center</th>
                <th>Paid Amount</th>

            </tr>
        </table>
    </div> --}}

</div>
</div>
@endsection

@section('scripts')
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

function print(){
    // var date=document.getElementById("date");
    var divContents = document.getElementById("printDiv").innerHTML;
            var a = window.open('', '', 'height=500, width=500');
            a.document.write('<html>');
            // a.document.write('<body > <h1>$.{date}<br>');
            a.document.write(divContents);
            a.document.write('</body></html>');
            a.document.close();
            a.print();
}
        function get_loans(){
            loans_table.innerHTML = `
                    <tr>
                        <th>Center No</th>
                        <th>Center</th>
                        <th>Collection</th>
                        <th>Not Paid </th>
                        <th>Document Charges</th>
                        <th>Total</th>
                    </tr>
                     `
            $.ajax({
                type: 'POST',
                url: '{{('/getcentertotofaday')}}',
                data: new FormData(form) ,
                processData: false,
                contentType: false,
                success: function(data){
                    console.log(data);
                    return show_data(data)
                }
            })
            // $.ajax({
            //     type: 'POST',
            //     url: '{{('/getcenterdocofaday')}}',
            //     data: new FormData(form) ,
            //     processData: false,
            //     contentType: false,
            //     success: function(data){
            //         console.log(data);
            //         return show_doc_data(data)
            //     }
            // })
        }

        let show_paid = 0
        let show_doc_paid = 0

        function show_data(data){
            let count = 0

            let paid = 0
            let grandTotal=0
            data.forEach(d => {

                html = `
                <tr>
                    <th>${d.center_no}</th>
                    <th>${d.center}</th>
                    <th>${d.total}</th>
                    <th>${d.total_not_paid}</th>
                    <th>${d.total_doc}</th>
                    <th>${parseFloat(d.total_doc)+parseFloat(d.total_not_paid)+parseFloat(d.total)}</th>
                </tr>

                `
                grandTotal+=parseFloat(d.total_doc)+parseFloat(d.total_not_paid)+parseFloat(d.total)
                loans_table.innerHTML += html
                count++
                // total += d.total
                // paid += parseInt(d.total)
                // show_paid += parseInt(d.total)

                // console.log(loans_table.innerHTML);
            })

            html = `
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Grand Total</th>
                    <th>${grandTotal}</th>
                </tr>
                `
                loans_table.innerHTML += html
            // document.querySelector('#count').textContent = count
            // document.querySelector('#total_amount').textContent = paid
            // document.querySelector('#total_collection').textContent = show_paid
            // document.querySelector('#paid_amount').textContent = paid

        }
        // function show_doc_data(data){
        //     let count = 0

        //     let paid = 0
        //     data.forEach(d => {
        //         html = `
        //         <tr>
        //             <th>${d.center_no}</th>
        //             <th>${d.center}</th>
        //             <th>${d.total}</th>
        //         </tr>
        //         `
        //         doc_table.innerHTML += html
        //         count++
        //         // total += d.total
        //         paid += parseInt(d.total)
        //         show_paid += parseInt(d.total)
        //         // console.log(loans_table.innerHTML);
        //     })
        //     document.querySelector('#total_doc').textContent = paid
        //     document.querySelector('#total_collection').textContent = show_paid

        // }

        function deleteRecord(id, paid_amount){
            console.log(paid_amount);
            $.ajax({
                    type: 'POST',
                    url: '{{url('/temp/view/deleteLoans')}}',
                    data: {
                        id,
                        paid_amount
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data)
                        return get_loans()
                    }
                })
        }

</script>
@endsection
