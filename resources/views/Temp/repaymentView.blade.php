@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>View</h2>
                        <br>
                        <h3> Count :
                            <span id="count">0</span><br>
                            Total Amount : <span id="total_amount">0</span><br>
                            Paid Amount : <span id="paid_amount">0</span>
                        </h3>


                    </div>
                    <div class="pull-right">

                        <form id="form" method="post">
                            {{-- <form action="/temp/view/getloans" method="post"> --}}
                            {{-- @csrf --}}
                            <label for="">Select Center : </label>
                            <select required name="center" id="select_center">
                                @foreach($centers as $center)
                                <option value="{{$center->center_name}}">{{$center->center_no}}-{{$center->center_name}}
                                </option>
                                @endforeach
                            </select>
                            <input required type="date" name="date" id="date" oninput="console.log(this.value)">

                            <a onclick="get_loans()" class="btn btn-success" data-toggle="modal"
                                data-target="#exampleModal">Get Loans
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <table id="loans_table" class="table table-bordered" id="loans_table">
        <tr>
            <th>NIC</th>
            <th>Center</th>
            <th>Borrower No</th>
            <th>Paid Amount</th>
            <th>Date</th>
            <th>Total Payed</th>
            <th>Total Payed 2</th>

        </tr>
        {{-- @foreach($repayments as $r)
            <tr>
                <th>{{$r->borrower_nic}}</th>
        <th>{{$r->paid_amount}}</th>
        <th>{{$r->group_no}}</th>

        </tr>
        @endforeach --}}


    </table>
</div>
</div>
@endsection

@section('scripts')
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function get_loans(){
            loans_table.innerHTML = `
                    <tr>
                        <th>NIC</th>
                        <th>Center</th>
                        <th>Borrower No</th>
                        <th>Paid Amount</th>
                        <th>Date</th>
                        <th>Total Payed</th>
			<th>Total Payed 2</th>
                    </tr>
                    `
            $.ajax({
                type: 'POST',
                url: '{{('/temp/view/getloans')}}',
                data: new FormData(form) ,
                processData: false,
                contentType: false,
                success: function(data){
                    console.log(data);
                    return show_data(data)
                }
            })
        }

        function show_data(data){
            let count = 0
            let total = 0
            let paid = 0
            data.forEach(d => {
                html = `
                <tr>
                    <th>${d.nic}</th>
                    <th>${d.center}</th>
                    <th>${d.borrower_no}</th>
                    <th>${d.paid_amount}</th>
                    <th>${d.payment_date}</th>
                    <th>${d.total}</th>
			<th>${d.total_payed}</th>
                                                      @if(Auth::user()->access_level == 'super_user')

                    <th> <a  class="btn btn-danger" id="${d.id}" onclick="deleteRecord(this.id, ${d.paid_amount})">Delete</a> </th>
@endif
                </tr>
                `
                loans_table.innerHTML += html
                count++
                total += d.total
                paid += parseInt(d.paid_amount)
                // console.log(loans_table.innerHTML);
            })
            document.querySelector('#count').textContent = count
            document.querySelector('#total_amount').textContent = total
            document.querySelector('#paid_amount').textContent = paid

        }

        function deleteRecord(id, paid_amount){
            console.log(paid_amount);
            $.ajax({
                    type: 'POST',
                    url: '{{url('/temp/view/deleteLoans')}}',
                    data: {
                        id,
                        paid_amount
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data)
                        return get_loans()
                    }
                })
        }

</script>
@endsection
