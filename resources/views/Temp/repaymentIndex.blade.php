@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Repayment</h2>

                    </div>
                    <div class="pull-right">

                        <input type="number" name="" id="fcid_value" oninput="fcid(this.value)">
                        <button class="btn btn-danger d-none" id="fcid_button">FCID</button>
                        <label for="">
                            <h3>Center Total Collection : <span id="center_total"> 0 </span> </h3>
                        </label>
                        <label for="">Select Center : </label>
                        <select name="" id="select_center">
                            @foreach($centers as $center)
                            <option value="{{$center->center_name}}">{{$center->center_name}}</option>
                            @endforeach
                        </select>
                        <button type="button" class="btn btn-success" data-toggle="modal" onclick="getLoans()"
                            data-target="#exampleModal">Get Loans
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="date" name="" id="date_all_input">
    <button class="btn btn-primary" id="date_all_btn" onclick="date_to_all()">Apply date to all</button>

    <input type="number" name="" id="payment_all_input">
    <button class="btn btn-primary" id="payment_all_btn" onclick="payment_to_all()">Apply payment to all</button>

    <table class="table table-bordered" id="loans_table">
        <tr>
            <th>NIC</th>
            <th>Name</th>
            <th>Total Amount</th>
            <th>Balance Amount</th>
            <th>Group No</th>
            <th>Loan Date</th>
            <th>
                Date
                {{-- <br>
                    <button class="btn btn-primary" id="date_all_btn">Apply date to all</button> --}}
            </th>
            <th>
                Payment
                {{-- <br>
                    <button class="btn btn-primary" id="payment_all_btn">Apply payment to all</button> --}}
            </th>
        </tr>

    </table>
</div>
</div>

@endsection

@section('scripts')
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let records_arr = []


        function fcid(value){
            fcid_button.classList.add('d-none')
            if(parseInt(value) !== parseInt(center_total.textContent)){
                fcid_button.classList.remove('d-none')
            }
        }

        function getLoans(){

            loans_table.innerHTML = `
                    <tr>
                        <th>NIC</th>
                        <th>Name</th>
                        <th>Total Amount</th>
                        <th>Balance Amount</th>
                        <th>Group No</th>
                        <th>Loan Date</th>
                        <th>Date</th>
                        <th>Payment</th>
                    </tr>
                    `

            $.ajax({
                type: 'POST',
                url: '{{('/temp/repayments/getloans')}}',
                data: {
                    'center': select_center.value,
                } ,
                success: function(data){
                    console.log(data);
                    if(data.length > 0){

                    data.forEach(record => {
                        html = `

                        <td>${record.nic}<br>${record.borrower_no}</td>
                        <td>${record.full_name}</td>
                        <td>${record.loan_amount}</td>
                        <td>${record.due}</td>
                        <td>${record.group_no}</td>
                        <td>${record.release_date}</td>
                        <td> <input type="date" class="dates" id="date_${record.id}"></td>
                        <td><input type="number" class="payments" id="payment_${record.id}"></td>
                        <td><button class="btn btn-primary" id="btn_${record.id}" onclick="collect(this.id,${record.due})">Collect</button></td>
                        <td><button class="btn btn-warning" id="btn_${record.id}" onclick="notpaidtemp(this.id,${record.due})">Not Paid</button></td>
                        `
                        loans_table.innerHTML += html
                        records_arr.push(record)
                    })
                    // console.log(records_arr, 'records_arr')

                }

                }
            })


        }

        function notpaidtemp(id,balance){

            id = id.slice(id.indexOf('_')+1, id.length)

            $.ajax({
                    type: 'POST',
                    url: '{{('/temp/repayments/notpaid')}}',
                    data: {
                        'id' : id,
                    },
                    success: function(data){
                        console.log(data);
                        center_total.textContent = data
                        // return getLoans();
                        document.querySelector(`#btn_${id}`).disabled = true
                        setTimeout(() => {
                            document.querySelector(`#btn_${id}`).disabled = false
                        },5000)
                    }

                })


            }
        function collect(id,balance){

            id = id.slice(id.indexOf('_')+1, id.length)
            console.log(document.querySelector(`#payment_${id}`).value)
                console.log(balance)
                // if(parseFloat(balance)>parseFloat(document.querySelector(`#payment_${id}`).value)){
                    if(parseFloat(document.querySelector(`#payment_${id}`).value)>0){
                        if(parseFloat(document.querySelector(`#payment_${id}`).value)<parseFloat(balance)){

                $.ajax({
                    type: 'POST',
                    url: '{{('/temp/repayments/collect')}}',
                    data: {
                        'id' : id,
                        'date' : document.querySelector(`#date_${id}`).value,
                        'payment' : document.querySelector(`#payment_${id}`).value,
                    },
                    success: function(data){
                        console.log(data);
                        center_total.textContent = data
                        // return getLoans();
                        document.querySelector(`#btn_${id}`).disabled = true
                        setTimeout(() => {
                            document.querySelector(`#btn_${id}`).disabled = false
                        },5000)
                    }

                })
            }else{
                swal.fire('කරුණාකර ලබාගත යුතු  අගයක් ලබාදෙන්න')
            }
            }else{
                swal.fire('කරුණාකර 0 ට වැඩි අගයක් ලබාදෙන්න')
            }

            // }else{
                // swal.fire('sdsadsad')
            // }
            }

            function date_to_all(){
                records_arr.forEach(r => {
                        // console.log(document.querySelectorAll('.dates') )
                        document.querySelectorAll('.dates').forEach(date => {
                            date.value = date_all_input.value
                        })
                    })
            }

            function payment_to_all(){
                records_arr.forEach(r => {
                        // console.log(document.querySelectorAll('.dates') )
                        document.querySelectorAll('.payments').forEach(date => {
                            date.value = payment_all_input.value
                        })
                    })
            }






</script>
@endsection
