@extends('layouts.master')
@section('title')

@endsection
@section('content')

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Available Staff</strong>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Mobile</th>
                                        <th>Mail</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- <td>afasf</td>
                                    <td>afsa</td>
                                    <td>afas</td>
                                    <td>asfsa</td>
                                    <td><input type="button" class="" value="VIEW"></td> --}}
                                    @foreach ($staffs as $staff)
                                        <tr>
                                            <td>{{ $staff->name }}</td>
                                            <td>{{ $staff->designation }}</td>
                                            <td>{{ $staff->mobile }}</td>
                                            <td>{{ $staff->mail }}</td>
                                            <td><input type="button" class="" value="VIEW">
                                                <a class="btn btn-primary" href="{{ route('staff.edit', $staff->id) }}"><i
                                                        class="fa fa-bars"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
