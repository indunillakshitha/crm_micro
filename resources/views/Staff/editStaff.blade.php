@extends('layouts.master')
@section('title')

    Edit Branch

@endsection
@section('content')
<div class="">
    <div class="card">
        <div   class="card-body">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Staff</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('staff.index') }}"> Back</a>
            </div>
        </div>
    </div>

    {{-- @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Warning!</strong> Please check input field code<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif --}}

            <form action="{{ route('staff.update',$staff->id) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">

                            <strong>Branch Name:</strong>
                            <input type="text" name="branch" oninput="toCap()" class="form-control"
                            placeholder="" id="Cap" value="{{$staff->branch}}">
                            {{-- <select name="branch" class="form-control" id="branch">

                                @foreach ($branches as $branch)
                                    <option value={{$branch->branch_name}}>
                                        {{$branch->branch_name}}
                                    </option>
                                @endforeach
                            </select> --}}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Full Name:</strong>
                            <input type="text" name="name" oninput="toCap()" class="form-control"
                        placeholder="" id="Cap" value="{{$staff->name}}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Applying name:</strong>
                            <input type="text" name="applying_name" oninput="toCap()" class="form-control"
                                   placeholder="" id="Cap" value="{{$staff->applying_name}}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Relation's Contact :</strong>
                            <input type="text" name="relational_contact" oninput="toCap()" class="form-control"
                                   placeholder="" id="Cap" value="{{$staff->relational_contact}}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong for="recipient-name" class="col-form-label">Designation:</strong>
                            <select name="designation" class="form-control">
                                <option value="Director">Director</option>
                                <option value="Operation Manager">Operation Manager</option>
                                <option value="Senior Manager">Senior Manager</option>
                                <option value="Branch Manager">Branch Manager</option>
                                <option value="Admin Officer">Admin Officer</option>
                                <option value="Assistant Manager">Assistant Manager</option>
                                <option value="Account Head">Account Head</option>
                                <option value="Account Executive">Account Executive</option>
                                <option value="Agent">Field Executive"</option>
                                {{-- <option value="Agent">Agent</option> --}}
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>NIC:</strong>
                            <input type="text" name="nic" class="form-control"
                            value="{{$staff->nic}}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Address :</strong>
                            <input type="text" name="address" oninput="toCap()" class="form-control"
                            value="{{$staff->address}}" id="Cap">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Mobile:</strong>
                            <input type="text" name="mobile"  class="form-control"
                            value="{{$staff->mobile}}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Email:</strong>
                            <input type="text" name="mail"  class="form-control"
                            value="{{$staff->mail}}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Education Qualifications :</strong>
                            <select name="edu_qual" class="form-control">
                                <option value="O/L">O/L</option>
                                <option value="A/L">A/L</option>
                                <option value="Diploma">Diploma</option>
                                <option value="HND">HND</option>
                                <option value="Degree">Degree</option>
                                <option value="Other">Other</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Proffessional Qualification:</strong>
                            <input type="text" name="pro_qual" class="form-control"
                                   id="Cap" oninput="toCap()" value="{{$staff->pro_qual}}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Proffessional Experience :</strong>
                            <input type="text" name="pro_exp" class="form-control"
                                   id="Cap" oninput="toCap()" value="{{$staff->pro_exp}}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong for="recipient-name" class="col-form-label">Civil Status:</strong>
                        <select name="civil_status" class="form-control">
                            <option value="Married">Married</option>
                            <option value="Unmarried">Unmarried</option>
                            <option value="Married">Engaged</option>
                            <option value="Married">Divorced</option>
                            <option value="Unmarried">Widow</option>
                        </select>
                    </div>
                    </div>
                    <div class="custom-file col-xs-6 col-sm-6 ">
                        <input type="file" class="custom-file-input" name="img_nic" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose NIC</label>
                    </div>
                    <div class="custom-file col-xs-6 col-sm-6 ">
                        <input type="file" class="custom-file-input" name="img_birth_certificate" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose Birth Certificate</label>
                    </div>
                    <div class="custom-file col-xs-6 col-sm-6 ">
                        <input type="file" class="custom-file-input" name="img_gr_cer" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose Gramasewaka Certificate</label>
                    </div>
                    <div class="custom-file col-xs-6 col-sm-6 ">
                        <input type="file" class="custom-file-input" name="img_police_cer" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose Police Certificate</label>
                    </div>
                    <div class="custom-file col-xs-6 col-sm-6 ">
                        <input type="file" class="custom-file-input" name="img_married_cer" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose Married Certificate</label>
                    </div>
                    <div class="custom-file col-xs-6 col-sm-6 ">
                        <input type="file" class="custom-file-input" name="img_edu_1_cer" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose Education Certificates</label>
                    </div>
                    <div class="custom-file col-xs-6 col-sm-6 ">
                        <input type="file" class="custom-file-input" name="img_edu_2_cer" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose Education Certificates 2</label>
                    </div>
                    <div class="custom-file col-xs-6 col-sm-6 ">
                        <input type="file" class="custom-file-input" name="img_edu_3_cer" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose Education Certificates 3</label>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection
