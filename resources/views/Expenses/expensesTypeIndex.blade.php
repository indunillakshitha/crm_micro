@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')


    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Expenses Types  </h2>
                        </div>
                        <div class="pull-right">
                            <!-- <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#exampleModalagent">Add New Expenses Type
                            </button> -->
                            <a class="btn btn-success" href="{{ route('expensestype.create') }}"> Add New Expenses </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">


        <table class="table table-bordered">
            <tr>
                <th>Expense Type</th>
                <th>Comment</th>
                {{-- <th>Type</th> --}}

            </tr>
        </table>
        @foreach ($expensesTypes as $expensesType)
            <div class="card" style="width: 100%;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-5">{{ $expensesType->expense_type}}</div>
                        <div class="col-sm-5">{{ $expensesType->comment }}</div>
                        <div class="col-sm-2" width="10px">
                            <div class="row">
                                <form action="#" method="POST">
                                    <a class="btn btn-info row-cols-sm-4"
                                       href="#">Show</a>
                                    <a class="btn btn-primary row-cols-sm-4"
                                       href="#">Edit</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger row-cols-sm-4">Delete</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach

    </div>

@endsection


