@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')


    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Expenses</h2>
                        </div>
                        <div class="pull-right">
                            <!-- <button type="button" class="btn btn-success" data-toggle="modal"
                                       href="{{ route('addexpense.create') }}" data-target="#exampleModalagent">Add New Expenses
                                </button> -->
                            <a class="btn btn-success" href="{{ route('addexpense.create') }}"> Add New Expenses </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">


        <table class="table table-bordered">
            <tr>
                <th>Type</th>
                <th>Amount</th>
                {{-- <th>Type</th> --}}

            </tr>
        </table>
        @foreach ($expenses as $expense)
            <div class="card" style="width: 100%;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-5">{{ $expense->expense_type }}</div>
                        <div class="col-sm-5">{{ $expense->expense_amount }}</div>
                        <div class="col-sm-2" width="10px">
                            <div class="row">
                                <form action="#" method="POST">
                                    <a class="btn btn-info row-cols-sm-4"
                                       href="#">Show</a>
                                    <a class="btn btn-primary row-cols-sm-4"
                                       href="/referenceExpense/create/{{$expense->id}}">Add Expense</a>
                                    <a class="btn btn-info row-cols-sm-4" href="#">Show</a>
                                    <a class="btn btn-primary row-cols-sm-4" href="/voucher/export/{{ $expense->id }}">Print
                                        Voucher</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger row-cols-sm-4">Delete</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach

    </div>

@endsection
