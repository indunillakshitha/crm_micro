
@extends('layouts.master')
@section('title')

    Create New Expense
@endsection
@section('content')



<div class="animated fadeIn">
    <div class="card">
        <div   class="card-body">

            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Create New Expense</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('addexpense.index') }}"> Back</a>
                    </div>
                </div>
            </div>
        </div></div></div>

            <div class="">
            <div class="card">
                <div   class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Warning!</strong> Please check your input code<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            <div class="modal-body">
                <form action="{{ route('addexpense.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                                <strong>Expense Type:</strong><br><br>
                                <select name="expense_type" class="form-control" id="expense_type" value="expense_type">

                                    <option value="0" selected="true">-Select-</option>
                                    @foreach($expensesTypes as $expenseType)
                                        <option
                                            value="{{$expenseType->expense_type}}">{{$expenseType->expense_type}}</option>
                                    @endforeach

                                </select>

                            </div>


                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Expense Amount:</strong>
                                <input type="text" name="expense_amount" class="form-control" placeholder="Expense Amount" id="expense_amount" >
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group col-6">
                                <strong>Expense Date:</strong>
                                <input class="date form-control" type="text" name="expense_date" id="expense_date">
                                {{-- <input type="text" name="expense_date" class="form-control" placeholder="2020-12-12"> --}}
                            </div>
                             <div class="form-group col-6">
                                <strong>Branch :</strong>
                                <input  type="text" name="branch" id="branch" class="form-control" placeholder="Branch ">
                                {{-- <input type="text" name="expense_date" class="form-control" placeholder="2020-12-12"> --}}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Ledger Account :</strong>
                                <input type="text" name="ledger_account" class="form-control" placeholder="Ledger Account" id="ledger_account" >
                            </div>
                        </div>
                         <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Bank :</strong>
                                <input type="text" name="bank" class="form-control" placeholder="Bank" id="bank" >
                            </div>
                        </div>
                         <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Cheque No :</strong>
                                <input type="text" name="cheque_no" class="form-control" placeholder="Cheque No" id="cheque_no" >
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group col-6">
                                <strong>Cheque Date :</strong>
                                <input class="date form-control" type="text" name="cheque_date" id="cheque_date">
                                {{-- <input type="text" name="cheque_date" class="form-control" placeholder="2020-12-12"> --}}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Link to Loan :</strong>
                                <input type="text" name="link_to_loan" class="form-control" placeholder="Link to Loan" id="link_to_loan" >
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script type="text/javascript">
            $('.date').datepicker({

                format: 'yyyy-mm-dd'

            });
 </script>
            @endsection
