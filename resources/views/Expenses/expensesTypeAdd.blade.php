
@extends('layouts.master')
@section('title')

    Add Center
@endsection
@section('content')



<div class="animated fadeIn">
    <div class="card">
        <div   class="card-body">

            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Create New Expenses Type</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('expensestype.index') }}"> Back</a>
                    </div>
                </div>
            </div>
        </div></div></div>

            <div class="">
            <div class="card">
                <div   class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Warning!</strong> Please check your input code<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            <div class="modal-body">
                <form action="{{ route('expensestype.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Expenses Type:</strong>
                                <input type="text" name="expense_type" class="form-control" placeholder="Expenses Type" id="expense_type" >
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Comment :</strong>
                                <input type="text" name="comment" class="form-control" placeholder="Comment" id="comment">
                            </div>
                        </div>




                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
            </div>
        </div>
    </div>


@endsection


