@extends('layouts.master')
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Due Report</h2>
                    </div>

                </div>

            </div>
        </div>
        <table class="table table-bordered col-12" id="loan_table">

            @isset($mon)
            @foreach ($mon as $item)
            <tr>
                <td>{{ $item->center }}</td>
                <td><?php echo number_format( $item->sum  , 2 , '.' , ',' ) ?></td>
            </tr>
            @endforeach
            @endisset
        </table>
    </div>
</div>
<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Collection Report</h2>
                    </div>

                </div>

            </div>
        </div>
        <table class="table table-bordered col-12" id="loan_table">

            @isset($collection)
            @foreach ($collection as $item)
            <tr>
                <td>{{ $item->center }}</td>
                <td><?php echo number_format( $item->collection  , 2 , '.' , ',' ) ?></td>
            </tr>
            @endforeach
            @endisset
        </table>
    </div>
</div>

@endsection
