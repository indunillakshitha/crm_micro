@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h4>Change Your Current Password</h4>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="/home"> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-header">
            <strong>Confirm </strong> and Reset
        </div>
        <div class="card-body card-block">
            @if(session()->has('message'))
                <div class="alert alert-success">
                {{ session()->get('message') }}
                </div>
            @endif
            <form action="{{ route('change_pasword.store') }}" method="POST"" method="post" class="">
                @csrf
                <div class="form-group">
                   <input type="password" id="old_p" name="old_p" placeholder="Enter Current Password.." class="form-control">
                    {{-- <span class="help-block">
                        Please enter your current password</span> --}}
                </div>
                <div class="form-group">
                    <input type="password" id="password" name="password" placeholder="Enter New Password.." class="form-control">
                    {{-- <span class="help-block">Please enter your new password</span> --}}
                </div>
                <div class="form-group">

                    <input type="password" id="password_c" name="password_c" placeholder="Confirm New Password.." class="form-control">
                    {{-- <span class="help-block">Confirm your new password</span> --}}
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>

                </div>
            </form>
        </div>

    </div>
</div>

@endsection


