@extends('layouts.master')

@section('title')

    Charts | CRM
@endsection
@section('content')
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="bar-chart" width="800" height="450"></canvas>
        </div>
        </div>
</div>
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="line-chart" width="800" height="450"></canvas>
        </div>
        </div>
</div>
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="line-chart2" width="800" height="450"></canvas>
        </div>
        </div>
</div>
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="bar-chart-grouped" width="800" height="450"></canvas>
        </div>
        </div>
</div>
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="bar-chart2" width="800" height="450"></canvas>
    </div>
    </div>
</div>
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
        <canvas id="mixed-chart" width="800" height="450"></canvas>
         </div>
    </div>
</div>






                @endsection

@section('scripts')
<script type="text/javascript">
        var ldata = @json($Users);
        var ctdata = @json($Cols);
        var edata = @json($Exps);
        var cloans = @json($Cloans);
        var colCap = @json($ColCap);
        var colInt = @json(($ColInt));
        var colAdv = @json(($ColAdv));
        var arrTot = @json(($ArrTot));
        var arrCap = @json(($ArrCap));
        var arrInt = @json(($ArrInt));


    $(function () {

            new Chart(document.getElementById("bar-chart"), {

    type: 'bar',
    data: {
      labels: ["10000", "15000", "20000", "25000"],
      datasets: [
        {
          label: "Ammount",
          backgroundColor: ["#a7e81b", "#e8711b","#15ce9c","#15a1ce","#c45850"],
          data: ldata
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Loans Amount By Each Category'
      }
    }
});
// ---
new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
    datasets: [{
        data: cloans,
        label: "Lons ",
        borderColor: "#3e95cd",
        fill: false
      },
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Loans Count By Per Month'
    }
  }
});
// ----
new Chart(document.getElementById("line-chart2"), {
  type: 'line',
  data: {
    labels: [0,"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
    datasets: [{
        data: colInt,
        label: "Interest",
        borderColor: "#3e95cd",
        fill: false
      }, {
        data: colCap,
        label: "Capital",
        borderColor: "#8e5ea2",
        fill: false
      }, {
        data: ctdata,
        label: "Total",
        borderColor: "#3cba9f",
        fill: false
      },
       {
        data: colAdv,
        label: "Advanced",
        borderColor: "#e8711b",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Collections(per month)'
    }
  }
});

new Chart(document.getElementById("bar-chart-grouped"), {
    type: 'bar',
    data: {
      labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
      datasets: [
        {
          label: "Interest",
          backgroundColor: "#3e95cd",
          data: arrInt
        }, {
          label: "Capital",
          backgroundColor: "#8e5ea2",
          data: arrCap
        },{
          label: "Total",
          backgroundColor: "#3cba9f",
          data: arrTot
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Arrears (per month)'
      }
    }
});

new Chart(document.getElementById("bar-chart2"), {
    type: 'bar',
    data: {
      labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
      datasets: [
        {
          label: "Expence",
          backgroundColor: "#3cba9f",
          data: edata
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Expences (per month)'
      }
    }
});

new Chart(document.getElementById("mixed-chart"), {
    type: 'bar',
    data: {
      labels: [0,"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
      datasets: [{
          label: "Income",
          type: "line",
          borderColor: "#8e5ea2",
          data: [408,547,675,734],
          fill: false
        },  {
          label: "Income",
          type: "bar",
          backgroundColor: "rgba(0,0,0,0.2)",
          data: [408,547,675,734],
        },
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Other Incomes'
      },
      legend: { display: false }
    }
});







    })
</script>

@endsection

