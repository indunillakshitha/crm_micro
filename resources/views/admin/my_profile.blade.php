@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

<div class="mx-auto">
    <div class="card animated fadeIn">
        <div class="card-header">
            <strong class="card-title mb-3">Profile Card</strong>
        </div>
        <div class="card-body">
            <div class="mx-auto d-block">
                <img class="rounded-circle mx-auto d-block" src="images/admin.jpg" alt="Card image cap">
                <h5 class="text-sm-center mt-2 mb-1">{{$staff_member->name}}</h5>
                <div class="location text-sm-center"><i class="fa fa-user"></i> {{$staff_member->designation}}</div>
                <div class="location text-sm-center"><i class="fa fa-tag"></i> {{$staff_member->nic}}</div>
                <div class="location text-sm-center"><i class="fa fa-flag"></i> {{$staff_member->center}}</div>
                <div class="location text-sm-center"><i class="fa fa-mobile"></i> {{$staff_member->mobile}}</div>
                <div class="location text-sm-center"><i class="fa fa-email"></i> {{$staff_member->mail}}</div>
                <div class="location text-sm-center"><i class="fa fa-map-marker"></i> {{$staff_member->address}}</div>
            </div>
            <hr>
            <div class="card-text text-sm-center">
                <a href="#"><i class="fa fa-facebook pr-1"></i></a>
                <a href="#"><i class="fa fa-twitter pr-1"></i></a>
                <a href="#"><i class="fa fa-linkedin pr-1"></i></a>
                <a href="#"><i class="fa fa-pinterest pr-1"></i></a>
            </div>
        </div>
    </div>
</div>

@endsection


