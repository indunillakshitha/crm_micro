@extends('layouts.master')

@section('title')

Dashboard | CRM
@endsection

@section('content')

<div class="row">
    <?php  $access_level = Auth::user()->access_level ?>
    @if($access_level == 'super_user')
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-success border-success"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Total Loans</div>
                        <div class="stat-digit count">{{$loan_count}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Total Customers</div>
                        <div class="stat-digit count">{{$borrower_count}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Loans to Pay</div>
                        <div class="stat-digit count">{{$loans_to_pay}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Total Customers</div>
                        <div class="stat-digit">961</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-layout-grid2 text-warning border-warning"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Active Centers</div>
                        <div class="stat-digit count">{{$centers}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endif
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-layout-grid2 text-warning border-warning"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Today Appraisals</div>
                        <div class="stat-digit count">{{$apprasails}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12 borrower_id">
                <div class="pull-left">
                    <h2>Loans Summery</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <?php  $access_level = Auth::user()->access_level ?>
    @if($access_level == 'super_user')
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-success border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Issued Loans</div>
                        <div class="stat-digit count">{{$issued_loans}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-success"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Active Loans</div>
                        <div class="stat-digit count">{{$active_loans}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-success border-success"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Setle Loans</div>
                        <div class="stat-digit count">{{$setle_loans}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">New Loans</div>
                        <div class="stat-digit count">{{$new_loans}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Pending Loans</div>
                        <div class="stat-digit count">{{$pending_loans}}</div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="card col-12">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Total Outstanding</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Capital Outstanding</div>
                        <div class="stat-digit ">
                            <?php echo number_format( $total_capital_out , 2 , '.' , ',' ) ?>
                        </div>
                        <a class="btn btn-outline-light btn-lg btn-block " href="#">See More
                            Details</a>

                    </div>
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Interest Outstanding</div>
                        <div class="stat-digit ">
                            <?php echo number_format( $total_inter_out , 2 , '.' , ',' ) ?>
                        </div>
                        <a class="btn btn-outline-light btn-lg btn-block " href="#">See More
                            Details</a>

                    </div>
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Total Outstanding</div>
                        <div class="stat-digit ">
                            <?php echo number_format( $total_outstanding , 2 , '.' , ',' ) ?>
                        </div>
                        <a class="btn btn-outline-light btn-lg btn-block " href="#">See More
                            Details</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card col-12">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Repayment Of Week</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Monday</div>
                        <div class="stat-digit ">
                            <?php echo number_format( $mon_payments , 2 , '.' , ',' ) ?>/
                            <?php echo number_format( $mon , 2 , '.' , ',' ) ?>
                        </div>
                        <a class="btn btn-outline-light btn-lg btn-block " href="{{url('centersdue/monday')}}">See More
                            Details</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Tuesday</div>
                        <div class="stat-digit ">
                            <?php echo number_format( $tue_payments , 2 , '.' , ',' ) ?>/<?php echo number_format( $tue , 2 , '.' , ',' ) ?>
                        </div>
                    </div>
                    <a class="btn btn-outline-light btn-lg btn-block " href="{{url('centersdue/tuesday')}}">See More
                        Details</a>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Wednesday</div>
                        <div class="stat-digit ">
                            <?php echo number_format( $wed_payments , 2 , '.' , ',' ) ?>/<?php echo number_format( $wed , 2 , '.' , ',' ) ?>
                        </div>
                    </div>
                    <a class="btn btn-outline-light btn-lg btn-block " href="{{url('centersdue/wednesday')}}">See More
                        Details</a>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Thursday</div>
                        <div class="stat-digit ">
                            <?php echo number_format( $thu_payments , 2 , '.' , ',' ) ?>/<?php echo number_format( $thu , 2 , '.' , ',' ) ?>
                        </div>
                    </div>
                    <a class="btn btn-outline-light btn-lg btn-block " href="{{url('centersdue/thursday')}}">See More
                        Details</a>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Friday</div>
                        <div class="stat-digit ">
                            <?php echo number_format( $fri_payments , 2 , '.' , ',' ) ?>/<?php echo number_format( $fri , 2 , '.' , ',' ) ?>
                        </div>
                    </div>
                    <a class="btn btn-outline-light btn-lg btn-block " href="{{url('centersdue/friday')}}">See More
                        Details</a>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Saturday</div>
                        <div class="stat-digit ">
                            <?php echo number_format( $sat_payments , 2 , '.' , ',' ) ?>/<?php echo number_format( $sat , 2 , '.' , ',' ) ?>
                        </div>
                    </div>
                    <a class="btn btn-outline-light btn-lg btn-block " href="{{url('centersdue/saturday')}}">See More
                        Details</a>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Sunday</div>
                        <div class="stat-digit ">
                            <?php
                                echo number_format( $sun_payments , 2 , '.' , ',' ) ?>/<?php echo number_format( $sun , 2 , '.' , ',' ) ?>
                        </div>
                    </div>
                    <a class="btn btn-outline-light btn-lg btn-block " href="{{url('centersdue/sunday')}}">See More
                        Details</a>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Total</div>
                        <div class="stat-digit ">
                            <?php
                        echo number_format( $sun_payments+$sat_payments+$fri_payments+$thu_payments+$wed_payments+$tue_payments+$mon_payments, 2 , '.' , ',' ) ?>
                            /<?php echo number_format( $sun_all, 2 , '.' , ',' ) ?></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="">
        <div class="card col-12">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Repayment Collection Reports</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card col-12">
            <div class="card-body ">
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="pull-left">
                        </div>

                        <div class="col-1">
                            <label for="">Select Branch:</label>
                        </div>
                        <div class="pull-right col-3">
                            <select name="select_branch1" id="select_branch1" class="form-control">
                                <option value="">--Select--</option>
                                @foreach($branches as $branch)
                                <option value="{{$branch->branch_name}}">{{$branch->branch_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="pull-left">
                        </div>
                        <div class="col-1">
                            <label for="">Select Center:</label>
                        </div>
                        <div class="pull-right col-3">
                            <select name="" id="select_center1" class="form-control">
                                <option value="">--Select--</option>
                                @foreach($centers as $center)
                                <option value="{{$center->center_name}}">{{$center->center_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-4">
                            <div class="col-12">
                                <div class="col-3">

                                    <label for="">Select Date </label>
                                </div>
                                <div class="col-9">

                                    <input type="date" name="da" id="selected_date1" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="pull-right col-1 ">
                            <button type="button" class="btn btn-success" onclick="repayments()" id="search_btn1"
                                data-toggle="modal" data-target="#exampleModal">Search
                            </button>
                        </div>


                    </div>
                </div>
                <table class="table table-bordered" id="collections_reports_table1">


                </table>
            </div>



        </div>
        @endif




    </div>
    <div class="card  col-6">
        <div class="card-body ">
            <div class="stat-widget-one col-12">
                <canvas id="bar-chartn" width="800" height="450"></canvas>
            </div>
        </div>
        <a class="btn btn-outline-light btn-lg btn-block " href="/centertotalreport">See More Details</a>
    </div>
    <div class="card  col-6">
        <div class="card-body ">
            <div class="stat-widget-one col-12">
                <canvas id="line-chartn" width="800" height="450"></canvas>
            </div>
        </div>
        <a class="btn btn-outline-light btn-lg btn-block " href="#">See More Details</a>
    </div>
    <div class="card  col-6">
        <div class="col-12">
            <div class="pull-right col-8">
                <label for="">Select Branch:</label>
                <select name="select_branchA" id="select_branchA" class="form-control">
                    <option value="">Select</option>
                    @foreach($branches as $branch)
                    <option value="{{$branch->branch_name}}">{{$branch->branch_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-8 ">
                <br>
                <button type="button" onclick="branchWise(this)" class="btn pull-right col-4 btn-success"
                    id="search_btnA">Check</button>
            </div>

        </div>

        <div class="card-body ">
            <div class="stat-widget-one col-12">
                <canvas id="bar-chartA" width="800" height="450"></canvas>
            </div>
        </div>




    </div>











    @endsection

    @section('scripts')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });





        var pNl = @json($PronLost);
        var CulTot = @json($CuTot);





    $(function () {

new Chart(document.getElementById("bar-chartn"), {

    type: 'bar',

    data: {
      labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
      datasets: [
        {
          label: "Ammount",
          backgroundColor: "#15ce9c",
          data: pNl
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Profit And Lost'
      }
    }
});

new Chart(document.getElementById("line-chartn"), {
  type: 'line',
  data: {
    labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
    datasets: [{
        data: CulTot,
        label: "Ammount ",
        borderColor: "#e8711b",
        fill: false
      },
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Cumulative'
    }
  }
});






    })
var myChart;
function branchWise(){
    console.log(select_branchA.value);

    $.ajax({

        type: 'GET',
        url: '{{('/home/branchExp')}}',
        data:{'branch':select_branchA.value},
        dataType: 'JSON',
        success: function (data) {
            console.log(data);
            if (myChart) {
        myChart.destroy();
      }
            var ctx = document.getElementById("bar-chartA").getContext("2d");
            myChart = new Chart(ctx, {

                type: 'bar',
                data: {
                labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
                datasets: [
                    {
                    label: "Ammount",
                    backgroundColor:   "#3cba9f",
                    data:data.Exp,
                    }
                ]
                },
                options: {
                legend: { display: false },
                title: {
                    display: true,
                    text: 'Expences (per month)'
                }
                }
            });

        }
    })

}
    const select_branch = document.querySelector('#select_branch1')

    const select_center = document.querySelector('#select_center1')

    const selected_date = document.querySelector('#selected_date1')

function repayments(){
    let date = new Date(selected_date1.value)
    var start = new Date(date.getFullYear(), 0, 0)
    var diff = (date - start) + ((start.getTimezoneOffset() - date.getTimezoneOffset()) * 60 * 1000)
    var oneDay = 1000 * 60 * 60 * 24;
    var day = Math.floor(diff / oneDay);
    console.log(select_branch.value);
    console.log(select_center.value);
    console.log(selected_date.value);

    $.ajax({

        type: 'GET',
        url: '{{('/home/WeekRepayDetalis')}}',
        data:{'day': selected_date.value,'branch':select_branch.value,'center':select_center.value},
        dataType: 'JSON',
        success: function (data) {
            console.log(data);

                document.querySelector('#collections_reports_table1').innerHTML = `
                         <tr><th colspan=7 ><center>Repayment ollection</center></th></tr>
                        <tr>
                            <th>Monday</th>
                            <th>Tuesday</th>
                            <th>Wednesday</th>
                            <th>Thursday</th>
                            <th>Friday</th>
                            <th>Saturday</th>
                            <th>Sunday</th>


                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${data.Week[0]}</th>
                                     <th>${data.Week[1]}</th>
                                     <th>${data.Week[2]}</th>
                                     <th>${data.Week[3]}</th>
                                     <th>${data.Week[4]}</th>
                                     <th>${data.Week[5]}</th>
                                     <th>${data.Week[6]}</th>

                                </tr>`

                                document.querySelector('#collections_reports_table1').innerHTML += html

        }
    })

}

    </script>
    @endsection
