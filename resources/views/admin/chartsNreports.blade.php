@extends('layouts.master')

@section('title')

Dashboard | CRM
@endsection

@section('content')



<div class="row">
    <?php  $access_level = Auth::user()->access_level ?>
    @if($access_level == 'super_user')

    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="col-lg-6 col-md-6">
                    <div class="stat-widget-one col-6">
                        <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text">Total Collection</div>
                            <div class="stat-digit "><?php echo number_format( $loansSum , 2 , '.' , ',' ) ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="stat-widget-one col-6">
                        <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text">Balance</div>
                            <div class="stat-digit "><?php echo number_format( $totalArrearse , 2 , '.' , ',' ) ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="stat-widget-one col-6">
                        <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text">Advanced</div>
                            <div class="stat-digit ">
                                <?php echo number_format( $totalAdvancedPayment , 2 , '.' , ',' ) ?></div>

                        </div>
                    </div>
                </div>

                <a class="btn btn-outline-light btn-lg btn-block " href="/centertotalreport">See More Details</a>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">


                <div class="stat-widget-one col-4">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Branch Total {{Auth::user()->branch}}</div>
                        <div class="stat-digit "><?php echo number_format( $tot , 2 , '.' , ',' ) ?></div>

                    </div>
                </div>

                <a class="btn btn-outline-light btn-lg btn-block " href="/dashboardViewCollection">See More Details</a>
            </div>
        </div>
    </div>
    @endif




</div>
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="bar-chartn" width="800" height="450"></canvas>
        </div>
    </div>
    <a class="btn btn-outline-light btn-lg btn-block " href="/centertotalreport">See More Details</a>
</div>
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="line-chartn" width="800" height="450"></canvas>
        </div>
    </div>
    <a class="btn btn-outline-light btn-lg btn-block " href="#">See More Details</a>
</div>
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="bar-chart" width="800" height="450"></canvas>
        </div>
    </div>
    <a class="btn btn-outline-light btn-lg btn-block " href="/centertotalreport">See More Details</a>
</div>

<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="line-chart" width="800" height="450"></canvas>
        </div>
    </div>
    <a class="btn btn-outline-light btn-lg btn-block " href="#">See More Details</a>
</div>
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="line-chart2" width="800" height="450"></canvas>
        </div>
    </div>
    <a class="btn btn-outline-light btn-lg btn-block " href="/collectionsreport">See More Details</a>
</div>
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="bar-chart-grouped" width="800" height="450"></canvas>
        </div>
    </div>
    <a class="btn btn-outline-light btn-lg btn-block " href="/arrearseCapitalCollectionsReport">See More Details</a>
</div>
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="bar-chart3" width="800" height="450"></canvas>
        </div>
        </div>
        <a class="btn btn-outline-light btn-lg btn-block " href="/centertotalreport" >See More Details</a>
</div>
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="bar-chart2" width="800" height="450"></canvas>
        </div>
    </div>
    <a class="btn btn-outline-light btn-lg btn-block " href="/centertotalreport">See More Details</a>
</div>

<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
        <canvas id="bar-chart4" width="800" height="450"></canvas>
         </div>
    </div>

        <div class="col-12">

                <div class="pull-right col-3">
                    <label for="">Select Branch:</label>
                    <select name="select_branchC" id="select_branchC">
                    <option value="">Select</option>
                    @foreach($branches as $branch)
                    <option value="{{$branch->branch_name}}" >{{$branch->branch_name}}</option>
                    @endforeach
                    </select>
                </div>


                <div class="pull-right col-3">
                    <label for="">Select Center:</label>
                    <select name="" id="select_centerC">
                    <option value="">Select</option>
                    @foreach($centers as $center)
                    <option value="{{$center->center_name}}" >{{$center->center_name}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="col-3">
                    <label for="">From:</label>
                    <input type="date" name="da" id="selected_date1C"class="form-control"  >
                </div>
                <div class="col-3">
                    <label for="">To:</label>
                    <input type="date" name="da" id="selected_date2C"class="form-control"  >
                </div>

    </div>

    <div class="col-7 ">
        <br>
        <button type="button" class="btn pull-right col-4 btn-success" onclick="collectionBar(this)" id="search_btnC" >Check</button>
    </div>
<a class="btn btn-outline-light btn-lg btn-block " href="/collectionsreport" >See More Details</a>
</div>
{{-- Next --}}
<div class="card  col-6">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
        <canvas id="bar-chartA" width="800" height="450"></canvas>
         </div>
    </div>

        <div class="col-12">

                <div class="pull-right col-3">
                    <label for="">Select Branch:</label>
                    <select name="select_branchA" id="select_branchA">
                    <option value="">Select</option>
                    @foreach($branches as $branch)
                    <option value="{{$branch->branch_name}}" >{{$branch->branch_name}}</option>
                    @endforeach
                    </select>
                </div>


                <div class="pull-right col-3">
                    <label for="">Select Center:</label>
                    <select name="" id="select_centerA">
                    <option value="">Select</option>
                    @foreach($centers as $center)
                    <option value="{{$center->center_name}}" >{{$center->center_name}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="col-3">
                    <label for="">From:</label>
                    <input type="date" name="da" id="selected_date1A"class="form-control"  >
                </div>
                <div class="col-3">
                    <label for="">To:</label>
                    <input type="date" name="da" id="selected_date2A"class="form-control"  >
                </div>

    </div>

    <div class="col-7 ">
        <br>
        <button type="button" class="btn pull-right col-4 btn-success" onclick="arrearsBar(this)" id="search_btnA" >Check</button>
    </div>

</div>
{{-- End --}}




@endsection

@section('scripts')
<script type="text/javascript">
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });




        var ldata = @json($Users);
        var ctdata = @json($Cols);
        var edata = @json($Exps);
        var cloans = @json($Cloans);
        var colCap = @json($ColCap);
        var colInt = @json($ColInt);
        var colAdv = @json($ColAdv);
        var arrTot = @json($ArrTot);
        var arrCap = @json($ArrCap);
        var arrInt = @json($ArrInt);
        var outStd = @json($OutStd);
        var pNl = @json($PronLost);
        var CulTot = @json($CuTot);
        console.log(CulTot);

    const select_branchC = document.querySelector('#select_branchC');

    const select_centerC = document.querySelector('#select_centerC');

    const selected_date1C = document.querySelector('#selected_date1C');

    const selected_date2C = document.querySelector('#selected_date2C');

    const select_branchA = document.querySelector('#select_branchA');

    const select_centerA = document.querySelector('#select_centerA');

    const selected_date1A = document.querySelector('#selected_date1A');

    const selected_date2A = document.querySelector('#selected_date2A');
var myChart1;
var myChart2;


function collectionBar(){

    // console.log(select_branchC.value);
    // console.log(select_centerC.value);
    // console.log(selected_date1C.value);
    $.ajax({

        type: 'GET',
        url: '{{('/home/getCloChart')}}',
        data:{'branch':select_branchC.value,'center':select_centerC.value,'dateFrom':selected_date1C.value,'dateTo':selected_date2C.value},
        dataType: 'JSON',
        success: function (data) {
            console.log(data);
            if (myChart1) {
        myChart1.destroy();
      }
            var ctx = document.getElementById("bar-chartA").getContext("2d");

           myChart1 = new Chart(document.getElementById("bar-chart4"), {

                type: 'bar',
                data: {
                labels: data.ClAdv,
                datasets: [
                    {
                    label: "Ammount",
                    backgroundColor:   "#3cba9f",
                    data: data.CoAdv
                    }
                ]
                },
                options: {
                legend: { display: false },
                title: {
                    display: true,
                    text: 'Collections'
                }
                }
            });

        }
    })
}

function arrearsBar(){

    // console.log(select_branchA.value);
    // console.log(select_centerA.value);
    // console.log(selected_date1A.value);
    $.ajax({

        type: 'GET',
        url: '{{('/home/getArrChart')}}',
        data:{'branch':select_branchA.value,'center':select_centerA.value,'dateFrom':selected_date1A.value,'dateTo':selected_date2A.value},
        dataType: 'JSON',
        success: function (data) {
            console.log(data);
            if (myChart2) {
        myChart2.destroy();
      }
            var ctx2 = document.getElementById("bar-chartA").getContext("2d");


            myChart2 = new Chart(ctx2, {

                type: 'bar',
                data: {
                labels: data.AlAdv,
                datasets: [
                    {
                    label: "Ammount",
                    backgroundColor:   "#3cba9f",
                    data:data.ArAdv,
                    }
                ]
                },
                options: {
                legend: { display: false },
                title: {
                    display: true,
                    text: 'Arrears'
                }
                }
            });

        }
    })
}


    $(function () {

new Chart(document.getElementById("bar-chartn"), {

    type: 'bar',

    data: {
      labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
      datasets: [
        {
          label: "Ammount",
          backgroundColor: "#15ce9c",
          data: pNl
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Profit And Lost'
      }
    }
});

new Chart(document.getElementById("line-chartn"), {
  type: 'line',
  data: {
    labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
    datasets: [{
        data: CulTot,
        label: "Ammount ",
        borderColor: "#e8711b",
        fill: false
      },
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Cumulative'
    }
  }
});

new Chart(document.getElementById("bar-chart"), {

    type: 'bar',

    data: {
      labels: ["10000", "15000", "20000", "25000"],
      datasets: [
        {
          label: "Ammount",
          backgroundColor: ["#a7e81b", "#e8711b","#15ce9c","#15a1ce","#c45850"],
          data: ldata
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Loans Amount By Each Category'
      }
    }
});
// ---
new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
    datasets: [{
        data: cloans,
        label: "Lons ",
        borderColor: "#3e95cd",
        fill: false
      },
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Loans Count By Per Month'
    }
  }
});
// ----
new Chart(document.getElementById("line-chart2"), {
  type: 'line',
  data: {
    labels: [0,"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
    datasets: [{
        data: colInt,
        label: "Interest",
        borderColor: "#3e95cd",
        fill: false
      }, {
        data: colCap,
        label: "Capital",
        borderColor: "#8e5ea2",
        fill: false
      }, {
        data: ctdata,
        label: "Total",
        borderColor: "#3cba9f",
        fill: false
      },
       {
        data: colAdv,
        label: "Advanced",
        borderColor: "#e8711b",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Collections(per month)'
    }
  }
});

new Chart(document.getElementById("bar-chart-grouped"), {
    type: 'bar',
    data: {
      labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
      datasets: [
        {
          label: "Interest",
          backgroundColor: "#3e95cd",
          data: arrInt
        }, {
          label: "Capital",
          backgroundColor: "#8e5ea2",
          data: arrCap
        },{
          label: "Total",
          backgroundColor: "#3cba9f",
          data: arrTot
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Arrears (per month)'
      }
    }
});
new Chart(document.getElementById("bar-chart3"), {

    type: 'bar',
    data: {
      labels: ["Total", "Capital", "Interest"],
      datasets: [
        {
          label: "Ammount",
          backgroundColor: [ "#e8711b","#15a1ce","#c45850"],
          data: outStd
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Outstanig '
      }
    }
});


new Chart(document.getElementById("bar-chart2"), {
    type: 'bar',
    data: {
      labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
      datasets: [
        {
          label: "Expence",
          backgroundColor: "#3cba9f",
          data: edata
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Expences (per month)'
      }
    }
});

new Chart(document.getElementById("mixed-chart"), {
    type: 'bar',
    data: {
      labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
      datasets: [{
          label: "Income",
          type: "line",
          borderColor: "#8e5ea2",
          data: [],
          fill: false
        },  {
          label: "Income",
          type: "bar",
          backgroundColor: "rgba(0,0,0,0.2)",
          data: [],
        },
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Other Incomes'
      },
      legend: { display: false }
    }
});






    })


</script>
@endsection
