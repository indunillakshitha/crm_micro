@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')



<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Approved and Pending Expences</h2>
                    </div>
                    <div class="pull-right">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="animated fadeIn">
    <div class="card animated fadeIn" style="width: 100%;">
        <div class="card-body">
            <table class="table table-bordered" id="data_table">
                <thead>
                    <tr>
                        <th>Invoice Id</th>
                        <th>Date</th>
                        <th>Category</th>
                        <th>Payee</th>
                        <th>Total Amount</th>
                        <th>Issue</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($approveds as $approved)
                    <tr>
                    <td>{{ $approved->voucher_id}}</td>
                    <td>{{ $approved->date}}</td>
                    <td>{{ $approved->category}}</td>
                    <td>{{ $approved->payee}}</td>
                    <td>Rs.<?php echo number_format($approved->total , 2 , '.' , ',' ) ?></td>
                    <td> <a class="btn btn-info row-cols-sm-4"
                        href="/issueapprovals/{{$approved->voucher_id}}">Issue</a>
                    <a class="btn btn-info row-cols-sm-4 ml-3"
                        href="/voucher/export/{{$approved->voucher_id}}">Print</a></td>


                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>



</div>

@endsection

@section('scripts')
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });




</script>
<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>


@endsection
