@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')



<div class="row justify-content-center container-fluid">
    <div class="col-md-12">
        <div class="card p-3">
            <form action="" method="POST">
                @csrf
                <div class="form-row justify-content-center">
                    <div class="col-md-4 mb-3 ml-3">
                        <label for="validationServerUsername">Voucher No</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            </div>
                            <input type="text" class="form-control is-valid" id="validationServerUsername" disabled
                                value="{{$invoice->voucher_id }}" name="package_title" placeholder="{{$invoice->voucher_id }}"
                                aria-describedby="inputGroupPrepend3" required>

                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationServer02">Voucher Total</label>
                        <input type="number" class="form-control is-valid" id="validationServer02" disabled
                            name="package_price" placeholder="Rs.<?php echo number_format($invoice->invoice_total , 2 , '.' , ',' ) ?>"
                            required>


                    </div>
                </div>
                <table class="table">
                    <tr>
                        <th>Date</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th>payee</th>
                        <th>Mount</th>
                    </tr>
                    @isset($details)
                    @foreach($details as $detail )
                    <tr>
                        <th>{{$detail->date}}</th>
                        <th>{{$detail->category}}</th>
                        <th>{{$detail->description}}</th>
                        <th>{{$detail->payee}}</th>
                        <th>{{$detail->amount}}</th>
                    </tr>
                    @endforeach
                    @endisset

                </table>
            </form>

            <a class="btn btn-info row-cols-sm-4" href="/comexapprove/{{$invoice->id}}">Approve</a>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });




</script>



@endsection
