@extends('layouts.master')
@section('title')

    Canty International
@endsection
@section('content')
<div class="">
    <div class="card">
        <div   class="card-body">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="pull-left">
                    <h2>Loan Categories</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('category.create') }}"> Create New Category</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Category Name</th>
            <th>Ammount of Loan</th>
            <th>Interest</th>
            <th>Duration</th>
            <th>Interest Rate</th>
            <!-- <th>Branch Name</th>
            <th> Address</th> -->


            <th width="200px">Action</th>
        </tr>
        @foreach ($categories as $category)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $category->category_type }}</td>
            <td>{{$category->fixed_ammount}}</td>
            <td>{{$category->interest}}</td>
            <td>{{$category->duration_period}}</td>
            <td>{{ $category->category_rate }}</td>

            <td width = "10px">
            <form action="{{ route('category.destroy',$category->id) }}" method="POST">

                    <a class="btn btn-info" href="{{ route('category.show',$category->id) }}">Show</a>

                    <a class="btn btn-primary" href="{{ route('category.edit',$category->id) }}">Edit</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>

        </tr>
        @endforeach
    </table>

    {!! $categories->links() !!}

@endsection
