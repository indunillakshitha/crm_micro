
@extends('layouts.master')
@section('title')

    Canty International
@endsection
@section('content')



<div class="">
    <div class="card">
        <div   class="card-body">

            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Create New Category</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('category.index') }}"> Back</a>
                    </div>
                </div>
            </div>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Warning!</strong> Please check your input code<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            <div class="modal-body">
                <form action="{{ route('category.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Category Name:</strong>
                                <input type="text" name="category_type" class="form-control" placeholder="Category Name" id="category_type">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Ammount of Loan:</strong>
                                <input type="number" value="fixed_ammount" name="fixed_ammount" class="form-control" placeholder="Ammount of Loan" id="fixed_ammount" oninput="genCategory()">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Interest:</strong>
                                <input type="number" value="interest"name="interest" class="form-control" placeholder="Interest" id="interest" oninput="genCategory()">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Duration (Weeks):</strong>
                                <input type="number" name="duration_period" class="form-control" placeholder="Duration" id="duration_period" oninput="genCategory()">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Interest Rate:</strong>
                                <input type="number" step="0.01" name="category_rate" class="form-control" placeholder="Interest Rate" id="category_rate">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Instalment:</strong>
                                <input type="number"  step="0.01" name="instalment" class="form-control" placeholder="Instalment" id="instalment">
                            </div>
                        </div>


                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
            </div>
        </div>
    </div>


@section('scripts')
<script>

    const fixed_ammount = document.querySelector('#fixed_ammount');
    const interest = document.querySelector('#interest');
    const duration_period = document.querySelector('#duration_period');
    const category_rate = document.querySelector('#category_rate');
    const instalment = document.querySelector('#instalment');

    setInterval(() => {
        genCategory();
    }, 50);


    function genCategory(){

        let fixd = fixed_ammount.value;
        let intr = interest.value;
        let due = duration_period.value;
        let intrRat = 0;
        let instal = 0;
        
        intrRat = ( intr / (fixd*due) )*4*100;
        category_rate.value=intrRat.toFixed(2);

        instal = (fixd/due) +(intr/due);
        instalment.value = instal.toFixed(2);

    }


</script>

@endsection


@endsection
