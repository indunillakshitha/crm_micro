@extends('layouts.master')
@section('title')

    Canty International
@endsection
@section('content')
<div class="container">
    <div class="card">
        <div   class="card-body">

            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2> Show Center</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('category.index') }}"> Back</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Category Type:</strong>
                        <input type="text" name="category_type" disabled class="form-control" placeholder="  {{ $category->category_type }}">

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Rate:</strong>

                        <input type="text" name="category_rate" disabled class="form-control" placeholder="{{ $category->category_rate }}">

                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
