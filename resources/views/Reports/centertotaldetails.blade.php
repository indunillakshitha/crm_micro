@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Center Collections Reports By Borrowers</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body ">
    <br>
    <br>
    <br>
    <div class="pull-right col-8 ">
        <table class="table table-bordered" id="collections_reports_table1">
            <tr>
                <th>Center</th>
                <th>Borrower</th>
                <th>Loan Amount</th>
                <th>Payments(weeks)</th>
                <th>Status</th>
                <th>Total Paid</th>
                <th >Total Balance</th>
            </tr>
            <tr>
                @foreach($totals as $tota)
                <tr>
                <th>{{$tota->center}}</th>
                <th>{{$tota->borrower_no}}</th>
                <th style="text-align: right"><?php echo number_format( $tota->loan_amount , 2 , '.' , ',' ) ?></th>
                <th style="text-align: center">{{$tota->payments_count}} / {{$tota->loan_duration}} </th>
                <th>{{$tota->status}} </th>
                <th style="text-align: right"><?php echo number_format( $tota->paid , 2 , '.' , ',' ) ?></th>
                <th style="text-align: right"><?php echo number_format( $tota->balance , 2 , '.' , ',' ) ?></th>
                </tr>
                @endforeach
            </tr>
        </table>
    </div>


    @endsection
