@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

    <div class="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Arrearse Reports</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body ">








                                        <div class="col-3">
                                                 <label for="">Select Center:</label>

                                        <div class="pull-right ">
                                                <select name="" id="select_center1">
                                                    @foreach($centers as $center)
                                                    <option value="{{$center->center_name}}" >{{$center->center_name}}</option>
                                                    @endforeach
                                                </select>
                                        </div>

 </div>




                                        <input type="hidden" name="da" id="selected_date1"class="form-control" value="{{$time}} " >

<div class="pull-right col-8 ">
                                            <button type="button" class="btn btn-success" id="search_btn1" data-toggle="modal"
                                                    data-target="#exampleModal">Search
                                            </button>
                                        </div>

<br>
<br>
<br>

                        <table class="table table-bordered" id="collections_reports_table1">


                                            </table>
                        <table class="table table-bordered" id="collection_table1">


                                            </table>


                    </div>

@endsection

@section('scripts')

<script type="text/javascript">
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    const select_center1 = document.querySelector('#select_center1')

    const selected_date1 = document.querySelector('#selected_date1')







search_btn1.addEventListener('click',e =>{

    // let date = new Date(selected_date1.value)
    // var start = new Date(date.getFullYear(), 0, 0)
    // var diff = (date - start) + ((start.getTimezoneOffset() - date.getTimezoneOffset()) * 60 * 1000)
    // var oneDay = 1000 * 60 * 60 * 24;
    var day = selected_date1.value
    console.log(day);
    console.log(select_center1.value);


                $.ajax({
                    type: 'GET',
                    url: '{{('/dashboardView/details')}}',
                    data: {'day': day,'center':select_center1.value},
                    dataType: 'JSON',
                    success: function (data) {
                        // console.log(data);

                        var tot = 0;;
                        var tot1 = 0;
                        var tot2 = 0;
                        var count = 0;
                        var count1 = 0;
                        var count2 = 0;
                        var l =0;
                        var i =0;
                        var k =0;
                        var ddd = 0;
                        var bbb = 0;
                        var gg = 0;
                        var GG = 0;

                        var a =0 ;
                        var b =0;
                        var c =0;
                        var d =0;
                         var e =0;
                          var f =0;
                           var g =0;
                            var h =0;
                             var i =0;
                        data.forEach(record=> {
                                tot += record.paid;
                                bbb = record.release_date;
                                ddd = new Date(bbb);
                                var start = new Date(ddd.getFullYear(), 0, 0)
                                var diff = (ddd - start) + ((start.getTimezoneOffset() - ddd.getTimezoneOffset()) * 60 * 1000)
                                var oneDay = 1000 * 60 * 60 * 24;
                                var a = Math.floor(diff / oneDay);
                                b = day - a ;
                                c = b / 7;
                                d = Math.floor(c);
                                e = record.payments_count;
                                f = d - e;

                                // console.log("didti didin")
                                // console.log(a)
                                // console.log(b)
                                // console.log(c)
                                // console.log(d)
                                // console.log(e)
                                // console.log(f)
                                // console.log("didti didin")

                                if((f <= 0) ){
                                    h = record.instalment;
                                    i = h * record.payments_count;
                                    j = d * record.instalment;
                                    l =  i - j;
                                    tot1 = tot1+l;
                                    count1 += count1+1;
                                    var l =0;
                                    var j =0;
                                    var h =0;
                                    var i =0;


                                }else if(f > 0){
                                    h = record.instalment;
                                    i = h * record.payments_count;
                                    j = d * record.instalment;
                                    k = j - i;

                                    gg = record.loan_amount + record.interest ;
                                    GG = gg - record.paid;
                                    if(k >= gg){
                                       tot2 = tot2 + GG;
                                    }
                                    tot2 = tot2 + k;
                                    count2 = count2+1;
                                    var l =0;
                                    var j =0;
                                    var h =0;
                                    var i =0;

                                    }



                        })
                        count = count1+count2;
                        document.querySelector('#collections_reports_table1').innerHTML = `


                        <tr>
                                                <th>Total Loans</th>
                                                     <th>Total Paid</th>
                                                     <th>Total Arrearse</th>
                                                 <th>Total Advanced</th>



                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1">

                                                            <th>${count}</th>
                                                            <th>${tot.toFixed(2)}</th>
                                                         <th>${tot2.toFixed(2)}</th>
                                                            <th>${tot1.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collections_reports_table1').innerHTML += html

// ----------------------------------------------------------------------------------------------------------------------------------------


                        document.querySelector('#collection_table1').innerHTML = `

                         <tr>
                            <th>No</th>
                            <th>Borrower No</th>
                            <th>NIC</th>
                            <th>Loan Amount</th>
                            <th>Paid</th>
                            <th>Arrears</th>
                            <th>Advanced</th>
                        </tr>
                        `
                            data.forEach(record=> {
                                bbb = record.release_date;
                                ddd = new Date(bbb);
                                var start = new Date(ddd.getFullYear(), 0, 0)
                                var diff = (ddd - start) + ((start.getTimezoneOffset() - ddd.getTimezoneOffset()) * 60 * 1000)
                                var oneDay = 1000 * 60 * 60 * 24;
                                var a = Math.floor(diff / oneDay);
                                b = day - a ;
                                c = b / 7;
                                d = Math.floor(c);
                                e = record.payments_count;
                                f = d - e;
                                // console.log("didti didin")
                                // console.log(a)
                                // console.log(b)
                                // console.log(c)
                                // console.log(a)
                                // console.log(e)
                                // console.log(f)
                                // console.log("didti didin")

                                if(f <= 0 ){
                                    h = record.instalment;
                                    i = h * record.payments_count;
                                    j = d * record.instalment;
                                    l =  i - j;
                                    var j =0;
                                    var h =0;
                                    var i =0;


                                }else {
                                    h = record.instalment;
                                    i = h * record.payments_count;
                                    j = d * record.instalment;
                                    k = j - i;
                                    gg = record.loan_amount + record.interest ;
                                    GG = gg - record.paid;
                                    console.log(k)
                                    console.log(gg)
                                    console.log(GG)
                                    if(k >= gg){
                                        k = GG;
                                    }
                                    var l =0;
                                    var j =0;
                                    var h =0;
                                    var i =0;


                                }


                            html =
                                `
                                <tr  id="collection_table1">
                                     <th>${record.id}</th>
                                     <th>${record.borrower_no}</th>
                                     <th>${record.nic}</th>
                                     <th>${record.loan_amount}
                                     <th>${record.paid}</th>
                                     <th>${k.toFixed(2)}</th>
                                     <th>${l.toFixed(2)}</th>





                                </tr>`

                                document.querySelector('#collection_table1').innerHTML += html


                        var ddd = 0;
                        var bbb = 0;
                        var start = 0;
                        var diff = 0;
                        var oneDay = 0;
                        var a =0 ;
                        var b =0;
                        var c =0;
                        var d =0;
                        var e =0;
                        var f =0;
                                })


                    }
                });



})












</script>

@endsection
