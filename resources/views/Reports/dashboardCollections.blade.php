@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Collections Reports</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body ">








            {{-- <div class="col-3">
                                                 <label for="">Select Center:</label>

                                        <div class="pull-right ">
                                                <select name="" id="select_center1">
                                                    @foreach($centers as $center)
                                                    <option value="{{$center->center_name}}" >{{$center->center_name}}
            </option>
            @endforeach
            </select>
        </div>

    </div>--}}



    {{-- <input type="hidden" name="da" id="selected_date1"class="form-control" value="{{$time}} " >

    <div class="pull-right col-8 ">
        <button type="button" class="btn btn-success" id="search_btn1" data-toggle="modal"
            data-target="#exampleModal">Get All Centers
        </button>
    </div> --}}
    <br>
    <br>
    <br>
    <div class="pull-right col-8 ">
        <table class="table table-bordered" id="collections_reports_table1">
            <tr>
                <th>Loans Count</th>
                <th>Total Collection</th>
            </tr>
            <tr>
                <th>{{$totloacount}}</th>
                <th><?php echo number_format( $tot , 2 , '.' , ',' ) ?></th>
            </tr>



        </table>
        <table class="table table-bordered" id="collection_table1">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Center Name</th>
                    <th>Loan Count</th>
                    <th>Collection</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($data as $d)
                <tr>
                    <th>{{ ++$i }}</th>
                    <th>{{$d->center_name}}</th>
                    <th>{{$d->loan_count_collection}}</th>
                    <th>{{$d->center_collection}}</th>
                </tr>
                @endforeach
            </tbody>


        </table>


    </div>


    @endsection

    {{-- @section('scripts')

<script type="text/javascript">
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


    const selected_date1 = document.querySelector('#selected_date1')







search_btn1.addEventListener('click',e =>{

    // let date = new Date(selected_date1.value)
    // var start = new Date(date.getFullYear(), 0, 0)
    // var diff = (date - start) + ((start.getTimezoneOffset() - date.getTimezoneOffset()) * 60 * 1000)
    // var oneDay = 1000 * 60 * 60 * 24;
    var day = selected_date1.value



                $.ajax({
                    type: 'GET',
                    url: '{{('/dashboardViewCollection/details')}}',
    data: {'day': day},
    dataType: 'JSON',
    success: function (data) {

    // console.log(data);



    document.querySelector('#collection_table1').innerHTML = `

    <tr>
        <th>Center Name</th>
        <th>Loan Count</th>
        <th>Collection</th>

    </tr>
    `
    data.forEach(record=> {


    html =
    `

    <tr id="collection_table1">
        <th>${record.center_name}</th>
        <th>${record.loan_count_collection}</th>
        <th>${record.center_collection}</th>






    </tr>`

    document.querySelector('#collection_table1').innerHTML += html



    })


    }
    });



    })












    </script>

    @endsection --}}
