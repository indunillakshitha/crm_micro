@extends('layouts.master')

@section('title')

    Dashboard | CRM
@endsection

@section('content')
<div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Loans Summery</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="row">

        <?php  $access_level = Auth::user()->access_level ?>
        @if($access_level == 'super_user')
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-success border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Issued Loans</div>
                    <div class="stat-digit count">{{$issued_loans}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-success"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Active Loans</div>
                        <div class="stat-digit count">{{$active_loans}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-success border-success"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Setle Loans</div>
                        <div class="stat-digit count">{{$setle_loans}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">New Loans</div>
                        <div class="stat-digit count">{{$new_loans}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Pending Loans</div>
                        <div class="stat-digit count">{{$pending_loans}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif


    </div>

@endsection

@section('scripts')

@endsection
