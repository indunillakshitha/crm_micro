@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Center Collections Reports</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body ">
    <br>
    <br>
    <br>
    <div class="pull-right col-8 ">
        <table class="table table-bordered" id="collections_reports_table1">
            <tr>
                <th>Center</th>
                <th>Total Paid</th>
                <th>Total Balance</th>
                <th>More</th>
            </tr>
            <tr>
                @foreach($total as $tota)
                <tr>
                <th>{{$tota->center}}</th>
                <th><?php echo number_format( $tota->paid , 2 , '.' , ',' ) ?></th>
                <th><?php echo number_format( $tota->balance , 2 , '.' , ',' ) ?></th>
                <th><a href="/centerdetails/{{$tota->center}}" class="btn btn-primary">CLICK for MORE</a></th>
                </tr>
                @endforeach
            </tr>
        </table>
    </div>


    @endsection
