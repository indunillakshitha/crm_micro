@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

    <div class="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Weekly Arrearse Customer Review</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body ">
                                <div class="row">
                                    <div class="col-lg-12 ">
                                        <div class="pull-left">
                                        </div>

                                        <div class="col-1">
                                            <label for="">Select Branch:</label>
                                        </div>
                                        <div class="pull-right col-2">
                                                <select name="select_branch1" id="select_branch1">
                                                    <option value="" >--Select--</option>
                                                    @foreach($branches as $branch)
                                                    <option value="{{$branch->branch_name}}" >{{$branch->branch_name}}</option>
                                                    @endforeach
                                                </select>
                                        </div>
                                        <div class="pull-left">
                                        </div>
                                        <div class="col-1">
                                                 <label for="">Select Center:</label>
                                        </div>
                                        <div class="pull-right col-3">
                                                <select name="" id="select_center1">
                                                    <option value="" >--Select--</option>
                                                    @foreach($centers as $center)
                                                    <option value="{{$center->center_name}}" >{{$center->center_name}}</option>
                                                    @endforeach
                                                </select>
                                        </div>
                                        <div class="pull-right col-2 ">
                                            <button type="button"  onclick="search(this)" class="btn btn-success" id="search_btn1" data-toggle="modal"
                                                    data-target="#exampleModal">Search
                                            </button>
                                        </div>
                                        <div class="pull-right col-3 ">
                                            <a class="btn btn-outline-dark btn-lg btn-block " href="/arrearsCustomerReviewReport/customer">See More Details</a>
                                        </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
            <div class="card-body ">
                                <div class="row">
                                    <div class="col-lg-12 ">
                                        <table class="table table-bordered" id="collections_reports_table1">
                                        </table>
                                        </div>
                            </div>
                        </div>
                    </div>
@endsection

@section('scripts')

<script type="text/javascript">
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


function search(){
    const branch = document.querySelector('#select_branch1');
    const center = document.querySelector('#select_center1');
    console.log(branch.value);
    $('#search_btn1').attr("disabled",function () {
        $.ajax({
            type: 'GET',
            url: '{{('/arrearsCustomerReviewReport/getWeekBranchCV')}}',
            data: {'center':center.value,'branch':branch.value} ,

            success: function(data){
                console.log(data);

                document.querySelector('#collections_reports_table1').innerHTML = `
                        <tr><th colspan=8 ><center> Arrearse Customer Review</center></th></tr>
                        <tr>
                            <th colspan=2 ><center></center></th>
                            <th colspan=2 ><center>Not Paid</center></th>
                            <th colspan=2 ><center>Under Payment</center></th>
                            <th colspan=2 ><center>Under Payment</center></th>
                        </tr>
                        <tr>
                            <th>Branch</th>
                            <th>Center</th>
                            <th>No Of</th>
                            <th>Ammount (Rs.)</th>
                            <th>No Of</th>
                            <th>Ammount (Rs.)</th>
                            <th>No Of</th>
                            <th>Ammount (Rs.)</th>
                        </tr>


                        `
                        html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${branch.value}</th>
                                     <th>${center.value}</th>
                                     <th>${data.Send[1]}</th>
                                     <th>${data.Send[0]}</th>
                                     <th>${data.Send[3]}</th>
                                     <th>${data.Send[2]}</th>
                                     <th>${data.Send[1] + data.Send[3]}</th>
                                     <th>${data.Send[0] + data.Send[2]}</th>
                                </tr>

                                `

                                document.querySelector('#collections_reports_table1').innerHTML += html
            },

        })});
    }



</script>

@endsection
