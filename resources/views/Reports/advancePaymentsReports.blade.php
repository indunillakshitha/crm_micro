@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

    <div class="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Advanced Payments Collections Reports</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body ">



                                <div class="row">
                                    <div class="col-lg-12 ">
                                        <div class="pull-left">
                                        </div>

                                        <div class="col-1">
                                            <label for="">Select Branch:</label>
                                        </div>
                                        <div class="pull-right col-2">
                                                <select name="select_branch1" id="select_branch1">
                                                    @foreach($branches as $branch)
                                                    <option value="{{$branch->branch_name}}" >{{$branch->branch_name}}</option>
                                                    @endforeach
                                                </select>
                                        </div>

                                        <div class="pull-left">
                                        </div>
                                        <div class="col-1">
                                                 <label for="">Select Center:</label>
                                        </div>
                                        <div class="pull-right col-2">
                                                <select name="" id="select_center1">
                                                    @foreach($centers as $center)
                                                    <option value="{{$center->center_name}}" >{{$center->center_name}}</option>
                                                    @endforeach
                                                </select>
                                        </div>


                                        <div class="col-1">
                                                 <label for="">Select Center:</label>
                                        </div>
                                        <div class="pull-right col-2">
                                                <select name="" id="select_routine">

                                                    <option value="0" >All</option>
                                                    <option value="1" >Daily</option>
                                                    <option value="2" >Weekly</option>
                                                    <option value="3" >Monthly</option>

                                                </select>
                                        </div>

                                        <div class="col">
                                        <div class="col-2">
                                            <label for="">Select Date </label>
                                        <input type="date" name="da" id="selected_date1"class="form-control"  >
                                        </div>
                                        </div>

                                        <div class="pull-right col-1 ">
                                            <button type="button" class="btn btn-success" id="search_btn1" data-toggle="modal"
                                                    data-target="#exampleModal">Search
                                            </button>
                                        </div>


                                </div>
                            </div>
                        </div>
                        <table class="table table-bordered" id="collections_reports_table1">


                                            </table>
                        <table class="table table-bordered" id="collection_table1">


                                            </table>
                        <table class="table table-bordered" id="collection_table2">


                        </table>
                        <table class="table table-bordered" id="collection_table3">


                        </table>
                        <table class="table table-bordered" id="collection_table4">


                        </table>
                        <table class="table table-bordered" id="collection_table5">


                        </table>

                    </div>
@endsection

@section('scripts')

<script type="text/javascript">
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    const select_branch1 = document.querySelector('#select_branch1')

    const select_center1 = document.querySelector('#select_center1')

    const selected_date1 = document.querySelector('#selected_date1')

    const select_routine = document.querySelector('#select_routine')




search_btn1.addEventListener('click',e =>{

    let date = new Date(selected_date1.value)
    var start = new Date(date.getFullYear(), 0, 0)
    var diff = (date - start) + ((start.getTimezoneOffset() - date.getTimezoneOffset()) * 60 * 1000)
    var oneDay = 1000 * 60 * 60 * 24;
    var day = Math.floor(diff / oneDay);
    console.log(day);
    console.log(select_branch1.value);
    console.log(select_center1.value);


                $.ajax({
                    type: 'GET',
                    url: '{{('/collectionsreport/getdaily')}}',
                    data: {'day': day,'branch':select_branch1.value,'center':select_center1.value,'ref':select_routine.value},
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data);

                        var loan = data.length;
                        var tot =0;
                        var tot1 =0;
                        var tot2 =0;
                        var tot3 =0;
                        var tot4 =0;
                        var tot5 =0;

                        var a =0 ;
                        var c =0;
                         var c1 =0;
                          var c2 =0;
                           var c3 =0;
                            var c4 =0;
                             var c5 =0;
                        var b =0;
                        var d =0;

                        data.forEach(record=> {
                                // tot += record.loan_amount;
                                if(record.loan_amount == 10000){
                                    a = 1300 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot1 += d  ;}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c1 = 1 + c1;
                                    tot1.toFixed(2);
                                }else if (record.loan_amount == 15000){
                                    a = 1147.06 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);
                                     // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot2 += d  ;}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c2 = 1 + c2;
                                    tot2.toFixed(2)
                                }else if (record.loan_amount == 20000){
                                    a = 1368.42 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);
                                     // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot3 += d ;
                                    console.log(record.total_payed);
                                    console.log(a);}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c3 = 1 + c3;
                                    tot3.toFixed(2)
                                }else if (record.loan_amount == 25000){
                                    a = 1547.62 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);
                                     // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot4 += d  ;}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c4 = 1 + c4;
                                    tot4.toFixed(2)
                                }else if (record.installment == 1218.75){
                                    a = 1218.75 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);
                                     // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot5 += d  ;}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c5 = 1 + c5;
                                    tot5.toFixed(2)
                                }else{
                                    echo("No Resultes")
                                }

                                tot = tot1+tot2+tot3+tot4+tot5;
                                c = c1+c2+c3+c4+c5;
                                tot.toFixed(2)

                        })
                         document.querySelector('#collections_reports_table1').innerHTML = `
                         <tr><th colspan=2 ><center> Advanced Payments</center></th></tr>
                        <tr>
                            <th>Toatal Loans</th>
                            <th>Total Amount(Rs.)</th>

                        </tr>
                        `



                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c}</th>
                                     <th>${tot.toFixed(2)}</th>




                                </tr>`

                                document.querySelector('#collections_reports_table1').innerHTML += html
// ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table1').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (10000)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c1}</th>
                                     <th>${tot1.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table1').innerHTML += html

// ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table2').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (15000)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c2}</th>
                                     <th>${tot2.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table2').innerHTML += html

                                // ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table3').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (20000)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c3}</th>
                                     <th>${tot3.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table3').innerHTML += html

                                // ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table4').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (25000)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c4}</th>
                                     <th>${tot4.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table4').innerHTML += html

                                // ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table5').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (15000-16 weeks)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c5}</th>
                                     <th>${tot5.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table5').innerHTML += html

                    }
                });



})


select_branch1.addEventListener('click',e =>{
$(document).ready(function () {
        console.log(select_branch1.value);
    $(select_branch1).click(function (e) {
        $.ajax({
                    type: 'GET',
                    url: '{{('/collectionsreport/getbranch')}}',
                    data: {'branch':select_branch1.value,'ref':select_routine.value},
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data);
                        var loan = data.length;
                        var tot =0;
                        var tot1 =0;
                        var tot2 =0;
                        var tot3 =0;
                        var tot4 =0;
                        var tot5 =0;

                        var a =0 ;
                        var c =0;
                         var c1 =0;
                          var c2 =0;
                           var c3 =0;
                            var c4 =0;
                             var c5 =0;
                        var b =0;
                        var d =0;

                        data.forEach(record=> {
                                // tot += record.loan_amount;
                                if(record.loan_amount == 10000){
                                    a = 1300 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot1 += d  ;}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c1 = 1 + c1;
                                    tot1.toFixed(2);
                                }else if (record.loan_amount == 15000){
                                    a = 1147.06 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);
                                     // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot2 += d  ;}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c2 = 1 + c2;
                                    tot2.toFixed(2)
                                }else if (record.loan_amount == 20000){
                                    a = 1368.42 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);
                                     // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot3 += d ;
                                    console.log(record.total_payed);
                                    console.log(a);}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c3 = 1 + c3;
                                    tot3.toFixed(2)
                                }else if (record.loan_amount == 25000){
                                    a = 1547.62 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);
                                     // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot4 += d  ;}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c4 = 1 + c4;
                                    tot4.toFixed(2)
                                }else if (record.installment == 1218.75){
                                    a = 1218.75 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);
                                     // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot5 += d  ;}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c5 = 1 + c5;
                                    tot5.toFixed(2)
                                }else{
                                    echo("No Resultes")
                                }

                                tot = tot1+tot2+tot3+tot4+tot5;
                                c = c1+c2+c3+c4+c5;
                                tot.toFixed(2)

                        })
                         document.querySelector('#collections_reports_table1').innerHTML = `
                         <tr><th colspan=2 ><center> Advanced Payments</center></th></tr>
                        <tr>
                            <th>Toatal Loans</th>
                            <th>Total Amount(Rs.)</th>

                        </tr>
                        `



                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c}</th>
                                     <th>${tot.toFixed(2)}</th>




                                </tr>`

                                document.querySelector('#collections_reports_table1').innerHTML += html
// ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table1').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (10000)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c1}</th>
                                     <th>${tot1.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table1').innerHTML += html

// ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table2').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (15000)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c2}</th>
                                     <th>${tot2.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table2').innerHTML += html

                                // ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table3').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (20000)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c3}</th>
                                     <th>${tot3.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table3').innerHTML += html

                                // ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table4').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (25000)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c4}</th>
                                     <th>${tot4.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table4').innerHTML += html

                                // ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table5').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (15000-16 weeks)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c5}</th>
                                     <th>${tot5.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table5').innerHTML += html

                    }
                });





    });
});
})

select_center1.addEventListener('click',e =>{
$(document).ready(function () {
        console.log(select_center1.value);

    $(select_center1).click(function (e) {

        $.ajax({
                    type: 'GET',
                    url: '{{('/collectionsreport/getcenter')}}',
                    data: {'center':select_center1.value,'ref':select_routine.value},
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data);

                         var loan = data.length;
                        var tot =0;
                        var tot1 =0;
                        var tot2 =0;
                        var tot3 =0;
                        var tot4 =0;
                        var tot5 =0;

                        var a =0 ;
                        var c =0;
                         var c1 =0;
                          var c2 =0;
                           var c3 =0;
                            var c4 =0;
                             var c5 =0;
                        var b =0;
                        var d =0;

                        data.forEach(record=> {
                                // tot += record.loan_amount;
                                if(record.loan_amount == 10000){
                                    a = 1300 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot1 += d  ;}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c1 = 1 + c1;
                                    tot1.toFixed(2);
                                }else if (record.loan_amount == 15000){
                                    a = 1147.06 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);
                                     // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot2 += d  ;}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c2 = 1 + c2;
                                    tot2.toFixed(2)
                                }else if (record.loan_amount == 20000){
                                    a = 1368.42 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);
                                     // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot3 += d ;
                                    console.log(record.total_payed);
                                    console.log(a);}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c3 = 1 + c3;
                                    tot3.toFixed(2)
                                }else if (record.loan_amount == 25000){
                                    a = 1547.62 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);
                                     // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot4 += d  ;}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c4 = 1 + c4;
                                    tot4.toFixed(2)
                                }else if (record.installment == 1218.75){
                                    a = 1218.75 * record.installment_no;
                                    // b = 300 * record.installment_no;
                                    // console.log("Check1",record.total_payed);
                                    // console.log("Check2",record.installment_no);
                                    // console.log("Check3",a);
                                     // console.log("Check3",a);

                                    d = record.total_payed - a;
                                    if(d > 0){
                                    tot5 += d  ;}
                                    a=0;
                                    b=0;
                                    d =0;
                                    c5 = 1 + c5;
                                    tot5.toFixed(2)
                                }else{
                                    echo("No Resultes")
                                }

                                tot = tot1+tot2+tot3+tot4+tot5;
                                c = c1+c2+c3+c4+c5;
                                tot.toFixed(2)

                        })
                         document.querySelector('#collections_reports_table1').innerHTML = `
                         <tr><th colspan=2 ><center> Advanced Payments</center></th></tr>
                        <tr>
                            <th>Toatal Loans</th>
                            <th>Total Amount(Rs.)</th>

                        </tr>
                        `



                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c}</th>
                                     <th>${tot.toFixed(2)}</th>




                                </tr>`

                                document.querySelector('#collections_reports_table1').innerHTML += html
// ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table1').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (10000)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c1}</th>
                                     <th>${tot1.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table1').innerHTML += html

// ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table2').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (15000)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c2}</th>
                                     <th>${tot2.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table2').innerHTML += html

                                // ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table3').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (20000)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c3}</th>
                                     <th>${tot3.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table3').innerHTML += html

                                // ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table4').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (25000)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c4}</th>
                                     <th>${tot4.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table4').innerHTML += html

                                // ----------------------------------------------------------------------------------------------------------------------------
                                document.querySelector('#collection_table5').innerHTML = `

                        <tr><th colspan=2 ><center> Loan Type (15000-16 weeks)</center></th></tr>
                        <tr>
                            <th>Loans Count</th>
                            <th>Amount(Rs.)</th>

                        </tr>
                        `
                            html =
                                `
                                <tr  id="collections_reports_table1_row">
                                     <th>${c5}</th>
                                     <th>${tot5.toFixed(2)}</th>

                                </tr>`

                                document.querySelector('#collection_table5').innerHTML += html

                    }
                });



    });
});
})





</script>

@endsection
