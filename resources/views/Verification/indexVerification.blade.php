@extends('layouts.master')
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Pending Loans</h2>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 ">
                    <input class="form-control" type="text" placeholder="Branch Name" aria-label="Search"
                        id="branch_selector" name="branch_selector" value="">
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <input class="form-control" type="text" placeholder="Center Name" aria-label="Search"
                        id="center_selector" name="center_selector" value="">
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <input class="form-control" type="text" placeholder="Group Number" aria-label="Search"
                        id="group_selector" name="group_selector" value="">
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3">

                    <button type="button" name="filter" id="filter" class="btn btn-info btn-sm">Search</button>

                    <button type="button" name="reset" id="reset" class="btn btn-default btn-sm">Reset</button>
                </div>

            </div>
        </div>
    </div>
</div>

{{-- @if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif --}}

<table class="table " id="loan_table">
    <tr>
        <th>No</th>
        <th>Borrower No</th>
        <th>Loan Stage</th>
        <th>Loan Amount</th>

        <th>Status</th>


        <th width="200px">Action</th>
    </tr>
{{$i=0}}
    @foreach ($loans as $loan)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $loan->borrower_no }}</td>
        <td>{{ $loan->loan_stage }}</td>
        <td>{{ $loan->loan_amount }}</td>
        <td>{{ $loan->status }}</td>
        <td width="100px">

            <form action="{{ route('verify.destroy',$loan->id) }}" method="POST">

                {{-- <a class="btn btn-info btn-sm" href="https://cantylivelocations.firebaseapp.com/saveaddress/{{$loan->nic}}"
                target="_blank"><i class="fa fa-location-arrow"></i></a> --}}
                {{-- <a class="btn btn-info btn-sm" href="http://localhost:8080/saveaddress/{{$loan->nic}}"
                target="_blank"><i class="fa fa-location-arrow"></i></a> --}}
                <a class="btn btn-info btn-sm" href="http://cantyinternational.com/api/vueaddress/{{$loan->nic}}"
                    target="_blank"><i class="fa fa-location-arrow"></i></a>
                {{-- <a class="btn btn-info btn-sm" href="http://127.0.0.1:8000//api/vueaddress/{{$loan->nic}}"
                target="_blank"><i class="fa fa-location-arrow"></i></a> --}}
                <a class="btn btn-info btn-sm" href="/viewborrower/{{$loan->id}}" target="_blank"><i
                        class="fa fa-users"></i></a>
                {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Show</button>--}}
                <a class="btn btn-success btn-sm" href="/verify/test/{{$loan->id}}">Verify</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger btn-sm">Reject</button>
            </form>
        </td>

    </tr>

    @endforeach
</table>


@section('scripts')

<script type="text/javascript">
    $(document).ready(function () {
             $('#loan_table').DataTable();
    //          processing: true,
    //          serverSide: true,
    //         fill_datatable();

    // function fill_datatable(group_selector = '', center_selector = '')
    // {
    //     var dataTable = $('#loan_table').DataTable({
    //         processing: true,
    //         serverSide: true,
    //         ajax:{
    //             url: "{{ route('approving.index') }}",
    //             data:{center_selector:center_selector, group_selector:group_selector}
    //         },
    //         columns: [
    //             {
    //                 data:'borrower_no',
    //                 name:'borrower_no'
    //             },
    //             {
    //                 data:'branch',
    //                 name:'branch'
    //             },
    //             {
    //                 data:'center',
    //                 name:'center'
    //             },
    //             {
    //                 data:'group_no',
    //                 name:'group_no'
    //             },
    //             {
    //                 data:'loan_stage',
    //                 name:'loan_stage'
    //             },
    //             {
    //                 data:'loan_stage',
    //                 name:'loan_stage'
    //             }
    //         ]
    //     });
    // }

    // $('#filter').click(function(){
    //     var center_selector = $('#center_selector').val();
    //     var group_selector = $('#group_selector').val();

    //     if(center_selector != '' &&  group_selector != '')
    //     {
    //         $('#loan_table').DataTable().destroy();
    //         fill_datatable(center_selector, group_selector);
    //     }
    //     else
    //     {
    //         alert('Select Both filter option');
    //     }
    // });

    // $('#reset').click(function(){
    //     $('#center_selector').val('');
    //     $('#group_selector').val('');
    //     $('#loan_table').DataTable().destroy();
    //     fill_datatable();
    // });



            // var la = $(loans).val();

        // $(branch_selector).click(function(){
        //     var branch = $(this).val();
        //     console.log(branch)


        //     $.ajax({
        //         type: 'GET',
        //         url: '{{ ('/bseclector') }}',
        //         data: {'id':branch},
        //         dataType: 'JSON',
        //         success: function (data) {
        //             console.log(data);
        //             data.forEach(function(item){
        //                 console.log(item);
        //                 // $('#loan_table').append('$loans'+item);
        //            })




        //      }

        //         });
        //     });

        // $(center_selector).click(function () {
        //     var center = $(this).val();
        //     console.log(center);

        //     $.ajax({
        //         type: 'GET',
        //         url: '{{ ('/cseclector') }}',
        //         data: {'id':center},
        //         dataType: 'JSON',
        //         success: function (data) {
        //             console.log(data);

        //         }
        //     });

        // });
        // $(group_selector).click(function () {
        //     var group = $(this).val();
        //     console.log(group);
        //     var tmp = null;
        //     $.ajax({
        //         type: 'GET',
        //         url: '{{ ('/gseclector') }}',
        //         data: {'id':group},
        //         dataType: 'JSON',
        //         success: function (data) {
        //             console.log(data);


        //         //    data.forEach(function(item){

        //         //                                 // $('#loan_table').append('$loans'+item);
        //         //         // $('#loan_table').append(${item+'$loans');
        //         //         $('#foreach').append(item);
        //         //         // tmp = item;
        //         //         console.log(item);
        //         //         // $('#loan_table').append('<td>'+item.loan_stage+'</td>');

        //         //    })
        //         // });
        //         }
        //     });

        // });
        });


</script>




@endsection
@endsection
