@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')




<div class="">
    <div class="card">
        <div   class="card-body">

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Borrower Details:</h2>
            </div>

            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('approving.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <li >
        <a href="#homes" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
            Borrower Details
        </a>

        <form action="">
            @csrf

    <ul class="collapse list-unstyled" id="homes">
        <li>
    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Branch No:</strong>
                <input type="text" name="branch_no" disabled class="form-control" placeholder=" {{ $borrower->branch_no }}">

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Center:</strong>

                <input type="text" name="center" disabled class="form-control" placeholder="{{ $borrower->center }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Group No:</strong>

                <input type="text" name="group_no" disabled class="form-control" placeholder="{{ $borrower->group_no }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Application No:</strong>

                <input type="text" name="application_no" disabled class="form-control" placeholder="{{ $borrower->application_no }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Unique Id:</strong>

                <input type="text" name="borrower_no" disabled class="form-control" placeholder="{{ $borrower->borrower_no }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Full Name:</strong>

                <input type="text" name="full_name" disabled class="form-control" placeholder="{{ $borrower->full_name }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>NIC:</strong>

                <input type="text" name="nic" disabled class="form-control" placeholder="  {{ $borrower->nic }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Birthday:</strong>

                <input type="text" name="birthday" disabled class="form-control" placeholder="  {{ $borrower->birthday }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Address:</strong>

                <input type="text" name="address" disabled class="form-control" placeholder="  {{ $borrower->address }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>LP:</strong>

                <input type="text" name="lp_no" disabled class="form-control" placeholder="  {{ $borrower->lp_no }}">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Mobile :</strong>

                <input type="text" name="mobile_no" disabled class="form-control" placeholder="{{ $borrower->mobile_no }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>E mail::</strong>

                <input type="text" name="email" disabled class="form-control" placeholder="{{ $borrower->email }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Occupation:</strong>

                <input type="text" name="occupation" disabled class="form-control" placeholder="{{ $borrower->occupation }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Amount:</strong>

                <input type="text" name="requested_amount" disabled class="form-control" placeholder="  {{ $borrower->requested_amount }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Duration(weeks):</strong>

                <input type="text" name="duration_weeks" disabled class="form-control" placeholder="  {{ $borrower->duration_weeks }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Loan Stage:</strong>

                <input type="text" name="loan_stage" disabled class="form-control" placeholder="  {{ $borrower->loan_stage }}">
            </div>
        </div>
        </div>
        </div>
        </div>
    </li>

</ul>
</form>
</li>




        <!-- <div class="">
            <div class="card">
                <div   class="card-body">

            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Loan Details:</h2>
                    </div>

                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('approving.index') }}"> Back</a>
                    </div>
                </div>
            </div>
            <a href="#homess" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                Borrower Details
            </a>

            <form action="">
                @csrf
            <ul class="collapse list-unstyled" id="homess">
                <li>

            <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Borrower No:</strong>
                        <input type="text" name="borrower_no" disabled class="form-control" placeholder=" {{ $loan->borrower_no }}">

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Loan Stage:</strong>

                        <input type="text" name="loan_stage" disabled class="form-control" placeholder="{{ $loan->loan_stage }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Loan Ammount:</strong>

                        <input type="text" name="loan_amount" disabled class="form-control" placeholder="{{ $loan->loan_amount }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Loan Release Date:</strong>

                        <input type="text" name="release_date" disabled class="form-control" placeholder="{{ $loan->release_date }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Loan Duration:</strong>

                        <input type="text" name="" disabled class="form-control" placeholder="{{ $loan->loan_duration }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Duration Period:</strong>

                        <input type="text" name="duration_period" disabled class="form-control" placeholder="{{ $loan->duration_period }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Repayment Cycle:</strong>

                        <input type="text" name="repayment_cycle" disabled class="form-control" placeholder="{{ $loan->repayment_cycle }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Number of Repayments:</strong>

                        <input type="text" name="number_of_repayments" disabled class="form-control" placeholder="  {{ $loan->number_of_repayments }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Loan Purpose:</strong>

                        <input type="text" name="loan_purpose" disabled class="form-control" placeholder="  {{ $loan->loan_purpose }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Interest Rate:</strong>

                        <input type="text" name="interest_rate" disabled class="form-control" placeholder="  {{ $loan->interest_rate }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Status:</strong>

                        <input type="text" name="status" disabled class="form-control" placeholder="  {{ $loan->status }}">
                    </div>
                </div>

                </div>
                </div>
                </div>
            </form>
        </li> -->







                <div class="">
                    <div class="card">
                        <div   class="card-body">

                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2> Appraisal Details:</h2>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('approving.index') }}"> Back</a>
                            </div>

                        </div>
                    </div>
                    <a href="#homesss" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                        Borrower Details
                    </a>

                    <form action="">
                        @csrf
                    <ul class="collapse list-unstyled" id="homesss">
                        <li>

                    <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Borrower No:</strong>
                                <input type="text" name="borrower_no" disabled class="form-control" placeholder="       {{ $apprasails->borrower_no }}">

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Husbund:</strong>

                                <input type="text" name="husband" disabled class="form-control" placeholder="       {{ $apprasails->husband }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Son:</strong>

                                <input type="text" name="son" disabled class="form-control" placeholder="       {{ $apprasails->son }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Daugter:</strong>

                                <input type="text" name="daughter" disabled class="form-control" placeholder="      {{ $apprasails->daughter }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Guardian:</strong>

                                <input type="text" name="guardian" disabled class="form-control" placeholder="      {{ $apprasails->guardian }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Other:</strong>

                                <input type="text" name="other" disabled class="form-control" placeholder="     {{ $apprasails->other }}">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Total Earn:</strong>

                                <input type="text" name="total_earn" disabled class="form-control" placeholder="    {{ $apprasails->total_earn }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Household Expenses:</strong>

                                <input type="text" name="household_expenses" disabled class="form-control" placeholder="    {{ $apprasails->household_expenses }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Other Expenses:</strong>

                                <input type="text" name="other_expenses" disabled class="form-control" placeholder="    {{ $apprasails->other_expenses }}">
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>As Guarantor Credit Facility Count :</strong>

                                <input type="text" name="as_borrower_facility_count" disabled class="form-control" placeholder="    {{ $apprasails->as_borrower_facility_count }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Total Amount::</strong>

                                <input type="text" name="as_borrower_total_amount" disabled class="form-control" placeholder="      {{ $apprasails->as_borrower_total_amount }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>As Guarantor Credit Facility Count :</strong>

                                <input type="text" name="as_guarantor_facility_count" disabled class="form-control" placeholder="       {{ $apprasails->as_guarantor_facility_count }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Total Amount:</strong>

                                <input type="text" name="as_guarantor_total_amount" disabled class="form-control" placeholder="     {{ $apprasails->as_guarantor_total_amount }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Total Expenses :</strong>

                                <input type="text" name="total_expenses" disabled class="form-control" placeholder="    {{ $apprasails->total_expenses }}">
                            </div>
                        </div>

                        </div>
                        </div>
                        </div>
                    </form>
                </li>

@endsection

@section('scripts')

@endsection
