@extends('layouts.master')
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Pay Loans</h2>
                    </div>
                </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 "  >
                        <h6>Branch</h6>
                        <input class="form-control" type="text" placeholder="Branch Name" aria-label="Search" id="branch_selector" name="branch_selector" value="" >
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3" >
                        <h6>Center</h6>
                        <input class="form-control" type="text" placeholder="Center Name" aria-label="Search" id="center_selector" name="center_selector" value="" >
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3" >
                        <h6>Group</h6>
                        <input class="form-control" type="text" placeholder="Group Number" aria-label="Search" id="group_selector" name="group_selector" value="">
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3" >

                        <button type="button" name="filter" id="filter" class="btn btn-info btn-sm">Search</button>

                        <button type="button" name="reset" id="reset" class="btn btn-default btn-sm">Reset</button>
                    </div>

            </div>
        </div>
    </div>
</div>

{{-- @if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif --}}

<table class="table table-bordered" id="data_table">
    <thead>
    <tr>
        <th>Center</th>
        <th>Release Date</th>

        <th>Loan Count</th>
        <th>Action</th>


    </tr>
</thead>

<tbody>
    @foreach ($loans as $loan)



    <tr>
        <td>{{ $loan->center }}</td>
        <td>{{ $loan->release_date }}</td>
        <td>{{ $loan->loan_count }}</td>


        <td >

            <form action="" method="POST">

                <a class="btn btn-success btn-sm my-1" href="/singleissue/{{$loan->center}}">Single Issue</a>
                {{-- <a class="btn btn-success btn-sm my-1" href="/groupissue/{{$loan->id}}">Group Issue</a> --}}

                @csrf
                @method('DELETE')

                {{-- <button type="submit" class="btn btn-danger btn-sm">Reject</button> --}}
            </form>
        </td>

    </tr>

    @endforeach
</tbody>
</table>
<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>





@endsection

