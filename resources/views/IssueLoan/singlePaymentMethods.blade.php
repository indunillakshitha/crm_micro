@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

    <div class="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Loan Payments | Single </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body ">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Cash</a>
              </li>
            <li class="nav-item">
              <a class="nav-link  " id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Bank</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Cheque</a>
            </li>

          </ul>
          <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade  " id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

            <form action="/markaspayedsingle/{{$pendings[0]->center}}" method="get" class="px-5">

                    <div class="form-group row">
                        <label for="bank_name">Bank Name </label>
                        <input type="text" name="bank_name" class="form-control" placeholder="Enter bank name">
                    </div>
                    <div class="form-group row">
                        <label for="">Branch Name</label>
                        <input type="text" name="branch_name" class="form-control" placeholder="Enter branch name">
                    </div>
                    <div class="form-group row">
                        <label for="">Account Number:</label>
                        <input type="text" name="acc_no" class="form-control" placeholder="Enter account number">
                    </div>
                    <div class="form-group row">
                        <label for="">Amount</label>
                    <input type="text" name="amount" class="form-control" readonly value="{{$amount}}">
                    </div>
                    <div class="form-group row">
                        <label for="">Due Start date</label>
                    <input type="date" name="due" class="form-control" id="bank_date" >
                    </div>

                    <input type="hidden" name="issueType" value="single">
                    <input type="hidden" name="paymentType" value="bank">
                    {{-- <input type="hidden" name="borrower_no" value="{{$loan->borrower_no}}"> --}}
                    <input type="hidden" name="date_index" id="bank_date_index">
                    <input type="hidden" name="month" id="bank_month_index">


                    <button type="submit" id="bank_sub_btn" class="btn btn-primary mb-2">Mark as Payed</button>
                </form>

            </div>

            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                {{-- <form action="/markaspayedsingle/{{$loan->id}}" method="get" class="px-5"> --}}
                    <div class="form-group row">
                        <label for="">Cheque Number </label>
                        <input type="text" name="cheque_no" class="form-control" placeholder="Enter cheque number">
                    </div>
                    <div class="form-group row">
                        <label for="">Amount</label>
                    {{-- <input type="text" name="amount" class="form-control" readonly value="{{$amount}}"> --}}
                    </div>
                    <div class="form-group row">
                        <label for="">Due date</label>
                    <input type="date" name="due" id="cheque_date" class="form-control"  >
                    </div>

                    <input type="hidden" name="issueType" value="single">
                    <input type="hidden" name="paymentType" value="cheque">
                    {{-- <input type="hidden" name="borrower_no" value="{{$loan->borrower_no}}"> --}}
                    <input type="hidden" name="date_index" id="cheque_date_index">
                    <input type="hidden" name="month" id="cheque_month_index">



                    <button type="submit" id="cheque_sub_btn" class="btn btn-primary mb-2">Mark as Payed</button>
                </form>
            </div>
            <div class="tab-pane fade show active" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                <form action="/markaspayedsingle/{{$pendings[0]->center}}" method="get" class="px-5">
                    <div class="form-group row">
                        <label for="">Center</label>
                    <input type="text" name="center" class="form-control" readonly value="{{$pendings[0]->center}}">
                    </div>
                    <div class="form-group row">
                        <label for="">Amount</label>
                    <input type="text" name="amount" class="form-control" readonly value="{{$amount}}">
                    </div>
                    <div class="form-group row">
                        <label for="">Document Charged date</label>
                    <input type="date" name="doc_date" id="doc_date"class="form-control"  required>
                    </div>
                    <div class="form-group row">
                        <label for="">Due date</label>
                    <input type="date" name="due" id="cash_date"class="form-control"  required>
                    </div>

                    <input type="hidden" name="issueType" value="single">
                    <input type="hidden" name="paymentType" value="cash">
                    {{-- <input type="hidden" name="borrower_no" value="{{$loan->borrower_no}}"> --}}
                    <input type="hidden" name="date_index" id="cash_date_index">
                    <input type="hidden" name="month" id="cash_month_index">

                    <button type="submit" id="cash_sub_btn" class="btn btn-primary mb-2">Mark as Issued</button>
                </form>
            </div>
          </div>
            </div>
        </div>
    </div>
    {{--        </div>--}}
    </div>
@endsection

@section('scripts')

<script>

    // const bank_sub_btn = document.querySelector('#bank_sub_btn')
    const bank_date = document.querySelector('#bank_date')

    // const cheque_sub_btn = document.querySelector('#cheque_sub_btn')
    const cheque_date = document.querySelector('#cheque_date')

    // const cash_sub_btn = document.querySelector('#cash_sub_btn')
    const cash_date = document.querySelector('#cash_date')
    const cash_date_index = document.querySelector('#cash_date_index')
    const cash_month_index = document.querySelector('#cash_month_index')

    const bank_date_index = document.querySelector('#bank_date_index')
    const bank_month_index = document.querySelector('#bank_month_index')

    const cheque_date_index = document.querySelector('#cheque_date_index')
    const cheque_month_index = document.querySelector('#cheque_month_index')

    bank_date.addEventListener('change', e => {

        // console.log(bank_date.value)
        let date = new Date(bank_date.value)
        // console.log(date)
        var start = new Date(date.getFullYear(), 0, 0)
        // console.log(start)
        var diff = (date - start) + ((start.getTimezoneOffset() - date.getTimezoneOffset()) * 60 * 1000)
        // console.log(diff);
        var oneDay = 1000 * 60 * 60 * 24;
        var day = Math.floor(diff / oneDay);
        console.log('Day of year: ' + day);
        bank_date_index.value = day
        let month = date.getMonth()
        bank_month_index.value = month


    })
    cheque_date.addEventListener('change', e => {

        let date = new Date(cheque_date.value)
        var start = new Date(date.getFullYear(), 0, 0)
        var diff = (date - start) + ((start.getTimezoneOffset() - date.getTimezoneOffset()) * 60 * 1000)
        var oneDay = 1000 * 60 * 60 * 24;
        var day = Math.floor(diff / oneDay);
        console.log('Day of year: ' + day);
        cheque_date_index.value = day
        let month = date.getMonth()
        cheque_month_index.value = month
    })
    cash_date.addEventListener('input', e => {

        let date = new Date(cash_date.value)
        var start = new Date(date.getFullYear(), 0, 0)
        var diff = (date - start) + ((start.getTimezoneOffset() - date.getTimezoneOffset()) * 60 * 1000)
        var oneDay = 1000 * 60 * 60 * 24;
        var day = Math.floor(diff / oneDay);
        console.log('Day of year: ' + day);
        cash_date_index.value = day

        let month = date.getMonth()
        cash_month_index.value = month
        // console.log(month)
    })

</script>

@endsection
