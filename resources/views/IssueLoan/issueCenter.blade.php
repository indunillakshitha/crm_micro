@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

    <div class="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Repayments Docs</h2>

                        </div>

                        <div class="pull-right">
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#exampleModal">Today Repayments
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    Branch : {{$branch}}
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="form-control" for="">Center</label>
                        <select name="" id="select_branch" class="form-control">
                            @foreach($centers as $center)
                                <option value="{{$center->center_name}}">{{$center->center_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="mx-3">
                        <button class="btn btn-success">
                        <a href="/markaspayedcenter/{{$center->id}}">Center Issue</a>
                        </button>
                    </div>
                </div>
            </div>
        </div>



    </div>
    </div>
@endsection

@section('scripts')

@endsection
