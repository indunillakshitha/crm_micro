@extends('layouts.master')

@section('title')

Dashboard | CRM
@endsection

@section('content')

<div class="card  col-12">


    <div class="col-6">
        <div class="row">
            <div class="col">
                <div class="pull-right col-2">
                    <label for="">Branch:</label>
                    <select name="select_branchC" id="select_branchC" class="form-control">
                        <option value="">Select</option>
                        @foreach($branches as $branch)
                        <option value="{{$branch->branch_name}}">{{$branch->branch_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="pull-right col-2">
                    <label for="">Center:</label>
                    <select name="" id="select_centerC" class="form-control">
                        <option value="">Select</option>
                        @foreach($centers as $center)
                        <option value="{{$center->center_name}}">{{$center->center_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-3">
                    <label for="">From:</label>
                    <input type="date" name="da" id="selected_date1C" class="form-control">
                </div>
                <div class="col-3">
                    <label for="">To:</label>
                    <input type="date" name="da" id="selected_date2C" class="form-control">
                </div>
                <div class="col-2">
                    <label for="">Period</label>
                    <select name="" id="select_period_type" class="form-control">
                        <option value="monthly">Monthly</option>
                        <option value="weekly">Weekly</option>
                        <option value="daily">Daily</option>

                    </select>
                </div>
            </div>
            <div class="col-12 ">
                <br>
                <button type="button" class="btn pull-right col-4 btn-success" onclick="collectionBar(this)"
                    id="search_btnC">Check</button>
            </div>
            <a class="btn btn-outline-light btn-lg btn-block " href="/collectionsreport">See More Details</a>
        </div>
    </div>

    <div class="card-body ">
        <div class="stat-widget-one col-12">
            <canvas id="myChart" width="800" height="400"></canvas>
        </div>
    </div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    const select_branchC = document.querySelector('#select_branchC');//take a value of selected branch
    const select_centerC = document.querySelector('#select_centerC');//take a value of selected center
    const selected_date1C = document.querySelector('#selected_date1C');//take a date of selected dateFrom
    const selected_date2C = document.querySelector('#selected_date2C');//take a date of selected dateTo
    const select_period_type = document.querySelector('#select_period_type');//take a date of selected dateTo
var myChart1; // declare a variable for above chart


function collectionBar(){

    $.ajax({

        type: 'GET',
        url: '{{('/newcollection')}}',
        data:{'branch':select_branchC.value,'center':select_centerC.value,'dateFrom':selected_date1C.value,
        'dateTo':selected_date2C.value,'select_period_type':select_period_type.value},
        dataType: 'JSON',
        success: function (data) {
            console.log(data);

            return draw_chart(data)
        }
    })
}


    function draw_chart(data){

        let labels = [];

        let totals_arr=[];
        let capitals_arr=[];
        let interests_arr=[];

        // let i=1;
        data.forEach(item => {
            labels.push(item.week_index)
            totals_arr.push(item.total)
            capitals_arr.push(item.capital)
            interests_arr.push(item.interest)
            // i++
        })
        console.log(labels);
        console.log(totals_arr);
        console.log(capitals_arr);
        console.log(interests_arr);


        var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels,
            datasets: [
            {
                label: 'Total',
                data: totals_arr,

            },
            {
                label: 'Capital',
                data: capitals_arr,

            },
            {
                label: 'Interest',
                data: interests_arr,

            },
        ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    }
    </script>
@endsection
