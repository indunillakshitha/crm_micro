@extends('layouts.master')
@section('title')

    Canty International
@endsection
@section('content')
{{--    -----------------------------------------------                      Appraisal form start    -----------------}}
<div class="modal fade bd-example-modal-lg" id="add_appraisal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pls fill</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/addexpenseproduct" method="POST">
                    @csrf

                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Product Name :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="product">
                            </div>
                        </div>
                    </div>
                  
                    <hr>
                    <hr>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit"class="btn btn-primary">Add Product</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

    {{--//--------------------------------------------------------------------------------------------   ------------}}
    <div class="">
        <div class="card">
            <div   class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Appraisal Expense Products</h2>
                        </div>
                        <div class="pull-right">
                           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_appraisal" data-whatever="@mdo">Add a Product</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <table class="table table-bordered">
        <tr>
            <th>Product ID</th>
            <th>Product Name</th>


        </tr>
        @foreach ($products as $product) 
            <tr>
                <td>{{$product->id }}</td>
                <td>{{$product->product }}</td>

            </tr>
            @endforeach



    </table>



@endsection
