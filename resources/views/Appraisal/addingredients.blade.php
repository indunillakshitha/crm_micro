@extends('layouts.master')
@section('content')


<div class="container">
    <div class="card">
        <div   class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                         <div class="pull-left">
                        <h2>Appraisal Expense Ingredients</h2>
                         </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card ">
        <div   class="card-body ">

                        {{-- <form action="addAppraisalExpensesProduct" method="POST"> --}}
                        <form action="/addingredient" method="POST">
                            @csrf

                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="husband" class="col-form-label">Product</label>
                                    </div>
                                    <div class="col-6">
                                        <input type="text" class="form-control" name="product">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="son" class="col-form-label">Ingredients :</label>
                                    </div>
                                    <div class="col-6">
                                        <input type="text" class="form-control" name="ingredient">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="recipient-name" class="col-form-label">Minimum Price :</label>
                                    </div>
                                    <div class="col-6">
                                        <input type="number" class="form-control" name="min_price">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="recipient-name" class="col-form-label">Maximum Price :</label>
                                    </div>
                                    <div class="col-6">
                                        <input type="number" class="form-control" name="max_price">
                                    </div>
                                </div>
                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit"class="btn btn-primary">Submit Appraisal</button>
                            </div>

                        </form>
                    </div>
                    <div class="pull-right">
{{--                                                    <a class="btn btn-success" href="{{ route('appraisalexpense.addAppraisalExpensesProduct') }}"> Create New Branch</a>--}}
{{--                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_appraisal_earning" data-whatever="@mdo">Add an Appraisal</button>--}}

                    </div>
                </div>

            </div>


</div>





@endsection
