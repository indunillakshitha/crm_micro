@extends('layouts.master')
@section('title')

    Canty International
@endsection
@section('content')
{{--    -----------------------------------------------                      Appraisal form start    -----------------}}
<div class="modal fade bd-example-modal-lg" id="add_appraisal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pls fill</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('appraisal.store') }}" method="POST">
                    @csrf

                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Borrower No :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="borrower_no">
                            </div>
                        </div>
                    </div>
                    <li >
                        <a href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                            Self Employee
                        </a>

                        <ul class="collapse list-unstyled" id="homeSubmenu3">
                            <li>
                                <div class="content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5 class="title">Add Earnings</h5>
                                                </div>
                                                <div class="card-body">

                                                    <div class="row">
                                                        <div class="col-md-5 pr-1">
                                                            <div class="form-group">
                                                                <label>Product</label>
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 px-1">
                                                            <div class="form-group">
                                                                <label>Category</label>
                                                                <input type="text" class="form-control" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-1">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Period</label>
                                                                <input type="email" class="form-control" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-1">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Email address</label>
                                                                <input type="email" class="form-control" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-1">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Unit Price</label>
                                                                <input type="email" class="form-control" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-1">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Total Units</label>
                                                                <input type="email" class="form-control" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-1">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Earn</label>
                                                                <input type="email" class="form-control" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <table class="table table-bordered">
                                                            <tr>
                                                                <th>Pro</th>
                                                                <th>Cat</th>
                                                                <th>Per</th>
                                                                <th>Up</th>
                                                                <th>Tp</th>
                                                                <th>Earn</th>
                                                            </tr>
                                                            <tr>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                            </tr>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </li>

                        </ul>
                    </li>



                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="husband" class="col-form-label">Husband :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="husband">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="son" class="col-form-label">Son :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="son">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Daughter :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="daughter">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Guardian :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="guardian">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Other :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="other">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Total Earn :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="total_earn">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Household Expenses :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="household_expenses">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Other Expenses :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="other_expenses">
                            </div>
                        </div>
                    </div>
                    <li >
                        <a href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                            Self Employee
                        </a>

                        <ul class="collapse list-unstyled" id="homeSubmenu3">
                            <li>
                                <div class="content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5 class="title">Add Expenses</h5>
                                                </div>
                                                <div class="card-body">

                                                    <div class="row">
                                                        <div class="col-md-5 pr-1">
                                                            <div class="form-group">
                                                                <label>Product</label>
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 px-1">
                                                            <div class="form-group">
                                                                <label>Ingredients</label>
                                                                <input type="text" class="form-control" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-1">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Period</label>
                                                                <input type="email" class="form-control" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-1">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Buying Price</label>
                                                                <input type="email" class="form-control" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-1">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Total Units</label>
                                                                <input type="email" class="form-control" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-1">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Expenses</label>
                                                                <input type="email" class="form-control" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-1">
                                                            <div class="form-group">
                                                                <input type="submit">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <table class="table table-bordered">
                                                            <tr>
                                                                <th>Pro</th>
                                                                <th>Cat</th>
                                                                <th>Per</th>
                                                                <th>Up</th>
                                                                <th>Tp</th>
                                                                <th>Earn</th>
                                                            </tr>
                                                            <tr>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                            </tr>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </li>

                        </ul>
                    </li>


                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">As Guarantor Credit Facility Count :</label>
                            </div>
                            <div class="col-6">
                                <label for="recipient-name" class="col-form-label text_align-center" > Total amount :</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="as_borrower_facility_count">

                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="as_borrower_total_amount">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">As Guarantor Credit Facility Count :</label>
                            </div>
                            <div class="col-6">
                                <label for="recipient-name" class="col-form-label">Total amount :</label>

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="as_guarantor_facility_count">
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="as_guarantor_total_amount">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Total Expenses :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="total_expenses">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <hr>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit"class="btn btn-primary">Submit Appraisal</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

    {{--//--------------------------------------------------------------------------------------------   ------------}}
    <div class="">
        <div class="card">
            <div   class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Appraisal Summery</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-success" href="createapprasialpage">Add an Appraisal</a>

                            {{--                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_appraisal" data-whatever="@mdo">Add an Appraisal</button>--}}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <table class="table table-bordered" id="data_table">
        <thead>
        <tr>
            <th>No</th>
            <th>NIC</th>
            <th>Center</th>
            <th>Rate</th>
            <th>Earnings</th>
            <th> Expenses</th>
            <th>Income</th>
            <th>Action</th>

        </tr>
        </thead>

        <tbody>
        @foreach ($apprasails as $appraisal)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $appraisal->nic }}</td>
                <td>{{ $appraisal->center }}</td>
                <td>{{ $appraisal->budjet_ratio }}</td>
                <td>{{ $appraisal->total_earn }}</td>
                <td>{{ $appraisal->total_expenses }}</td>
                <td>{{ $appraisal->sum }}</td>


                <td width = "10px">
           <form action="{{ route('appraisal.destroy',$appraisal->id) }}" method="POST">

                        <a class="btn btn-info" href="{{ route('appraisal.destroy',$appraisal->id) }}"><i class="fa fa-eye"></i></a>
{{--                                         <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Show</button>--}}

                        <a class="btn btn-primary" href="{{ route('appraisal.destroy',$appraisal->id) }}"><i class="fa fa-bars"></i></a>
{{--                                           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Edit</button>--}}
                        @csrf
                        {{-- @method('DELETE') --}}

                        {{-- <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button> --}}
<!-- {{--                                          <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Delete</button>--}} -->
                    </form>
                </td>

            </tr>
        @endforeach
    </tbody>
    </table>

    <script type="text/javascript">
        $(document).ready(function() {
             $('#data_table').DataTable();
        } );

    </script>

{{--    {!! $apprasails->links() !!}--}}

@endsection
