@extends('layouts.master')
@section('title')

    Create Appraisal
@endsection
@section('content')




    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Create Appraisal</h2>
                        </div>
                    </div>
                </div>
                <div>

                    {{-- <form action="{{ route('appraisal.store') }}" method="POST"> --}}
                    @csrf

                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Branch :</label>
                            </div>
                            <div class="col-6">
                            <input type="text" class="form-control" readonly value="{{$branch}}" id="branch">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Centers :</label>
                            </div>
                            <div class="col-6">

                                    <select name="" id="center"  onclick="genBorId()"class="form-control">
                                    @foreach($centers as $center)
                                        <option value="{{$center->center_no}}">{{$center->center_name}}</option>
                                         @endforeach
                                    </select>

                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Group No :</label>
                            </div>
                            <div class="col-6">
                                <select name="group_no" class="form-control" id="group" onclick="genBorId()">
                                <option value="1">Group 1</option>
                                <option value="2">Group 2</option>
                                <option value="3">Group 3</option>
                                <option value="4">Group 4</option>
                                <option value="5">Group 5</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="borrowersCount" value={{$borrowersCount}} >
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Borrower No :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="borrower_no"
                                       id="borrower_no" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Requesting Loan Amount :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="req_loan"
                                       id="req_loan">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                            <label for="loan_purpose">loan Purpose</label>
                            </div>
                            <div class="col-6">
                            <select type="text" name="loan_purpose" id="loan_purpose" class="form-control">
                                <option value="Education">Education</option>
                                <option value="Business">Business</option>
                                <option value="Health">Health</option>
                                <option value="Building">Building</option>
                                <option value="Repayment">Repayment</option>
                                <option value="Other">Other</option>
                            </select>
                            </div>

                        </div>
                    </div>
                    <li>
                        <a href="#homes" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                            Self Employee
                        </a>
                        <form action="">
                            @csrf
                            <ul class="collapse list-unstyled" id="homes">
                                <li>
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5 class="title">Add Earnings</h5>
                                                    </div>
                                                    <div class="card-body btn-outline-default">

                                                        <div class="row">
                                                            <div class="col-md-5 pr-1">
                                                                <div class="form-group">
                                                                    <label>Product</label>
                                                                    {{-- <input type="text" class="form-control"> --}}
                                                                    <select name="" id="productType"
                                                                            class="form-control">
                                                                        @foreach ($earnings as $earning)
                                                                            <option
                                                                                value="{{$earning->product}}">{{$earning->product}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            {{--                                                                <div class="col-md-3 px-1">--}}
                                                            {{--                                                                    <div class="form-group">--}}
                                                            {{--                                                                        <label>Category</label>--}}
                                                            {{--                                                                        <input type="text" class="form-control" >--}}
                                                            {{--                                                                    </div>--}}
                                                            {{--                                                                </div>--}}
                                                            <div class="col-md-4 pl-1">
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Period</label>
                                                                    <input type="email" class="form-control"
                                                                           id="EarningsPeriod">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5 pl-1">
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Unit Price</label>
                                                                    <input readonly type="text" class="form-control"
                                                                           id="EarningsUnitPrice">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 pl-1">
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Total Units</label>
                                                                    <input type="email" class="form-control"
                                                                           id="EarningsTotalUnits">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 pl-1">
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Earn</label>
                                                                    <input type="email" class="form-control" id="earns"
                                                                           readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 pl-1">
                                                                <div class="form-group">
                                                                    <button type="submit" id="addEarning"
                                                                            class="btn btn-primary">Add Earning
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <table class="table table-bordered" id="erTable">
                                                                <tr>
                                                                    <th>Product</th>
                                                                    {{-- <th>Cat</th> --}}
                                                                    <th>Period</th>
                                                                    <th>Unit Price</th>
                                                                    <th>Total Units</th>
                                                                    <th>Earn</th>
                                                                </tr>
                                                                {{-- <tr>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>
                                                                </tr> --}}

                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </form>
                    </li>


                    {{-- <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="husband" class="col-form-label">Husband :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="husband" id="husband">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="son" class="col-form-label">Son :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="son" id="son">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Daughter :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="daughter" id="daughter">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Guardian :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="guardian" id="guardian">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Other :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="other" id="other">
                            </div>
                        </div>
                    </div> --}}
                    <hr>
                    <form action="">
                        @csrf
                        <div class="row">
                            <div class="col-md-4 pr-1">
                                <div class="form-group">
                                    <label for="income_relationship">Income Relationship</label>
                                    <select name="income_relationship" id="income_relationship" class="form-control">
                                        <option value="Borrower">Borrower</option>
                                        <option value="Husband">Husband</option>
                                        <option value="Son">Son</option>
                                        <option value="Daughter">Daughter</option>
                                        <option value="Guardian">Guardian</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 pr-1">
                                <div class="form-group">
                                    <label for="income_relationship">Occupation Type</label>
                                    <select name="job_type" id="job_type" class="form-control">
                                        <option value="Government">Government</option>
                                        <option value="Private">Private</option>
                                        <option value="Self Employeed">Self Employeed</option>
                                        <option value="Semi Government">Semi Government</option>
                                        <option value="Labour">Labour</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 pr-1">
                                <div class="form-group">
                                    <label for="income">Salary</label>
                                    <input type="number" name="salary" id="salary" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4 pr-1">
                                <div class="form-group">
                                    <label for="income">Participation Amount</label>
                                    <input type="number" name="income" id="income" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4 pr-1">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-sm mt-3 pt-2" id="add_rela_income_btn">Add
                                        Income
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <table class="table table-bordered table-dark mx-2 my-2" id="main_income_table">
                            <tr>
                                <th>Income Relationship</th>
                                <th>Income Value</th>
                                <th>Job Type</th>
                                <th>Salary</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Total Earn :</label>
                            </div>
                            <div class="col-6">
                                <input readonly type="text" class="form-control" name="total_earn" id="total_earn">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <strong>Household Expenses :</strong>
                            </div>
                            <div class="col-6">
                                {{-- <input type="number" class="form-control" name="household_expenses" --}}
                                       {{-- id="household_expenses"> --}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Foods</label>
                            </div>
                            <div class="col-6">
                                <input type="number" class="form-control" name="foods" id="foods">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Hire Purchasing :</label>
                            </div>
                            <div class="col-6">
                                <input type="number" class="form-control" name="hire_purchasing" id="hire_purchasing">
                            </div>
                        </div>
                    </div> <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Accomadation :</label>
                            </div>
                            <div class="col-6">
                                <input type="number" class="form-control" name="accomadation" id="accomadation">
                            </div>
                        </div>
                    </div> <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Health :</label>
                            </div>
                            <div class="col-6">
                                <input type="number" class="form-control" name="health" id="health">
                            </div>
                        </div>
                    </div> <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Education:</label>
                            </div>
                            <div class="col-6">
                                <input type="number" class="form-control" name="education" id="education">
                            </div>
                        </div>
                    </div> <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Repayment :</label>
                            </div>
                            <div class="col-6">
                                <input type="number" class="form-control" name="repayment" id="repayment">
                            </div>
                        </div>
                    </div> <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Water Bill :</label>
                            </div>
                            <div class="col-6">
                                <input type="number" class="form-control" name="water_bill" id="water_bill">
                            </div>
                        </div>
                    </div> <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Electricity Bill :</label>
                            </div>
                            <div class="col-6">
                                <input type="number" class="form-control" name="electricity_bill" id="electricity_bill">
                            </div>
                        </div>
                    </div>  <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Other Expenses :</label>
                            </div>
                            <div class="col-6">
                                <input type="number" class="form-control" name="other_expenses" id="other_expenses">
                            </div>
                        </div>
                    </div>
                    <li>
                        <a href="#homeSu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                            Self Employee
                        </a>

                        <form>
                            @csrf

                            <ul class="collapse list-unstyled" id="homeSu">
                                <li>
                                    <div class="content ">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card btn-outline-default">
                                                    <div class="card-header">
                                                        <h5 class="title">Add Expenses</h5>
                                                    </div>
                                                    <div class="card-body btn-outline-default">

                                                        <div class="row">
                                                            <div class="col-md-5 pr-1">
                                                                <div class="form-group">
                                                                    <label>Product</label>
                                                                    {{-- <input type="text" class="form-control"> --}}
                                                                    <select name="" id="expenseType"
                                                                            class="form-control">
                                                                        @foreach ($expenses as $expense)
                                                                            <option
                                                                                value="{{$expense->product}}">{{$expense->product}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 px-1">
                                                                <div class="form-group">
                                                                    <label>Ingredients</label>
                                                                    {{-- <input type="text" class="form-control" id="ings"> --}}
                                                                    <select name="ings" id="ings"
                                                                            class="form-control"></select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 pl-1">
                                                                <div class="form-group">
                                                                    <label for="">Period</label>
                                                                    <input type="number"
                                                                           name="period" class="form-control"
                                                                           id="expensePeriod">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 pl-1">
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Buying Price</label>
                                                                    <input type="number"
                                                                           class="form-control" id="buyingPrice">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 pl-1">
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Total Units</label>
                                                                    <input type="number"
                                                                           name="total_units"
                                                                           class="form-control" id="totalExpenseUnits">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 pl-1">
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Expenses</label>
                                                                    <input type="email"
                                                                           name="expenses"
                                                                           class="form-control" id="expenses" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 pl-1">
                                                                <div class="form-group">
                                                                    <button type="submit" id="addExpense"
                                                                            class="btn btn-primary">Add Expense
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <table class="table table-bordered" id="exTable">
                                                                <tr>
                                                                    <th>Product</th>
                                                                    {{-- <th>Ingredients</th> --}}
                                                                    <th>Period</th>
                                                                    <th>Buying Price</th>
                                                                    <th>Total Units</th>
                                                                    <th>Expenses</th>
                                                                </tr>
                                                                {{-- <tr>
                                                                    <td >
                                                                        <input type="text" class="form-control" id="exPro">
                                                                    </td>
                                                                    <td >
                                                                        <input type="text" class="form-control" id="exIng">
                                                                    </td>
                                                                    <td >
                                                                        <input type="text" class="form-control" id="exBuyPr">
                                                                    </td>
                                                                    <td >
                                                                        <input type="text" class="form-control" id="exToUni">
                                                                    </td>
                                                                    <td >
                                                                        <input type="text" class="form-control" id="">
                                                                    </td>
                                                                    <td >
                                                                        <input type="text" class="form-control" id="exEx">
                                                                    </td>

                                                                </tr> --}}

                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </form>
                    </li>


                    <hr>
                    {{-- <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">As Guarantor Credit Facility Count :</label>
                            </div>
                            <div class="col-6">
                                <label for="recipient-name" class="col-form-label text_align-center" > Total amount :</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="as_borrower_facility_count">

                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="as_borrower_total_amount">
                            </div>
                        </div>
                    </div> --}}
                    <li>
                        <a href="#otherloansdrop" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                            Other Loans
                        </a>

                        <form>
                            @csrf

                            <ul class="collapse list-unstyled" id="otherloansdrop">
                                <li>
                                    <div class="content ">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card btn-outline-default">
                                                    <div class="card-header">
                                                        <h5 class="title">Other Loans</h5>
                                                    </div>
                                                    <div class="card-body btn-outline-default">

                                                        <div class="row">
                                                            <div class="col-md-5 pr-1">
                                                                <div class="form-group">
                                                                    <label>Bank Name</label>
                                                                    <input type="text" class="form-control"
                                                                           id="bank_name">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 px-1">
                                                                <div class="form-group">
                                                                    <label>Total Loan</label>
                                                                    <input type="number" class="form-control"
                                                                           id="total_loan">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 pl-1">
                                                                <div class="form-group">
                                                                    <label for="">Monthly Payment</label>
                                                                    <input type="number"
                                                                           name="period" class="form-control"
                                                                           id="monthly_payment">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 pl-1">
                                                                <div class="col-md-4 pl-1">
                                                                    <div class="form-group">
                                                                        <button type="submit" id="add_loan"
                                                                                class="btn btn-primary">Add Expense
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <table class="table table-bordered" id="loansTable">
                                                                <tr>
                                                                    <th>Bank Name</th>
                                                                    <th>Total Loan</th>
                                                                    <th>Monthly Payment Price</th>
                                                                </tr>
                                                                <tr>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </form>
                    </li>
                    <br>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Total Expenses :</label>
                            </div>
                            <div class="col-6">
                                <input readonly type="text" class="form-control" name="total_expenses"
                                       id="total_expenses">
                            </div>
                        </div>
                        <input type="hidden" id="hiddenTotal" name="real_total">
                    </div>
                    <hr>
                    <hr>

                    <form action="{{route('appraisal.store')}}" method="POST">
                        @csrf

                        <input type="hidden" name="borrower_no" id="form_borrower_no">
                        <input type="hidden" name="reqesting_loan_amount" id="form_req_loan">
                        <input type="hidden" name="loan_purpose" id="form_loan_purpose">
                        <input type="hidden" name="self_employee_total" id="form_self_employee_total">
                        <input type="hidden" name="total_earn" id="form_total_earn">
                        <input type="hidden" name="household_expenses" id="form_household_expenses">
                        <input type="hidden" name="self_employee_expenses" id="form_self_employee_expenses">
                        <input type="hidden" name="other_expenses" id="form_other_expenses">
                        <input type="hidden" name="total_expenses" id="form_total_expenses">
                        <input type="hidden" name="sum" id="form_sum">

                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label for="budget_ratio"> Total Earns / Total Expenses</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control" name="budget_ratio" id="form_budget_ratio"
                                           readonly>
                                </div>
                            </div>
                        </div>
                        {{-- <input type="hidden" name="">
                        <input type="hidden" name="">
                        <input type="hidden" name="">
                        <input type="hidden" name=""> --}}

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit Appraisal</button>
                        </div>
                    </form>

                    </form>
                </div>

            </div>

        </div>
    </div>
    <input type="hidden" id="hidden_realy_income">
    <input type="hidden" id="hidden_loans_income">

    <input type="hidden" id="hiddenEx">
    <input type="hidden" id="hiddenEr">

    </div>

@section('scripts')

    <script type="text/javascript">


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        const branch = document.querySelector('#branch')
        const center = document.querySelector('#center')
        const group = document.querySelector('#group')
        const borrowersCount = document.querySelector('#borrowersCount')
        const borrower_no = document.querySelector('#borrower_no')

        function genBorId(){
            let branchString = `${branch.value}`
            branchString = branchString.slice(0, 3);
            branchString = branchString.toUpperCase();
            // console.log(branchString);

            let centerString = `00${center.value}`;
            // let centerString = this.centerString;
            centerString = centerString.slice(centerString.length - 3, centerString.length);

            let borrowerString = `00${borrowersCount.value}`;
            borrowerString = borrowerString.slice(borrowerString.length - 3, borrowerString.length);

            let groupString = `00${group.value}`;
            groupString = groupString.slice(groupString.length - 3, groupString.length);

            let id = `${branchString}${centerString}${groupString}${borrowerString}`
            // console.log(id);
            borrower_no.value = id;
        }

        const ings = document.querySelector('#ings')
        const expenseType = document.querySelector('#expenseType')

        $(document).ready(function () {


            $(productType).click(function () {
                var product = $(this).val();
                console.log(product);

                $.ajax({
                    type: 'GET',
                    url: '{{url('/createapprasialpage/products')}}',
                    data: {'id': product},
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data);
                        $('#EarningsUnitPrice').val(data)
                    }

                });
            });

            $('#expenseType').click(function () {
                let expense = $(this).val();
                console.log(expense);

                $.ajax({
                    type: 'GET',
                    // url: '{{url('/createapprasialpage/expenses')}}',
                    url: '{{('/getings')}}',
                    data: {'product': expense},
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data, 'got ings')
                        // $('#buyingPrice').val(data[0]);
                        // $('#ings').val(data[1]);

                        // $('#buyingPrice').val(data.buying_price)
                        ings.innerHTML = `
                    <select name="ings" id="ings" class="form-control"></select>
                    `

                        data.forEach(record => {
                            // $('#ings').val(record.ingredient)
                            html =
                                `<option id="${record.id}" value="${record.ingredient}">${record.ingredient}</option>
                        `

                            // $('#ings').append(html(html))
                            ings.innerHTML += html
                        })

                    }
                })

                // axios.get('/createapprasialpage/expenses', {
                //     params : {
                //         id: expense
                //     }
                // })
                // .then(res => console.log(res))
                // .catch(err => console.log(err))
            })

            // $('#addExpense').click(function (e){
            //     e.preventDefault()
            //     console.log('addd')
            //     addExpense();
            //     console.log($('#expenseType').val())
            //     // $('#exPro').val() = $('#expenseType').val();
            //     $('#exPro').val() =1333;
            // })

            //  $.ajax({
            //         type: 'GET',
            //         url: '{{('/getings')}}',
            //         data: { 'product': expense},
            //         dataType: 'JSON',
            //         success: function(data){
            //             console.log(data)
            //         }
            //     })
        });

        ings.addEventListener('click', (e) => {
            // console.log(e.target)
            $.ajax({
                type: 'GET',
                url: '{{('/getingprice')}}',
                data: {
                    'ing': ings.value,
                    'product': expenseType.value
                },
                dataType: 'JSON',
                success: function (data) {
                    console.log(data)
                    buyingPrice.value = data[0].buying_price

                }
            })
        })

        const borrowerNo = document.querySelector('#borrower_no');
        const req_loan = document.querySelector('#req_loan');
        const loan_purpose = document.querySelector('#loan_purpose');
        const erTable = document.querySelector('#erTable');
        const earnType = document.querySelector('#productType');
        const earningsUnitPrice = document.querySelector('#EarningsUnitPrice');
        const earningsTotalUnits = document.querySelector('#EarningsTotalUnits');
        const EarningsPeriod = document.querySelector('#EarningsPeriod');
        const earns = document.querySelector('#earns');

        // relationship incomes
        const income_relationship = document.querySelector('#income_relationship')
        const income = document.querySelector('#income')
        const job_type = document.querySelector('#job_type')
        const salary = document.querySelector('#salary')

        // household expenses
        const foods = document.querySelector('#foods')
        const hire_purchasing = document.querySelector('#hire_purchasing')
        const accomadation = document.querySelector('#accomadation')
        const health = document.querySelector('#health')
        const education = document.querySelector('#education')
        const repayment = document.querySelector('#repayment')
        const water_bill = document.querySelector('#water_bill')
        const electricity_bill = document.querySelector('#electricity_bill')

        const exTable = document.querySelector('#exTable');
        const productName = document.querySelector('#expenseType');
        // const ingredients = document.querySelector('#ings');
        const buyingPrice = document.querySelector('#buyingPrice')
        const totalExpenseUnits = document.querySelector('#totalExpenseUnits')
        const expensePeriod = document.querySelector('#expensePeriod')
        const expenses = document.querySelector('#expenses')

        const total_expenses = document.querySelector('#total_expenses')
        // const household_expenses = document.querySelector('#household_expenses')
        const other_expenses = document.querySelector('#other_expenses')
        // total_expenses.value = parseInt(0);

        const hiddenEx = document.querySelector('#hiddenEx');
        hiddenEx.value = 0;
        // household_expenses.value = 0;
        other_expenses.value = 0;

        const total_earn = document.querySelector('#total_earn')
        const hiddenEr = document.querySelector('#hiddenEr')
        const hidden_realy_income = document.querySelector('#hidden_realy_income')
        hidden_realy_income.value = 0;
        hiddenEr.value = 0

        const hiddenTotal = document.querySelector('#hiddenTotal')
        hiddenTotal.value = 0

        // form
        const form_borrower_no = document.querySelector('#form_borrower_no')
        const form_req_loan = document.querySelector('#form_req_loan')
        const form_loan_purpose = document.querySelector('#form_loan_purpose')
        const form_self_employee_total = document.querySelector('#form_self_employee_total')
        const form_total_earn = document.querySelector('#form_total_earn')
        const form_household_expenses = document.querySelector('#form_household_expenses')
        const form_self_employee_expenses = document.querySelector('#form_self_employee_expenses')
        const form_other_expenses = document.querySelector('#form_other_expenses')
        const form_total_expenses = document.querySelector('#form_total_expenses')
        const form_sum = document.querySelector('#form_sum')
        const form_budget_ratio = document.querySelector('#form_budget_ratio')

        form_budget_ratio.value = 0;
        hidden_loans_income.value = 0;


        setInterval(() => {
            calEarn()
        }, 1);

        function calEarn() {
            earns.value = earningsUnitPrice.value * earningsTotalUnits.value * EarningsPeriod.value;
        }

        // cal household expenses
        foods.value = 0
        hire_purchasing.value = 0
        accomadation.value = 0
        health.value = 0
        education.value = 0
        repayment.value = 0
        water_bill.value = 0
        electricity_bill.value = 0
        setInterval(() => {
            form_household_expenses.value = parseInt(foods.value) + parseInt(hire_purchasing.value) + parseInt(accomadation.value) + parseInt(health.value) + parseInt(education.value) + parseInt(repayment.value) + parseInt(water_bill.value) + parseInt(electricity_bill.value)
        }, 1);

        // cal product expenses
        setInterval(() => {
            expenses.value = buyingPrice.value * totalExpenseUnits.value * expensePeriod.value;
        }, 1);

        // cal total expenses
        setInterval(() => {
            total_expenses.value = parseInt(form_household_expenses.value) + parseInt(other_expenses.value) + parseInt(hiddenEx.value) + parseInt(hidden_loans_income.value);
        }, 1);

        // cal total earnings
        setInterval(() => {
            total_earn.value = parseInt(hiddenEr.value)
                + parseInt(hidden_realy_income.value)
        }, 1);

        // cal real total
        setInterval(() => {
            // hiddenTotal.value =  parseInt(hiddenEr.value) -  parseInt(hiddenEx.value)
            hiddenTotal.value = total_earn.value - total_expenses.value
            form_budget_ratio.value = total_earn.value / total_expenses.value
        }, 1);

        // set form
        setInterval(() => {
            form_borrower_no.value = borrowerNo.value
            form_req_loan.value = req_loan.value
            form_loan_purpose.value = loan_purpose.value
            form_self_employee_total.value = hiddenEr.value
            form_total_earn.value = total_earn.value
            // form_household_expenses.value = req_loan.value
            form_self_employee_expenses.value = hiddenEx.value
            form_other_expenses.value = other_expenses.value
            form_total_expenses.value = total_expenses.value
            form_sum.value = hiddenTotal.value
        }, 1);


        // earnings
        document.querySelector('#addEarning').addEventListener('click', e => {
            e.preventDefault()

            $.ajax({
                type: 'POST',
                url: '{{url('/createapprasialpage/expenses/addearning')}}',
                data: {
                    'borrower_id': borrowerNo.value,
                    'product_name': earnType.value,
                    'period': EarningsPeriod.value,
                    'total_units': earningsTotalUnits.value,
                    'earnings': earns.value,
                    'unit_price': earningsUnitPrice.value
                },
                dataType: 'JSON',
                success: function (data) {
                    console.log(data, 'optimus prime')
                }
            })
                .then(function () {
                    return $.ajax({
                        type: 'GET',
                        url: '{{('/createapprasialpage/expenses/getearning')}}',
                        data: {'borrower_id': borrowerNo.value},
                        success: function (data) {
                            console.log(data, 'hasta la vista')

                            erTable.innerHTML = `
                    <tr>
                        <th>Product</th>
                        <th>Period</th>
                        <th>Unit Price</th>
                        <th>Total Units</th>
                        <th>Earn</th>
                    </tr>
                    `

                            // total_earn.value = 0;
                            hiddenEr.value = 0;
                            data.forEach(record => {
                                console.log(record.product_name)
                                hiddenEr.value = parseInt(record.earnings) + parseInt(hiddenEr.value);

                                let html = `
                        <tr>
                            <td > ${record.product_name}  </td>
                            <td > ${record.period}  </td>
                            <td > ${record.unit_price}  </td>
                            <td > ${record.total_units}  </td>
                            <td > ${record.earnings}  </td>
                        </tr>
                        `
                                erTable.innerHTML += html
                            })
                        }
                    })
                })
        })

        //expenses
        document.querySelector('#addExpense').addEventListener('click', e => {
            e.preventDefault()
            // console.log(borrowerNo)
            $.ajax({
                type: 'POST',
                url: '{{url('/createapprasialpage/expenses/addexpense')}}',
                data: {
                    'borrower_id': borrowerNo.value,
                    // borrowerNo,
                    // 'ingredients': ingredients.value,
                    'buyingPrice': buyingPrice.value,
                    'total_units': totalExpenseUnits.value,
                    'period': expensePeriod.value,
                    'expenses': expenses.value,
                    'product_name': productName.value,
                },
                dataType: 'JSON',
                success: function (data) {
                    console.log(data, '123bumblebee')

                }
            })
                .then(function () {
                    return $.ajax({
                        type: 'GET',
                        url: '{{('/createapprasialpage/expenses/getexpense')}}',
                        data: {'borrower_id': borrowerNo.value},
                        success: function (data) {
                            console.log(data)
                            exTable.innerHTML = `
                    <tr>
                        <th>Product</th>
                        {{-- <th>Ingredients</th> --}}
                            <th>Period</th>
                            <th>Buying Price</th>
                            <th>Total Units</th>
                            <th>Expenses</th>
                        </tr>
`
                            // household_expenses.value =  0;
                            // other_expenses.value= 0;
                            // total_expenses.value = parseInt(household_expenses.value)+parseInt(other_expenses.value);
                            hiddenEx.value = 0;
                            data.forEach(record => {
                                console.log(record.product_name)
                                // total_expenses.value = parseInt(record.expenses) + parseInt(total_expenses.value);
                                hiddenEx.value = parseInt(record.expenses) + parseInt(hiddenEx.value);
                                // recordExpenses = parseInt(record.expenses) + parseInt(recordExpenses)

                                let html = `
                        <tr>
                            <td > ${record.product_name}  </td>
                            <td > ${record.period}  </td>
                            <td > ${record.buying_price}  </td>
                            <td > ${record.total_units}  </td>
                            <td > ${record.expenses}  </td>
                        </tr>
                        `
                                exTable.innerHTML += html
                            })
                        }
                    })
                })
        })

        document.querySelector('#add_rela_income_btn').addEventListener('click', (e) => {
            e.preventDefault()
            $.ajax({
                type: 'POST',
                url: '{{('/addmainincome')}}',
                data: {
                    'borrower_id': borrowerNo.value,
                    'income_relationship': income_relationship.value,
                    'income': income.value,
                    'job_type': job_type.value,
                    'salary': salary.value
                },
                dataType: 'JSON',
                success: function (data) {
                    console.log(data, 'income added')
                }
            })
                .then(function () {
                    return $.ajax({
                        type: 'GET',
                        url: '{{('/getmainincome')}}',
                        data: {'borrower_id': borrowerNo.value},
                        success: function (data) {
                            console.log(data)
                            main_income_table.innerHTML = `
                    <tr>
                        <th>Income Relationship</th>
                        <th>Income Value</th>
                        <th>Job Type</th>
                        <th>Salary</th>
                    </tr>
                    `
                            hidden_realy_income.value = 0
                            data.forEach(record => {
                                hidden_realy_income.value = parseInt(hidden_realy_income.value) + parseInt(record.income)
                                let html = `
                        <td > ${record.income_relationship}  </td>
                        <td > ${record.income}  </td>
                        <td > ${record.job_type}  </td>
                        <td > ${record.salary}  </td>
                        `

                                main_income_table.innerHTML += html
                            })
                        }
                    })
                })
        })

        const bank_name = document.querySelector('#bank_name')
        const total_loan = document.querySelector('#total_loan')
        const monthly_payment = document.querySelector('#monthly_payment')
        const loansTable = document.querySelector('#loansTable')

        document.querySelector('#add_loan').addEventListener('click', e => {
            e.preventDefault()

            $.ajax({
                type: 'POST',
                url: '{{('/appraisalloans/addloan')}}',
                data: {
                    'borrower_id': borrowerNo.value,
                    'bank_name': bank_name.value,
                    'total_loan': total_loan.value,
                    'monthly_payment': monthly_payment.value,
                },
                dataType: 'JSON',
                success: function (data) {
                    console.log(data, 'hakuna matata')
                },
                error: function (err) {
                    console.log(err)
                }
            })
                .then(function () {
                    return $.ajax({
                        type: 'GET',
                        url: '{{('/appraisalloans/getloan')}}',
                        data: {'borrower_id': borrowerNo.value},
                        success: function (data) {
                            console.log(data)
                            loansTable.innerHTML = `
                    <tr>
                        <th>Bank Name</th>
                        <th>Total Loan</th>
                        <th>Monthly Payment Price</th>
                    </tr>
                    `
                            hidden_loans_income.value = 0
                            data.forEach(record => {
                                hidden_loans_income.value = parseInt(hidden_loans_income.value) + parseInt(record.monthly_paymentt)
                                let html = `
                        <td > ${record.bank_name}  </td>
                        <td > ${record.total_loan}  </td>
                        <td > ${record.monthly_paymentt}  </td>
                        `

                                loansTable.innerHTML += html
                            })
                        }
                    })
                })
        })

    </script>
@endsection


@endsection
