@extends('layouts.master')
@section('title')

    Canty International
@endsection
@section('content')
    <div class="modal fade" id="addcategorymodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Expense</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        {{--                        <span aria-hidden="true">&times;</span>--}}
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('borrower.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Product</label>
                            <input type="text" class="form-control" id="product">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Ingredients:</label>
                            <input type="text" class="form-control" id="ingredient">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Buting Price:</label>
                            <input type="text" class="form-control" id="buying_price">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" href="{{ route('branch.create') }}" class="btn btn-primary">Add</button>
                </div>
            </div>
        </div>
    </div>

    {{--//--------------------------------------------------------------------------------------------------------}}

    <div class="container">
        <div class="card">
            <div   class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Branches</h2>
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addcategorymodel" data-whatever="@mdo">Create New Earning Category</button>
                            {{--                            <a class="btn btn-success" href="{{ route('branch.create') }}"> Create New Earning Category</a>--}}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Product</th>
            <th>Ingredient</th>
            <th>Buying Price</th>


            <th width="200px">Action</th>
        </tr>
        @foreach ($expenses_products as $expenses_product)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $expenses_product->product }}</td>
                <td>{{ $expenses_product->ingredient }}</td>
                <td>{{ $expenses_product->buying_price }}</td>
                <td width = "10px">
                    <form href="{{ route('branch.destroy',$category->id) }}" >

                        {{--                        <a class="btn btn-info" href="{{ route('branch.show',$branch->id) }}">Show</a>--}}
                        {{--                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Show</button>--}}

                        <a class="btn btn-primary" href="{{ route('branch.edit',$category->id) }}">Edit</a>
                        {{--                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Edit</button>--}}
                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger">Delete</button>
                        {{--                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Show</button>--}}
                    </form>
                </td>

            </tr>
        @endforeach
    </table>

    {!! $branches->links() !!}

@endsection
