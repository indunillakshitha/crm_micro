@extends('layouts.master')
@section('title')


@endsection
@section('content')
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="pull-left">
                        <h2>Appraisals All</h2>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="card animated fadeIn" style="width: 100%;">
    <div class="card-body">
        <table class="table table-bordered" id="data_table">
            <thead>
            <tr>
                <th>NIC</th>
                <th>Toptal Earn </th>
                <th>Total Expences</th>
                <th>Ratio</th>
                <th> Purpose</th>

            </tr>
        </thead>

        <tbody>
            @foreach ($appraisals as $appraisal)
            <tr>
                <td>{{ $appraisal->nic}}</td>
                <td>{{ $appraisal->total_earn}}</td>
                <td>{{ $appraisal->total_expenses}}</td>
                <td>{{ $appraisal->budget_ratio}}</td>
                <td>{{ $appraisal->loan_purpose}}</td>


            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>

@endsection
