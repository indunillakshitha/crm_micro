@extends('layouts.master')
@section('title')

    Canty International
@endsection
@section('content')
    <div class="modal fade bd-example-modal-lg" id="add_appraisal_earning" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pls fill</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('appraisalearning.store') }}" method="POST">
                        @csrf

                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label for="recipient-name" class="col-form-label">Product :</label>
                                </div>
                                <div class="col-6">
                                    <input type="text" class="form-control" name="product">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label for="husband" class="col-form-label">Categorry Name :</label>
                                </div>
                                <div class="col-6">
                                    <input type="text" class="form-control" name="category_name">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label for="son" class="col-form-label">Minimum Price :</label>
                                </div>
                                <div class="col-6">
                                    <input type="text" class="form-control" name="min">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label for="son" class="col-form-label">Maximum Price :</label>
                                </div>
                                <div class="col-6">
                                    <input type="text" class="form-control" name="max">
                                </div>
                            </div>
                        </div>


                        <hr>
                        <hr>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit"class="btn btn-primary">Submit Appraisal Earning</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>


    {{--//--------------------------------------------------------------------------------------------------------}}

    <div class="container">
        <div class="card">
            <div   class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Appraisal Earning Products</h2>
                        </div>
                        <div class="pull-right">
{{--                            <a class="btn btn-success" href="{{ route('branch.create') }}"> Create New Branch</a>--}}
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_appraisal_earning" data-whatever="@mdo">Add an Appraisal</button>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

{{--    @if ($message = Session::get('success'))--}}
{{--        <div class="alert alert-success">--}}
{{--            <p>{{ $message }}</p>--}}
{{--        </div>--}}
{{--    @endif--}}

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Product</th>
            <th>Category Name</th>
            <th>Unit Price</th>


            <th width="250px">Action</th>
        </tr>
        @foreach ($apprasailEarningProducts as $apprasailEarningProduct)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $apprasailEarningProduct->product }}</td>
                <td>{{ $apprasailEarningProduct->category_name }}</td>
                <td>{{ $apprasailEarningProduct->unit_price }}</td>

                <td >
                    <form href="" >
                        <div class="row ">
                            <a class="btn btn-info" href=""><i class="fa fa-eye"></i></a>
                            {{--                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Show</button>--}}

                            <a class="btn btn-primary" href=""><i class="fa fa-bars"></i></a>
                            {{--                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Edit</button>--}}
                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                            {{--                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Show</button>--}}

                        </div>
                    </form>
                </td>

            </tr>
        @endforeach
    </table>



@endsection
