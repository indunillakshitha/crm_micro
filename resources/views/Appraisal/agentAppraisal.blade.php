@extends('layouts.master')
@section('title')

Create Appraisal
@endsection
@section('content')




<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Create Appraisal</h2>
                    </div>
                </div>
            </div>
            <div>

                {{-- <li>
                    <a href="#newBorrower" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                        Create Appraisal for a new borrower
                    </a>
                        @csrf
                        <ul class="collapse list-unstyled" id="newBorrower">
                            <li>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="recipient-name" class="col-form-label">Branch :</label>
                                        </div>
                                        <div class="col-6">
                                        <input type="text" class="form-control" readonly value="{{$branch}}"
                id="branch">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col">
                <label for="recipient-name" class="col-form-label">Centers :</label>
            </div>
            <div class="col-6">

                <select name="" id="center" class="form-control">
                    @foreach($centers as $center)
                    <option value="{{$center->center_no}}">{{$center->center_name}}</option>
                    @endforeach
                </select>

            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col">
                <label for="recipient-name" class="col-form-label">Group No :</label>
            </div>
            <div class="col-6">
                <select name="group_no" class="form-control" id="group" onclick="get_borrowers_count()">
                    <option value="1">Group 1</option>
                    <option value="2">Group 2</option>
                    <option value="3">Group 3</option>
                    <option value="4">Group 4</option>
                    <option value="5">Group 5</option>
                </select>
            </div>
            <input type="hidden" id="center_name">
        </div>
    </div>
    </li>
    </ul>
    </li> --}}

    {{-- <li>
                    <a href="#existingBorrower" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                        Create Appraisal for an existing borrower
                    </a>
                        @csrf
                        <ul class="collapse list-unstyled" id="existingBorrower">
                            <li>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="recipient-name" class="col-form-label">Borrower NIC :</label>
                                        </div>
                                        <div class="col-6">
                                        <input type="text" class="form-control"  id="borrower_nic">
                                        <button class="btn btn-secondary btn-sm mt-3 pt-2" id="check_nic_btn" onclick="check_nic()">Check NIC
                                        </button>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                </li> --}}

    <div class="form-group">
        <div class="row">
            <div class="col">
                <label for="recipient-name" class="col-form-label">Borrower NIC :</label>
            </div>
            <div class="col-6">
            <input type="text" class="form-control" id="borrower_nic" value="{{$id}}">
                <button class="btn btn-secondary btn-sm mt-3 pt-2" id="check_nic_btn" onclick="check_nic()">Check NIC
                </button>
            </div>
        </div>

        <div id="nic_message" class="d-none"></div>
        <button id="del_prev_app_btn" onclick="del_prev_app()" class="d-none btn-success">Delete previous
            Appraisal</button>
    </div>


    <input type="hidden" id="borrowersCount" value={{$borrowersCount}}>
    {{-- <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="recipient-name" class="col-form-label">Borrower No :</label>
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="borrower_no"
                                       id="borrower_no" >
                            </div>
                        </div>
                    </div> --}}
    <div id="after_nic">
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Center :</label>
                </div>
                <select class="form-control" id="center">
                    @foreach($centers as $center)
                    <option value="{{$center->center_name}}">{{$center->center_name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="row d-none" >
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Requesting Loan Amount :</label>
                </div>
                <div class="col-6">
                    <input type="text" class="form-control" name="req_loan" id="req_loan" value="0">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <label for="loan_purpose">loan Purpose</label>
                </div>
                <div class="col-6">
                    <select type="text" name="loan_purpose" id="loan_purpose" class="form-control">
                        <option value="Business">Business</option>
                        <option value="Education">Education</option>
                        <option value="Health">Health</option>
                        <option value="Building">Building</option>
                        <option value="Repayment">Repayment</option>
                        <option value="clothe">clothe</option>
                        <option value="Purchasing">Purchasing</option>
                        <option value="Other">Other</option>
                    </select>
                </div>

            </div>
        </div>
        <hr>
        <li>
            <a href="#homes" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                Self Employee | Earnings
            </a>
            <form action="" id="self_emp_earns_form">
                @csrf
                <ul class="collapse list-unstyled" id="homes">
                    <li>
                        <div class="content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="title">Add Earnings</h5>
                                        </div>
                                        <div class="card-body btn-outline-default">

                                            <div class="row">
                                                <div class="col-md-5 pr-1">
                                                    <div class="form-group">
                                                        <label>Product</label>
                                                        {{-- <input type="text" class="form-control"> --}}
                                                        <select name="" id="productType" class="form-control">
                                                            @foreach ($earnings as $earning)
                                                            <option value="{{$earning->product}}">{{$earning->product}}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 pl-1">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Period</label>
                                                        <input type="number" class="form-control" id="EarningsPeriod">
                                                    </div>
                                                </div>
                                                <input type="hidden" id="min_earn">
                                                <input type="hidden" id="max_earn">
                                                <div class="col-md-5 pl-1">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Unit Price</label>
                                                        <input type="number" class="form-control"
                                                            id="EarningsUnitPrice">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 pl-1">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Total Units</label>
                                                        <input type="number" class="form-control"
                                                            id="EarningsTotalUnits">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 pl-1">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Earn</label>
                                                        <input type="number" class="form-control" id="earns" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 pl-1">
                                                    <div class="form-group">
                                                        <button type="submit" id="addEarning"
                                                            class="btn btn-primary">Add Earning
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="container text-center bg-danger d-none " id="range_error_earn">
                                                Earning not added...Not in price range
                                            </div>

                                            <div class="row">
                                                <table class="table table-bordered" id="erTable">
                                                    <tr>
                                                        <th>Product</th>
                                                        {{-- <th>Cat</th> --}}
                                                        <th>Period</th>
                                                        <th>Unit Price</th>
                                                        <th>Total Units</th>
                                                        <th>Earn</th>
                                                    </tr>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </li>

                </ul>
            </form>
        </li>
        <li>
            <a href="#homeSu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                Self Employee | Expenses
            </a>

            <form id="self_emp_exp_form">
                @csrf

                <ul class="collapse list-unstyled" id="homeSu">
                    <li>
                        <div class="content ">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card btn-outline-default">
                                        <div class="card-header">
                                            <h5 class="title">Add Expenses</h5>
                                        </div>
                                        <div class="card-body btn-outline-default">



                                            <div class="row">
                                                <div class="col-md-5 pr-1">
                                                    <div class="form-group">
                                                        <label>Product</label>
                                                        {{-- <input type="text" class="form-control"> --}}
                                                        <select name="" id="expenseType" class="form-control">
                                                            @foreach ($expenses as $expense)
                                                            <option value="{{$expense->product}}">{{$expense->product}}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="container text-center bg-danger d-none " id="range_error">
                                                Expense not added...Not in price range
                                            </div>

                                            <div class="row">
                                                <div class="col-2 px-1"> Ingredient</div>
                                                <div class="col-2 px-1"> Buying Price </div>
                                                <div class="col-2 px-1"> Period</div>
                                                <div class="col-2 px-1"> Total Units </div>
                                                <div class="col-2 px-1"> Expenses</div>
                                                <div class="col-2 px-1"> </div>
                                            </div>
                                            <div class="" id="ings_section">

                                            </div>

                                            <div class="row">
                                                <table class="table table-bordered" id="exTable">
                                                    <tr>
                                                        <th>Product</th>
                                                        {{-- <th>Ingredients</th> --}}
                                                        <th>Period</th>
                                                        <th>Buying Price</th>
                                                        <th>Total Units</th>
                                                        <th>Expenses</th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </li>

                </ul>
            </form>
        </li>
        <hr>

        <form action="" id="relay_form">
            @csrf
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label for="income_relationship">Income Relationship</label>
                        <select name="income_relationship" id="income_relationship" class="form-control">
                            <option value="Borrower">Borrower</option>
                            <option value="Husband">Husband</option>
                            <option value="Son">Son</option>
                            <option value="Daughter">Daughter</option>
                            <option value="Guardian">Guardian</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label for="income_relationship">Occupation Type</label>
                        <select name="job_type" id="job_type" class="form-control">
                            <option value="Government">Government</option>
                            <option value="Private">Private</option>
                            <option value="Self Employeed">Self Employeed</option>
                            <option value="Semi Government">Semi Government</option>
                            <option value="Labour">Labour</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label for="income">Income</label>
                        <input type="number" name="salary" id="salary" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pr-1 d-none" >
                    <div class="form-group">
                        <label for="income">Participation Amount</label>
                        <input type="number" name="income" id="income" class="form-control" value="0">
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <button class="btn btn-primary btn-sm mt-3 pt-2" id="add_rela_income_btn">Add
                            Income
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <table class="table table-bordered table-dark mx-2 my-2" id="main_income_table">
                <tr>
                    <th>Income Relationship</th>
                    <th>Income Value</th>
                    <th>Job Type</th>
                    <th>Salary</th>
                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </table>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <button class="btn btn-primary btn-sm mt-3 pt-2" id="cal_total_earns_btn"
                        onclick="cal_total_earns()">Calculate Total Earnings
                    </button>
                    <label for="recipient-name" class="col-form-label">Total Earn :</label>
                </div>
                <div class="col-6">
                    <input readonly type="text" class="form-control" name="total_earn" id="total_earn">
                </div>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <strong>Household Expenses :</strong>
                </div>
                <div class="col-6">
                    {{-- <input type="number" class="form-control" name="household_expenses" --}}
                    {{-- id="household_expenses"> --}}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Foods</label>
                </div>
                <div class="col-6">
                    <input type="number" class="form-control" name="foods" id="foods">
                </div>
            </div>
        </div>
        <div class="form-group d-none">
            <div class="row">
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Hire purchasing :</label>
                </div>
                <div class="col-6">
                    <input type="number" class="form-control" name="hire_purchasing" id="hire_purchasing">
                </div>
            </div>
        </div>
        <div class="form-group ">
            <div class="row">
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Rent:</label>
                </div>
                <div class="col-6">
                    <input type="number" class="form-control" name="rent" id="rent" value="0">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Education:</label>
                </div>
                <div class="col-6">
                    <input type="number" class="form-control" name="education" id="education">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Health Care:</label>
                </div>
                <div class="col-6">
                    <input type="number" class="form-control" name="health" id="health">
                </div>
            </div>
        </div>
        <div class="form-group d-none">
            <div class="row">
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Accomadation :</label>
                </div>
                <div class="col-6">
                    <input type="number" class="form-control" name="accomadation" id="accomadation">
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Repayment :</label>
                </div>
                <div class="col-6">
                    <input type="number" class="form-control" name="repayment" id="repayment">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Tax :</label>
                </div>
                <div class="col-6">
                    <input type="number" class="form-control" name="tax" id="tax" value="0">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Leasing :</label>
                </div>
                <div class="col-6">
                    <input type="number" class="form-control" name="leasing" id="leasing" value="0">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Clothes :</label>
                </div>
                <div class="col-6">
                    <input type="number" class="form-control" name="clothes" id="clothes" value="0">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Electricity Bill :</label>
                </div>
                <div class="col-6">
                    <input type="number" class="form-control" name="electricity_bill" id="electricity_bill">
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="recipient-name" class="col-form-label">Water Bill :</label>
                    </div>
                    <div class="col-6">
                        <input type="number" class="form-control" name="water_bill" id="water_bill">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <label for="recipient-name" class="col-form-label">Other Expenses :</label>
                </div>
                <div class="col-6">
                    <input type="number" class="form-control" name="other_expenses" id="other_expenses">
                </div>
            </div>
        </div>



        <hr>
        <li>
            <a href="#otherloansdrop" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                Loans
            </a>

            <form id="loans_form">
                @csrf

                <ul class="collapse list-unstyled" id="otherloansdrop">
                    <li>
                        <div class="content ">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card btn-outline-default">
                                        <div class="card-header">
                                            <h5 class="title">Loans</h5>
                                        </div>
                                        <div class="card-body btn-outline-default">

                                            <div class="row">
                                                <div class="col-md-5 pr-1">
                                                    <div class="form-group">
                                                        <label>Bank Name</label>
                                                        <input type="text" class="form-control" id="bank_name">
                                                    </div>
                                                </div>
                                                <div class="col-md-3 px-1">
                                                    <div class="form-group">
                                                        <label>Loan Amount</label>
                                                        <input type="number" class="form-control" id="total_loan">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 pl-1">
                                                    <div class="form-group">
                                                        <label for="">Monthly Payment</label>
                                                        <input type="number" name="period" class="form-control"
                                                            id="monthly_payment">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 pl-1">
                                                    <div class="col-md-4 pl-1">
                                                        <div class="form-group">
                                                            <button type="submit" id="add_loan"
                                                                class="btn btn-primary">Add Expense
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <table class="table table-bordered" id="loansTable">
                                                    <tr>
                                                        <th>Bank Name</th>
                                                        <th>Loan Amount</th>
                                                        <th>Monthly Payment Price</th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </li>

                </ul>
            </form>
        </li>
        <br>
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <button class="btn btn-primary btn-sm mt-3 pt-2" id="cal_total_exp_btn"
                        onclick="cal_total_exp()">Calculate Total Expenses
                    </button>
                    <label for="recipient-name" class="col-form-label">Total Expenses :</label>
                </div>
                <div class="col-6">
                    <input readonly type="text" class="form-control" name="total_expenses" id="total_expenses">
                </div>
            </div>
            <input type="hidden" id="hiddenTotal" name="real_total">
        </div>
        <hr>
        <hr>

        <form action="{{route('appraisal.store')}}" method="POST" id="form">
            @csrf

            {{-- <input type="hidden" name="borrower_no" id="form_borrower_no"> --}}
            <input type="hidden" name="nic" id="form_nic">
            <input type="hidden" name="reqesting_loan_amount" id="form_req_loan">
            <input type="hidden" name="loan_purpose" id="form_loan_purpose">
            <input type="hidden" name="self_employee_total" id="form_self_employee_total">
            <input type="hidden" name="total_earn" id="form_total_earn">
            <input type="hidden" name="household_expenses" id="form_household_expenses">
            <input type="hidden" name="self_employee_expenses" id="form_self_employee_expenses">
            <input type="hidden" name="other_expenses" id="form_other_expenses">
            <input type="hidden" name="total_expenses" id="form_total_expenses">
            <input type="hidden" name="sum" id="form_sum">
            <input type="hidden" name="center" id="form_center">

            <input type="hidden" name="foods" id="form_foods">
            <input type="hidden" name="hire_purchasing" id="form_hire_purchasing">
            <input type="hidden" name="accomadation" id="form_accomadation">
            <input type="hidden" name="health" id="form_health">
            <input type="hidden" name="education" id="form_education">
            <input type="hidden" name="repayment" id="form_repayment">
            <input type="hidden" name="water_bill" id="form_water_bill">
            <input type="hidden" name="electricity_bill" id="form_electricity_bill">
            <input type="hidden" name="rent" id="form_rent">
            <input type="hidden" name="tax" id="form_tax">
            <input type="hidden" name="leasing" id="form_leasing">
            <input type="hidden" name="clothes" id="form_clothes">

            <div class="form-group">
                <div class="row">
                    <div class="col">
                        {{-- <label for="budget_ratio"> Total Earns / Total Expenses</label> --}}
                    </div>
                    <div class="col">
                        <input type="hidden" class="form-control" name="budget_ratio" id="form_budget_ratio" readonly
                            value="0">
                        <input type="hidden" class="form-control" name="budget_ratio_self_employee"
                            id="form_budget_ratio_self_employee" readonly value="0">
                        <input type="hidden" class="form-control" name="budget_ratio_with_participation"
                            id="form_budget_ratio_with_participation" readonly value="0">
                        <input type="hidden" class="form-control" name="budget_ratio_with_salary"
                            id="form_budget_ratio_with_salary" readonly value="0">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="sub_btn">Submit Appraisal</button>
            </div>
        </form>

        </form>
    </div>

</div>

</div>
</div>
<input type="hidden" id="hidden_realy_income">
<input type="hidden" id="hidden_realy_salary">
<input type="hidden" id="hidden_loans_income">

<input type="hidden" id="hiddenEx">
<input type="hidden" id="hiddenEr">

</div>
</div>

@section('scripts')

<script type="text/javascript">
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        document.querySelector('#sub_btn').addEventListener('click', e => {
            e.preventDefault()
            console.log(document.querySelector('#form_budget_ratio').value);

            if( !document.querySelector('#form_nic').value || document.querySelector('#form_budget_ratio').value === 'NaN' ){
                return  Swal.fire({
                title: 'Error!',
                text: 'Please fill all fields',
                icon: 'error',
                confirmButtonText: 'Okay'
                })
            }

            if(cal_total_exp_btn.disabled == false || cal_total_earns_btn.disabled == false){
                return  Swal.fire({
                title: 'Error!',
                text: 'Please calculate earnings and expenses',
                icon: 'error',
                confirmButtonText: 'Okay'
                })
            }

            document.querySelector('#form').submit()

        })

        const branch = document.querySelector('#branch')
        const center = document.querySelector('#center')
        const center_name = document.querySelector('#center_name')
        const group = document.querySelector('#group')
        const borrowersCount = document.querySelector('#borrowersCount')
        // const borrower_no = document.querySelector('#borrower_no')

        // center.addEventListener('click', (e) => {
        //     center_name.value = e.target.options[e.target.selectedIndex].text
        //     console.log(e.target.options[e.target.selectedIndex].text)
        //     group.click()
        // })

        function get_borrowers_count() {
            $.ajax({
                type: 'GET',
                url: '{{('/getborrowerscount')}}',
                data: {
                    'branch': branch.value,
                    'center': center_name.value,
                    'group': group.value,
                },
                success: function(data){
                    console.log(data)

                    borrowersCount.value = parseInt(data.length) + 1
                    console.log(borrowersCount.value)
                    return genBorId()
                }
            })
        }

        function genBorId(){
            let branchString = `${branch.value}`
            branchString = branchString.slice(0, 3);
            branchString = branchString.toUpperCase();
            // console.log(branchString);

            let centerString = `00${center.value}`;
            // let centerString = this.centerString;
            centerString = centerString.slice(centerString.length - 3, centerString.length);

            let borrowerString = `00${borrowersCount.value}`;
            borrowerString = borrowerString.slice(borrowerString.length - 3, borrowerString.length);

            let groupString = `00${group.value}`;
            groupString = groupString.slice(groupString.length - 3, groupString.length);

            let id = `${branchString}${centerString}${groupString}${borrowerString}`
            // console.log(id);
            borrower_no.value = id;
        }

        const check_nic_btn = document.querySelector('#check_nic_btn')
        const borrower_nic = document.querySelector('#borrower_nic')
        const after_nic = document.querySelector('#after_nic')
        const nic_message = document.querySelector('#nic_message')
        const del_prev_app_btn = document.querySelector('#del_prev_app_btn')


        function check_nic(){
            if(after_nic.classList.contains('d-none')){
                after_nic.classList.remove('d-none')
                console.log('re')
            }

            if (borrower_nic.value.length !== 10 && borrower_nic.value.length !== 12) {

                console.log(borrower_nic.value.length)
                console.log('invalid nic')
                // if(nic_message.classList.contains('btn-secondary')){
                //     nic_message.classList.remove('btn-secondary')

                // } else {
                //     nic_message.classList.remove('btn-success')
                // }
                // nic_message.classList.add('btn-danger')
                nic_message.textContent = 'Invalid NIC | Check NIC again'
                nic_message.classList.remove('d-none')
                nic_message.classList.add('bg-danger')
                after_nic.classList.add('d-none')


            } else {
                after_nic.classList.remove('d-none')
                del_prev_app_btn.classList.add('d-none')

                $.ajax({
                    type: 'POST',
                    url: '{{('/getnicappraisals')}}',
                    data: {'nic': borrower_nic.value},
                    success: function(data){
                        console.log(data)
                        if(data == 'no borrower'){
                            if(nic_message.classList.contains('bg-danger')){
                                nic_message.classList.remove('bg-danger')
                            } else {
                                nic_message.classList.remove('bg-success')
                            }
                            nic_message.classList.remove('d-none')
                            nic_message.classList.add('bg-success')
                            nic_message.textContent = 'Creating Appraisal for a new Borrower'


                        } else {
                            if(nic_message.classList.contains('bg-danger')){
                                nic_message.classList.remove('bg-danger')
                            }else {
                                nic_message.classList.remove('bg-success')
                            }
                            nic_message.classList.remove('d-none')
                            nic_message.classList.add('bg-danger')
                            nic_message.textContent = 'Existing NIC | Delete previous appraisal records to continue '
                            after_nic.classList.add('d-none')
                            del_prev_app_btn.classList.remove('d-none')
                        }
                    }
                })
            }
        }

        function del_prev_app(){
            $.ajax({
                type: 'GET',
                url: '{{('/delprevapp')}}',
                data: {'nic': borrower_nic.value},
                success: function(data){
                    console.log(data)

                    after_nic.classList.remove('d-none')
                    nic_message.classList.add('d-none')
                    del_prev_app_btn.classList.add('d-none')

                }
            })
        }


        const ings = document.querySelector('#ings')
        const expenseType = document.querySelector('#expenseType')

        $(document).ready(function () {


            $(productType).click(function () {
                var product = $(this).val();
                console.log(product);

                $.ajax({
                    type: 'GET',
                    url: '{{url('/createapprasialpage/products')}}',
                    data: {'id': product},
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data);
                        // $('#EarningsUnitPrice').val(data)
                        $('#min_earn').val(data.min)
                        $('#max_earn').val(data.max)
                    }

                });
            });

            $('#expenseType').click(function () {
                let expense = $(this).val();
                console.log(expense);

                $.ajax({
                    type: 'GET',
                    // url: '{{url('/createapprasialpage/expenses')}}',
                    url: '{{('/getings')}}',
                    data: {'product': expense},
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data, 'got ings')
                        // $('#buyingPrice').val(data[0]);
                        // $('#ings').val(data[1]);

                        // $('#buyingPrice').val(data.buying_price)
                        document.querySelector('#ings_section').innerHTML = ``

                        data.forEach(record => {
                            html =
                                `
                                <div class="row" id="ings_row">
                                                            <div class="col-2 px-1">
                                                                <div class="form-group">
                                                                    <input name="ings" id="ings_${record.id}"
                                                                            class="form-control ings" readonly value="${record.ingredient}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2 pl-1">
                                                                <div class="form-group">
                                                                    <input type="number" id="buyingPrice_${record.id}"
                                                                           class="form-control buyingPrice" >
                                                                </div>
                                                            </div>
                                                            <div class="col-2 pl-1">
                                                                <div class="form-group">
                                                                    <input type="number" id="expensePeriod_${record.id}"
                                                                           name="period" class="form-control expensePeriod">
                                                                </div>
                                                            </div>
                                                            <div class="col-2 pl-1">
                                                                <div class="form-group">
                                                                    <input type="number"
                                                                           name="total_units" id="totalExpenseUnits_${record.id}"
                                                                           class="form-control" class="totalExpenseUnits">
                                                                </div>
                                                            </div>
                                                            <div class="col-2 pl-1">
                                                                <div class="form-group">
                                                                    <input type="email"
                                                                           name="expenses" id="expenses_${record.id}"
                                                                           class="form-control expenses" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-2 pl-1 ">
                                                                <div class="form-group">
                                                                    <button  id="addExpenseXXX_${record.id}"
                                                                            class="btn btn-primary addExpense_btn ">Add Expense
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                        `

                            // $('#ings').append(html(html)) <<<<<----- not working
                            // ings.innerHTML += html
                            document.querySelector('#ings_section').innerHTML += html
                        })

                    }
                })

            })
        })

        const borrowerNo = document.querySelector('#borrower_no');
        const req_loan = document.querySelector('#req_loan');
        const loan_purpose = document.querySelector('#loan_purpose');
        const erTable = document.querySelector('#erTable');
        const earnType = document.querySelector('#productType');
        const earningsUnitPrice = document.querySelector('#EarningsUnitPrice');
        const earningsTotalUnits = document.querySelector('#EarningsTotalUnits');
        const EarningsPeriod = document.querySelector('#EarningsPeriod');
        const earns = document.querySelector('#earns');

        // relationship incomes
        const income_relationship = document.querySelector('#income_relationship')
        const income = document.querySelector('#income')
        const job_type = document.querySelector('#job_type')
        const salary = document.querySelector('#salary')

        // household expenses
        const foods = document.querySelector('#foods')
        const hire_purchasing = document.querySelector('#hire_purchasing')
        const accomadation = document.querySelector('#accomadation')
        const health = document.querySelector('#health')
        const education = document.querySelector('#education')
        const repayment = document.querySelector('#repayment')
        const water_bill = document.querySelector('#water_bill')
        const electricity_bill = document.querySelector('#electricity_bill')

        const exTable = document.querySelector('#exTable');
        const productName = document.querySelector('#expenseType');
        // const ingredients = document.querySelector('#ings');
        const buyingPrice = document.querySelector('#buyingPrice')
        const totalExpenseUnits = document.querySelector('#totalExpenseUnits')
        const expensePeriod = document.querySelector('#expensePeriod')
        const expenses = document.querySelector('#expenses')

        const total_expenses = document.querySelector('#total_expenses')
        // const household_expenses = document.querySelector('#household_expenses')
        const other_expenses = document.querySelector('#other_expenses')
        // total_expenses.value = parseInt(0);

        const hiddenEx = document.querySelector('#hiddenEx');
        hiddenEx.value = 0;
        // household_expenses.value = 0;
        other_expenses.value = 0;

        const total_earn = document.querySelector('#total_earn')
        const hiddenEr = document.querySelector('#hiddenEr')
        const hidden_realy_income = document.querySelector('#hidden_realy_income')
        const hidden_realy_salary = document.querySelector('#hidden_realy_salary')
        hidden_realy_income.value = 0;
        hidden_realy_salary.value = 0;
        hiddenEr.value = 0

        const hiddenTotal = document.querySelector('#hiddenTotal')
        hiddenTotal.value = 0

        // form
        // const form_borrower_no = document.querySelector('#form_borrower_no')
        const form_nic = document.querySelector('#form_nic')
        const form_req_loan = document.querySelector('#form_req_loan')
        const form_loan_purpose = document.querySelector('#form_loan_purpose')
        const form_self_employee_total = document.querySelector('#form_self_employee_total')
        const form_total_earn = document.querySelector('#form_total_earn')
        const form_household_expenses = document.querySelector('#form_household_expenses')
        const form_foods = document.querySelector('#form_foods')
        const form_hire_purchasing = document.querySelector('#form_hire_purchasing')
        const form_accomadation = document.querySelector('#form_accomadation')
        const form_health = document.querySelector('#form_health')
        const form_education = document.querySelector('#form_education')
        const form_repayment = document.querySelector('#form_repayment')
        const form_water_bill = document.querySelector('#form_water_bill')
        const form_electricity_bill = document.querySelector('#form_electricity_bill')
        const form_self_employee_expenses = document.querySelector('#form_self_employee_expenses')
        const form_other_expenses = document.querySelector('#form_other_expenses')
        const form_total_expenses = document.querySelector('#form_total_expenses')
        const form_sum = document.querySelector('#form_sum')
        const form_budget_ratio = document.querySelector('#form_budget_ratio')
        const form_center = document.querySelector('#form_center')
        const form_budget_ratio_self_employee = document.querySelector('#form_budget_ratio_self_employee')
        const form_budget_ratio_with_participation = document.querySelector('#form_budget_ratio_with_participation')
        const form_budget_ratio_with_salary = document.querySelector('#form_budget_ratio_with_salary')

        form_budget_ratio.value = 0;
        hidden_loans_income.value = 0;


        setInterval(() => {
            calEarn()
        }, 1);

        function calEarn() {
            earns.value = earningsUnitPrice.value * earningsTotalUnits.value * EarningsPeriod.value;
        }

        // cal household expenses
        foods.value = 0
        hire_purchasing.value = 0
        accomadation.value = 0
        health.value = 0
        education.value = 0
        repayment.value = 0
        water_bill.value = 0
        electricity_bill.value = 0
        setInterval(() => {
            form_household_expenses.value = parseInt(foods.value) + parseInt(hire_purchasing.value) + parseInt(accomadation.value) + parseInt(health.value) + parseInt(education.value) + parseInt(repayment.value) + parseInt(water_bill.value) + parseInt(electricity_bill.value) + parseInt(rent.value) + parseInt(tax.value) + parseInt(leasing.value) + parseInt(clothes.value)
        }, 1);

        // cal product expenses
        // setInterval(() => {
        //     expenses.value = buyingPrice.value * totalExpenseUnits.value * expensePeriod.value;
        // }, 1);

        // //cal total expenses
        const cal_total_exp_btn = document.querySelector('#cal_total_exp_btn')
        function cal_total_exp(){
            total_expenses.value = parseInt(form_household_expenses.value) + parseInt(other_expenses.value) + parseInt(hiddenEx.value) + parseInt(hidden_loans_income.value)
            cal_total_exp_btn.disabled = true
            Array.from(document.querySelectorAll('.deleteExpense')).forEach(btn => {
                btn.remove()
            })
            Array.from(document.querySelectorAll('.deleteLoan')).forEach(btn => {
                btn.remove()
            })
        }

        // setInterval(() => {
        //     total_expenses.value = parseInt(form_household_expenses.value) + parseInt(other_expenses.value) + parseInt(hiddenEx.value) + parseInt(hidden_loans_income.value);
        // }, 1);

        // cal total earnings
        const cal_total_earns_btn = document.querySelector('#cal_total_earns_btn')
        function cal_total_earns(){
            total_earn.value = parseInt(hiddenEr.value)+ parseInt(hidden_realy_salary.value)
            cal_total_earns_btn.disabled = true
            Array.from(document.querySelectorAll('.deleteRelayIncome')).forEach(btn => {
                btn.remove()
            })
            Array.from(document.querySelectorAll('.deleteEarning')).forEach(btn => {
                btn.remove()
            })
        }
        // setInterval(() => {
        //     total_earn.value = parseInt(hiddenEr.value)
        //         + parseInt(hidden_realy_income.value)
        // }, 1);

        // cal real total
        setInterval(() => {
            // hiddenTotal.value =  parseInt(hiddenEr.value) -  parseInt(hiddenEx.value)
            hiddenTotal.value = total_earn.value - total_expenses.value

            form_budget_ratio.value = total_earn.value / total_expenses.value

            form_budget_ratio_self_employee.value = hiddenEr.value/(hiddenEx.value+form_household_expenses.value+hidden_loans_income.value)

            form_budget_ratio_with_participation.value =  (hiddenEr.value+hidden_realy_income.value)/(hiddenEx.value
            +form_household_expenses.value+hidden_loans_income.value)

            form_budget_ratio_with_salary.value =  (hiddenEr.value+hidden_realy_salary.value)/(hiddenEx.value+form_household_expenses.value+hidden_loans_income.value)
        }, 1);

        // set form
        setInterval(() => {
            // form_borrower_no.value = borrowerNo.value
            form_nic.value = borrower_nic.value
            form_req_loan.value = req_loan.value
            form_loan_purpose.value = loan_purpose.value
            form_self_employee_total.value = hiddenEr.value
            form_total_earn.value = total_earn.value
            // form_household_expenses.value = req_loan.value
            form_foods.value = foods.value
            form_hire_purchasing.value = hire_purchasing.value
            form_accomadation.value = accomadation.value
            form_health.value = health.value
            form_education.value = education.value
            form_repayment.value = repayment.value
            form_water_bill.value = water_bill.value
            form_electricity_bill.value = electricity_bill.value
            form_self_employee_expenses.value = hiddenEx.value
            form_other_expenses.value = other_expenses.value
            form_total_expenses.value = total_expenses.value
            form_sum.value = hiddenTotal.value
            form_center.value = center.value
            form_rent.value = rent.value
            form_tax.value = tax.value
            form_leasing.value = leasing.value
            form_clothes.value = clothes.value
        }, 1);


        // earnings
        document.querySelector('#addEarning').addEventListener('click', e => {
            e.preventDefault()
            document.querySelector('#range_error_earn').classList.add('d-none')

            $.ajax({
                type: 'POST',
                url: '{{url('/createapprasialpage/expenses/addearning')}}',
                data: {
                    'nic': borrower_nic.value,
                    'product_name': earnType.value,
                    'period': EarningsPeriod.value,
                    'total_units': earningsTotalUnits.value,
                    'earnings': earns.value,
                    'unit_price': earningsUnitPrice.value
                },
                dataType: 'JSON',
                success: function (data) {
                    // console.log(data, 'optimus prime')

                    if(data == 'not_in_range'){
                        return document.querySelector('#range_error_earn').classList.remove('d-none')
                    }
                }
            })
                .then(function () {
                    return get_earnings_module()
                })
        })

        // --------- GET EARNINGS - AJAX MODULE----------
        function get_earnings_module(){
            $.ajax({
                        type: 'GET',
                        url: '{{('/createapprasialpage/expenses/getearning')}}',
                        data: {'nic': borrower_nic.value,},
                        success: function (data) {
                            console.log(data, 'hasta la vista')

                            erTable.innerHTML = `
                    <tr>
                        <th>Product</th>
                        <th>Period</th>
                        <th>Unit Price</th>
                        <th>Total Units</th>
                        <th>Earn</th>
                        <th></th>
                    </tr>
                    `

                            // total_earn.value = 0;
                            hiddenEr.value = 0;
                            data.forEach(record => {
                                console.log(record.product_name)
                                hiddenEr.value = parseInt(record.earnings) + parseInt(hiddenEr.value);

                                let html = `
                        <tr>
                            <td > ${record.product_name}  </td>
                            <td > ${record.period}  </td>
                            <td > ${record.unit_price}  </td>
                            <td > ${record.total_units}  </td>
                            <td > ${record.earnings}  </td>
                            <td > <a  id="deleteEarningXXX_${record.id}"
                                        class="btn btn-danger deleteEarning" onclick="delete_earning(this.id)">Remove Earning
                                    </a>
                            </td>

                        </tr>
                        `
                                erTable.innerHTML += html
                                document.querySelector('#self_emp_earns_form').reset()
                            })
                        }
                    })
        }

        //  ------------------------------


        //  ############ DELETE EARNINGS ----------------------

            function delete_earning(id){
                id = id.slice(id.indexOf('_')+1, id.length)
                // console.log(id);

                $.ajax({
                    type: 'POST',
                    url: '{{url('/createapprasialpage/earnings/delearning')}}',
                    data: {
                        id
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data)
                        return get_earnings_module()
                    }
                })
            }

        // ---------------------------------------------------

        //// expenses updated
        expenseType.addEventListener('click', e => {
            setTimeout(() => {
                const add_exp_btns = document.querySelectorAll('.addExpense_btn')
                // console.log(add_exp_btns)
                const arr = Array.from(add_exp_btns)

                console.log(arr)
                return get_exp_btns_array(arr)
            },1000)

        })

        function get_exp_btns_array(arr){
            arr.forEach(btn => {
                btn.addEventListener('click', e => {
                    e.preventDefault()
                    // console.log('fuck')
                    // btn.disabled = true
                    // btn.textContent = 'Expense Added'
                    document.querySelector('#range_error').classList.add('d-none')

                    let row_id = e.target.id
                    row_id = row_id.slice(row_id.length-1 , row_id.length)
                    // console.log(row_id)

                    return set_and_get_expenses(row_id, btn)
                })
            })
        }

        function set_and_get_expenses(row_id, btn){
            // console.log(row_id)

            const ing = document.querySelector(`#ings_${row_id}`)
            const expenses = document.querySelector(`#expenses_${row_id}`)
            const buyingPrice = document.querySelector(`#buyingPrice_${row_id}`)
            const expensePeriod = document.querySelector(`#expensePeriod_${row_id}`)
            const totalExpenseUnits = document.querySelector(`#totalExpenseUnits_${row_id}`)

            expenses.value = buyingPrice.value * expensePeriod.value * totalExpenseUnits.value

            $.ajax({
                type: 'POST',
                url: '{{url('/createapprasialpage/expenses/addexpense')}}',
                data: {
                    'nic': borrower_nic.value,
                    'ing' : ing.value,
                    'buyingPrice': buyingPrice.value,
                    'total_units': totalExpenseUnits.value,
                    'period': expensePeriod.value,
                    'expenses': expenses.value,
                    'product_name': productName.value,
                },
                dataType: 'JSON',
                success: function (data) {
                    console.log(data, '123bumblebee')

                    if(data == 'not_in_range'){
                        return document.querySelector('#range_error').classList.remove('d-none')
                        // return console.log('hgjg');
                    } else{
                        btn.disabled = true
                        btn.textContent = 'Expense Added'
                    }
                }
            })
            .then(function () {
                    return get_expenses_module()
                })
        }

        // -------------------- GET EXPENSES MODULE -------------
        function get_expenses_module() {
            $.ajax({
                        type: 'GET',
                        url: '{{('/createapprasialpage/expenses/getexpense')}}',
                        data: {'nic': borrower_nic.value},
                        success: function (data) {
                            console.log(data)
                            exTable.innerHTML = `
                    <tr>
                        <th>Product</th>
                        {{-- <th>Ingredients</th> --}}
                            <th>Period</th>
                            <th>Buying Price</th>
                            <th>Total Units</th>
                            <th>Expenses</th>
                            <th></th>
                        </tr>
`
                            // household_expenses.value =  0;
                            // other_expenses.value= 0;
                            // total_expenses.value = parseInt(household_expenses.value)+parseInt(other_expenses.value);
                            hiddenEx.value = 0;
                            data.forEach(record => {
                                console.log(record.product_name)
                                // total_expenses.value = parseInt(record.expenses) + parseInt(total_expenses.value);
                                hiddenEx.value = parseInt(record.expenses) + parseInt(hiddenEx.value);
                                // recordExpenses = parseInt(record.expenses) + parseInt(recordExpenses)

                                let html = `
                        <tr>
                            <td > ${record.product_name}  </td>
                            <td > ${record.period}  </td>
                            <td > ${record.buying_price}  </td>
                            <td > ${record.total_units}  </td>
                            <td > ${record.expenses}  </td>
                            <td > <a  id="deleteExpenseXXX_${record.id}"
                                        class="btn btn-danger deleteExpense" onclick="delete_expense(this.id)">Remove Expenses
                                    </a>
                            </td>
                        </tr>
                        `
                                exTable.innerHTML += html

                                // document.querySelector('#self_emp_exp_form').reset()
                            })
                        }
                    })
        }

        //  -----------------------------------------------------

        //  ############ DELETE EXPENSES ----------------------

        function delete_expense(id){
                id = id.slice(id.indexOf('_')+1, id.length)
                // console.log(id);

                $.ajax({
                    type: 'POST',
                    url: '{{url('/createapprasialpage/expenses/delexpense')}}',
                    data: {
                        id
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data)
                        return get_expenses_module()
                    }
                })
            }

        // ---------------------------------------------------

        document.querySelector('#add_rela_income_btn').addEventListener('click', (e) => {
            e.preventDefault()
            $.ajax({
                type: 'POST',
                url: '{{('/addmainincome')}}',
                data: {
                    'nic': borrower_nic.value,
                    'income_relationship': income_relationship.value,
                    'income': income.value,
                    'job_type': job_type.value,
                    'salary': salary.value
                },
                dataType: 'JSON',
                success: function (data) {
                    console.log(data, 'income added')
                }
            })
                .then(function () {
                    return get_relay_incomes_module()
                })
        })

        // ----------------- GET RELATIONSHIP INCOMES MODULE ----------------
        function get_relay_incomes_module(){
            $.ajax({
                        type: 'GET',
                        url: '{{('/getmainincome')}}',
                        data: {'nic': borrower_nic.value},
                        success: function (data) {
                            console.log(data)
                            main_income_table.innerHTML = `
                    <tr>
                        <th>Income Relationship</th>
                        <th>Job Type</th>
                        <th>Income</th>
                        <th></th>
                    </tr>
                    `
                            hidden_realy_income.value = 0
                            hidden_realy_salary.value = 0
                            data.forEach(record => {
                                hidden_realy_income.value = parseInt(hidden_realy_income.value) + parseInt(record.income)
                                hidden_realy_salary.value = parseInt(hidden_realy_salary.value) + parseInt(record.salary)
                                let html = `
                        <td > ${record.income_relationship}  </td>
                        <td > ${record.job_type}  </td>
                        <td > ${record.salary}  </td>
                        <td > <a  id="deleteRelayIncomeXXX_${record.id}"
                                        class="btn btn-danger deleteRelayIncome" onclick="delete_relay_income(this.id)">Remove Income
                                    </a>
                        </td>
                        `

                                main_income_table.innerHTML += html
                                document.querySelector('#relay_form').reset()
                            })
                        }
                    })
        }
        // ----------------------------------------------------------

         //  ############ DELETE RELATIONSHIP INCOMES    ----------------------

        function delete_relay_income(id){
                id = id.slice(id.indexOf('_')+1, id.length)
                // console.log(id);

                $.ajax({
                    type: 'POST',
                    url: '{{url('/createapprasialpage/earnings/delrelayincome')}}',
                    data: {
                        id
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data)
                        return get_relay_incomes_module()
                    }
                })
            }

        // ---------------------------------------------------

        const bank_name = document.querySelector('#bank_name')
        const total_loan = document.querySelector('#total_loan')
        const monthly_payment = document.querySelector('#monthly_payment')
        const loansTable = document.querySelector('#loansTable')

        document.querySelector('#add_loan').addEventListener('click', e => {
            e.preventDefault()

            $.ajax({
                type: 'POST',
                url: '{{('/appraisalloans/addloan')}}',
                data: {
                    'borrower_id': borrower_nic.value,
                    'bank_name': bank_name.value,
                    'total_loan': total_loan.value,
                    'monthly_payment': monthly_payment.value,
                },
                dataType: 'JSON',
                success: function (data) {
                    console.log(data, 'hakuna matata')
                },
                error: function (err) {
                    console.log(err)
                }
            })
                .then(function () {
                    return get_loans_module()

                })
        })

        // -------------------GET OTHER LOANS MODULE-----------------------------

        function get_loans_module(){
            $.ajax({
                        type: 'GET',
                        url: '{{('/appraisalloans/getloan')}}',
                        data: {'borrower_id': borrower_nic.value},
                        success: function (data) {
                            console.log(data)
                            loansTable.innerHTML = `
                    <tr>
                        <th>Bank Name</th>
                        <th>Loan Amount</th>
                        <th>Monthly Payment Price</th>
                        <th></th>
                    </tr>
                    `
                            hidden_loans_income.value = 0
                            data.forEach(record => {
                                hidden_loans_income.value = parseInt(hidden_loans_income.value) + parseInt(record.monthly_paymentt)
                                let html = `
                        <td > ${record.bank_name}  </td>
                        <td > ${record.total_loan}  </td>
                        <td > ${record.monthly_paymentt}  </td>
                        <td > <a  id="deleteLoanXXX_${record.id}"
                                        class="btn btn-danger deleteLoan" onclick="delete_loan(this.id)">Remove Income
                                    </a>
                        </td>
                        `

                                loansTable.innerHTML += html
                                document.querySelector('#loans_form').reset()
                            })
                        }
                    })
        }
        // -------------------------------------------

        // ---------------------------- DELETE OTHER LOANS ---------------
        function delete_loan(id){
                id = id.slice(id.indexOf('_')+1, id.length)
                // console.log(id);

                $.ajax({
                    type: 'POST',
                    url: '{{url('/createapprasialpage/expenses/delloans')}}',
                    data: {
                        id
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data)
                        return get_loans_module()
                    }
                })
            }
        // -------------------------------------------------

</script>
@endsection


@endsection
