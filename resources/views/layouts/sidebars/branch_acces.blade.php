<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>BRANCH</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        {{-- <li>
            <a href="/branch/create">ADD branch</a>
        </li> --}}
        <li>
            <a href="/branch">Add Branch</a>
        </li>
        <li>
            <a href="#">EDIT Branch</a>
        </li>
    </ul>
</li>

<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>CENTER</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        {{-- <li>
            <a href="/center/create">ADD center</a>
        </li> --}}
        <li>
            <a href="/centeri">Add center</a>
        </li>
        <li>
            <a href="/centerleader">Center Leaders</a>
        </li>
    </ul>

</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>BORROWER</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        {{-- <li>
            <a href="/borrower">ADD borrower</a>
        </li> --}}
        <li>
            <a href="/borrower">Add borrower</a>
        </li>
        <li>
            <a href="/borrowers">View Borrowers</a>
        </li>

    </ul>
</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>APPRAISAL</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        <li>
            <a href="/appraisal">Appraisal</a>
        </li>
        <li>
            <a href="/appraisalall">All</a>
        </li>
        <li>
            <a href="/appraisalearning">Appraisal Earings</a>
        </li>
        <li>
        <li>
            <a href="/appraisalproducts">Appraisal Expenses Products</a>
        </li>
        <li>
            <a href="/appraisalingredients">Add Expenses Ingredients</a>
        </li>
        <li class="{{''== request()->path() ?'active':''}}">
            <a href="https://cantylivelocations.firebaseapp.com/upload" target="_blank">
            Location
            </a>
        </li>

    </ul>
</li>
<li class="menu-item-has-children dropdown ">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>LOAN</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        <li>
            <a href="/loan">View All Loans</a>
        </li>
        <li>
            <a href="/loan/create">Add Loan</a>
        </li>
        <li>
            <a href="/verify">Loan Verification</a>
        </li>
        {{-- <li> --}}
            {{-- <a href="/approving">Pending Loans</a> --}}
        {{-- </li> --}}
        <li>
            <a href="/approved">Approved Loans</a>
        </li>
        <li>
            <a href="/issued">Issued Loans</a>
        </li>
        <li>
            <a href="/issuedcenter">Issued Loans | Center</a>
        </li>
        <li>
            <a href="/category">Loan Categories</a>
        </li>

    </ul>
</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>PAYMENT</a>
    <ul class="sub-menu children dropdown-menu">


        <li>
            <a href="{{ route('guarantor.index') }}">Guarantors</a>
        </li>

        <li>
            <a href="{{ route('witness.index') }}">Witnesses</a>
        </li>

     </ul>
</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>REPAYMENTS</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        <li>
            <a href="{{route('repayment.index')}}">Center Collection</a>
        </li>
        <li>
            <a href="/repayments/officecollection">Office Collection</a>
        </li>
        <li>
            <a href="/repayments/bulkcollection">Bulk Collection</a>
        </li>
        <li>
            <a href="{{route('repayment_date.index')}}">Repayments For Selected</a>
        </li>
        <li>
            <a href="{{route('repayment_any.index')}}">Old Repayments</a>
        </li>
        <li>
            <a href="#">Upload Repayments - CSV </a>
        </li>
        <li>
            <a href="/repaymentdoc">Repayments Doc</a>
        </li>
        <li>
            <a href="#">Approve Repayments</a>
        </li>
        <li>
            <a href="/repayments/edit">Edit Repayments</a>
        </li>
    </ul>
</li>
{{-- <li class="{{''== request()->path() ?'active':''}}">

<a href="">
    Collateral Register
</a>
</li> --}}
    <li class="menu-item-has-children dropdown">
    <a href="https://cantylivelocations.firebaseapp.com/upload" target="_blank">
        Location
    </a>
</li>
<li class="menu-item-has-children dropdown animated fadeIn">
    <a href="">
        Calendar
    </a>
</li>
<li class="menu-item-has-children dropdown animated fadeIn">
    <a href="/postalcode/create">
        Add Postal Code
    </a>
</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>STAFF</a>
    <ul class="sub-menu children dropdown-menu" id="x">
        <li>
            <a href="{{ route('staff.index') }}"> Staff</a>
        </li>
        <li>
            <a href="{{ route('staff.create') }}">Add Staff</a>
        </li>

    </ul>
</li>




<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>EXPENXES</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        <li>
            <a href="{{ route('company_ex.index')}}">Add Expence</a>
        </li>
        <li>
            <a href="{{ route('ongoing_check.index')}}">Change Ongoing Check</a>
        </li>
        <li>
            <a href="{{ route('compant_ex_cat.index')}}">Add Expences Category</a>
        </li>
    </ul>
</li>


