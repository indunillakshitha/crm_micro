<li class="menu-item-has-children dropdown animated fadeIn">
    <a href="/branches/change">
        Change Branch
    </a>

</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
            class="menu-icon fa fa-laptop"></i>BRANCH</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        {{-- <li>
            <a href="/branch/create">ADD branch</a>
        </li> --}}
        <li>
            <a href="/branch">Add Branch</a>
        </li>
        <li>
            <a href="#">EDIT Branch</a>
        </li>
    </ul>
</li>

<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
            class="menu-icon fa fa-laptop"></i>CENTER</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        {{-- <li>
            <a href="/center/create">ADD center</a>
        </li> --}}
        <li>
            <a href="/centeri">Add center</a>
        </li>
        <li>
            <a href="/centerleader">Center Leaders</a>
        </li>
        <li>
            <a href={{route('shedule.index')}}>Center Shedule</a>
        </li>
    </ul>

</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
            class="menu-icon fa fa-laptop"></i>BORROWER</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        {{-- <li>
            <a href="/borrower">ADD borrower</a>
        </li> --}}
        <li>
            <a href="/borrower">Add borrower</a>
        </li>
        <li>
            <a href="/borrowers">View Borrowers</a>
        </li>
        <li>
            <a href="{{ route('groupchange.index') }}">Group Change</a>
        </li>

    </ul>
</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
            class="menu-icon fa fa-laptop"></i>APPRAISAL</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        <li>
            <a href="/appraisal">Appraisal</a>
        </li>
        <li>
            <a href="/appraisalall">All</a>
        </li>
        <li>
            <a href="/appraisalearning">Appraisal Earings</a>
        </li>
        <li>
        <li>
            <a href="/appraisalproducts">Appraisal Expenses Products</a>
        </li>
        <li>
            <a href="/appraisalingredients">Add Expenses Ingredients</a>
        </li>
        <li class="{{''== request()->path() ?'active':''}}">
            <a href="https://cantylivelocations.firebaseapp.com/upload" target="_blank">
                Location
            </a>
        </li>

    </ul>
</li>
<li class="menu-item-has-children dropdown ">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
            class="menu-icon fa fa-laptop"></i>LOAN</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        <li>
            <a href="summery">Loans Summery </a>
        </li>
        <li>
            <a href="/loan">View All Loans</a>
        </li>
        <li>
            <a href="/loan/create">Add Loan</a>
        </li>
        <li>
            <a href="/verify">Loan Verification</a>
        </li>
        <li>
            <a href="/approving">Pending Loans</a>
        </li>
        <li>
            <a href="/approved">Approved Loans</a>
        </li>
        <li>
            <a href="/issued">Issued Loans</a>
        </li>
        <li>
            <a href="/issuedcenter">Issued Loans | Center</a>
        </li>
        <li>
            <a href="/category">Loan Categories</a>
        </li>

    </ul>
</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
            class="menu-icon fa fa-laptop"></i>PAYMENT</a>
    <ul class="sub-menu children dropdown-menu">
        <li>
            <a href="/arrears">Loans in Arrears</a>
        </li>
        <li>
            <a href="/norepayments">No Repayments</a>
        </li>
        <li>
            <a href="/delimnation">Delimnation</a>
        </li>
        <li>
            <a href="#">Principal Outstanding</a>
        </li>
        <li>
            <a href="/onemonth">1 Month Late Loans</a>
        </li>
        <li>
            <a href="/threemonth">3 Months Late Loans</a>
        </li>
        <li>
            <a href="#">Loan Calculator</a>
        </li>
        <li>
            <a href="{{ route('guarantor.index') }}">Guarantors</a>
        </li>
        <li>
            <a href="#">Loan Comments</a>
        </li>
        <li>
            <a href="#">Due Loans</a>
        </li>
        <li>
            <a href="{{ route('witness.index') }}">Witnesses</a>
        </li>

    </ul>
</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
            class="menu-icon fa fa-laptop"></i><span class="badge badge-light" id="np_val"></span>REPAYMENTS </a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        <li>
            <a href="/newrepayments">Collect from Center</a>
        </li>
        <li>
            <a href="/newnotpaids">Not Paid Collection</a>
        </li>
        {{-- <li>
            <a href="/repayments/officecollection">Office Collection</a>
        </li> --}}
        {{-- <li>
            <a href="/repayments/bulkcollection">Bulk Collection</a>
        </li> --}}
        {{-- <li>
            <a href="{{route('repayment_date.index')}}">Repayments For Selected</a>
</li> --}}
{{-- <li>
            <a href="{{route('repayments_centers.index')}}">Repayments Of Center</a>
</li> --}}
{{-- <li>
            <a href="{{route('repayment_any.index')}}">Old Repayments</a>
</li> --}}
<li>
    <a href="/delimnation">Denomination</a>
</li>
<li>
    <a href="/deposites">Denomination Deposites</a>
</li>
<li>
    <a href="/denominatiionprint">Denomination Print</a>
</li>
<li>
    <a href="/npreport">Not Paid Summery</a>
</li>
{{-- <li>
            <a href="/repaymentdoc">Repayments Doc</a>
        </li> --}}
{{-- <li>
            <a href="#">Approve Repayments</a>
        </li> --}}
{{-- <li>
            <a href="/repayments/edit">Edit Repayments</a>
        </li> --}}
</ul>
</li>
{{-- <li class="{{''== request()->path() ?'active':''}}">

<a href="">
    Collateral Register
</a>
</li> --}}
<li class="menu-item-has-children dropdown">
    <a href="https://cantylivelocations.firebaseapp.com/upload" target="_blank">
        Location
    </a>
</li>
<li class="menu-item-has-children dropdown animated fadeIn">
    <a href="">
        Calendar
    </a>
</li>
<li class="menu-item-has-children dropdown animated fadeIn">
    <a href="/exportindex">
        Backup CSV
    </a>
</li>
<li class="menu-item-has-children dropdown animated fadeIn">
    <a href="/postalcode/create">
        Add Postal Code
    </a>
</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
            class="menu-icon fa fa-laptop"></i>STAFF</a>
    <ul class="sub-menu children dropdown-menu" id="x">
        <li>
            <a href="{{ route('staff.index') }}"> Staff</a>
        </li>
        <li>
            <a href="{{ route('staff.create') }}">Add Staff</a>
        </li>

    </ul>
</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
            class="menu-icon fa fa-laptop"></i>Pending Approvals</a>
    <ul class="sub-menu children dropdown-menu" id="x">
        <li>
            <a href="{{ route('verifycomex.index') }}">Company Expences</a>
        </li>
        <li>
            {{-- <a href="{{ route('staff.create') }}">Add Staff</a> --}}
        </li>

    </ul>
</li>
{{-- <li class="{{''== request()->path() ?'active':''}}">

<a href="#homeSubmenu6" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">

    Collection Sheets
</a>
<ul class="collapse list-unstyled" id="homeSubmenu6">
    <li>
        <a href="#"> Daily Collection Sheet </a>
    </li>
    <li>
        <a href="#"> Missed Repayments Sheet </a>
    </li>
    <li>
        <a href="#"> Past Maturity Date Loans </a>
    </li>
    <li>
        <a href="#"> Send SMS </a>
    </li>
    <li>
        <a href="#"> Send Email </a>
    </li>

</ul>
</li> --}}
{{-- <li class="{{''== request()->path() ?'active':''}}">

<a href="#homeSubmenu7" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">

    Company expences
</a>
<ul class="collapse list-unstyled" id="homeSubmenu7">

    <li>
        <a href="#"> Staff Transactions Report </a>
    </li>
    <li>
        <a href="#"> Approve Transactions </a>
    </li>
</ul>
</li> --}}
{{-- <li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>INVESTORES</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        <li>
            <a href="{{route('investors.index')}}">Investors</a>
</li>
<li>
    <a href="{{route('investors.create')}}"> Add Investor</a>
</li>
</ul>
</li> --}}
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
            class="menu-icon fa fa-laptop"></i>INVESTMENTS</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        <li>
            <a href="{{route('investors.index')}}">Investors</a>
        </li>
        <li>
            <a href="{{route('investors.create')}}"> Add Investor</a>
        </li>
        <li>
            <a href="{{route('investments.index')}}">Investments</a>
        </li>
        <li>
            <a href="{{route('investments.create')}}"> Add Investment</a>
        </li>
    </ul>
</li>
{{-- <li class="{{''== request()->path() ?'active':''}}">

<a href="#" ">

        E-Signatures
    </a>
  </li> --}}
  <li class=" menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
            class="menu-icon fa fa-laptop"></i>PAYROLL</a>
    <ul class="sub-menu children dropdown-menu animated fadeIn">
        <li>
            <a href="/payroll"> View Payroll </a>
        </li>
        <li>
            <a href="/addPayroll"> Add payroll </a>
        </li>
        <li>
            <a href="/reportPayroll"> Payroll Report </a>
        </li>
    </ul>
    </li>
    <li class="menu-item-has-children dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
                class="menu-icon fa fa-laptop"></i>EXPENXES</a>
        <ul class="sub-menu children dropdown-menu animated fadeIn">
            <li>
                <a href="/companyexpenses/all">Expences</a>
            </li>
            <li>
                <a href="{{ route('company_ex.index')}}">Add Expence</a>
            </li>
            <li>
                <a href="{{ route('ongoing_check.index')}}">Change Ongoing Check</a>
            </li>
            <li>
                <a href="{{ route('compant_ex_cat.index')}}">Add Expences Category</a>
            </li>
            <li>
                <a href="{{ route('approvedex.index')}}">Approved Ex</a>
            </li>

        </ul>
    </li>
    <li class="menu-item-has-children dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
                class="menu-icon fa fa-laptop"></i>OTHER INCOME</a>
        <ul class="sub-menu children dropdown-menu animated fadeIn">
            <li>
                <a href="otherincomes"> View Other Income </a>
            </li>
            <li>
                <a href=""> Add Other Income </a>
            </li>
        </ul>
    </li>
    <li class="menu-item-has-children dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
                class="menu-icon fa fa-laptop"></i>ASSEST MANAGMENT</a>
        <ul class="sub-menu children dropdown-menu animated fadeIn">
            <li>
                <a href="#"> View Asset Management </a>
            </li>
            <li>
                <a href="#"> Add Asset Management </a>
            </li>

        </ul>
    </li>
    <li class="menu-item-has-children dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
                class="menu-icon fa fa-laptop"></i>REPORTS</a>
        <ul class="sub-menu children dropdown-menu animated fadeIn">
            <li>
                <a href="newdashboard"> Dashboard </a>
            </li>
            <li>
                <a href="summery"> Summery </a>
            </li>
            {{-- <li>
            <a href="#"> Loan Report </a>
        </li> --}}
            <li>
                <a href="collectionsreport"> Collections </a>
            </li>
            <li>
                <a href="interestCollectionsReport"> Interest Collections </a>
            </li>
            <li>
                <a href="arrearsCustomerReviewReport"> Arrearse Collections </a>
            </li>
            <li>
                <a href="capitalCollectionsReport">Capital Collections </a>
            </li>
            <li>
                <a href="totalOutstandingCollectionsReport">Total Outstanding </a>
            </li>
            <li>
                <a href="arrearsInterestCollectionsReport"> Arrearse Interest Collections </a>
            </li>
            <li>
                <a href="arrearseCapitalCollectionsReport">Arrearse Capital Collections </a>
            </li>
            <li>
                <a href="advancedPaymentsCollectionsReport"> Advanced Payments Collections </a>
            </li>
            {{-- <li>
            <a href="#"> Loan Officer Report </a>
        </li>
        <li>
            <a href="#"> Loan Products Report </a>
        </li>
        <li>
            <a href="#"> MFRS Rations </a>
        </li>
        <li>
            <a href="#"> Monthly Report </a>
        </li>
        <li>
            <a href="#"> Outstanding Report </a>
        </li>
        <li>
            <a href="#"> Portfolio At Risk (PAR) </a>
        </li>
        <li>
            <a href="#"> At a Glance Report </a>
        </li>
        <li>
            <a href="#"> All Entries </a>
        </li> --}}
        </ul>
    </li>
    <li class="menu-item-has-children dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
                class="menu-icon fa fa-laptop"></i>ACCOUNTING</a>
        <ul class="sub-menu children dropdown-menu animated fadeIn">
            <li>
                <a href="#"> Cash flow Accumulated </a>
            </li>
            <li>
                <a href="#"> Cash Flow Monthly </a>
            </li>
            <li>
                <a href="#"> Cash Flow Projection </a>
            </li>
            <li>
                <a href="#"> Profit / Loss </a>
            </li>
            <li>
                <a href="#"> Balance Sheet </a>
            </li>
            <li>
                <a href="#"> Accounting Integration </a>
            </li>

        </ul>
    </li>
    <li class="menu-item-has-children dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
                class="menu-icon fa fa-laptop"></i>TEMPORARY</a>
        <ul class="sub-menu children dropdown-menu animated fadeIn">
            <li>
                <a href="/temp/issueIndex"> Issue</a>
            </li>
            <li>
                <a href="/temp/repaymentIndex"> Repayment </a>
            </li>
            <li>
                <a href="/temp/repaymentView"> View </a>
            </li>
            <li>
                <a href='/temp/totalByDate'> Total By Date </a>
            </li>
            {{-- <li>
                <a href="/documentcharges">Document Charges</a>
            </li> --}}
            <li>
                <a href="/centertotalsget">Center Totals</a>
            </li>
            <li>
                <a href="/totalofaday">Center Total of A Day</a>
            </li>
            <li>
                <a href="/totalofadayex">Center Total of A Day per Executives</a>
            </li>
            <li>
                <a href="/getledger">Ledger</a>
            </li>

        </ul>
    </li>
    <li class="menu-item-has-children dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
                class="menu-icon fa fa-laptop"></i>Document Charges</a>
        <ul class="sub-menu children dropdown-menu animated fadeIn">

            <li>
                <a href="/documentcharges">Get Document Charges</a>
            </li>
            <li>
                <a href="/deletedocumentcharges">Delete Document Charges</a>
            </li>
        </ul>
    </li>
    <li class="menu-item-has-children dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
                class="menu-icon fa fa-laptop"></i>SOS</a>
        <ul class="sub-menu children dropdown-menu animated fadeIn">
            <li>
                <a href="/sossettlement"> Change Settlements</a>
            </li>
            <li>
                <a href="sosinactive"> Borrower Deactivate </a>
            </li>


        </ul>
    </li>
    <li class="menu-item-has-children dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
                class="menu-icon fa fa-laptop"></i>Test</a>
        <ul class="sub-menu children dropdown-menu animated fadeIn">
            <li>
                <a href="/testChart"> Test</a>
            </li>
            >

        </ul>
    </li>
