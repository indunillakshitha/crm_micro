
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href={{asset("assets/img/ccanty.png")}}>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        @yield('title')
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    {{--    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />--}}
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/now-ui-dashboard.css?v=1.5.0')}}" rel="stylesheet"/>
    {{--    <link href="../assets/css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet" />--}}
<!-- CSS Just for demo purpose, don't include it in your project -->
    {{-- <link href="../assets/demo/demo.css" rel="stylesheet" /> --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    {{-- font awesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css"
          integrity="sha512-xA6Hp6oezhjd6LiLZynuukm80f8BoZ3OpcEYaqKoCV3HKQDrYjDE1Gu8ocxgxoXmwmSzM4iqPvCsOkQNiu41GA=="
          crossorigin="anonymous"/>
    <link rel="stylesheet" href="{{asset('assets/css/dataTables.min.css')}}">
    {{-- All fields to cap from sandee boi :v --}}
    <script src="{{ asset('js/firstLetterCap.js') }}" defer></script>


</head>

<body class="">
<div class="wrapper ">

    <div class="sidebar " data-color="blue">
        <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
        <div class="logo">
            <img src={{asset("assets/img/logo.png")}} width="237" height="90">
        </div>
        <div class="sidebar-wrapper" id="sidebar-wrapper">
            <?php  $access_level = Auth::user()->access_level ?>
            <ul class="nav">

                @if($access_level == 'super_user')
               @include('layouts/sidebars/super_user')

               @elseif($access_level == 'agent' || $access_level == 'field_executive')
                @include('layouts/sidebars/agent')

               @elseif($access_level == 'accountant' || $access_level == 'assistant_accountant')
                @include('layouts/sidebars/accountant')

               @elseif($access_level == 'branch_manager' )
                @include('layouts/sidebars/branch_manager')


               @endif
            </ul>
        </div>
    </div>

    <div class="main-panel" id="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-toggle">
                        <button type="button" class="navbar-toggler">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <a class="navbar-brand" href="#pablo">Development Stage</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                        aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                    <form>
                        {{-- <div class="input-group no-border"> --}}
                            {{-- <input type="text" value="" class="form-control" placeholder="Search..."> --}}
                            {{-- <div class="input-group-append"> --}}
                                {{-- <div class="input-group-text"> --}}
                                    {{-- <a href="/borrower"><i class="now-ui-icons business_badge"></i></a> --}}
                                {{-- </div> --}}
                            {{-- </div> --}}
                        {{-- </div> --}}
                        {{-- <div class="input-group no-border"> --}}
                            {{-- <input type="text" value="" class="form-control" placeholder="Search..."> --}}
                            {{-- <div class="input-group-append"> --}}
                                {{-- <div class="input-group-text"> --}}
                                    {{-- <a href="/loan"><i class="now-ui-icons business_money-coins "></i></a> --}}
                                {{-- </div> --}}
                            {{-- </div> --}}
                        {{-- </div> --}}
                    {{-- </form> --}}
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/borrower">
                                <i class="now-ui-icons business_badge"></i>
                                <p>
                                    <span class="d-lg-none d-md-block">Stats</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/loan">
                                <i class="now-ui-icons business_money-coins"></i>
                                <p>
                                    <span class="d-lg-none d-md-block">Stats</span>
                                </p>
                            </a>
                        </li>
                        {{-- <li class="nav-item">
                            <a class="nav-link" href="#pablo">
                                <i class="now-ui-icons media-2_sound-wave"></i>
                                <p>
                                    <span class="d-lg-none d-md-block">Stats</span>
                                </p>
                            </a>
                        </li> --}}
                        <li class="nav-item ">
                            {{-- <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a> --}}
                            <a class="dropdown-item" href="/logout"> Logout </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="now-ui-icons location_world"></i>
                                <p>
                                    <span class="d-lg-none d-md-block">Some Actions</span>
                                </p>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                {{-- <li class=" {{'account' == request()->path() ? 'active' : ''  }} " > --}}
                                <a class="dropdown-item" href=" {{route('account.create')}} ">Add User</a>
                                {{-- <a class="dropdown-item" href=" {{route('account.adduser')}} ">Add User</a> --}}
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                                {{-- </li> --}}
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#pablo">
                                <i class="now-ui-icons users_single-02"></i>
                                <p>
                                    <span class="d-lg-none d-md-block">Account</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->


        <div class="panel-header panel-header-sm">
        </div>


        <div class="content">
            @include('layouts/inc/messages')
            @yield('content')


        </div>


        <footer class="footer">
            <div class=" container-fluid ">


                <div class="copyright" id="copyright">
                    &copy;
                    <script>
                        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                    </script>
                    , Designed and Coded by <a href="https://autonomousfac.web.app/" target="_blank">Autonomous
                        Factory</a>.
                </div>
            </div>
        </footer>
    </div>
</div>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script src="{{asset('assets/js/dataTables.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>


<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script>
<!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
{{-- <script src="../assets/demo/demo.js"></script> --}}

<script>
    if (typeof module === 'object') {
        window.module = module;
        module = undefined;
    }
</script>


<!-- normal script imports etc  -->
<script src="scripts/jquery.min.js"></script>
<script src="scripts/vendor.js"></script>


<!-- Insert this line after script imports -->
<script>
    if (window.module) module = window.module;
</script>

@yield('scripts')
</body>

</html>
