@extends('layouts.master')
@section('title')


@endsection
@section('content')
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="pull-left">
                        <h2>Expences All</h2>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="card animated fadeIn" style="width: 100%;">
    <div class="card-body">
        <table class="table table-bordered" id="data_table">
            <thead>
            <tr style="background-color: darkgrey">
                <th>Voucher</th>
                <th>Date</th>
                <th>Category</th>
                <th>Payee</th>
                <th>Total Amount(Rs.)</th>
                <th>Action</th>

            </tr>
        </thead>

        <tbody>
            @foreach ($expences as $expence)
            <tr>
                <td style="text-align: center" >{{ $expence->voucher_id}}</td>
                <td style="text-align: center">{{ $expence->date}}</td>
                <td style="text-align: center">{{ $expence->category}}</td>
                <td style="text-align: center">{{ $expence->payee}}</td>
                <td style="text-align: right"><?php echo number_format( $expence->total , 2 , '.' , ',' ) ?></td>
                <td>
                    @if($expence->status== 'PENDING'   )
                <a href="{{route('company_ex.edit',$expence->voucher_id)}}" class="btn btn-primary">EDIT</a>
                @endif
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>

@endsection
