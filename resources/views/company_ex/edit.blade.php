@extends('layouts.master')
@section('title')

Expences
@endsection
@section('content')
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="pull-left">
                        <h2>Expences </h2>
                    </div>
                    <div class="pull-right">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row justify-content-center container-fluid">
    <div class="col-md-12">
        <div class="card p-3">
            <form id="form" action="{{ route("company_ex.store")}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row justify-content-center">
                    <div class="col-md-4 mb-3 ml-3">
                        <label for="validationServerUsername">Voucher Id</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            </div>
                            <input type="text" class="form-control is-valid" id="voucher_id"
                                value="{{$summery->voucher_id}}" name="voucher_id" placeholder="Enter a Package title"
                                readonly aria-describedby="inputGroupPrepend3">
                            @if ($errors->has('voucher_id'))
                            <div class="valid-feedback">
                                {{ $errors->first('voucher_id') }}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationServer02">Image</label>
                        <input type="file" name="image" id="image">
                    </div>
                </div>
                <div class="form-row justify-content-center">
                    <div class="col-md-4 mb-3">
                        <label for="validationServer02">Category</label>
                        <select name="category" id="category" class="form-control is-valid">
                            @foreach ($categories as $c)
                            <option value="{{$c->category}}">{{$c->category}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationServer02">Date</label>
                        <input type="date" class="form-control is-valid" id="date" name="date" value=""
                            placeholder="Enter a Package Price">
                        @if ($errors->has('date'))
                        <div class="valid-feedback">
                            {{ $errors->first('date') }}
                        </div>
                        @endif

                    </div>
                </div>
                <div class="form-row justify-content-center">
                    <div class="col-md-4 mb-3 ml-3">
                        <label for="validationServerUsername">Payee</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            </div>
                            <input type="text" class="form-control is-valid" id="payee" name="payee"
                                placeholder="Enter a Package title" aria-describedby="inputGroupPrepend3"
                                oninput="toCap(this.value, this.id)">
                            @if ($errors->has('payee'))
                            <div class="valid-feedback">
                                {{ $errors->first('payee') }} </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationServer02">Remarks</label>
                        <input type="text" class="form-control is-valid" id="remarks" name="remarks"
                            placeholder="Enter a Package Price" value="{{ old('remarks') }}"
                            oninput="toCap(this.value, this.id)">
                        @if ($errors->has('date'))
                        <div class="valid-feedback">
                            {{ $errors->first('date') }}
                        </div>
                        @endif

                    </div>
                </div>
                <div class="form-row justify-content-center">
                    <div class="col-md-4 mb-3 ml-3">
                        <label for="validationServerUsername">Description</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            </div>
                            <input type="text" class="form-control is-valid" id="description"
                                value="{{ old('description') }}" name="description" placeholder="Enter a Package title"
                                aria-describedby="inputGroupPrepend3" oninput="toCap(this.value, this.id)">
                            @if ($errors->has('payee'))
                            <div class="valid-feedback">
                                {{ $errors->first('payee') }} </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationServer02">Amount</label>
                        <input type="number" class="form-control is-valid" id="amount" name="amount" value=""
                            placeholder="Enter a Package Price" oninput="item_total.value=this.value*quantity.value">
                        @if ($errors->has('date'))
                        <div class="valid-feedback">
                            {{ $errors->first('date') }}
                        </div>
                        @endif
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationServer02">Quantity</label>
                        <input type="number" class="form-control is-valid" id="quantity" name="quantity" value=""
                            placeholder="Enter a Quantity" oninput="item_total.value=this.value*amount.value">
                        @if ($errors->has('quantity'))
                        <div class="valid-feedback">
                            {{ $errors->first('quantity') }}
                        </div>
                        @endif

                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationServer02">Item Total Amount</label>
                        <input type="number" class="form-control is-valid" id="item_total" name="item_total" value="">
                        @if ($errors->has('item_total'))
                        <div class="valid-feedback">
                            {{ $errors->first('item_total') }}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-row justify-content-center">
                    <div class="col-md-4 mb-3">
                        <label for="validationServer02">Supporting Document</label>
                        <select name="supporting_document" id="supporting_document" class="form-control is-valid">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                </div>
                {{-- <div class="form-row justify-content-center">

                    <div class="col-md-4 mb-3">
                        <label for="validationServer02"> Total Invoice Amount</label>
                        <input type="number" class="form-control is-valid" id="invoice_total"
                            value="{{$summery->invoice_total}}">
                        @if ($errors->has('item_total'))
                        <div class="valid-feedback">
                            {{ $errors->first('item_total') }}
                        </div>
                        @endif
                    </div>
                </div> --}}


                <div class="form-row justify-content-center">
                    <button class="btn btn-primary" id="sub_btn">Submit </button>
                </div>
            </form>
        </div>
    </div>


    <table class="table" id="results_table">
        <tr>
            <th>Voucher ID</th>
            <th>Category</th>
            <th>Date</th>
            <th>Payee</th>
            <th>Remarks</th>
            <th>Description</th>
            <th>Amount</th>
            <th>Quantity</th>
            <th>Item Total</th>
            <th>Supporting Document</th>
        </tr>
        @foreach ($expences as $expence)
        <tr>
            <td> {{$expence->voucher_id}} </td>
            <td> {{$expence->category}} </td>
            <td> {{$expence->date}} </td>
            <td> {{$expence->payee}} </td>
            <td> {{$expence->remarks}}</td>
            <td> {{$expence->description}} </td>
            <td> {{$expence->amount}} </td>
            <td> {{$expence->quantity}} </td>
            <td> {{$expence->item_total}} </td>
            <td> {{$expence->supporting_document}} </td>
            <td> <a id="${record.id}" class="btn btn-danger deleteEarning" onclick="delete_expense(this.id)">Remove
                </a>
            </td>

        </tr>
        @endforeach
    </table>
</div>

<button class="btn btn-primary" id="refresh_btn" onclick="location.reload()">Refresh </button>


{{-- <div class="card animated fadeIn" style="width: 100%;">
    <div class="card-body">
        <table class="table table-bordered" id="data_table">
            <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Center No</th>
                <th>Branch Name</th>
                <th> Address</th>
                <th> Mobile</th>
                <th width="200px">Action</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($centers as $center)
            <tr>
                <td>{{ ++$i }}</td>
<td>{{ $center->center_name }}</td>
<td>{{ $center->center_no }}</td>
<td>{{ $center->branch_no }}</td>
<td>{{ $center->center_address }}</td>
<!-- <td>{{ $center->center_province  }}</td>
            <td>{{ $center->center_city }}</td>
            <td>{{ $center->center_postal }}</td> -->
<td>{{ $center->center_mobile }}</td>
<!-- <td>{{ $center->center_land }}</td>
            <td>{{ $center->center_email }}</td> -->
<td width="10px">
    <form action="{{ route('center.destroy',$center->id) }}" method="POST">

        <a class="btn btn-info" href="{{ route('center.show',$center->id) }}"><i class="fa fa-eye"></i></a>

        <a class="btn btn-primary" href="{{ route('center.edit',$center->id) }}"><i class="fa fa-bars"></i></a>

        @csrf
        @method('DELETE')

        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
    </form>
</td>

</tr>
@endforeach
</tbody>
</table>
</div>
</div>
{!! $centers->links() !!} --}}

<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );


</script>

@section('scripts')

<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        document.querySelector('#sub_btn').addEventListener('click', e => {
            e.preventDefault()
            // let formData = new FormData(document.querySelector('#form'))
            // console.log(form);

            $.ajax({
                type: 'POST',
                url: '{{url('/companyexpenses/addexpense')}}',
                data: new FormData(form),
                // dataType: 'JSON',
                processData: false,
                contentType: false,
                success: function (data) {
                    console.log(data, 'optimus prime')
                    return get_expenses_module()
                }
            })
        })


    //add expenses
    //     document.querySelector('#sub_btnn').addEventListener('click', e => {
    //     e.preventDefault()
    //     $.ajax({
    //             type: 'POST',
    //             url: '{{url('/companyexpenses/addexpense')}}',
    //             data: {
    //                 'voucher_id': voucher_id.value,
    //                 'category': category.value,
    //                 'date': date.value,
    //                 'payee': payee.value,
    //                 'remarks': remarks.value,
    //                 'description': description.value,
    //                 'amount': amount.value
    //             },
    //             dataType: 'JSON',
    //             success: function (data) {
    //                 console.log(data, 'optimus prime')
    //                 return get_expenses_module()
    //             }
    //         })

    // })

    function get_expenses_module(){
        $.ajax({
                        type: 'POST',
                        url: '{{('/companyexpenses/getexpense')}}',
                        data: {'voucher_id': voucher_id.value,},
                        success: function (data) {
                            console.log(data, 'hasta la vista')
                            results_table.innerHTML = `
                            <tr>
                                <th>Voucher ID</th>
                                <th>Category</th>
                                <th>Date</th>
                                <th>Payee</th>
                                <th>Remarks</th>
                                <th>Description</th>
                                <th>Amount</th>
                                <th>Quantity</th>
                                <th>Item Total</th>
                            </tr>
                            `
                            data.forEach(record => {
                            let html = `
                            <tr>
                                <td > ${record.voucher_id}  </td>
                                <td > ${record.category}  </td>
                                <td > ${record.date}  </td>
                                <td > ${record.payee}  </td>
                                <td > ${record.remarks}  </td>
                                <td > ${record.description}  </td>
                                <td > ${record.amount}  </td>
                                <td > ${record.quantity}  </td>
                                <td > ${record.item_total}  </td>
                                <td > <a  id="${record.id}"
                                            class="btn btn-danger deleteEarning" onclick="delete_expense(this.id)">Remove
                                        </a>
                                </td>

                            </tr>
                        `
                        results_table.innerHTML += html

                    })


                    }
                })
    }

    function delete_expense(id){
        $.ajax({
                    type: 'POST',
                    url: '{{url('/companyexpenses/deleteexpense')}}',
                    data: {
                        id
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data)
                        return get_expenses_module()
                    }
                })
    }


</script>

@endsection

@endsection
