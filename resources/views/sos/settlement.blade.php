@extends('layouts.master')
@section('title')

Loans
@endsection
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Emergency situations</h2>
                    </div>

            </div>

        </div>
    </div>
</div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered" id="data_table">
    <thead>
        <tr>
            <th>No</th>
            <th>Borrower No</th>
            <th>Loan Stage</th>
            <th>Blance Amount</th>
            <th>Loan Amount</th>

            <th>Status</th>


            <th width="200px">SElect</th>
            <th width="200px">Action</th>
        </tr>
    </thead>

    <tbody>
        {{-- {{$i=1}} --}}
        @foreach ($loans as $loan)
        <tr>
            <td>{{ $loan->id }}</td>
            <td>{{ $loan->borrower_no }}</td>
            <td>{{ $loan->loan_amount }}</td>
            <td>{{ $loan->loan_stage }}</td>
            <td>{{ $loan->due }}</td>
            <td> <span class="badge badge-danger">{{ $loan->status }}</span></td>


            <td width="10px">
                <select name="st" id="st" class="form-control" onclick="stt.value=this.value">
                    <option value="Pending">Vefified</option>
                    <option value="Approved">Approved</option>
                    <option value="Issued">Print Mode</option>
                    <option value="Payed">Isssued</option>
                    <option value="SETTLED">Settled</option>

                </select>
                <input type="hidden" id="stt">
            </td>
            <td>
                <a class="btn btn-primary" onclick="changeStatus({{$loan->id}},stt.value)"><i class="fa fa-check"></i></a>

            </td>

        </tr>
        @endforeach
    </tbody>
</table>

<script type="text/javascript">
    function changeStatus(id,x){
    console.log(id,x)
    var status=stt.value
    $.ajax({
        type: 'GET',
        url: '{{('/sossettlementchange')}}',
        data: {
            id:id,
            status:status
        } ,
        success: function(data){
            console.log(data.success);
            Swal.fire({
                    title: 'Loan Updated',
                    text: data.success,
                    icon: 'success',
                    timer: 2000
                }).then((value)=>{
                    location.reload();
                })
        }
    })

}
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>
{{-- {!! $loans->links() !!} --}}

@endsection
