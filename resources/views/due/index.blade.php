@extends('layouts.master')
@section('title')

Loans
@endsection
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Arrears Today</h2>
                    </div>
                    <div class="pull-right">
                        {{-- <a class="btn btn-success" href="{{ route('loan.create') }}"> Add Loan</a> --}}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered" id="data_table">
    <thead>
        <tr>
            <th>No</th>
            <th>Borrower No</th>
            <th>Loan Stage</th>
            <th>Loan Amount</th>

            <th>Status</th>


        </tr>
    </thead>

    <tbody>
        {{$i=1}}
        @foreach ($due as $du)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $du->branch }}</td>
            <td>{{ $loan->borrower_nic }}</td>
            <td>{{ $loan->loan_amount }}</td>
            <td>{{ $loan->status }}</td>



        </tr>
        @endforeach
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>
{{-- {!! $loans->links() !!} --}}

@endsection
