@extends('layouts.master')
@section('title')

Investement
@endsection
@section('content')



<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Create New Investment</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('investments.index') }}"> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="">
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Warning!</strong> Please check your input code<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="modal-body">
                <form action="{{ route('investments.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group col-6">
                                <strong>Investing By</strong>
                                <select name="invested_by" required class="form-control" id="expense_type"
                                    value="expense_type">
                                    <option value="0" selected="true">-Select-</option>
                                    @foreach($investores as $investore)
                                    <option value="{{$investore->id}}">{{$investore->title}} {{$investore->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group col-6">
                                <strong>Start Date</strong>
                                <input type="date" required name="start_date" id="nic" class="form-control"
                                    placeholder="NIC">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group col-6">
                                <strong>Amount</strong>
                                <input type="number" name="amount" required class="form-control"
                                    placeholder="Investing Amount" id="ff">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group col-6">
                                <strong>Period (Years)</strong>
                                <input class="date form-control" required type="number" name="period" id="expense_date">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group col-6">
                                <strong>Rate</strong>
                                <input type="number" required name="rate" class="form-control" placeholder="Rate"
                                    id="ledger_account">
                            </div>
                        </div>


                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
    $('.date').datepicker({

                format: 'yyyy-mm-dd'

            });
</script>
@endsection
