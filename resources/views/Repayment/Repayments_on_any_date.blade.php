@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

    <div class="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                        </div>
                        <div class="col-3">
                            <label for="">Select date</label>
                        <input type="date" name="da" id="selected_date"class="form-control"  >
                        {{-- <div class="col-3"> --}}
                            <label for="">Select Paid date</label>
                        <input type="date" name="paid_date" id="paid_date"class="form-control"  >
                        </div>
                        <div class="pull-right col-6">
                            <button type="button" class="btn btn-success" id="search_btn" data-toggle="modal"
                                    data-target="#exampleModal">Search
                            </button>
                            {{--                            <a class="btn btn-success" href="{{ route('borrower.create') }}"> Add New Borrower</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <table class="table table-bordered" id="repayment_table">
            <tr>
                <th>Loan Id</th>
                <th>Total amount</th>
                <th>Due Amount</th>
                <th>Status</th>
            </tr>

        </table>

    </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

const search_btn = document.querySelector('#search_btn')
const selected_date = document.querySelector('#selected_date')
const paid_date = document.querySelector('#paid_date')
search_btn.addEventListener('click', e => {

let date = new Date(selected_date.value)
var start = new Date(date.getFullYear(), 0, 0)
var diff = (date - start) + ((start.getTimezoneOffset() - date.getTimezoneOffset()) * 60 * 1000)
var oneDay = 1000 * 60 * 60 * 24;
var day = Math.floor(diff / oneDay);
console.log('Day of year: ' + day);


             $.ajax({
                    type: 'GET',
                    url: '{{('/getrepaymentsdate')}}',
                    data: {'day': day},
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data, 'got repay date')

                        document.querySelector('#repayment_table').innerHTML = `
                        <tr>
                            <th>Loan Id</th>
                            <th>Total amount</th>
                            <th>Due Amount</th>
                            <th>Status</th>
                        </tr>
                        `
                        row_id =0;
                        data.forEach(record => {
                            html =
                                `
                                <tr  id="repayment_row">
                                     <th id="loan_${row_id}">${record.id}</th>
                                     <th>${record.loan_amount}</th>
                                     <th>${record.today_payment}</th>
                                     <th><input id="todaypayment_${row_id}" ></th>
                                     <th><button id="collectbtn_${row_id}" class="btn btn-primary fa fa-check collectbtns"></button></th>

                                </tr>`

                                document.querySelector('#repayment_table').innerHTML += html
                                row_id++
                        })
                        get_collect_btn_array()

                    }
                })

})

function get_collect_btn_array(row_id){
    const collect_btns = document.querySelectorAll('.collectbtns');
    // console.log(collect_btns)
    const arr = Array.from(collect_btns)

    arr.forEach(btn => {
        btn.addEventListener('click', e => {
            // console.log('aa')
            let row_id = e.target.id
            row_id = row_id.slice(row_id.indexOf('_')+1, row_id.length)
            // console.log(row_id)
            set_and_get_repayments(row_id)
        })
    })
}

function set_and_get_repayments(row_id){

    const loanid = document.querySelector(`#loan_${row_id}`)
    const today_payment = document.querySelector(`#todaypayment_${row_id}`)
    const paid_date = document.querySelector('#paid_date')

    let pa_date = new Date(paid_date.value)
    var start = new Date(pa_date.getFullYear(), 0, 0)
    var diff = (pa_date - start) + ((start.getTimezoneOffset() - pa_date.getTimezoneOffset()) * 60 * 1000)
    var oneDay = 1000 * 60 * 60 * 24;
    var p_date = Math.floor(diff / oneDay);
    console.log('paid date: ' + p_date);
    $.ajax({
        type: 'POST',
        url: '{{('/setrepaymentsdate')}}',
        data: {
            'id': loanid.textContent,
            'paid_date':p_date,
            'today_payment' :today_payment.value,
        },
        success: function(data){
            console.log(data)
            search_btn.click()
        }
        })
}


</script>
@endsection
