@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

    <div class="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                        </div>
                        <div class="col-3">
                            <label for="">Select Date</label>
                        <input type="date" name="da" id="selected_date"class="form-control"  >
                        </div>
                        <div class="col-4">
                            <label for="">Select Center</label>
                        <!-- <input type="text" name="cent" id="selected_center"class="form-control" > -->
                        <select name="center" class="form-control" id="center" value="center">

                                    <option value="0" selected="true">-Select-</option>
                                    @foreach($centers as $center)
                                        <option
                                            value="{{$center->center_name}}">{{$center->center_name}}</option>
                                    @endforeach

                                </select>
                        </div>
                        <div class="pull-right col-5">
                            <button type="button" class="btn btn-success" id="search_btn" data-toggle="modal"
                                    data-target="#exampleModal">Search
                            </button>
                            {{--<a class="btn btn-success" href="{{ route('borrower.create') }}"> Add New Borrower</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                        </div>
                        <div class="col-3">
                            <label for="">Center Collection</label>
                        <input type="text" name="collection" id="collection_val"class="form-control"  value="">
                        </div>
                        {{-- <div class="col-4">
                            <label for=""> Center</label> --}}
                        <!-- <input type="text" name="cent" id="selected_center"class="form-control" > -->

                        {{-- </div> --}}
                        <div class="pull-right col-5">
                            <button type="button" class="btn btn-success" id="" data-toggle="modal"
                                    data-target="#">Check
                            </button>
                            {{--<a class="btn btn-success" href="{{ route('borrower.create') }}"> Add New Borrower</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <table class="table table-bordered" id="repayment_table">
            <tr>
                <th>No</th>
                <th>Loan ID</th>
                <th>Total amount</th>
                <th>Due Amount</th>
                <th>Collect</th>
            </tr>

        </table>

    </div>

@endsection

@section('scripts')
<script type="text/javascript">
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

const search_btn = document.querySelector('#search_btn')
const selected_date = document.querySelector('#selected_date')
const center = document.querySelector('#center')

search_btn.addEventListener('click', e => {

let date = new Date(selected_date.value)
var start = new Date(date.getFullYear(), 0, 0)
var diff = (date - start) + ((start.getTimezoneOffset() - date.getTimezoneOffset()) * 60 * 1000)
var oneDay = 1000 * 60 * 60 * 24;
var day = Math.floor(diff / oneDay);
var cent = center.value
console.log('Day of year: ' + day);
console.log('Day of year: ' + cent);
// console.log('Day of year: ' + start);
// console.log('Day of year: ' + diff);




             $.ajax({
                    type: 'GET',
                    url: '{{('/getcenterrepayments')}}',
                    data: {'day': day,'center':cent},
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data)

                        document.querySelector('#repayment_table').innerHTML = `
                        <tr>
                            <th>No</th>
                            <th>Loan ID</th>
                            <th>Total amount</th>
                            <th>Due Amount</th>
                            <th>Collect</th>
                        </tr>
                        `
                        row_id =0;
                        data.forEach(record => {
                            html =
                                `
                                <tr  id="repayment_row">
                                     <th id="loan_${row_id}">${record.id}</th>
                                     <th>${record.loan_id}</th>
                                     <th>${record.loan_amount}</th>
                                     <th>${record.due_amount}</th>
                                     <th><input id="todaypayment_${row_id}" ></th>
                                     <th><button id="collectbtn_${row_id}" class="btn btn-primary fa fa-check collectbtns"></button></th>

                                </tr>`

                                document.querySelector('#repayment_table').innerHTML += html
                                row_id++
                        })
                        get_collect_btn_array()

                    }
                })

})

center.addEventListener('click', e => {

var cent = center.value
console.log(cent);
                        $.ajax({
                            type: 'GET',
                            url: '{{('/getcentercollection')}}',
                            data: {'center':cent},
                            dataType: 'JSON',

                            success: function (center) {
                                console.log(center)
                                                            $("#collection_val").val(center);

                            }
                        });

})
// function get_collect_btn_array(row_id){
//     const collect_btns = document.querySelectorAll('.collectbtns');
//     // console.log(collect_btns)
//     const arr = Array.from(collect_btns)

//     arr.forEach(btn => {
//         btn.addEventListener('click', e => {
//             // console.log('aa')
//             let row_id = e.target.id
//             row_id = row_id.slice(row_id.indexOf('_')+1, row_id.length)
//             // console.log(row_id)
//             set_and_get_repayments(row_id)
//         })
//     })
// }

// function set_and_get_repayments(row_id){

//     const loanid = document.querySelector(`#loan_${row_id}`)
//     const today_payment = document.querySelector(`#todaypayment_${row_id}`)
//     $.ajax({
//         type: 'POST',
//         url: '{{('/getrepaymentsdate')}}',
//         data: {
//             'id': loanid.textContent,
//             'today_payment' :today_payment.value,
//         },
//         success: function(data){
//             console.log(data)
//             search_btn.click()
//         }
//         })
// }


</script>
@endsection
