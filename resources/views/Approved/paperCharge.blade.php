@extends('layouts.master')
@section('title')

    Create New Branch

@endsection
@section('content')


    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Papers Charges</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('approved.index') }}"> Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> Please check your input code<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                 <form action="/approved/pprCharg/{{$loan->id}}" method="POST">

                    @csrf
                    
                    
                    <div class="row">
                        
                        <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Ammount :</strong>
                            <input type="number" name="fees" value="{{ $loan->fees }}" class="form-control" placeholder="">
                        </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
