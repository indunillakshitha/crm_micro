@extends('layouts.master')
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Approved Loans</h2>
                    </div>
                    <div class="pull-right">
                        Print Center Disbursement :
                        <select name="" id="select_center">
                            @foreach($centers as $center)
                                <option value="{{$center->center_name}}">{{$center->center_name}}</option>
                            @endforeach
                        </select>

                    <input type="hidden" name="" id="branch" value="{{Auth::user()->branch}}">
                        <button class="btn btn-success" onclick="printDisbursement()">Print </button>
                    </div>
                </div>
                    {{-- <div class="col-xs-3 col-sm-3 col-md-3 "  >
                        <h6>Branch</h6>
                        <input class="form-control" type="text" placeholder="Branch Name" aria-label="Search" id="branch_selector" name="branch_selector" value="" >
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3" >
                        <h6>Center</h6>
                        <input class="form-control" type="text" placeholder="Center Name" aria-label="Search" id="center_selector" name="center_selector" value="" >
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3" >
                        <h6>Group</h6>
                        <input class="form-control" type="text" placeholder="Group Number" aria-label="Search" id="group_selector" name="group_selector" value="">
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3" >

                        <button type="button" name="filter" id="filter" class="btn btn-info btn-sm">Search</button>

                        <button type="button" name="reset" id="reset" class="btn btn-default btn-sm">Reset</button>
                    </div> --}}



            </div>
        </div>
    </div>
</div>

{{-- @if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif --}}

<table class="table table-bordered col-12" id="loan_table">
    <tr>
        <th >No</th>
        <th >Borrower No</th>
        <th>Loan Stage</th>
        <th>Loan Amount</th>

        <th>Release Date</th>


        <th width="400px">Print Docs</th>
    </tr>
    @foreach ($loans as $loan)



    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $loan->borrower_no }}</td>
        <td>{{ $loan->loan_stage }}</td>
        <td>{{ $loan->loan_amount }}</td>

        <td>{{ $loan->release_date }}</td>

        <td width="100px">

            <form action="{{ route('approved.destroy',$loan->id) }}" method="POST">

{{--                <a class="btn btn-info btn-sm" href="{{ route('approved.show',$loan->id) }}">Details of Borrower</a>--}}
<a class="btn btn-info btn-sm col-3" href="/promissory/export/{{$loan->id}}"><i class="fa fa-print"data-toggle="tooltip" title="Promissory" aria-hidden="true"></i>Promissary</a>
                <a class="btn btn-info btn-sm col-3" href="/agreement/export/{{$loan->id}}"><i class="fa fa-print  "data-toggle="tooltip" title="Agreement" aria-hidden="true"></i>agreement</a>
                <a class="btn btn-info btn-sm col-3" href="/guarantor-bond/export/{{$loan->id}}"><i class="fa fa-print" data-toggle="tooltip" title="Guarantor Bond" aria-hidden="true"></i>G-Bond</a>
                <a class="btn btn-info btn-sm col-3" href="/disbursement-group/export/{{$loan->id}}"><i class="fa fa-print" data-toggle="tooltip" title="Disbursement"aria-hidden="true"></i>Dis</a>
                {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Show</button>--}}
                <!-- <a class="btn btn-danger btn-sm" href="/approved/pprCharg/{{$loan->id}}"></a> -->
                {{-- <a class="btn btn-info btn-sm" href="/approved/pprChrgView/{{$loan->id}}">Papers Charges</a> --}}
                <a class="btn btn-success btn-sm" href="/approved/issue/{{$loan->id}}">Next</a>

                @csrf
                @method('DELETE')

                {{-- <button type="submit" class="btn btn-danger btn-sm">Reject</button> --}}
            </form>
        </td>

    </tr>

    @endforeach
</table>

{!! $loans->links() !!}


<script>
    function printDisbursement(){
        window.location.href = `/disbursement/${branch.value}/${select_center.value}`;
    }
</script>


@endsection

