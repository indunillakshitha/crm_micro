@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')

<div class="">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Change Branch</h2>
                    </div>
                    <div class="pull-right">
                        <h5>Current Branch
                            <span class="col btn btn-success  ">{{$current_branch}}</span></h5>

                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="card-body px-5 py-5">
            <div class="row">
                @foreach ($branches as $b)
                    @if($b->branch_name == $current_branch)
                        <a class="col btn btn-success mx-5 text-white" href="/branches/change/{{$b->branch_name}}">{{$current_branch}}</a>
                    @else
                        <a class="col btn btn-primary mx-5 text-white" href="/branches/change/{{$b->branch_name}}">{{$b->branch_name}}</a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

</script>
@endsection
