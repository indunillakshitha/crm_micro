@extends('layouts.master')
@section('title')

    Branches

@endsection
@section('content')


    {{--//--------------------------------------------------------------------------------------------------------}}

    <div class="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="pull-left">
                            <h2>Branches</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-success" href="{{ route('branch.create') }}"> Create New Branch</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{-- @if ($message = Session::get('success'))
        --}}
        {{-- <div class="alert alert-success">--}}
            {{-- <p>{{ $message }}</p>--}}
            {{-- </div>--}}
        {{-- @endif--}}
    <div class="card animated fadeIn" style="width: 100%;">
        <div class="card-body">
            {{-- <table class="table table-bordered"> --}}
                <table id="data_table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>No</th>
                            <th> Address</th>
                            <!-- <th> Province</th>
                <th> City</th>
                <th> Postal</th> -->
                            <th> Mobile</th>
                            <!-- <th> Land</th>
                <th> Email</th> -->

                            <th width="200px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($branches as $branch)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $branch->branch_name }}</td>
                                <td>{{ $branch->branch_no }}</td>
                                <td>{{ $branch->branch_address }}</td>
                                {{--
                                <!-- <td>{{ $branch->branch_province }}</td>
                <td>{{ $branch->branch_city }}</td>
                <td>{{ $branch->branch_postal }}</td> --> --}}
                                <td>{{ $branch->branch_mobile }}</td>
                                {{--
                                <!-- <td>{{ $branch->branch_land }}</td>
                <td>{{ $branch->branch_email }}</td> --> --}}
                                <td width="10px">
                                    <form action="{{ route('branch.destroy', $branch->id) }}" method="POST">

                                        <a class="btn btn-info" href="{{ route('branch.show', $branch->id) }}"> <i
                                                class="fa fa-eye"></i></a>
                                             {{--<button type="button" class="btn btn-primary"
                                               data-toggle="modal"
                                               data-target="#exampleModal">Show</button>--}}

                                        <a class="btn btn-primary" href="{{ route('branch.edit', $branch->id) }}"><i
                                                class="fa fa-bars"></i></a>
                                             {{--<button type="button" class="btn btn-primary"
                                              data-toggle="modal"
                                              data-target="#exampleModal">Edit</button>--}}
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>

                                    </form>
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>No</th>
                            <th> Address</th>
                            <!-- <th> Province</th>
                    <th> City</th>
                    <th> Postal</th> -->
                            <th> Mobile</th>
                            <!-- <th> Land</th>
                    <th> Email</th> -->

                            <th width="200px">Action</th>
                        </tr>
                    </tfoot>
                </table>
        </div>
    </div>
    {!! $branches->links() !!}


    <script type="text/javascript">
        $(document).ready(function() {
            $('#data_table').DataTable();
        });

    </script>
@endsection
