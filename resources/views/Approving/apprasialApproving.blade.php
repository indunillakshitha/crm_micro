@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')












<div class="container">
    <div class="card">
        <div   class="card-body">
            
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Appraisal:</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="#"> Back</a>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="#"> Next</a>
            </div>
        </div>
    </div>
   


    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Borrower No:</strong>
                <input type="text" name="borrower_no" disabled class="form-control" placeholder="       {{ $apprasails->borrower_no }}">

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Husbund:</strong>

                <input type="text" name="husband" disabled class="form-control" placeholder="       {{ $apprasails->husband }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Son:</strong>

                <input type="text" name="son" disabled class="form-control" placeholder="       {{ $apprasails->son }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Daugter:</strong>

                <input type="text" name="daughter" disabled class="form-control" placeholder="      {{ $apprasails->daughter }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Guardian:</strong>

                <input type="text" name="guardian" disabled class="form-control" placeholder="      {{ $apprasails->guardian }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Other:</strong>

                <input type="text" name="other" disabled class="form-control" placeholder="     {{ $apprasails->other }}">
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Total Earn:</strong>
        
                <input type="text" name="total_earn" disabled class="form-control" placeholder="    {{ $apprasails->total_earn }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Household Expenses:</strong>

                <input type="text" name="household_expenses" disabled class="form-control" placeholder="    {{ $apprasails->household_expenses }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Other Expenses:</strong>

                <input type="text" name="other_expenses" disabled class="form-control" placeholder="    {{ $apprasails->other_expenses }}">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>As Guarantor Credit Facility Count :</strong>

                <input type="text" name="as_borrower_facility_count" disabled class="form-control" placeholder="    {{ $apprasails->as_borrower_facility_count }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Total Amount::</strong>

                <input type="text" name="as_borrower_total_amount" disabled class="form-control" placeholder="      {{ $apprasails->as_borrower_total_amount }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>As Guarantor Credit Facility Count :</strong>

                <input type="text" name="as_guarantor_facility_count" disabled class="form-control" placeholder="       {{ $apprasails->as_guarantor_facility_count }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Total Amount:</strong>

                <input type="text" name="as_guarantor_total_amount" disabled class="form-control" placeholder="     {{ $apprasails->as_guarantor_total_amount }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Total Expenses :</strong>

                <input type="text" name="total_expenses" disabled class="form-control" placeholder="    {{ $apprasails->total_expenses }}">
            </div>
        </div>
        
        </div>
        </div>
        </div>
       


    
@endsection

@section('scripts')

@endsection
