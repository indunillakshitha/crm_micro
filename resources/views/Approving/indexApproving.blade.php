@extends('layouts.master')
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}


@if(session()->has('message'))
<div class="alert alert-success">
{{ session()->get('message') }}
</div>
@endif

<table class="table table-bordered" id="data_table">
    <thead>
    <tr>
        <th>No</th>
        <th>Borrower NIC</th>
        <th>Center</th>
        <th>Groupt</th>

        <th>Amount</th>


        <th width="200px">Action</th>
    </tr>
</thead>
<tbody>
    @foreach ($loans as $loan)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $loan->nic }}</td>
        <td>{{ $loan->center }}</td>
        <td>{{ $loan->group_no }}</td>

        <td>{{ $loan->loan_amount }}</td>

        <td width="100px">

            <form action="{{ route('approving.destroy',$loan->id) }}" method="POST">

                <a class="btn btn-info btn-sm" href="{{ route('approving.show',$loan->id) }}">Details of Borrower</a>
                <a class="btn btn-info btn-sm" href="https://cantylivelocations.firebaseapp.com/location/{{$loan->nic}}" target="_blank"><i class="fa fa-location-arrow"></i></a>
                {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Show</button>--}}
                <a class="btn btn-success btn-sm" href="/approving/test/{{$loan->id}}">Approve</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger btn-sm">Reject</button>
            </form>
        </td>

    </tr>

    @endforeach
</tbody>
</table>

<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>

{!! $loans->links() !!}

@section('scripts')

<script type="text/javascript">

        $(document).ready(function () {
             $('#loan_table').DataTable();
    //          processing: true,
    //          serverSide: true,
    //         fill_datatable();

    // function fill_datatable(group_selector = '', center_selector = '')
    // {
    //     var dataTable = $('#loan_table').DataTable({
    //         processing: true,
    //         serverSide: true,
    //         ajax:{
    //             url: "{{ route('approving.index') }}",
    //             data:{center_selector:center_selector, group_selector:group_selector}
    //         },
    //         columns: [
    //             {
    //                 data:'borrower_no',
    //                 name:'borrower_no'
    //             },
    //             {
    //                 data:'branch',
    //                 name:'branch'
    //             },
    //             {
    //                 data:'center',
    //                 name:'center'
    //             },
    //             {
    //                 data:'group_no',
    //                 name:'group_no'
    //             },
    //             {
    //                 data:'loan_stage',
    //                 name:'loan_stage'
    //             },
    //             {
    //                 data:'loan_stage',
    //                 name:'loan_stage'
    //             }
    //         ]
    //     });
    // }

    // $('#filter').click(function(){
    //     var center_selector = $('#center_selector').val();
    //     var group_selector = $('#group_selector').val();

    //     if(center_selector != '' &&  group_selector != '')
    //     {
    //         $('#loan_table').DataTable().destroy();
    //         fill_datatable(center_selector, group_selector);
    //     }
    //     else
    //     {
    //         alert('Select Both filter option');
    //     }
    // });

    // $('#reset').click(function(){
    //     $('#center_selector').val('');
    //     $('#group_selector').val('');
    //     $('#loan_table').DataTable().destroy();
    //     fill_datatable();
    // });



            // var la = $(loans).val();

        // $(branch_selector).click(function(){
        //     var branch = $(this).val();
        //     console.log(branch)


        //     $.ajax({
        //         type: 'GET',
        //         url: '{{ ('/bseclector') }}',
        //         data: {'id':branch},
        //         dataType: 'JSON',
        //         success: function (data) {
        //             console.log(data);
        //             data.forEach(function(item){
        //                 console.log(item);
        //                 // $('#loan_table').append('$loans'+item);
        //            })




        //      }

        //         });
        //     });

        // $(center_selector).click(function () {
        //     var center = $(this).val();
        //     console.log(center);

        //     $.ajax({
        //         type: 'GET',
        //         url: '{{ ('/cseclector') }}',
        //         data: {'id':center},
        //         dataType: 'JSON',
        //         success: function (data) {
        //             console.log(data);

        //         }
        //     });

        // });
        // $(group_selector).click(function () {
        //     var group = $(this).val();
        //     console.log(group);
        //     var tmp = null;
        //     $.ajax({
        //         type: 'GET',
        //         url: '{{ ('/gseclector') }}',
        //         data: {'id':group},
        //         dataType: 'JSON',
        //         success: function (data) {
        //             console.log(data);


        //         //    data.forEach(function(item){

        //         //                                 // $('#loan_table').append('$loans'+item);
        //         //         // $('#loan_table').append(${item+'$loans');
        //         //         $('#foreach').append(item);
        //         //         // tmp = item;
        //         //         console.log(item);
        //         //         // $('#loan_table').append('<td>'+item.loan_stage+'</td>');

        //         //    })
        //         // });
        //         }
        //     });

        // });
        });


</script>




@endsection
@endsection
