@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

<div class="">
    <div class="card">
        <div   class="card-body">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Borrower Details:</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{route('approving.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <li >
        <a href="#homes" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
            Borrower Details
        </a>
        <form action="">
            @csrf
    <ul class="collapse list-unstyled" id="homes">
        <li>
    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 row">
             <div class="form-group col">
                <strong>Application No:</strong>
                <input type="text" name="application_no" disabled class="form-control" placeholder="{{ $borrower->application_no }}">
            </div>
            <div class="form-group col">
                <strong>Branch :</strong>
                <input type="text" name="branch" disabled class="form-control" placeholder="{{ $borrower->branch }}">
            </div>
            <div class="form-group col">
                <strong>Branch No:</strong>
                <input type="text" name="branch_no" disabled class="form-control" placeholder="{{ $borrower->branch_no }}">
            </div>
            <div class="form-group col">
                <strong>Center:</strong>
                <input type="text" name="center" disabled class="form-control" placeholder="{{ $borrower->center }}">
            </div>
            <div class="form-group col">
                <strong>Group No:</strong>
                <input type="text" name="group_no" disabled class="form-control" placeholder="{{ $borrower->group_no }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 row">
            <div class="form-group col">
                <strong>Unique Id:</strong>
                <input type="text" name="borrower_no" disabled class="form-control" placeholder="{{ $borrower->borrower_no }}">
            </div>

            <div class="form-group col">
                <strong>Full Name:</strong>
                <input type="text" name="full_name" disabled class="form-control" placeholder="{{ $borrower->full_name }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 row">
            <div class="form-group col">
                <strong>NIC:</strong>
                <input type="text" name="nic" disabled class="form-control" placeholder="  {{ $borrower->nic }}">
            </div>
            <div class="form-group col">
                <strong>Birthday:</strong>
                <input type="text" name="birthday" disabled class="form-control" placeholder="  {{ $borrower->birthday }}">
            </div>
            <div class="form-group col">
                <strong>Gender:</strong>
                <input type="text" name="gender" disabled class="form-control" placeholder="  {{ $borrower->gender }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 row">
            <div class="form-group col">
                <strong>Address:</strong>
                <input type="text" name="address" disabled class="form-control" placeholder="  {{ $borrower->address }}">
            </div>



                <div class="form-group">
                <strong>Mobile :</strong>

                <input type="text" name="mobile_no" disabled class="form-control" placeholder="{{ $borrower->mobile_no }}">
                </div>

        </div>
         <div class="col-xs-12 col-sm-12 col-md-12 row">
            <div class="form-group col">
                <strong>Civil Status :</strong>
                <input type="text" name="civil_status" disabled class="form-control" placeholder="  {{ $borrower->civil_status }}">
            </div>
            </div>
        <div class="col-xs-12 col-sm-12 col-md-12 row">
            <div class="form-group col">
                <strong>Relationship :</strong>
                <input type="text" name="related_civil_status" disabled class="form-control" placeholder="  {{ $borrower->related_civil_status }}">
            </div>
            <div class="form-group col">
                <strong>Relation Name :</strong>
                <input type="text" name="h_g_name" disabled class="form-control" placeholder="  {{ $borrower->h_g_name }}">
            </div>
            </div>
             <div class="col-xs-12 col-sm-12 col-md-12 row">
            <div class="form-group col">
                <strong>Relation NIC :</strong>
                <input type="text" name="lp_no" disabled class="form-control" placeholder="  {{ $borrower->lp_no }}">
            </div>
             <div class="form-group col">
                <strong>Relation Contact Number:</strong>
                <input type="text" name="h_g_number" disabled class="form-control" placeholder="  {{ $borrower->h_g_number }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Remarks :</strong>
                <input type="text" name="remarks" disabled class="form-control" placeholder="{{ $borrower->remarks}}">
                </div>
            </div>
        </div>
        </div>
    </li>
</ul>
</form>
    </li>
        <div class="">
            <div class="card">
                <div   class="card-body">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Loan Details:</h2>
                    </div>

                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('approving.index') }}"> Back</a>
                    </div>
                </div>
            </div>
            <a href="#homess" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                Loan Details
            </a>
    <form action="">
        @csrf
        <ul class="collapse list-unstyled" id="homess">
                <li>
            <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 row">
                    <div class="form-group col">
                        <strong>Borrower No:</strong>
                        <input type="text" name="borrower_no" disabled class="form-control" placeholder=" {{ $loan->borrower_no }}">

                    </div>

                    <div class="form-group col">
                        <strong>Branch :</strong>

                        <input type="text" name="branch" disabled class="form-control" placeholder="{{ $loan->branch }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 row">
                    <div class="form-group col">
                        <strong>Center :</strong>

                        <input type="text" name="center" disabled class="form-control" placeholder="{{ $loan->center }}">
                    </div>

                    <div class="form-group col">
                        <strong>Group No :</strong>

                        <input type="text" name="group_no" disabled class="form-control" placeholder="{{ $loan->group_no }}">
                    </div>
                </div>
                {{-- <div class="col-xs-12 col-sm-12 col-md-12 row">
                    <div class="form-group col">
                        <strong>Loan Duration:(WEEKS)</strong>

                        <input type="text" name="" disabled class="form-control" placeholder="{{ $loan->loan_duration }}">
                    </div>

                    <div class="form-group col">
                        <strong>Duration Period:</strong>

                        <input type="text" name="duration_period" disabled class="form-control" placeholder="{{ $loan->duration_period }}">
                    </div>
                </div> --}}
                <div class="col-xs-12 col-sm-12 col-md-12 row">
                    <div class="form-group col">
                        <strong>Loan Stage :</strong>

                        <input type="text" name="loan_stage" disabled class="form-control" placeholder="{{ $loan->loan_stage }}">
                    </div>

                    <div class="form-group col">
                        <strong>Loan ReleaseDate :</strong>

                        <input type="text" name="release_date" disabled class="form-control" placeholder="  {{ $loan->release_date }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 row">
                    <div class="form-group col">
                        <strong>Loan Category:</strong>

                        <input type="text" name="loan_purpose" disabled class="form-control" placeholder="  {{ $loan->loan_purpose }}">
                    </div>

                    {{-- <div class="form-group col">
                        <strong>Interest Rate:</strong>

                        <input type="text" name="interest_rate" disabled class="form-control" placeholder="  {{ $loan->interest_rate }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 row">
                    <div class="form-group col">
                        <strong>Interesr:</strong>

                        <input type="text" name="loan_purpose" disabled class="form-control" placeholder="  {{ $loan->interest }}">
                    </div>

                    <div class="form-group col">
                        <strong>Installement:</strong>

                        <input type="text" name="interest_rate" disabled class="form-control" placeholder="  {{ $loan->instalment }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Status:</strong>

                        <input type="text" name="status" disabled class="form-control" placeholder="  {{ $loan->status }}">
                    </div> --}}
                </div>

                </div>
                </div>
                </div>
            </form>
        </li>
                <div class="">
                    <div class="card">
                        <div   class="card-body">

                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2> Appraisal Details:</h2>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('approving.index') }}"> Back</a>
                            </div>

                        </div>
                    </div>
                    <a href="#homesss" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                        Appraisal Details
                    </a>

                    <form action="">
                        @csrf
                    <ul class="collapse list-unstyled" id="homesss">
                        <li>

                    <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 row">
                            <div class="form-group col">
                                <strong>Borrower NIC:</strong>
                                <input type="text" name="borrower_nic" disabled class="form-control" placeholder="{{ $apprasails->borrower_nic }}">

                            </div>

                            <div class="form-group col">
                                <strong>Center :</strong>

                                <input type="text" name="center" disabled class="form-control" placeholder="{{ $apprasails->center }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 row">
                            <div class="form-group col">
                                <strong>Requesting Loan Amount :</strong>

                                <input type="text" name="reqesting_loan_amount" disabled class="form-control" placeholder="{{ $apprasails->reqesting_loan_amount }}">
                            </div>

                            <div class="form-group col">
                                <strong>loan Purpose :</strong>

                                <input type="text" name="loan_purpose" disabled class="form-control" placeholder="{{ $apprasails->loan_purpose }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 row">
                            <div class="form-group col">
                                <strong>Self Employee Total Earnings :</strong>

                                <input type="text" name="self_employee_total" disabled class="form-control" placeholder="{{ $apprasails->total_earn }}">
                            </div>

                            <div class="form-group col">
                                <strong>Self Employee Total Expenses :</strong>

                                <input type="text" name="self_employee_expenses" disabled class="form-control" placeholder="{{ $apprasails->total_expenses}}">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 row">
                            <div class="form-group col">
                                <strong>Foods :</strong>

                                <input type="text" name="total_earn" disabled class="form-control" placeholder="{{ $apprasails->foods }}">
                            </div>

                            <div class="form-group  col">
                                <strong>Hire Purchasign :</strong>

                                <input type="text" name="household_expenses" disabled class="form-control" placeholder="{{ $apprasails->hire_purchasing }}">
                            </div>
                            </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 row">
                            <div class="form-group col">
                                <strong>Accomadation :</strong>

                                <input type="text" name="accomadation" disabled class="form-control" placeholder="{{ $apprasails->accomadation }}">
                            </div>
                            <div class="form-group col">
                                <strong>Health :</strong>

                                <input type="text" name="health" disabled class="form-control" placeholder="{{ $apprasails->health }}">
                            </div>
                            </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 row">
                            <div class="form-group col">
                                <strong>Education :</strong>

                                <input type="text" name="education" disabled class="form-control" placeholder="{{ $apprasails->education }}">
                            </div>
                            <div class="form-group col">
                                <strong>Repayment :</strong>

                                <input type="text" name="repayment" disabled class="form-control" placeholder="{{ $apprasails->repayment }}">
                            </div>
                            </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 row">
                            <div class="form-group col">
                                <strong>Water Bill :</strong>

                                <input type="text" name="water_bill" disabled class="form-control" placeholder="{{ $apprasails->water_bill }}">
                            </div>
                            <div class="form-group col">
                                <strong>Electriity Bill :</strong>

                                <input type="text" name="electricity_bill" disabled class="form-control" placeholder="{{ $apprasails->electricity_bill }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 row">
                            <div class="form-group col">
                                <strong> :</strong>

                                <input type="text" name="budget_ratio_self_employee" disabled class="form-control" placeholder="{{ $apprasails->budget_ratio }}">
                            </div>
                            {{-- <div class="form-group col">
                                <strong>Ratio With Participation :</strong>

                                <input type="text" name="budget_ratio_with_participation" disabled class="form-control" placeholder="{{ $apprasails->budget_ratio_with_participation }}">
                            </div> --}}
                            {{-- <div class="form-group col">
                                <strong>Ratio With Salary :</strong>

                                <input type="text" name="budget_ratio_with_salary" disabled class="form-control" placeholder="{{ $apprasails->budget_ratio_with_salary }}">
                            </div> --}}
                        </div>
                        {{-- <div class="col-xs-12 col-sm-12 col-md-12 row">
                            <div class="form-group col">
                                <strong>Other Expenses:</strong>

                                <input type="text" name="other_expenses" disabled class="form-control" placeholder="{{ $apprasails->other_expenses }}">
                            </div>
                            <div class="form-group col">
                                <strong>As Guarantor Credit Facility Count :</strong>

                                <input type="text" name="as_borrower_facility_count" disabled class="form-control" placeholder="{{ $apprasails->as_borrower_facility_count }}">
                            </div>
                        </div> --}}
                        {{-- <div class="col-xs-12 col-sm-12 col-md-12 row">
                            <div class="form-group col">
                                <strong>Total Amount:</strong>

                                <input type="text" name="as_borrower_total_amount" disabled class="form-control" placeholder="{{ $apprasails->as_borrower_total_amount }}">
                            </div>

                            <div class="form-group col">
                                <strong>As Guarantor Credit Facility Count :</strong>

                                <input type="text" name="as_guarantor_facility_count" disabled class="form-control" placeholder="{{ $apprasails->as_guarantor_facility_count }}">
                            </div>
                        </div> --}}
                        {{-- <div class="col-xs-12 col-sm-12 col-md-12 row">
                            <div class="form-group col">
                                <strong>Total Amount:</strong>

                                <input type="text" name="as_guarantor_total_amount" disabled class="form-control" placeholder="{{ $apprasails->as_guarantor_total_amount }}">
                            </div>

                            <div class="form-group col">
                                <strong>Total Expenses :</strong>

                                <input type="text" name="total_expenses" disabled class="form-control" placeholder="{{ $apprasails->total_expenses }}">
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </form>
    </li>




    <div class="">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2> Earnings :</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('approving.index') }}"> Back</a>
                        </div>

                    </div>
                </div>
                    <a href="#homessss" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                       Self Employee | Earnings
                    </a>

                    <form action="">
                        @csrf
                    <ul class="collapse list-unstyled" id="homessss">
                        <li>

                   <div class="card animated fadeIn" style="width: 100%;">
    <div class="card-body">
        <table class="table table-bordered" id="data_table4">
            <thead>
            <tr>
                        <th>Product</th>
                        <th>Period</th>
                        <th>Unit Price</th>
                        <th>Total Units</th>
                        <th>Earn</th>
                    </tr>
        </thead>

        <tbody>
    @foreach ($earnings as $earning)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $earning->product_name }}</td>
                <td>{{ $earning->period }}</td>
                <td>{{ $earning->unit_price }}</td>
                <td>{{ $earning->total_units }}</td>

                <td>{{ $earning->earnings }}</td>



            </tr>
@endforeach
        </tbody>
        </table>
    </div>
</div>
                </div>
            </div>
        </form>
    </li>



    <div class="">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Expenses :</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('approving.index') }}"> Back</a>
                        </div>

                    </div>
                </div>
                    <a href="#homesssss" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                       Self Employee | Expenses
                    </a>

                    <form action="">
                        @csrf
                    <ul class="collapse list-unstyled" id="homesssss">
                        <li>

                   <div class="card animated fadeIn" style="width: 100%;">
    <div class="card-body">
        <table class="table table-bordered" id="data_table3">
            <thead>
             <tr>
                            <th>Product</th>

                            <th>Period</th>
                            <th>Buying Price</th>
                            <th>Total Units</th>
                            <th>Expenses</th>
                        </tr>
        </thead>

        <tbody>
    @foreach ($expenses as $expense)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $expense->product_name }}</td>
                <td>{{ $expense->period }}</td>
                <td>{{ $expense->buying_price }}</td>
                <td>{{ $expense->total_units }}</td>

                <td>{{ $expense->expenses }}</td>



            </tr>
@endforeach
        </tbody>
        </table>
    </div>
</div>
                </div>
            </div>
        </form>
    </li>




    <div class="">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Main Earnings:</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('approving.index') }}"> Back</a>
                        </div>

                    </div>
                </div>
                    <a href="#homessssss" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                       Income Relationship
                    </a>

                    <form action="">
                        @csrf
                    <ul class="collapse list-unstyled" id="homessssss">
                        <li>

                   <div class="card animated fadeIn" style="width: 100%;">
    <div class="card-body">
        <table class="table table-bordered" id="data_table2">
            <thead>
             <tr>
                            <th>Income Relationship</th>

                            <th>Income Value</th>
                            <th> Job Type</th>
                            <th>Salary</th>

                        </tr>
        </thead>

        <tbody>
    @foreach ($incomes as $income)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $income->income_relationship }}</td>
                <td>{{ $income->income }}</td>
                <td>{{ $income->job_type }}</td>
                <td>{{ $income->salary }}</td>





            </tr>
@endforeach
        </tbody>
        </table>
    </div>
</div>
                </div>
            </div>
        </form>
    </li>



    <div class="">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Other Loans:</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('approving.index') }}"> Back</a>
                        </div>

                    </div>
                </div>
                    <a href="#homesssssss" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                       Income Relationship
                    </a>

                    <form action="">
                        @csrf
                    <ul class="collapse list-unstyled" id="homesssssss">
                        <li>

                   <div class="card animated fadeIn" style="width: 100%;">
    <div class="card-body">
        <table class="table table-bordered" id="data_table1">
            <thead>
             <tr>
                            <th>Bank Name</th>

                            <th>Total Loan</th>
                            <th> Monthly Payment Price</th>

                        </tr>
        </thead>

        <tbody>
    @foreach ($otherLoans as $otherLoan)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $otherLoan->bank_name  }}</td>
                <td>{{ $otherLoan->total_loan }}</td>
                <td>{{ $otherLoan->monthly_paymentt }}</td>





            </tr>
@endforeach
        </tbody>
        </table>
    </div>
</div>
                </div>
            </div>
        </form>
    </li>


@endsection

@section('scripts')

@endsection
