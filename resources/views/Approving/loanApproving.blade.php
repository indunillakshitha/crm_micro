@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')




<div class="container">
    <div class="card">
        <div   class="card-body">

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Loan Details:</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="#"> Next</a>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="#"> Back</a>
            </div>
        </div>
    </div>
    
    
    
    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Borrower No:</strong>
                <input type="text" name="borrower_no" disabled class="form-control" placeholder=" {{ $loan->borrower_no }}">

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Loan Stage:</strong>

                <input type="text" name="loan_stage" disabled class="form-control" placeholder="{{ $loan->loan_stage }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Group No:</strong>

                <input type="text" name="loan_amount" disabled class="form-control" placeholder="{{ $loan->loan_amount }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Application No:</strong>

                <input type="text" name="release_date" disabled class="form-control" placeholder="{{ $loan->release_date }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Unique Id:</strong>

                <input type="text" name="duration_weeks" disabled class="form-control" placeholder="{{ $loan->duration_weeks }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Full Name:</strong>

                <input type="text" name="number_of_repayments" disabled class="form-control" placeholder="{{ $loan->number_of_repayments }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>NIC:</strong>
        
                <input type="text" name="loan_purpose" disabled class="form-control" placeholder="  {{ $loan->loan_purpose }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Birthday:</strong>
        
                <input type="text" name="interest_rate" disabled class="form-control" placeholder="  {{ $loan->interest_rate }}">
            </div>
        </div>
        
        </div>
        </div>
        </div>


        
@endsection

@section('scripts')

@endsection
