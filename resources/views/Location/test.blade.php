<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
</head>
<body>
  @csrf
  <button id="btn"> click</button>
  <button id="btn2"> click</button>

{{-- @section('scripts') --}}
<script type="text/javascript" >
    // import * as firebase from 'firebase'
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
    document.querySelector('#btn').addEventListener('click', (e)=>{

      console.log(e)
      $.ajax({
        type: 'GET',
        url: '{{('https://cantyapp-a3d23.firebaseio.com/')}}' ,
        success: function(data){
          console.log(data)
        },
        error: function(err){
          console.log(err)
        }
      })


      // Import Admin SDK
      // var admin = require("firebase-admin");

      // // Get a database reference to our posts
      // var db = admin.database();
      // var ref = db.ref("server/saving-data/fireblog/posts");

      // // Attach an asynchronous callback to read the data at our posts reference
      // ref.on("value", function(snapshot) {
      //   console.log(snapshot.val());
      // }, function (errorObject) {
      //   console.log("The read failed: " + errorObject.code);
      // });
    })
  </script>
{{-- @endsection --}}
</body>

</html>
