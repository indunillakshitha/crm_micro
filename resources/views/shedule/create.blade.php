@extends('layouts.master')
@section('title')

Shedule
@endsection
@section('content')



<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Create New Shedule</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('shedule.index') }}"> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="">
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Warning!</strong> Please check your input code<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="modal-body">
                <form action="{{ route('shedule.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group col-6">
                                <strong>Date</strong>
                                <select name="date"required class="form-control" id="expense_type" value="expense_type">
                                    <option value="0" selected="true">-Select-</option>
                                    <option value="monday">Monday</option>
                                    <option value="tuesday">Tuesday</option>
                                    <option value="wednesday">Wednesday</option>
                                    <option value="thursday">Thursday</option>
                                    <option value="friday">Friday</option>
                                    <option value="saturday">Saturday</option>
                                    <option value="sunday">Sunday</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group col-6">
                                <strong>Excecutive</strong>
                                <select name="excecutive_id"required class="form-control" id="expense_type" value="expense_type">
                                    <option value="0" selected="true">-Select-</option>
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group col-6">
                                <strong>Center</strong>
                                <select name="center_id"required class="form-control" id="expense_type" value="expense_type">
                                    <option value="0" selected="true">-Select-</option>
                                    @foreach($centers as $center)
                                    <option value="{{$center->center_no}}">{{$center->center_name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
    $('.date').datepicker({

                format: 'yyyy-mm-dd'

            });
</script>
@endsection
