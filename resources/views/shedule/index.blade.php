@extends('layouts.master')
@section('title')
    Center Schedule
@endsection
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Schedule</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{route('shedule.create')}}"> Add Schedule</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
{{--
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif --}}

<table class="table table-bordered" id="data_table">
    <thead>
    <tr>
        <th>No</th>
        <th>Day</th>
        <th>Center</th>
        <th>Excecutive</th>
        <th>Created</th>


        <th width="200px">Action</th>
    </tr>
</thead>

<tbody>
    @isset($shedules)
     @foreach ($shedules as $shedule)
    <tr>
        <td>{{ $shedule->id }}</td>
        <td>{{ $shedule->date }}</td>
        <td>{{ $shedule->center }}</td>
        <td>{{ $shedule->excecutive }}</td>
        <td>{{ $shedule->created_at }}</td>


        <td width="10px">
            <form action="{{ route('shedule.destroy',$shedule->id) }}" method="POST">

                <a class="btn btn-info" href="{{ route('shedule.show',$shedule->id) }}"><i class="fa fa-eye"></i></a>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Show</button>

                <a class="btn btn-primary" href="{{ route('shedule.edit',$shedule->id) }}"><i class="fa fa-bars"></i></a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>



            </form>
        </td>

    </tr>
    @endforeach
    @endisset
</tbody>
</table>

<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>

@endsection
