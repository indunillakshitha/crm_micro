@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')


    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Borrower</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('borrower.store') }}" method="POST" id="theform">
                    {{-- <form> --}}
                        @csrf
                        {{-- <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Branch:</label>
                            <input type="text" class="form-control" placeholder="" name="branch">
                        </div> --}}
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Branch Name :</label>
                       
                            {{-- <select name="branch" id="select-branch"  class="form-control">
                                @foreach($branches as $branch)
                                    <option value={{$branch->branch_name}}>
                                        {{$branch->branch_name}}
                                    </option>
                                @endforeach
                            </select> --}}
                        </div>
                        <input type="hidden" name="center" id="center_name">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Center Name :</label>
                            <select name="center_no" id="select_center" class="form-control">

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Group No:</label>
                            {{-- <input type="text" class="form-control" name="group_no"> --}}
                            <select name="group_no" class="form-control" id="group-select" >
                                <option value="1">Group 1</option>
                                <option value="2">Group 2</option>
                                <option value="3">Group 3</option>
                                <option value="4">Group 4</option>
                                <option value="5">Group 5</option>
                                <option value="6">Group 6</option>
                                <option value="7">Group 7</option>
                                <option value="8">Group 8</option>
                                <option value="9">Group 9</option>
                                <option value="10">Group 10</option>
                            </select>
                        </div>
                        {{-- <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Application No:</label>
                            <input type="text" class="form-control" name="application_no">
                        </div> --}}
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Unique Id:</label>
                            <input type="hidden" id="borrowersCount"  >
                            <input type="hidden" id="borrowersCountOfGroup"  >
                            <input type="text" class="form-control" name="borrower_no" id="uniId" readonly>
                            <br>
                            <a type="text" readonly id="id_status" value="" class="btn d-none white"> </a>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Full Name:</label>
                            <input type="text" class="form-control" required name="full_name" id="full_name"
                            oninput="toCap(this.value, this.id)">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Address:</label>
                            <input type="text" class="form-control" required name="address" id="address"
                            oninput="toCap(this.value, this.id)">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">NIC:</label>
                            <input type="text" class="form-control" name="nic" id="nic" oninput="genBday()">
                            <input type="text" readonly required id="nic_status" class="btn d-none white">
                            {{-- <button class="btn btn-danger" id="nic_status">aaadd</button> --}}
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Mobile Number:</label>
                            <input type="number" required class="form-control" name="mobile_no" id="mobile_no" max="10" min="10">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Birthday:</label>
                            <input type="text" class="form-control" name="birthday" id="bday" readonly>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Gender:</label>
                            <input type="text" required class="form-control" name="gender" id="gender" readonly>
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Civil Status:</label>
                            <select name="civil_status" id="civil_status" class="form-control">
                                <option value="Married">Married</option>
                                <option value="Unmarried">Unmarried</option>
                                <option value="Married">Engaged</option>
                                <option value="Married">Divorced</option>
                                <option value="Unmarried">Widow</option>
                                <option value="Unmarried">Ceperated</option>
                                <option value="Unmarried">Single</option>
                            </select>
                        </div>
                        {{-- <li> --}}
                            {{-- <a href="#homes" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle ">
                                Related to Civil Status
                            </a> --}}


                            {{-- <ul class="collapse list-unstyled" id="homes"> --}}
                                {{-- <li> --}}
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Relationship:</label>
                                        <select name="related_civil_status" id="related_civil_status"  class="form-control">
                                             <option value="Husband">Husband</option>
                                            <option value="Son">Son</option>
                                            <option value="Daughter">Daughter</option>
                                            <option value="Brother">Brother</option>
                                            <option value="Sister">Sister</option>
                                            <option value="Guardian">Guardian</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <label for="recipient-name" class="col-form-label">Name:</label>
                                            <input type="text" required class="form-control" name="h_g_name" id="h_g_name" oninput="toCap(this.value, this.id)">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Relation NIC</label>
                                            <input type="number" class="form-control" name="lp_no" id="lp_no">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Relation Contact Number:</label>
                                            <input type="number" class="form-control"  id="h_g_number" name="h_g_number">
                                        </div>
                                        {{-- <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Sallery:</label>
                                            <input type="text" class="form-control" name="h_g_sallery">
                                        </div> --}}
                                    </div>
                                {{-- </li> --}}

                            {{-- </ul> --}}

                        {{-- </li> --}}


                        {{-- <div class="form-group">
                            <label for="recipient-name" class="col-form-label">E mail:</label>
                            <input type="text" class="form-control" name="email">
                        </div> --}}
                        <div class="form-group">
                            {{-- <label for="recipient-name" class="col-form-label">Status</label> --}}
                            <input type="hidden" class="form-control" name="status" disabled value="active">
                        </div>
                        {{-- <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Loan Category:</label>
                            {{-- <input type="text" class="form-control" name="occupation"> --}}
                            {{-- <select name="occupation" class="form-control">
                                <option value="Self Employeed">Self Employeed</option>
                                <option value="Occupation">Occupation</option>
                            </select> --}}
                        {{-- </div>  --}}
                        <div class="form-group">
                            <label for="remarks" class="col-form-label">Remarks :</label>
                            {{-- <input type="text" class="form-control" name="occupation"> --}}
                            <select name="remarks" id="remarks" class="form-control">
                                <option value="None">None</option>
                                <option value="Center Leader">Center Leader</option>
                            </select>
                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit"  id="sub_btn" class="btn btn-primary">SUBMIT</button>
                        </div>

                        <div class="form-group bg-success d-none" id="suc_mess">
                            Borrower added successfully
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>





    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Borrowers</h2>

                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#exampleModal">Add New Borrower
                            </button>
                            {{--                            <a class="btn btn-success" href="{{ route('borrower.create') }}"> Add New Borrower</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="col-3">
                            <div class="">
                            <input type="text" name="s_nic" id="s_nic"class="form-control">
                            </div>

                        </div>
                        <div class="col-3">
                            <button type="button" class="btn btn-success" data-toggle="modal" onclick="getBorrowersByNic()">Search by NIC
                            </button ">
                        </div>
                    </div></div>
                    <br>
                        <div class="row">
                            <div class="col-lg-12 borrower_id">
                        <div class="col-3">
                            <select name="branch" id="branch"  class="form-control">
                                @foreach($branches as $branch)
                                    <option value={{$branch->branch_name}}>
                                        {{$branch->branch_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        {{-- <input type="hidden" name="center" id="center_name"> --}}
                        <div class="col-3">
                            <select name="cen" id="cen" class="form-control">

                            </select>
                        </div>
                        <div class="col-3">
                            {{-- <input type="text" class="form-control" name="group_no"> --}}
                            <select name="group" class="form-control" id="group" onclick="getBorrowers()">
                                <option value="1">Group 1</option>
                                <option value="2">Group 2</option>
                                <option value="3">Group 3</option>
                                <option value="4">Group 4</option>
                                <option value="5">Group 5</option>
                                <option value="5">Group 6</option>
                                <option value="5">Group 7</option>
                                <option value="5">Group 8</option>
                                <option value="5">Group 9</option>
                                <option value="5">Group 10</option>
                            </select>
                        </div>
                        <div class="">
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#exampleModal">Search
                            </button>
                            {{--                            <a class="btn btn-success" href="{{ route('borrower.create') }}"> Add New Borrower</a>--}}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="" id="borrowers">

    </div>

@endsection

@section('scripts')
<script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        document.querySelector('#sub_btn').addEventListener('click', e => {
            e.preventDefault()

            if(!document.querySelector('#uniId').value || !document.querySelector('#full_name').value){
                return  Swal.fire({
                title: 'Error!',
                text: 'Please fill all fields',
                icon: 'error',
                confirmButtonText: 'Okay'
                })
            }
            document.querySelector('#sub_btn').disabled = true

            $.ajax({
                type: 'POST',
                url: '{{('/addborrowerajax')}}',
                data: {
                    'branch' : document.querySelector('#select-branch').value,
                    'center' : document.querySelector('#center_name').value,
                    'group_no' : document.querySelector('#group-select').value,
                    'borrower_no' : document.querySelector('#uniId').value,
                    'full_name' : document.querySelector('#full_name').value,
                    'address' : document.querySelector('#address').value,
                    'nic' : document.querySelector('#nic').value,
                    'mobile_no' : document.querySelector('#mobile_no').value,
                    'birthday' : document.querySelector('#bday').value,
                    'gender' : document.querySelector('#gender').value,
                    'civil_status' : document.querySelector('#civil_status').value,
                    'related_civil_status' : document.querySelector('#related_civil_status').value,
                    'h_g_name' : document.querySelector('#h_g_name').value,
                    'lp_no' : document.querySelector('#lp_no').value,
                    'h_g_number' : document.querySelector('#h_g_number').value,
                    'remarks' : document.querySelector('#remarks').value,
                },
                success: function(data){
                    console.log(data);
                    // document.querySelector('#suc_mess').classList.remove('d-none')
                    // setTimeout(() => {
                    //     document.querySelector('#suc_mess').classList.add('d-none')
                    //     document.querySelector('#group').click()
                    //     document.querySelector('#sub_btn').disabled = false
                    // },2000)
                    Swal.fire({
                    title: 'Borrower Added',
                    text: 'Successfull',
                    icon: 'success',
                    timer: 2000
                    // confirmButtonText: 'Hako'
                    })
                    document.querySelector('#sub_btn').disabled = false
                    document.querySelector('#group-select').click()

                },
                // error:
            })

        })

        const branch = document.querySelector('#select-branch');
        const center = document.querySelector('#select_center');
        const center_name = document.querySelector('#center_name');
        const borrowersCount = document.querySelector('#borrowersCount');
        const borrowersCountOfGroup = document.querySelector('#borrowersCountOfGroup');

        const group = document.querySelector('#group-select');
        const uniId = document.querySelector('#uniId');

        // {{-- // setInterval(() => { --}}
        // {{-- //     genId(); --}}
        // {{-- // }, 50); --}}

        // ------------------------------------ get center accoring to branch------------------ --}}
        branch.addEventListener('click', () =>  {
            console.log(branch.value)
            $.ajax({
                type: 'GET',
                url : '{{('/getrightcenters')}}',
                data : { 'branch': branch.value},
                success: function(data){
                    // console.log(data)

                    center.innerHTML = `
                    <select name="center" id="select_center"  class="form-control"> </select>
                    `

                    data.forEach(record => {
                        console.log(record)

                        html = `
                        <option value="${record.center_no}">${record.center_name}</option>
                        `
                        center.innerHTML += html
                        // center.click()
                        // group.click()
                    })
                }
            })
         })

        //  update hidden center name
        center.addEventListener('click', (e) => {
            center_name.value = e.target.options[e.target.selectedIndex].text
            console.log(e.target.options[e.target.selectedIndex].text)
            // group.click()
            $.ajax({
                type: 'GET',
                url: '{{('/getborrowerscount')}}',
                data: {
                    'branch': branch.value,
                    'center': center_name.value,
                    'group': group.value,
                },
                success: function(data){
                    console.log(data)

                    borrowersCount.value = parseInt(data.length) + 1
                    // console.log(borrowersCount)
                    // genId()
                }
            })
        })

        const id_status = document.querySelector('#id_status');

        group.addEventListener('click', e => {
            $.ajax({
                type: 'GET',
                url: '{{('/getborrowerscountofgroup')}}',
                data: {
                    'branch': branch.value,
                    'center': center_name.value,
                    'group': group.value,
                },
                success: function(data){
                    console.log(data)

                    borrowersCountOfGroup.value = parseInt(data.length)
                    console.log(borrowersCountOfGroup)
                    console.log(borrowersCount)
                    if(borrowersCountOfGroup.value<5){
                        id_status.value = data;
                            console.log("available")
                            id_status.classList.remove('d-none')
                            id_status.classList.remove('btn-danger')
                            id_status.classList.add('btn-success')
                            id_status.textContent="Group Available"
                            id_status.href = null
                            genId()
                    }
                     else {
                            console.log("not available")
                            id_status.classList.remove('d-none')
                            id_status.classList.remove('btn-success')
                            id_status.classList.add('btn-danger')
                            id_status.textContent="Group Not Available"
                            id_status.target="_blank"
                            id_status.href = `/addborrower/deleteborrower/${branch.value}/${center_name.value}/${group.value}`
                            genId()

                        }
                    }


            })
        })

        // function genId(centerString) {

        function genId() {
            let branchString = `${branch.value}`
            branchString = branchString.slice(0, 3);
            branchString = branchString.toUpperCase();
            // console.log(branchString);

            let centerString = `00${center.value}`;
            // let centerString = this.centerString;
            centerString = centerString.slice(centerString.length - 3, centerString.length);

            let borrowerString = `00${borrowersCount.value}`;
            borrowerString = borrowerString.slice(borrowerString.length - 3, borrowerString.length);
            // let borrowerString = `00${10}`;
            // borrowerString = borrowerString.slice(borrowerString.length - 3, borrowerString.length);

            let groupString = `00${group.value}`;
            groupString = groupString.slice(groupString.length - 3, groupString.length);

            let id = `${branchString}${centerString}${groupString}${borrowerString}`
            console.log(id);
            return uniId.value = id;

        }

        // center.addEventListener('click', () => {
        //     $.ajax({
        //         type: 'GET',
        //         url: '{{('/getcentername')}}',
        //         data: {'name': center.value},
        //         success: function (data) {
        //             console.log(data, 'hakuna matata')
        //             centerString = `00${data}`;
        //             console.log(centerString)
        //             genId(centerString);
        //         }
        //     })
        // })






        // ----------------------------------UNIQUE ID ENDS--------------------------------------
        //
        //
        // ------------------------------------BDAY FROM NIC BEGINS----------------------------------

        const nic = document.querySelector('#nic')
        const bday = document.querySelector('#bday');
        const genderInput = document.querySelector('#gender')

        // setInterval(() => {
        // genBday();
        // }, 10000);


        // setInterval(() => {
        //     checkNIC();
        // }, 1000)


        function genBday() {

            let nicNo = nic.value;
            let dayText = 0;
            let year = '';
            let month = '';
            let day = '';
            let gender = '';

            if (nicNo.length !== 10 && nicNo.length !== 12) {
                console.log('invalid nic')
                bday.value = 'Invalid NIC'
                genderInput.value = 'Invalid NIC'

            // } else if(nicNo.length === 10 && !nicNo.isInteger(nicNo.substr(0,9))) {
            //     console.log('invalid nic')
            //     bday.value = 'Invalid NIC'
            //     genderInput.value = 'Invalid NIC'

            } else if(nicNo.length === 10 && nicNo[9] !== 'V') {
                console.log('invalid nic')
                bday.value = 'Use V in Uppercase'
                genderInput.value = 'Use V in Uppercase'
                console.log(nicNo[9])

            } else {

                // year
                if (nicNo.length === 10) {
                    year = "19" + nicNo.substr(0, 2);
                    dayText = parseInt(nicNo.substr(2, 3));
                } else {
                    year = nicNo.substr(0, 4);
                    dayText = parseInt(nicNo.substr(4, 3))
                }

                // gender
                if (dayText > 500) {
                    gender = "Female"
                    dayText = dayText - 500
                } else {
                    gender = "Male"
                }

                // day digit validation
                if (dayText < 1 && dayText > 366) {
                    console.log('invalid nic day')
                    bday.value = 'Invalid NIC'
                    genderInput.value = 'Invalid NIC'
                } else {

                    // month
                    if (dayText > 335) {
                        day = dayText - 355
                        month = "12"
                    } else if (dayText > 305) {
                        day = dayText - 305
                        month = "11"
                    } else if (dayText > 274) {
                        day = dayText - 274
                        month = "10"
                    } else if (dayText > 244) {
                        day = dayText - 244
                        month = "09"
                    } else if (dayText > 213) {
                        day = dayText - 213
                        month = "08"
                    } else if (dayText > 182) {
                        day = dayText - 182
                        month = "07"
                    } else if (dayText > 152) {
                        day = dayText - 152
                        month = "06"
                    } else if (dayText > 121) {
                        day = dayText - 121
                        month = "05"
                    } else if (dayText > 91) {
                        day = dayText - 91
                        month = "04"
                    } else if (dayText > 60) {
                        day = dayText - 60
                        month = "03"
                    } else if (dayText < 32) {
                        day = dayText
                        month = "01"
                    } else if (dayText > 31) {
                        day = dayText - 31
                        month = "02"
                    }


                    if (day < 10) {
                        day = `0${day}`
                    }

                    bday.value = `${year}-${month}-${day}`
                    genderInput.value = gender;

                    checkNIC();
                }
            }
        }

        const nic_status = document.querySelector('#nic_status');


        function checkNIC() {
            if (genderInput.value !== 'Invalid NIC' && bday.value !== 'Invalid NIC') {
                $.ajax({
                    type: 'POST',
                    url: '{{('/getnic')}}',
                    data: {'nic': nic.value},
                    success: function (data) {
                        console.log(data)
                        nic_status.value = data;
                        if (data === 'NIC already registered') {
                            nic_status.classList.remove('d-none')
                            nic_status.classList.remove('btn-success')
                            nic_status.classList.add('btn-danger')
                        } else if (data === 'NIC available') {
                            nic_status.classList.remove('d-none')
                            nic_status.classList.remove('btn-danger')
                            nic_status.classList.add('btn-success')
                        }
                    }
                })
            }
        }




        const b = document.querySelector('#branch')
        const c = document.querySelector('#cen')
        const g = document.querySelector('#group')
        const bo = document.querySelector('#borrowers')
        const n = document.querySelector('#s_nic')

        b.addEventListener('click', e=> {
                console.log(branch.value)
                $.ajax({
                    type: 'GET',
                    url : '{{('/addloan/getcenters')}}',
                    dataType: 'JSON',
                    data: {'branch': branch.value},
                    success: function(data){
                        console.log(data, 'centers here')

                        c.innerHTML = `
                        <select name="center" class="form-control" id="center"> </select>
                        `

                        data.forEach(record => {
                            html = `
                            <option id="${record.id}" value="${record.center_name}">${record.center_name}</option>
                            `

                            c.innerHTML += html
                            // borrower_no.value = ''

                        })
                        // center.click()
                    }
                })
            })

            function getBorrowers(){
               $.ajax({
                   type: 'GET',
                   url : '{{('/addloan/getborrowers')}}',
                   data: {
                       'branch': b.value,
                       'center': c.value,
                       'group': g.value,
                    },
                    success: function(data){
                        console.log(data)

                        bo.innerHTML = ``

                        data.forEach(record => {
                            html = `
                            <div class="animated fadeIn">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-12 borrower_id">
                                                <div class="col-4">
                                                    <h2>${record.full_name}</h2>
                                                </div>
                                                <div class="col-2">
                                                    <h2>${record.borrower_no}</h2>
                                                </div>
                                                <div class="col-2">
                                                    <h2>${record.nic}</h2>
                                                </div>
						<div class="col-2">
                                                    <h2>${record.mobile_no}</h2>
                                                </div>
                                                <div class="col-2">
                                                      <a href="/borrower/show/${record.borrower_no}"> <input  type="button" value="Show" class="btn btn-primary"></a>
                                                      <a href="/borrower/edit/${record.borrower_no}" target="_blank"> <input  type="button"  value="Edit" class="btn btn-primary"></a>

                                                      </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            `


                            bo.innerHTML += html

                        })
                        // borrower_no.click()
                    }
               })
           }
           function getBorrowersByNic(){
               console.log(n.value)
               $.ajax({
                   type: 'GET',
                   url : '{{('/addloan/getborrowersbynic')}}',
                   data: {
                       'nic': n.value,
                    },
                    success: function(data){
                        console.log(data)

                        // bo.innerHTML = `
                        // <select name="borrower_no" class="form-control" id="borrower_no" > </select>
                        // `

                        data.forEach(record => {
                            bo.innerHTML = `
                            <div class="animated fadeIn">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-12 borrower_id">
                                                <div class="col-3">
                                                    <h2>${record.full_name}</h2>
                                                </div>
                                                <div class="col-3">
                                                    <h2>${record.borrower_no}</h2>
                                                </div>
                                                <div class="col-3">
                                                    <h2>${record.mobile_no}</h2>
                                                </div>
                                                <div class="col-3">
                                                      <a href="/borrower/show/${record.borrower_no}"> <input  type="button" value="Show" class="btn btn-primary"></a>
                                                      <a href="/borrower/edit/${record.borrower_no}"> <input  type="button" value="Edit" class="btn btn-primary"></a>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            `

                            bo.innerHTML += html

                        })
                        // borrower_no.click()
                    }
               })
           }

    </script>



 {!! $borrowers->links() !!}
@endsection
