@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')
    <div class="card">
        <div class="card-header"><h2>Edit Repayment</h2></div>
        <div class="card-body">
        <form action="/editrepayments/updaterepayment/{{$repayment->id}}" method="POST">
                @csrf

                <div class="form-group">
                    <label for="" >NIC</label>
                    <input type="text" class="form-control" value={{$repayment->borrower_nic}} readonly>
                </div>
                <div class="form-group">
                    <label for="" >Paid Amount</label>
                    <input type="text" name="paid_amount" class="form-control" value={{$repayment->paid_amount}} >
                </div>
                <div class="form-group">
                    <label for="" >Collected Date</label>
                    <input type="date" name="date" class="form-control" >
                </div>

                <button type="submit" class="btn btn-success">Update Repayment</button>
            </form>
        </div>
    </div>
@endsection
