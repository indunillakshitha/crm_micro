@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Collect in</h2>

                    </div>
                    <div class="pull-right">

                        {{--                            <a class="btn btn-success" href="{{ route('borrower.create') }}">
                        Add New Borrower</a>--}}
                        <label for="">Select Center : </label>
                        <select name="" id="select_center">
                            @foreach($centers as $center)
                            <option value="{{$center->center_name}}">{{$center->center_name}}</option>
                            @endforeach
                        </select>
                        {{-- <label for="">Select Group : </label>
                            <select name="" id="select_group" >
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select> --}}
                        <a type="button" class="btn btn-success" data-toggle="modal" onclick="getLoans()"
                            data-target="#exampleModal">Get Loans
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <table class="table table-bordered" id="loans_table">
                <tr>
                    <th>NIC</th>
                    <th>Name</th>
                    <th>Borrower ID</th>
                    <th>Total Amount</th>
                    <th>Group No</th>
                    <th>Loan Date</th>
                    <th>Due Start Date</th>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let records_arr = [];

        function getLoans(){

            loans_table.innerHTML = `
                    <tr>
                        <th>NIC</th>
                        <th>Name</th>
                        <th>Borrower ID</th>
                        <th>Total Amount</th>
                        <th>Group No</th>
                        <th>Loan Date</th>
                        <th>Due Start Date</th>
                    </tr>
                    `

            $.ajax({
                type: 'POST',
                url: '{{('/temp/getloans')}}',
                data: {
                    'center': select_center.value,
                } ,
                success: function(data){
                    console.log(data);
                    if(data.length > 0){


                    data.forEach(record => {
                        html = `

                        <tr id='row_${record.id}'>
                        <td>${record.nic}</td>
                        <td>${record.full_name}</td>
                        <td>${record.borrower_no}</td>
                        <td>${record.loan_amount}</td>
                        <td>${record.group_no}</td>
                        <td> <input type="date" id="loan_date_${record.id}"></td>
                        <td><input type="date" id="due_start_date_${record.id}"></td>
                        <td><button class="btn btn-primary" id="${record.id}" onclick="issueLoan(this.id)">Issue</button></td>
                        </tr>

                        `
                        loans_table.innerHTML += html

                })

        }
    }
    })
}

        function issueLoan(id, index){




            console.log(document.querySelector(`#due_start_date_${id}`).value)
            $.ajax({
                type: 'POST',
                url: '{{('/temp/issueloans')}}',
                data: {
                    'id' : id,
                    'loan_date' : document.querySelector(`#loan_date_${id}`).value,
                    'due_start_date' : document.querySelector(`#due_start_date_${id}`).value,
                },
                success: function(data){
                    // console.log(data);
                    // return getLoans();
                    document.querySelector(`#row_${id}`).classList.add('d-none')
                    }

                })
            }



</script>
@endsection
