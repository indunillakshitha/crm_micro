@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Edit Repayments</h2>

                    </div>
                    <div class="pull-right">
                        <form action="/repayments/getbydate" method="POST">
                            @csrf
                            <label for="">Select Center : </label>
                            <select name="center" id="select_center">
                                @foreach($centers as $center)
                                <option value="{{$center->center_name}}">{{$center->center_name}}</option>
                                @endforeach
                            </select>
                            <label for="">Select Date : </label>
                            <input type="date" name="date">
                            {{-- <label for="">Select Group:</label>
                            <select name="group" id="select_group">
                                @for($i = 1 ; $i <= 10; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select> --}}
                            <button type="submit" class="btn btn-success" data-toggle="modal">Search</button>
                        </form>
                        {{-- <h4>Total Center Payments :  {{$total_center_payments}}</h4>
                        <h4>Total Group Payments :  {{$total_group_payments}}</h4> --}}
                        <h4>Total Payments :  {{$total_payments}}</h4>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <table class="table table-bordered" id="repayment_table">
        <tr>
            <th>NIC</th>
            <th>Borrower ID</th>
            <th>Payment Date</th>
            <th>Paid Amount</th>
            <th></th>
        </tr>

        @foreach ($repayments as $repayment)
            <tr>
                <th>{{ $repayment->borrower_nic }}</th>
                <th>{{ $repayment->borrower_id}}</th>
                <th>{{ $repayment->payment_date_full}}</th>
                <th>{{ $repayment->paid_amount }}</th>

                <form action="/repayment/edit/{{$repayment->id}}" method="get" target="_blank">
                    @csrf
                        <th>
                            <button type="submit" id="update_repay" name="submit" class="btn btn-info row-cols-sm-4"><i class="fa fa-pencil"> Edit</i></button>
                        </th>
                </form>


            </tr>
        @endforeach
    </table>

</div>
</div>
@endsection
