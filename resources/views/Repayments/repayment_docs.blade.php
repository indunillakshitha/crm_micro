@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

    <div class="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Repayments Docs</h2>

                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#exampleModal">Today Repayments
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    Branch : {{Auth::user()->branch}}
                </div>
                <div class="row">
                    <div class="form-group">
                        {{-- <label class="form-control" for="">Center</label> --}}
                        {{-- <select name="" id="select_branch" class="form-control"> --}}
                            @foreach($centers as $center)
                                {{-- <option value="{{$center->center_name}}">{{$center->center_name}}</option> --}}
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col"><label class="form-control" for="">{{$center->center_name}}</label>
                                        </div>
                                        <div class="col"><button class="btn btn-primary form-control">
                                            <a href="/repayments/{{Auth::user()->branch}}/{{$center->center_name}}">Print All</a>
                                        </button>
                                    </div>

                                    </div>

                                </div>
                            @endforeach
                        {{-- </select> --}}
                    {{-- </div> --}}

                    {{-- <button class="btn btn-primary">
                        <a href="/repayments/{{Auth::user()->branch}}/{{$center->center_name}}">Print All</a>
                    </button> --}}
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('scripts')

@endsection
