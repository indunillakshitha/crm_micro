@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

    <div class="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Office Collections</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <table class="table table-bordered" id="data_table">
           <thead>
            <tr>
                <th>NIC</th>
                <th>Center</th>
                <th>Group</th>
                <th>Total Amount</th>
                <th>Due Amount</th>
                <th>Payments Count</th>
                <th>Installment</th>
                <th>Loan Category</th>
                <th>Paying Amount</th>
                <th></th>

            </tr>
           </thead>

      <tbody>
        @foreach ($loans as $loan)
        <tr>
            <th>{{ $loan->nic }}</th>
            <th>{{ $loan->center }}</th>
            <th>{{ $loan->group_no }}</th>
            <th>{{ $loan->loan_amount }}</th>
            <th>{{ $loan->due }}</th>
            <th>{{ $loan->payments_count}}</th>
            <th>{{ $loan->instalment}}</th>
            <th>{{ $loan->loan_purpose}}</th>

            <form action="/repayment/update/{{$loan->id}}" method="PUT">
                @csrf
                <input type="hidden" value="{{$loan->id}}" id="loan_id">
                    <th><input type="number" name="paying_amount" id="paying_amount"></th>
                    <th>
                        <button type="submit" id="submit" class="btn btn-info row-cols-sm-4"><i class="fa fa-check"></i></button>
                        <a class="btn btn-info row-cols-sm-4"
                            href="{{ route('repayment.show',$loan->id) }}"><i class="fa fa-eye"></i></a>
                    </th>
            </form>


        </tr>
    @endforeach
      </tbody>

        </table>
    </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
             $('#data_table').DataTable();
        } );

    </script>
@endsection

@section('scripts')

<script>
     document.querySelector('#submit').addEventListener('click', e => {
            e.preventDefault()

            if(!document.querySelector('#paying_amount').value){
                return Swal.fire({
                            title: 'Add Paying Amount',
                            icon: 'error',
                            confirmButtonText: 'Okay'
                            })
            }

            $.ajax({
                type: 'GET',
                url: '{{('/repayments/centercollectionajax')}}',
                data: {
                    'id': document.querySelector('#loan_id').value,
                    'paying_amount': document.querySelector('#paying_amount').value,
                },
                success: function(data){
                    // console.log(data);
                    Swal.fire({
                            title: 'Print Documents',
                            // text: "You won't be able to revert this!",
                            icon: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Print'
                            })
                            // .then((result) => {
                            // if (result.value) {
                            //     Swal.fire(
                            //     'Successfull!',
                            //     // 'Documents printed success',
                            //     'success'
                            //     )
                            // }
                            // })
                            .then(() => {
                                return location.reload()
                            })
                }
            })
        })
</script>

@endsection
