@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Deposite</h2>

                    </div>
                    <div class="pull-right">
                        {{-- <button type="button" class="btn btn-success" data-toggle="modal"
                            data-target="#exampleModal">New Denomination
                        </button> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="col-3">
                        <div class="">
                        </div>

                    </div>
                    <div class="col-3">
                        </button>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="col-4">
                        <form id="form" method="post">
                            {{-- <input required type="date" name="date" id="date" oninput="console.log(this.value)"> --}}
                            <div class="row">
                                <input type="hidden" name="" id="cen_tot_id" value="{{$id}}">
                                <div class="col">
                                    <input type="text" name="user" id="user" value="{{$details[0]->user}}"
                                        class="form-control" oninput="console.log(this.value)">
                                    <input type="hidden" name="id" id="id" value="{{$details[0]->id}}"
                                        class="form-control">
                                </div>
                                <div class="col">
                                    <input type="date" name="date" value="{{$details[0]->date}}" id="date"
                                        class="form-control" oninput="console.log(this.value)">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-4 ml-3">
                    <div class="row">Need Rs. <input type="number" value="{{$details[0]->total_cash_available}}"
                            class="form-control" readonly id="collection">
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>



<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="">Slip Amount</label>
                        <input type="number" class="form-control" id="slip_amount">
                    </div>
                </div>
                <div class="col">
                    <br>
                    <button onclick="add_slip()" class="btn btn-success">Add Slip</button>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-4">
                    Deposited Rs. <input type="number" readonly class="form-control" name="available" id="available">
                </div>
            </div>
            <div class="row d-none" id="id_section">
                <hr>

                <div class="col-6">
                    {{-- <label for="">Identification Type</label> --}}

                    <div class="form-group">
                        <label for="">ATM Rejection</label>
                        <input type="number" class="form-control" id="atm_rejection">
                    </div>
                    <div class="form-group">
                        <label for="">Payments</label>
                        <input type="number" class="form-control" id="payments">
                    </div>
                    <div class="form-group">
                        <label for="">Loan Disbursment</label>
                        <input type="number" class="form-control" id="loan_disbursment">
                    </div>
                </div>

            </div>

            <br>
            <button type="button" class="btn btn-success" id="tot"
                onclick="check_tally( collection.value, available.value)">SAVE
            </button>

        </div>
    </div>

</div>
@endsection

@section('scripts')
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


           async function check_tally(collection, available){
            // console.log( collection, available, id_type, id_center);

                if(collection > available){
                    id_section.classList.remove('d-none')
                    console.log(available, collection);
                    if(
                        available !== '' &&
                        collection !== '' &&
                        atm_rejection.value !== '' &&
                        payments.value !== '' &&
                        loan_disbursment.value !== '' &&
                        loan_disbursment.value !== ''

                        ){
                        // sending named params
                        return add_delimination({collection, available})
                    }
                }

               if(parseInt(collection) == parseInt(available)){
                Swal.fire({title:'Collected',
                    // '',
                    icon:'success'
                }).then(() =>  add_delimination({collection, available}))

                // collection < available
            }
            // else {
            //         id_section.classList.remove('d-none')
            //         // console.log(id_type, id_center);
            //         if(id_type !== '' && id_center !== ''){
            //             // sending named params
            //             add_delimination({collection, available,  id_type, id_center})
            //         }

            // }
           }

           //accepting named params
           function add_delimination({collection, available}){
            //    return console.log(collection, available, id_type, id_center);

            $.ajax({
                type: 'GET',
                url: '{{('/adddeliminationdeposite')}}',
                data: {
                    'atm_tejection' : atm_rejection.value,
                    'payments' : payments.value,
                    'loan_disbursment' : loan_disbursment.value,
                    'denomination_center_totals_id' : loan_disbursment.value,
                    available,
                    'id' : cen_tot_id.value

                },
                success: function(data){
                    console.log(data);
                    Swal.fire('Delimination Added').then(() => location.reload())
                    }

            })

           }

    function add_slip(){
        $.ajax({
                type: 'GET',
                url: '{{('/add_slip')}}',
                data: {
                    'slip_amount' : slip_amount.value,
                    'denomination_id' : cen_tot_id.value

                },
                success: function(data){
                    console.log(data);
                    available.value = data
                    return Swal.fire('Slip Added')
                    }

            })
    }



</script>
@endsection
