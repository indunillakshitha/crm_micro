@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Denomination</h2>

                    </div>
                    <div class="pull-right">
                        <button type="button" class="btn btn-success" data-toggle="modal"
                            data-target="#exampleModal">New Denomination
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="col-3">
                        <div class="">
                        </div>

                    </div>
                    <div class="col-3">
                        </button>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    {{-- <div class="col-3">

                    </div> --}}
                    <div class="col-4">
                        <select name="user" id="user" class="form-control" onclick="getTotal(this.value)">
                            @foreach($users as $user)
                            <option value={{$user->name}}>
                                {{$user->name}}
                            </option>
                            @endforeach
                        </select>
                        <div class="row mt-3" id="results">
                        </div>
                    </div>
                    <div class="col-4">
                        <button type="button" class="btn btn-success" id="tot" onclick="check_tally( collection.value, available.value)" >COLLECTION
                        </button>
                        <a href="/dprint"  class="btn btn-success"  >PRINT
                        </a>

                    </div>
                    <div class="col-4">
                        <div class="row"> Rs. <input type="number" class="form-control"  readonly id="collection">
                        </div>
                        <div class="row"> Rs. <input type="text" class="form-control" readonly name="available" id="available">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="" id="borrowers">

</div>
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">5000</label>
                    <input type="number" class="form-control" name="lp_no" id="n5000" oninput="cal_notes()">
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">1000</label>
                    <input type="number" class="form-control" name="lp_no" id="n1000"
                    oninput="cal_notes()"
                    >
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">500</label>
                    <input type="number" class="form-control" name="lp_no" id="n500"
                    oninput="cal_notes()"
                    >
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">100</label>
                    <input type="number" class="form-control" name="lp_no" id="n100"
                    oninput="cal_notes()"
                    >
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">50</label>
                    <input type="number" class="form-control" name="lp_no" id="n50"
                    oninput="cal_notes()"
                    >
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">20</label>
                    <input type="number" class="form-control" name="lp_no" id="n20"
                    oninput="cal_notes()"
                    >
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">10</label>
                    <input type="number" class="form-control" name="lp_no" id="n10"
                    oninput="cal_notes()"
                    >
                </div>
            </div>

        </div>
    </div>

</div>
</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let del_data;


            function getTotal(x){
            //    console.log(x);
            results.innerHTML = ''
            let results_total = 0
               $.ajax({
                   type: 'GET',
                   url : '{{('/totalcoll')}}',
                   data: {
                       'user': x,
                    },
                    success: function(data){
                        del_data = data
                        console.log(data);
                        // document.getElementById('collection').value=data.

                        data.forEach(r => {
                            html = `
                            ${r.center} <input type="number" class="form-control"  readonly value="${r.sum}">
                            `
                            results.innerHTML += html
                            results_total += parseFloat(r.sum)

                        })
                        collection.value = results_total


                    }
               })
           }

           function cal_notes(){
               let v5000 = parseInt(n5000.value)*5000
               let v1000 = parseInt(n1000.value)*1000
               let v500 = parseInt(n500.value)*500
               let v100 = parseInt(n100.value)*100
               let v50 = parseInt(n50.value)*50
               let v20 = parseInt(n20.value)*20
               let v10 = parseInt(n10.value)*10

               v5000 = n5000.value.length == 0 ? 0 : v5000
               v1000 = n1000.value.length == 0 ? 0 : v1000
               v500 = n500.value.length == 0 ? 0 : v500
               v100 = n100.value.length == 0 ? 0 : v100
               v50 = n50.value.length == 0 ? 0 : v50
               v20 = n20.value.length == 0 ? 0 : v20
               v10 = n10.value.length == 0 ? 0 : v10

               let total = v5000+v1000+v500+v100+v50+v20+v10
                //   console.log(total);
                available.value = total
           }

           async function check_tally(collection, available){
               if(parseInt(collection) == parseInt(available)){
                Swal.fire({title:'Collected',
                    // '',
                    icon:'success'
                }).then(() =>  add_delimination(collection, available))

                } else {
                    const { value: text } = await Swal.fire({
                        inputLabel: 'Message',
                        input: 'textarea',
                        inputPlaceholder: 'Type the reson here',
                        inputAttributes: {
                            'aria-label': 'Type your message here'
                        },
                        showCancelButton: false,
                        allowOutsideClick : false,
                        })

                        if (text) {
                        Swal.fire(text).then(() =>  add_delimination(collection, available, text))
                        } else {
                            check_tally(collection, available)
                        }

            }
           }

           function add_delimination(collection, available, text=null){
               console.log(123);
            //    console.log(text);
            $.ajax({
                type: 'POST',
                url: '{{('/adddelimination')}}',
                data: {
                    'total_to_be' : collection,
                    'total_got' : available,
                    'remarks' : text,
                    '5000' : n5000.value,
                    '1000' : n1000.value,
                    '500' : n500.value,
                    '100' : n100.value,
                    '50' : n50.value,
                    '20' : n20.value,
                    '10' : n10.value,
                    'agent' : user.value,

                },
                success: function(data){
                    console.log(data);
                    // return call_print()
                    // return getLoans();
                    // document.querySelector(`#row_${id}`).classList.add('d-none')
                    }

            })

           }

           function call_print(){
            //    return window.location = `/dprint/${del_data}`
            $.ajax({
                type: 'POST',
                url: '{{('/dprint')}}',
                data: {
                    del_data
                },
                success: function(data){
                    console.log(data);
                    return call_print()
                    // return getLoans();
                    // document.querySelector(`#row_${id}`).classList.add('d-none')
                    }

            })
           }

</script>



@endsection
