<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
        content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords"
        content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>Invoice Template - Modern Admin - Clean Bootstrap 4 Dashboard HTML Template + Bitcoin Dashboard</title>
    <link rel="apple-touch-icon" href="{{asset('material_sup/app-assets/images/ico/apple-icon-120.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('material_sup/app-assets/images/ico/favicon.ico')}}">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700"
        rel="stylesheet">
    <link rel="stylesheet" type="text/css"
        href="{{asset('material_sup/app-assets/fonts/material-icons/material-icons.css')}}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css"
        href="{{asset('material_sup/app-assets/vendors/css/material-vendors.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('material_sup/app-assets/css/material.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('material_sup/app-assets/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('material_sup/app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('material_sup/app-assets/css/material-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('material_sup/app-assets/css/material-colors.css')}}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
        href="{{asset('material_sup/app-assets/css/core/menu/menu-types/material-vertical-menu-modern.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('material_sup/app-assets/css/pages/invoice.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('material_sup/assets/css/style.css')}}">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern material-vertical-layout material-layout  " data-open="click"
    data-menu="vertical-menu-modern" data-col="2-columns">


    <!-- BEGIN: Content-->
    <div class="app-content content">

        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section class="card">
                    <div id="invoice-template" class="card-body p-4">
                        <!-- Invoice Company Details -->
                        <div id="invoice-company-details" class="row">
                            <div class="col-sm-6 col-12 text-center text-sm-left">
                                <div class="media row">
                                    <div class="col-12 col-sm-3 col-xl-2">
                                        {{-- <img src="{{asset('material_sup/app-assets/images/logo/logo-80x80.png')}}"
                                        alt="company logo" class="mb-1 mb-sm-0" /> --}}
                                    </div>
                                    <div class="col-12 col-sm-9 col-xl-10">
                                        <div class="media-body">
                                            <ul class="ml-2 px-0 list-unstyled">
                                                <li class="text-bold-800">Canty International</li>
                                                <li>Kesbawa</li>
                                                {{-- <li>Melbourne,</li> --}}
                                                {{-- <li>Florida 32940,</li>
                                                <li>USA</li> --}}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-12 text-center text-sm-right">
                                <h2>Denomination</h2>
                                {{-- <p class="pb-sm-3"># INV-001001</p> --}}
                                <ul class="px-0 list-unstyled">
                                    {{-- <li>Balance Due</li> --}}
                                    {{-- <li class="lead text-bold-800">$12,000.00</li> --}}
                                </ul>
                            </div>
                        </div>
                        <!-- Invoice Company Details -->

                        <!-- Invoice Customer Details -->
                        <div id="invoice-customer-details" class="row pt-2">
                            <div class="col-12 text-center text-sm-left">
                                {{-- <p class="text-muted">Bill To</p> --}}
                            </div>
                            <div class="col-sm-6 col-12 text-center text-sm-left">
                                <ul class="px-0 list-unstyled">
                                    {{-- <li class="text-bold-800">Mr. Bret Lezama</li> --}}
                                    {{-- <li>4879 Westfall Avenue,</li> --}}
                                    {{-- <li>Albuquerque,</li> --}}
                                    {{-- <li>New Mexico-87102.</li> --}}
                                </ul>
                            </div>
                            <div class="col-sm-6 col-12 text-center text-sm-right">
                                <p><span class="text-muted"> Date :</span> {{$dcenter_total[0]->date}}</p>
                                {{-- <p><span class="text-muted">Terms :</span> Due on Receipt</p> --}}
                                {{-- <p><span class="text-muted">Due Date :</span> 10/05/2019</p> --}}
                            </div>
                        </div>
                        <!-- Invoice Customer Details -->

                        <!-- Invoice Items Details -->
                        <div id="invoice-items-details" class="pt-2">
                            <div class="row">
                                <div class="table-responsive col-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Center</th>
                                                <th>Center Collection</th>
                                                <th>Center Collection</th>
                                                <th>N/P Collection</th>
                                                <th>Document Charge</th>
                                                <th>Other</th>
                                                <th>Total</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $grandTotal=0 ?>
                                            @isset($repayments)
                                            @foreach ($repayments as $repayment)
                                            <tr>
                                                <th>{{$repayment->center_no}}</th>
                                                <th>{{$repayment->center}}</th>
                                                <th>{{$repayment->total}}</th>
                                                <th>{{$repayment->total_not_paid}}</th>
                                                <th>{{$repayment->total_doc}}</th>
                                                <th></th>
                                                <th><?php echo $repayment->total+$repayment->total_not_paid +$repayment->total_doc?>
                                                </th>
                                                <?php $grandTotal+= $repayment->total+$repayment->total_not_paid +$repayment->total_doc ?>
                                            </tr>
                                            @endforeach
                                            @endisset
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th><b>Grand Total</b></th>
                                                <th></th>
                                                <th></th>
                                                <th>{{$grandTotal}}</th>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p><b>Cash Structure</b></p>
                                                    {{-- <p class="text-muted">Simply dummy text of the printing and typesetting industry. --}}
                                                    </p>
                                                </td>
                                                {{-- <td class="text-right">$20.00/hr</td> --}}
                                                {{-- <td class="text-right">120</td> --}}
                                                <td class="text-right"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p>5000</p>
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"5000"})}}
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"5000"})*5000}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p>1000</p>
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"1000"})}}
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"1000"})*1000}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p>500</p>
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"500"})}}
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"500"})*500}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p>100</p>
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"100"})}}
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"100"})*5000}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p>50</p>
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"50"})}}
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"50"})*50}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p>20</p>
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"20"})}}
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"20"})*20}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p>10</p>
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"10"})}}
                                                </td>
                                                <td class="text-right">
                                                    {{($cash_structure[0]->{"10"})*10}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <p><b>Total</b></p>
                                                </td>
                                                <td></td>
                                                <td class="text-right">
                                                    {{$cash_structure[0]->total_got}}
                                                </td>
                                            </tr>

                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p><b>Banking Slip Amount</b></p>
                                                </td>
                                                {{-- <td class="text-right">$20.00/hr</td> --}}
                                                {{-- <td class="text-right">120</td> --}}
                                                {{-- <td class="text-right">$2400.00</td> --}}
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p>Variance</p>
                                                </td>
                                                {{-- <td class="text-right">$20.00/hr</td> --}}
                                                {{-- <td class="text-right">120</td> --}}
                                                <td class="text-right">{{$dcenter_total[0]->varians_avail_n_deposited}}
                                                </td>
                                            </tr>
                                            @isset($deposite)

                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p>ATM Rejection</p>
                                                </td>
                                                {<td class="text-right">{{$deposite[0]->atm_rejection}}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p>Payment</p>
                                                </td>
                                                {{-- <td class="text-right">$20.00/hr</td> --}}
                                                {{-- <td class="text-right">120</td> --}}
                                                <td class="text-right">{{$deposite[0]->payments}}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <p>Loan Disbursment</p>
                                                </td>
                                                {{-- <td class="text-right">$20.00/hr</td> --}}
                                                {{-- <td class="text-right">120</td> --}}
                                                <td class="text-right">{{$deposite[0]->loan_disbursment}}</td>
                                            </tr>
                                            @endisset


                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <!-- Invoice Footer -->
                        <div id="invoice-footer">
                            <div class="row">
                                <div class="col-sm-7 col-12 text-center text-sm-left">
                                    {{-- <h6>Terms & Condition</h6>
                                    <p>Test pilot isn't always the healthiest business.</p> --}}
                                </div>
                                <div class="col-sm-5 col-12 text-center">
                                    <button type="button" class="btn btn-info btn-print btn-lg my-1"><i
                                            class="la la-paper-plane-o mr-50"></i>
                                        Print
                                        Invoice</button>
                                </div>
                            </div>
                        </div>
                        <!-- Invoice Footer -->

                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>




    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('material_sup/app-assets/vendors/js/material-vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('material_sup/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('material_sup/app-assets/js/core/app.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('material_sup/app-assets/js/scripts/pages/material-app.js')}}"></script>
    <script src="{{asset('material_sup/app-assets/js/scripts/pages/invoice-template.js')}}"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
