@extends('layouts.master')
@section('title')


@endsection
@section('content')
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="pull-left">
                        <h2>Pending Deposites</h2>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="card animated fadeIn" style="width: 100%;">
    <div class="card-body">
        <table class="table table-bordered" id="data_table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Branch</th>
                    <th>Executive</th>
                    <th>Total Collection</th>
                    <th>Available</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($pending as $pendin)
                <tr>
                    <td>{{ $pendin->id}}</td>
                    <td>{{ $pendin->branch}}</td>
                    <?php $agent= Illuminate\Support\Facades\DB::table('users')->where('id',$pendin->agent_id)->get()?>
                    <td>
                        {{ $agent[0]->name}}</td>
                    <td>{{ $pendin->total_cash_collected }}</td>
                    <td>{{ $pendin->total_cash_available }}</td>
                    <td>{{ $pendin->created_at }}</td>
                    <td><a href="/denominatiionprint/{{$pendin->id}}" class="btn btn-primary">PRINT</a></td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>

@endsection
