@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Denomination</h2>

                    </div>
                    <div class="pull-right">
                        <button type="button" class="btn btn-success" data-toggle="modal"
                            data-target="#exampleModal">New Denomination
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="col-3">
                        <div class="">
                        </div>

                    </div>
                    <div class="col-3">
                        </button>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12 borrower_id">

                    <div class="col-4">
                        <form id="form" method="post">
                            {{-- <input required type="date" name="date" id="date" oninput="console.log(this.value)"> --}}
                            <div class="row">
                                <div class="col">
                                    <select name="user" id="user" class="form-control">
                                        @foreach($users as $user)
                                        <option value={{$user->id}}>
                                            {{$user->name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <input required type="date" name="date" id="date" oninput="console.log(this.value)">
                                </div>
                                <div class="col">
                                    <a onclick="get_loans()" class="btn btn-success" data-toggle="modal"
                                        data-target="#exampleModal">VIEW
                                    </a>
                                </div>
                            </div>
                        </form>
                        <div class="row mt-3" id="results">
                        </div>
                    </div>
                    <div class="col-4">
                        {{-- <a class="btn btn-success" id="tot"
                            onclick="check_tally( collection.value, available.value)">COLLECTION
                        </a>
                        <a href="/dprint" class="btn btn-success">PRINT
                        </a> --}}

                    </div>
                    <div class="col-4">
                        <div class="row"> Rs. <input type="number" class="form-control" readonly id="collection">
                        </div>
                        <div class="row"> Rs. <input type="text" class="form-control" readonly name="available"
                                id="available">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="">

    <div class="card">
        <table id="loans_table" class="table table-bordered" id="loans_table">
            <tr>
                <th>Center No</th>
                <th>Center</th>
                <th>Collection</th>
                <th>Not Paid </th>
                <th>Document Charges</th>
                <th>Total</th>
            </tr>
        </table>
    </div>
</div>

<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">5000</label>
                    <input type="number" class="form-control" name="lp_no" id="n5000" oninput="cal_notes()">
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">1000</label>
                    <input type="number" class="form-control" name="lp_no" id="n1000" oninput="cal_notes()">
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">500</label>
                    <input type="number" class="form-control" name="lp_no" id="n500" oninput="cal_notes()">
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">100</label>
                    <input type="number" class="form-control" name="lp_no" id="n100" oninput="cal_notes()">
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">50</label>
                    <input type="number" class="form-control" name="lp_no" id="n50" oninput="cal_notes()">
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">20</label>
                    <input type="number" class="form-control" name="lp_no" id="n20" oninput="cal_notes()">
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <label for="recipient-name" class="col-form-label">10</label>
                    <input type="number" class="form-control" name="lp_no" id="n10" oninput="cal_notes()">
                </div>



            </div>

            <div class="row d-none" id="id_section">
                <hr>
                <div class="col-6">
                    <label for="">Identification Type</label>
                    <select name="" id="id_type" class="form-control">
                        <option value="">Select Identification Type</option>
                        <option value="IDENTIFIED">IDENTIFIED</option>
                        <option value="UNIDENTIFIED">UNIDENTIFIED</option>
                    </select>
                </div>
                <div class="col-6">
                    <label for="">Center</label>
                    @php
                    $centers = Illuminate\Support\Facades\DB::table('centers')->where('branch_no',
                    Auth::user()->branch)->get();
                    @endphp
                    <select name="" id="id_center" class="form-control">
                        <option value="">Select Center</option>
                        @foreach ($centers as $item)
                        <option value="{{$item->center_name}}">{{$item->center_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <br>
            <button type="button" class="btn btn-success" id="tot"
                onclick="check_tally( collection.value, available.value, id_type.value, id_center.value)">SAVE
            </button>

        </div>
    </div>

</div>
@endsection

@section('scripts')
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function cal_notes(){
               let v5000 = parseInt(n5000.value)*5000
               let v1000 = parseInt(n1000.value)*1000
               let v500 = parseInt(n500.value)*500
               let v100 = parseInt(n100.value)*100
               let v50 = parseInt(n50.value)*50
               let v20 = parseInt(n20.value)*20
               let v10 = parseInt(n10.value)*10

               v5000 = n5000.value.length == 0 ? 0 : v5000
               v1000 = n1000.value.length == 0 ? 0 : v1000
               v500 = n500.value.length == 0 ? 0 : v500
               v100 = n100.value.length == 0 ? 0 : v100
               v50 = n50.value.length == 0 ? 0 : v50
               v20 = n20.value.length == 0 ? 0 : v20
               v10 = n10.value.length == 0 ? 0 : v10

               let total = v5000+v1000+v500+v100+v50+v20+v10
                //   console.log(total);
                available.value = total
           }

           async function check_tally(collection, available, id_type, id_center){
            // console.log( collection, available, id_type, id_center);

                if(collection > available){
                    return Swal.fire('Please Tally Available Money')
                }

               if(parseInt(collection) == parseInt(available)){
                Swal.fire({title:'Collected',
                    // '',
                    icon:'success'
                }).then(() =>  add_delimination({collection, available}))

                // collection < available
            } else {
                    // const { value: text } = await Swal.fire({
                    //     inputLabel: 'Message',
                    //     input: 'textarea',
                    //     inputPlaceholder: 'Type the reson here',
                    //     inputAttributes: {
                    //         'aria-label': 'Type your message here'
                    //     },
                    //     showCancelButton: false,
                    //     allowOutsideClick : false,
                    //     })

                    //     if (text) {
                    //     Swal.fire(text).then(() =>  add_delimination(collection, available, text))
                    //     } else {
                    //         check_tally(collection, available)
                    //     }

                    id_section.classList.remove('d-none')
                    // console.log(id_type, id_center);
                    if(id_type !== '' && id_center !== ''){
                        // sending named params
                        add_delimination({collection, available,  id_type, id_center})
                    }

            }
           }

           //accepting named params
           function add_delimination({collection, available, text=null, id_type=null, id_center=null}){
            //    return console.log(collection, available, id_type, id_center);
            $.ajax({
                type: 'GET',
                url: '{{('/adddelimination')}}',
                data: {
                    'total_to_be' : collection,
                    'total_got' : available,
                    'remarks' : text,
                    '5000' : n5000.value,
                    '1000' : n1000.value,
                    '500' : n500.value,
                    '100' : n100.value,
                    '50' : n50.value,
                    '20' : n20.value,
                    '10' : n10.value,
                    'agent' : user.value,
                    id_type,
                    id_center,
                    'date':date.value,
                    'user':user.value

                },
                success: function(data){
                    console.log(data);
                    Swal.fire('Delimination Added').then(() => location.reload())
                    // return call_print()
                    // return getLoans();
                    // document.querySelector(`#row_${id}`).classList.add('d-none')
                    }

            })

           }

        function get_loans(){
            loans_table.innerHTML = `
                    <tr>
                        <th>Center No</th>
                        <th>Center</th>
                        <th>Date</th>
                        <th>Collection</th>
                        <th>Not Paid </th>
                        <th>Document Charges</th>
                        <th>Total</th>
                    </tr>
                     `

            $.ajax({
                type: 'POST',
                url: '{{('/totalcoll')}}',
                data: new FormData(form) ,
                processData: false,
                contentType: false,
                success: function(data){
                    console.log(data);
                    return show_data(data)
                }
            })

        }

        let show_paid = 0
        let show_doc_paid = 0

        function show_data(data){
            let count = 0

            let paid = 0
            let grandTotal=0
            data.forEach(d => {

                html = `
                <tr>
                    <th>${d.center_no}</th>
                    <th>${d.center}</th>
                    <th>${d.payment_date}</th>
                    <th>${d.total}</th>
                    <th>${d.total_not_paid}</th>
                    <th>${d.total_doc}</th>
                    <th>${parseFloat(d.total_doc)+parseFloat(d.total_not_paid)+parseFloat(d.total)}</th>
                </tr>

                `
                grandTotal=grandTotal+parseFloat(d.total_doc)+parseFloat(d.total_not_paid)+parseFloat(d.total)

                loans_table.innerHTML += html
                count++

            })

            html = `
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Grand Total</th>
                    <th>${grandTotal}</th>
                </tr>
                `
                loans_table.innerHTML += html
                collection.value=grandTotal


            }
</script>
@endsection
