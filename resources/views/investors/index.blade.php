@extends('layouts.master')
@section('title')

    Investores
@endsection
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Investores</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('investors.create') }}"> Add Investores</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
{{--
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif --}}

<table class="table table-bordered" id="data_table">
    <thead>
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>NIC</th>
        <th>Addresst</th>
        <th>Designation</th>
        <th>Joined</th>


        <th width="200px">Action</th>
    </tr>
</thead>

<tbody>
    @isset($investors)
     @foreach ($investors as $investor)
    <tr>
        <td>{{ $investor->id }}</td>
        <td>{{ $investor->title }}{{ $investor->name }}</td>
        <td>{{ $investor->nic }}</td>
        <td>{{ $investor->address }}</td>
        <td>{{ $investor->designation }}</td>
        <td>{{ $investor->join_date }}</td>

        <td width="10px">
            <form action="{{ route('investors.destroy',$investor->id) }}" method="POST">

                <a class="btn btn-info" href="{{ route('investors.show',$investor->id) }}"><i class="fa fa-eye"></i></a>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Show</button>

                <a class="btn btn-primary" href="{{ route('investors.edit',$investor->id) }}"><i class="fa fa-bars"></i></a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>



            </form>
        </td>

    </tr>
    @endforeach
    @endisset
</tbody>
</table>

<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>

@endsection
