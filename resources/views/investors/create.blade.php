@extends('layouts.master')
@section('title')

Investor
@endsection
@section('content')



<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Create New investor</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('investors.index') }}"> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="">
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Warning!</strong> Please check your input code<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="modal-body">
                <form action="{{ route('investors.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <strong>Name</strong>
                            <div class="row">
                                <div class="col-1">
                                    <select name="title" class="form-control" id="expense_type" required value="expense_type">
                                        <option value="Mr.">Mr.</option>
                                        <option value="Mr.">Mrs.</option>
                                        <option value="Mr.">Ms.</option>
                                    </select>
                                </div>
                                <div class="col">
                                    <input type="text" name="name" class="form-control"
                                    placeholder="Investor Name" required id="ff" oninput="toCap(this.value, this.id)">
                                </div>
                            </div>

                        </div>


                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Nic</strong>
                                <input type="text" name="nic" id="nic" class="form-control" oninput="validate_nic(this.value, this)"
                                    placeholder="NIC" >
                                    <input type="text" readonly required id="nic_status" class="btn d-none white">

                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group col-6">
                                <strong>Address</strong>
                                <input class="date form-control"required type="text" name="address" id="expense_date" oninput="toCap(this.value, this.id)">
                                {{-- <input type="text" name="expense_date" class="form-control" placeholder="2020-12-12"> --}}
                            </div>
                            <div class="form-group col-6">
                                <strong>Designation</strong>
                                <select name="designation" class="form-control" id="expense_type" value="expense_type">
                                    <option value="0" selected="true">-Select-</option>
                                    @foreach($designnations as $designnation)
                                    <option value="{{$designnation->id}}">{{$designnation->designation}}
                                    </option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Join Date:</strong>
                                <input type="date" name="join_date" class="form-control"
                                    placeholder="Ledger Account"required id="ledger_account">
                            </div>
                        </div>


                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
    $('.date').datepicker({

                format: 'yyyy-mm-dd'

            });
</script>
@endsection
