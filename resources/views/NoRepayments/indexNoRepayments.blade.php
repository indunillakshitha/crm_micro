@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

    <div class="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>No Repayments</h2>

                        </div>
                        <!-- <div class="pull-right">
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#exampleModal">Today Repayments
                            </button>
                            <!-- {{--                            <a class="btn btn-success" href="#"> Add New Borrower</a>--}} -->
                        <!-- </div> --> 
                    </div>
                </div>
            </div>
        </div>

        <table class="table table-bordered">
            <tr>
                <th>Loan Id</th>
                <th>Total amount</th>
                <th>Due Amount</th>
                <th>Status</th>
                {{-- <th width="200px">Action</th> --}}

            </tr>
        </table>
        @foreach ($repayments as $repayment)
            <div class="card" style="width: 100%;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-1">{{ $repayment->loan_id }}</div>
                        <div class="col-sm-3">{{ $repayment->loan_amount }}</div>
                        <div class="col-sm-3">{{ $repayment->due_amount }}</div>
                        <div class="col-sm-3">{{ $repayment->status }}</div>
                        <div class="col-sm-2" width="10px">
                            <div class="row">
                            {{-- <form action="#" method="PUT"> --}}
                                    <a class="btn btn-info row-cols-sm-4"
                                       href="#"><i class="fa fa-eye"></i></a>
                                    <a class="btn btn-info row-cols-sm-4"
                                       href="#"><i class="fa fa-check"></i></a>
                                    {{-- <button class="btn btn-danger row-cols-sm-4"><i class="fa fa-check"></i></button> --}}
                                {{-- </form> --}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
    </div>
    </div>
@endsection

@section('scripts')

@endsection