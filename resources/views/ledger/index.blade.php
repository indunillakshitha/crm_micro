@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')

<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>View Ledger</h2>

                    </div>
                    <div class="pull-right">
<a onclick="fix()">FiX Bot</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="col-3">
                        <div class="">
                            <input type="text" name="s_nic" id="s_nic" class="form-control">
                        </div>
                    </div>
                    <div class="col-3">
                        <button type="button" class="btn btn-success" data-toggle="modal"
                            onclick="getBorrowersByNic()">Search by NIC
                        </button>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="col-3">
                        <select name="branch" id="branch" class="form-control" oninput="get_right_centers(this.value)">
                            <option>Select Branch</option>
                            @foreach($branches as $branch)
                            <option value={{$branch->branch_name}}>
                                {{$branch->branch_name}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-3">
                        <select name="cen" id="select_center" class="form-control">

                        </select>
                    </div>

                    <div class="col-3">
                        <select name="group" class="form-control" id="group" onclick="getBorrowers(this.value)">
                            <option value="1">Group 1</option>
                            <option value="2">Group 2</option>
                            <option value="3">Group 3</option>
                            <option value="4">Group 4</option>
                            <option value="5">Group 5</option>
                            <option value="6">Group 6</option>
                            <option value="7">Group 7</option>
                            <option value="8">Group 8</option>
                            <option value="9">Group 9</option>
                        </select>
                    </div>
                    <div class="col-3">
                        <select name="borrower" id="select_borrower" oninput="get_loans(this.value)"
                            class="form-control">
                            <option value="">select</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg-12 ">
                    <div class="col-3">
                        <select name="loans" id="select_loans" class="form-control" oninput="get_details(this.value)">


                        </select>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<button onclick="print()" class="btn btn-primary">Print</button>
<div id="printdiv">
<div class="animated fadeIn"  >
    <div class="card">
        <div class="card-body">
            <div class="row mt-3">
                <div class="col">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4"><strong>Customer Name</strong></div>
                            <div class="col-8"> <input class="date form-control" required type="text" name="period"
                                    id="cname"></div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4"><strong>NIC</strong></div>
                            <div class="col-8"> <input class="date form-control" required type="text" name="period"
                                    id="nic"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4"><strong>Address</strong></div>
                            <div class="col-8"> <input class="date form-control" required type="text" name="period"
                                    id="address"></div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4"><strong>Code</strong></div>
                            <div class="col-8"> <input class="date form-control" required type="text" name="period"
                                    id="borrower_id"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4">
                                <strong> Amount</strong></div>
                            <div class="col-8"> <input class="date form-control" required type="text" name="period"
                                    id="loan_amount"></div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4"><strong>Loan date</strong></div>
                            <div class="col-8"> <input class="date form-control" required type="text" name="period"
                                    id="loan_date"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4"><strong>Maturity Date</strong></div>
                            <div class="col-8"> <input class="date form-control" required type="text" name="period"
                                    id="maturity"></div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4"><strong>Due Amount</strong></div>
                            <div class="col-8"> <input class="date form-control" required type="text" name="period"
                                    id="due"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
{{-- </div> --}}
{{-- <div class="animated fadeIn"> --}}
    <div class="card">
        <div class="card-body">
            <table id="repayment_table" class="table table-bordered">
                <tr>
                    <th>Paid Date</th>
                    <th>Installament</th>
                    <th>Paid Amount</th>
                    <th>Total Paid </th>
                    <th>Balance</th>
                </tr>
            </table>

        </div>
    </div>
</div>

</div>
{{-- <div class="" id="borrowers">

</div> --}}

@endsection

@section('scripts')
<script type="text/javascript">
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        const branch = document.querySelector('#select-branch');
        const center = document.querySelector('#select_center');
        const center_name = document.querySelector('#center_name');

        // ------------------------------------ get center accoring to branch--------------------
        function fix() {
            // console.log(branch)
            Swal.fire({
  position: 'top-end',
  icon: 'loading',
  title: 'Fixing.....',
  showConfirmButton: false,
//   timer: 1500
})
            $.ajax({
                type: 'GET',
                url : '{{('/fixledger')}}',
                data : { 'branch': branch},
                success: function(data){
                    setTimeout(function(){

                        Swal.close()
                        Swal.fire({
                            title: 'Done',
                            icon: 'success',
                            text: 'Bot Is Stopping...',
                            confirmButtonText: 'Okay'
                        }).then((value)=>{
                            location.reload();

                        })
                    }, 300000);

                }
            })
        }
        // ------------------------------------ get center accoring to branch--------------------
        function get_right_centers(branch) {
            // console.log(branch)
            $.ajax({
                type: 'GET',
                url : '{{('/getrightcenters')}}',
                data : { 'branch': branch},
                success: function(data){

                    center.innerHTML = `
                    <select name="center" id="select_center"  class="form-control"> </select>
                    `
                    data.forEach(record => {

                        html = `
                        <option value="${record.center_name}">${record.center_name}</option>
                        `
                        center.innerHTML += html
                    })
                }
            })
        }

            function getBorrowers(g){
                const branch = document.querySelector('#branch');
                const center = document.querySelector('#select_center');
                const bor = document.querySelector('#select_borrower');
                // console.log(g)
               $.ajax({
                   type: 'GET',
                   url : '{{('/addloan/getborrowersforrep')}}',
                   data: {
                       'branch': branch.value,
                       'center': center.value,
                       'group': g,
                    },
                    success: function(data){
                        // console.log(data)
                    //     bor.innerHTML = `
                    // <select name="borrower" id="select_borrower"  class="form-control"> </select>
                    // `
                    bor.innerHTML =`<option value="">select</option>`
                    data.forEach(record => {
                        // console.log(record)

                        html = `
                        <option value="${record.borrower_no}">${record.borrower_no}</option>
                        `
                        bor.innerHTML += html

                    })
                    }
               })
           }
            function get_loans(id){

                // console.log(id)
               $.ajax({
                   type: 'GET',
                   url : '{{('/ledger/getl')}}',
                   data: {

                       'id': id,
                    },
                    success: function(data){
                    // console.log(data)
                    select_loans.innerHTML =`<option value="">select</option>`
                    data.forEach(record => {
                        // console.log(record)

                        html = `
                        <option value="${record.id}">${record.loan_amount}</option>
                        `
                        select_loans.innerHTML += html

                    })
                    }
               })
           }
           function print(){
    // var date=document.getElementById("date");
    var divContents = document.getElementById("printdiv").innerHTML;
            var a = window.open('', '', 'height=500, width=500');
            a.document.write('<html>');
            // a.document.write('<body > <h1>$.{date}<br>');
            a.document.write(divContents);
            a.document.write('</body></html>');
            a.document.close();
            a.print();
}
            function get_details(id){

                // console.log(id)
               $.ajax({
                   type: 'GET',
                   url : '{{('/ledger/getledger')}}',
                   data: {

                       'id': id,
                    },
                    success: function(data){
                    // console.log(data)
                    cname.value=data.borrower.full_name
                    nic.value=data.borrower.nic
                    address.value=data.borrower.address
                    borrower_id.value=data.borrower.borrower_no
                    loan_amount.value=data.loan.loan_amount
                    loan_date.value=data.loan.release_date
                    due.value=data.loan.due
                    maturity.value=data.loan.maturity

                    repayment_table.innerHTML = `
                                <tr>
                                        <th>No</th></th>
                                        <th>Paid Date</th>
                                        <th>Installament</th>
                                        <th>Paid Amount</th>
                                        <th>Total Paid </th>
                                        <th>Balance</th>
                                </tr>
                             `
                    var x=1
                    data.repayments.forEach(record => {
                        console.log(record)
                        html = `
                            <tr>
                                <td>${x}</td>
                                <td>${record.payment_date}</td>
                                <td>${record.installment}</td>
                                <td>${record.paid_amount}</td>
                                <td>${record.total_payed}</td>
                                <td>${record.due_amount}</td>
                            </tr>
                `
                repayment_table.innerHTML += html

                            x++
                    })
                    }
               })
           }
           function getBorrowersByNic(){
               console.log(n.value)
               $.ajax({
                   type: 'GET',
                   url : '{{('/addloan/getborrowersbynic')}}',
                   data: {
                       'nic': n.value,
                    },
                    success: function(data){
                        console.log(data.borrower)

                        // bo.innerHTML = `
                        // <select name="borrower_no" class="form-control" id="borrower_no" > </select>
                        // `

                        // data.forEach(record => {
                        //     bo.innerHTML = `
                        //     <div class="animated fadeIn">
                        //         <div class="card">
                        //             <div class="card-body">
                        //                 <div class="row">
                        //                     <div class="col-lg-12 borrower_id">
                        //                         <div class="col-3">
                        //                             <h2>${record.full_name}</h2>
                        //                         </div>
                        //                         <div class="col-3">
                        //                             <h2>${record.borrower_no}</h2>
                        //                         </div>
                        //                         <div class="col-3">
                        //                             <h2>${record.mobile_no}</h2>
                        //                         </div>
                        //                         <div class="col-3">
                        //                               <a href="/borrower/show/${record.borrower_no}"> <input  type="button" value="Show" class="btn btn-primary"></a>
                        //                               <a href="/borrower/edit/${record.borrower_no}"> <input  type="button" value="Edit" class="btn btn-primary"></a>

                        //                         </div>
                        //                     </div>
                        //                 </div>
                        //             </div>
                        //         </div>
                        //     </div>

                        //     `

                        //     bo.innerHTML += html

                        // })
                        // borrower_no.click()
                    }
               })
           }

</script>



@endsection
