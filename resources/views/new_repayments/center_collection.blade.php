@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Repayment</h2>

                    </div>
                    <div class="pull-right">

                        <input type="number" name="" id="fcid_value" oninput="fcid(this.value)">
                        <button class="btn btn-danger d-none" id="fcid_button">FCID</button>
                        <label for="">
                            <h3>Center Total Collection : <span id="center_total"> 0 </span> </h3>
                        </label>
                        <label for="">Select Center : </label>
                        <select name="" id="select_center">
                            @foreach($centers as $center)
                            <option value="{{$center->center}}">{{$center->center}}</option>
                            @endforeach
                        </select>
                        <button type="button" class="btn btn-success" data-toggle="modal" onclick="getLoans()"
                            data-target="#exampleModal">Get Loans
                        </button>
                        <a  class="btn btn-danger" onclick="finishCenter()">Finish Center</a>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <input type="date" name="" id="date_all_input" >
        <button class="btn btn-primary" id="date_all_btn" onclick="date_to_all()" >Apply date to all</button> --}}

    <input type="number" name="" id="payment_all_input">
    <button class="btn btn-primary" id="payment_all_btn" onclick="payment_to_all()">Apply payment to all</button>

    <table class="table table-bordered" id="loans_table">
        <tr>
            <th>NIC</th>
            <th>Name</th>
            <th>Total Amount</th>
            <th>Group No</th>
            <th>Loan Date</th>
            <th>
                {{-- <br>
                    <button class="btn btn-primary" id="date_all_btn">Apply date to all</button> --}}
            </th>
            <th>
                Payment
                {{-- <br>
                    <button class="btn btn-primary" id="payment_all_btn">Apply payment to all</button> --}}
            </th>
        </tr>

    </table>
</div>
</div>

@endsection

@section('scripts')
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
function finishCenter(){
    $.ajax({
                type: 'GET',
                url: '{{('/getnotpaidcustomers')}}',
                data: {
                    'center': select_center.value,
                } ,
                success: function(data){
                    console.log(data)
                    console.log(data.length);
                    if(data.length>0){
                        let borrowers_arr = [];
                        data.forEach(i => {
                            borrowers_arr.push(i.borrower_no)
                        })
                        Swal.fire({
                        text: `${borrowers_arr}`,
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes',
                        })
                        .then((result) => {
                            console.log(result);
                        if (result.value ==true) {
                            // return handle_approval_type(id, is_approved)
                            return setFinishCenter()

                        }
                        })
                    }


                }
            })
}
function setFinishCenter(){
    $.ajax({
                type: 'GET',
                url: '{{('/setnotpaids')}}',
                data: {
                    'center': select_center.value,
                } ,
                success: function(data){
                    console.log(data)
                    location.reload();


                }
            })
}
        let records_arr = []


        function fcid(value){
            fcid_button.classList.add('d-none')
            if(parseInt(value) !== parseInt(center_total.textContent)){
                fcid_button.classList.remove('d-none')
            }
        }

        function getLoans(){

            loans_table.innerHTML = `
                    <tr>
                        <th>Name</th>
                        <th>Group No</th>
                        <th>Balance</th>
                        <th>Due</th>
                        <th>Due</th>
                        <th>Payment</th>
                    </tr>
                    `

            $.ajax({
                type: 'POST',
                url: '{{('/newrepayments/getloans')}}',
                data: {
                    'center': select_center.value,
                } ,
                success: function(data){
                    console.log(data);
                    if(data.length > 0){

                    data.forEach(record => {
                        html = `

                        <td>${record.full_name}</td>
                        <td>${record.group_no}</td>
                        <td>${record.due}</td>
                        <td>${record.instalment}</td>
                        <td>${record.borrower_no}</td>
                        <td><input type="number" class="payments" id="payment_${record.id}" value="0"></td>
                        <td><button class="btn btn-primary" id="btn_${record.id}" onclick="collect(this.id)">Collect</button></td>
                        `
                        loans_table.innerHTML += html
                        records_arr.push(record)
                    })
                    // console.log(records_arr, 'records_arr')
                }

                }
            })
        }

        function collect(id){

                // console.log(document.querySelector(`#payment_${id}`))
                id = id.slice(id.indexOf('_')+1, id.length)
                // console.log(id)
                if(parseFloat(document.querySelector(`#payment_${id}`).value)>0){


                $.ajax({
                    type: 'POST',
                    url: '{{('/newrepayments/collect')}}',
                    data: {
                        'id' : id,
                        // 'date' : document.querySelector(`#date_${id}`).value,
                        'payment' : document.querySelector(`#payment_${id}`).value,
                    },
                    success: function(data){
                        console.log(data);
                        center_total.textContent = data
                        // return getLoans();
                        document.querySelector(`#btn_${id}`).disabled = true
                        setTimeout(() => {
                            document.querySelector(`#btn_${id}`).disabled = false
                        },500000)
                    }

                })
            }else{
                swal.fire('කරුණාකර 0 ට වැඩි අගයක් ලබාදෙන්න')
            }
            }
        function notcollect(id){

                            id = id.slice(id.indexOf('_')+1, id.length)
                            $.ajax({
                                type: 'POST',
                                url: '{{('/newrepayments/notcollect')}}',
                                data: {
                                    'id' : id,
                                    // 'date' : document.querySelector(`#date_${id}`).value,
                                    // 'payment' : document.querySelector(`#payment_${id}`).value,
                                },
                                success: function(data){
                                    console.log(data);
                                    center_total.textContent = data
                                    // return getLoans();
                                    document.querySelector(`#btn_not_${id}`).disabled = true
                                    setTimeout(() => {
                                        document.querySelector(`#btn_not_${id}`).disabled = false
                                    },5000)
                                }

                                 })


        }

            function date_to_all(){
                records_arr.forEach(r => {
                        // console.log(document.querySelectorAll('.dates') )
                        document.querySelectorAll('.dates').forEach(date => {
                            date.value = date_all_input.value
                        })
                    })
            }

            function payment_to_all(){
                records_arr.forEach(r => {
                        // console.log(document.querySelectorAll('.dates') )
                        document.querySelectorAll('.payments').forEach(date => {
                            date.value = payment_all_input.value
                        })
                    })
            }







</script>
@endsection
