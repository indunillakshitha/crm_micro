@extends('layouts.master')
@section('title')

Not Paids In Week
@endsection
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Not Paids</h2>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered" id="data_table">
    <thead>
        <tr>
            <th>No</th>
            <th>Cente</th>
            <th>Due</th>
            <th>Not Paid Count</th>


            <th width="200px">Action</th>
        </tr>
    </thead>

    <tbody>
        {{$i=1}}
        @foreach ($not_paids as $not_paid)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $not_paid->center }}</td>
            <td>{{ $not_paid->total_due }}</td>
            <td>{{ $not_paid->npc }}</td>
            <td><a href="/notpaidmore/{{$not_paid->center}}" class="form-control btn btn-primary">See More</a></td>
        </tr>
        @endforeach
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>
{{-- {!! $loans->links() !!} --}}

@endsection
