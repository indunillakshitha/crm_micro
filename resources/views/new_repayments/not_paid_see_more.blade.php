@extends('layouts.master')
@section('title')

Not Paids In Week
@endsection
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Not Paids</h2>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered" id="data_table">
    <thead>
        <tr>
            <th>No</th>
            <th>Borrower No</th>
            <th>Loan Amount</th>
            <th>Installment</th>
            <th>Total Paid</th>
            <th>Balance</th>
            <th>Not Paid Count</th>
            <th>Arrears Total</th>
            <th>Over Paid Total</th>
        </tr>
    </thead>

    <tbody>
        {{$i=1}}
        @foreach ($export_arr as $not_paid)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ json_encode($not_paid[0]->borrower_no)}}</td>
            <td>{{ json_encode($not_paid[0]->loan_amount)}}</td>
            <td>{{ json_encode($not_paid[0]->instalment)}}</td>
            <td>{{ json_encode($not_paid[0]->paid)}}</td>
            <td>{{ json_encode($not_paid[0]->due)}}</td>
            <td>{{ json_encode($not_paid[3])}}</td>
            <td>{{ json_encode($not_paid[1])}}</td>
            <td>{{ json_encode($not_paid[2])}}</td>
            {{-- <td>{{ $not_paid-> }}</td> --}}
            {{-- <td>{{ $not_paid-> }}</td> --}}
        </tr>
        @endforeach
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>
{{-- {!! $loans->links() !!} --}}

@endsection
