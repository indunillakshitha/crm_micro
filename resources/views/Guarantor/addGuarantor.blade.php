@extends('layouts.master')
@section('title')

    Add Guarantor

@endsection
@section('content')

    <div class="">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Add Guarantor</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('guarantor.index') }}"> Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> Please check your input code<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form action="{{ route('guarantor.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Guarantor Full Name :</strong>
                                <input type="text" name="guarantor_full_name" oninput="toCap()" class="form-control"
                                       placeholder="" id="Cap">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Guarantor No :</strong>
                                <input type="text" name="guarantor_no" class="form-control" value={{$guarantorCount}}
                                    readonly>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>NIC:</strong>
                                <input type="text" class="form-control" name="guarantor_nic" id="nic" oninput="genBday()">
                                <input type="text" readonly id="nic_status" class="btn d-none white">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                               <strong> Birthday:</strong>
                                <input type="text" class="form-control" name="guarantor_birthday" id="bday" readonly>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Gender:</strong>
                                <input type="text" class="form-control" name="guarantor_gender" id="gender" readonly>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Guarantor Address :</strong>
                                <input type="text" name="guarantor_address" class="form-control"
                                       id="Cap" oninput="toCap()">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Civil Status :</strong>
                                <input type="text" name="guarantor_civil_status" class="form-control" id="Cap" oninput="toCap()">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Guarantor Mobile:</strong>
                                <input type="text" name="guarantor_mobile_no" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Guarantor Land :</strong>
                                <input type="text" name="guarantor_lp_no" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Guarantor Email:</strong>
                                <input type="email" name="guarantor_email" class="form-control">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Loan Category:</strong>
                                <select name="guarantor_occupation" class="form-control">
                                    <option value="Self Employed">Self Employed</option>
                                    <option value="Occupation">Occupation</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        const nic = document.querySelector('#nic')
        const bday = document.querySelector('#bday');
        const genderInput = document.querySelector('#gender');

        genBday();

        function genBday() {

            let nicNo = nic.value;
            let dayText = 0;
            let year = '';
            let month = '';
            let day = '';
            let gender = '';

            if (nicNo.length !== 10 && nicNo.length !== 12) {
                console.log('invalid nic')
                bday.value = 'Invalid NIC'
                genderInput.value = 'Invalid NIC'
                console.log('invalid nic')
                bday.value = 'Invalid NIC'
                genderInput.value = 'Invalid NIC'
            } else {

                // year
                if (nicNo.length === 10) {
                    year = "19" + nicNo.substr(0, 2);
                    dayText = parseInt(nicNo.substr(2, 3));
                } else {
                    year = nicNo.substr(0, 4);
                    dayText = parseInt(nicNo.substr(4, 3))
                }

                // gender
                if (dayText > 500) {
                    gender = "Female"
                    dayText = dayText - 500
                } else {
                    gender = "Male"
                }

                // day digit validation
                if (dayText < 1 && dayText > 366) {
                    console.log('invalid nic day')
                    bday.value = 'Invalid NIC'
                    genderInput.value = 'Invalid NIC'
                } else {

                    // month
                    if (dayText > 335) {
                        day = dayText - 355
                        month = "12"
                    } else if (dayText > 305) {
                        day = dayText - 305
                        month = "11"
                    } else if (dayText > 274) {
                        day = dayText - 274
                        month = "10"
                    } else if (dayText > 244) {
                        day = dayText - 244
                        month = "09"
                    } else if (dayText > 213) {
                        day = dayText - 213
                        month = "08"
                    } else if (dayText > 182) {
                        day = dayText - 182
                        month = "07"
                    } else if (dayText > 152) {
                        day = dayText - 152
                        month = "06"
                    } else if (dayText > 121) {
                        day = dayText - 121
                        month = "05"
                    } else if (dayText > 91) {
                        day = dayText - 91
                        month = "04"
                    } else if (dayText > 60) {
                        day = dayText - 60
                        month = "03"
                    } else if (dayText < 32) {
                        day = dayText
                        month = "01"
                    } else if (dayText > 31) {
                        day = dayText - 31
                        month = "02"
                    }


                    if (day < 10) {
                        day = `0${day}`
                    }

                    bday.value = `${year}-${month}-${day}`
                    genderInput.value = gender;

                    checkNIC();
                }
            }
        }

        const nic_status = document.querySelector('#nic_status');

        function checkNIC() {
            if (genderInput.value !== 'Invalid NIC' && bday.value !== 'Invalid NIC') {
                $.ajax({
                    type: 'POST',
                    url: '{{('/getnic')}}',
                    data: {'nic': nic.value},
                    success: function (data) {
                        console.log(data)
                        nic_status.value = data;
                        if (data === 'NIC already registered') {
                            nic_status.classList.remove('d-none')
                            nic_status.classList.remove('btn-success')
                            nic_status.classList.add('btn-danger')
                        } else if (data === 'NIC available') {
                            nic_status.classList.remove('d-none')
                            nic_status.classList.remove('btn-danger')
                            nic_status.classList.add('btn-success')
                        }
                    }
                })
            }
        }
    </script>



@endsection


{{--{!! $guarantors->links() !!}--}}


