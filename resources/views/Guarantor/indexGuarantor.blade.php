@extends('layouts.master')
@section('title')

    Canty International

@endsection
@section('content')


    {{--//--------------------------------------------------------------------------------------------------------}}

    <div class="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="pull-left">
                            <h2>Guarantors</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-success" href="{{ route('guarantor.create') }}"> Add Guarantor</a>
                            {{--                            <a class="btn btn-success" href="{{ route('guarantor.create') }}"> Add Borrower as--}}
                            {{--                                Guarantor</a>--}}

                            <a href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false"
                               class="btn btn-success ">
                                Add Borrower as Guarantor
                            </a>

                            <ul class="collapse list-unstyled" id="homeSubmenu3">
                                <li>
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h6 class="title">Select Borrower ID</h6>
                                                    </div>
                                                    <div class="card-body">

                                                        <div class="row">
                                                            <div class="col-md-5 pr-1">
                                                                <div class="form-group">
                                                                    <label>Borrower ID</label>
                                                                    <select name="" id="productType" class="form-control">
{{--                                                                       @foreach ($borrowers as $borrower)--}}
{{--                                                                            <option--}}
{{--                                                                                value="{{$borrower->borrower_no}}">{{$borrower->borrower_no}}</option>--}}
{{--                                                                        @endforeach --}}
                                                                        <div class="form-group">
                                                                            <button type="submit" id="addEarning" class="btn btn-primary">Add Guarantor</button>
                                                                        </div>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>

                            </ul>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="card" style="width: 100%;">
        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Mobile</th>


                    <th width="200px">Action</th>
                </tr>
                @foreach ($guarantors as $guarantor)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $guarantor->guarantor_full_name }}</td>
                        <td>{{ $guarantor->guarantor_address}}</td>
                        <td>{{ $guarantor->guarantor_mobile_no }}</td>

                        <td width="10px">
                            <form action="{{ route('guarantor.destroy',$guarantor->id) }}" method="POST">

                                <a class="btn btn-info" href="{{ route('guarantor.show',$guarantor->id) }}">Show</a>

                                <a class="btn btn-primary" href="{{ route('guarantor.edit',$guarantor->id) }}">Edit</a>
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger">Delete</button>

                            </form>
                        </td>

                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    {!! $guarantors->links() !!}

@endsection
