@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Change Group</h2>

                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#exampleModal">Change
                            </button>
                            {{--                            <a class="btn btn-success" href="{{ route('borrower.create') }}"> Add New Borrower</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="col-3">
                            <div class="">
                            <input type="text" name="borrower_id" id="borrower_id"class="form-control"  placeholder="Enter a Borrrower ID">
                            </div>
                        </div>


                        <div class="col-3">
                            {{-- <input type="text" class="form-control" name="group_no"> --}}
                            <select name="group" class="form-control" id="group">
                                <option value="1">Group 1</option>
                                <option value="2">Group 2</option>
                                <option value="3">Group 3</option>
                                <option value="4">Group 4</option>
                                <option value="5">Group 5</option>
                                <option value="6">Group 6</option>
                                <option value="7">Group 7</option>
                                <option value="8">Group 8</option>
                                <option value="9">Group 9</option>
                                <option value="10">Group 10</option>
                                <option value="11">Group 11</option>
                            </select>
                        </div>
                        <div class="">
                            <a class="btn btn-success" data-toggle="modal"
                                    data-target="#exampleModal"
                                onclick="request_change(borrower_id.value, group.value)"
                                    >Request
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection

@section('scripts')

<script>
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function request_change(bi, g){
            // console.log(bi, g);
            $.ajax({
                type: 'POST',
                url: '{{('/groupchange')}}',
                data: {bi, g} ,
                success: function(data){
                    console.log(data);
                    // return show_data(data)
                    if(data == 'fail'){
                        return  Swal.fire({
                        title: 'Error!',
                        text: 'Please fill all fields',
                        icon: 'error',
                        confirmButtonText: 'Okay'
                        })
                    }else{
                        return  Swal.fire({
                        title: 'Success',
                        text: 'Group Changed',
                        icon: 'success',
                        confirmButtonText: 'Okay'
                        })
                    }

                }
            })
        }
</script>

@endsection


