@extends('layouts.master')
@section('title')

    Show Loan
@endsection

@section('content')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h4>Custom Tab</h4>
        </div>
        <div class="card-body">
            <div class="custom-tab">

                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="custom-nav-home-tab" data-toggle="tab" href="#custom-nav-home" role="tab" aria-controls="custom-nav-home" aria-selected="true">Borrower</a>
                        <a class="nav-item nav-link" id="custom-nav-profile-tab" data-toggle="tab" href="#custom-nav-profile" role="tab" aria-controls="custom-nav-profile" aria-selected="false">Loan</a>
                    </div>
                </nav>
                <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="custom-nav-home" role="tabpanel" aria-labelledby="custom-nav-home-tab">

                        <div class="col-md-6">
                        <aside class="profile-nav alt">
                            <section class="card">
                                <div class="card-header user-header alt bg-dark">
                                    <div class="media">
                                        <a href="#">
                                            <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="images/admin.jpg">
                                        </a>
                                        <div class="media-body">
                                            <h2 class="text-light display-6">{{$borrower->full_name}}</h2>
                                            <p>{{$borrower->borrower_no}}</p>
                                        </div>
                                    </div>
                                </div>


                                <ul class="list-group list-group-flush">
                                    {{-- <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-envelope-o">Borrower ID</i> <span class="badge badge-primary pull-right"> </span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-tasks"></i> Full Name <span class="badge badge-danger pull-right">{{$borrower->full_name}} </span></a>
                                    </li> --}}
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-phone-square"></i> Mobile <span class="badge badge-success pull-right">{{$borrower->mobile_no}} </span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa  fa-map-marker"></i> Address <span class="badge badge-warning pull-right r-activity">{{$borrower->address}} </span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-home"></i> Center <span class="badge badge-warning pull-right r-activity">{{$borrower->center}} </span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-tags"></i> NIC <span class="badge badge-warning pull-right r-activity">{{$borrower->nic}} </span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa  fa-users"></i> Gender <span class="badge badge-warning pull-right r-activity">{{$borrower->gender}} </span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa  fa-dot-circle-o"></i> Civil Status <span class="badge badge-warning pull-right r-activity">{{$borrower->civil_status}} </span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-comments-o"></i> Relationship  <span class="badge badge-warning pull-right r-activity">{{$borrower->related_civil_status}} </span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-comments-o"></i> Guardian or Husband <span class="badge badge-warning pull-right r-activity">{{$borrower->h_g_name}} </span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-comments-o"></i> Mobile <span class="badge badge-warning pull-right r-activity">{{$borrower->h_g_number}} </span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-comments-o"></i> Birthday <span class="badge badge-warning pull-right r-activity">birthday</span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-comments-o"></i> Occupation <span class="badge badge-warning pull-right r-activity">ccupation</span></a>
                                    </li>

                                </ul>

                            </section>
                        </aside>
                    </div>


                    </div>
                    <div class="tab-pane fade" id="custom-nav-profile" role="tabpanel" aria-labelledby="custom-nav-profile-tab">
                        <div class="col-md-4">
                            <aside class="profile-nav alt">
                                <section class="card">
                                    <div class="card-header user-header alt bg-dark">
                                        <div class="media">
                                            <a href="#">
                                                <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="images/admin.jpg">
                                            </a>
                                            <div class="media-body">
                                                <h2 class="text-light display-6">{{$loan->borrower_no}}</h2>
                                                <p>{{$loan->id}}</p>
                                            </div>
                                        </div>
                                    </div>


                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">
                                            <a href="#"> <i class="fa ti-home"></i>Branch<span class="badge badge-primary pull-right"> {{$loan->branch}}</span></a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="#"> <i class="fa fa-tasks"></i>Center<span class="badge badge-primary pull-right">15</span></a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="#"> <i class="fa fa-bell-o"></i>Group No<span class="badge badge-success pull-right"> {{$loan->group_no}}</span></a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="#"> <i class="fa fa-comments-o"></i>Stage<span class="badge badge-primary pull-right r-activity" </i> {{$loan->loan_stage}}</span></a>
                                        </li>
                                       <li class="list-group-item">
                                            <a href="#"> <i class="fa ti-money"></i> Amount<span class="badge badge-danger pull-right r-activity">{{$loan->loan_amount}}</span></a>
                                        </li>
                                       <li class="list-group-item">
                                            <a href="#"> <i class="fa fa-comments-o"></i>Duration<span class="badge badge-primary pull-right r-activity"> {{$loan->loan_duration}}</span></a>
                                        </li>
                                       <li class="list-group-item">
                                            <a href="#"> <i class="fa fa-comments-o"></i>Duration Period<span class="badge badge-primary pull-right r-activity"> {{$loan->duration_period}}</span></a>
                                        </li>
                                       <li class="list-group-item">
                                            <a href="#"> <i class="fa ti-money"></i> Interest<span class="badge badge-warning pull-right r-activity">{{$loan->interest}}</span></a>
                                        </li>
                                       <li class="list-group-item">
                                            <a href="#"> <i class="fa ti-money"></i> Installament<span class="badge badge-warning pull-right r-activity">{{$loan->instalment}}</span></a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="#"> <i class="fa fa-comments-o"></i> STATUS<span class="badge badge-primary pull-right r-activity">{{$loan->status}}</span></a>
                                        </li>

                                    </ul>

                                </section>
                            </aside>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
