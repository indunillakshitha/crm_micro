@extends('layouts.master')
@section('title')

Add Loan
@endsection

@section('content')


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->

</head>


<body>


    <div class="">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Create New Loan</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('loan.index') }}"> Back</a>
                        </div>
                    </div>
                </div>
                <form action="{{ route('loan.store') }}" method="POST">
                    @csrf
                    <div class="row">

                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <strong>Branch:</strong>
                                {{-- <input type="text" name="branch" class="form-control" placeholder="Branch" id="branch"> --}}
                                <select name="branch" class="form-control" id="branch">

                                    @foreach ($branches as $branch)
                                    <option value={{$branch->branch_name}}>
                                        {{$branch->branch_name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <strong>Center:</strong>
                                {{-- <input type="text" name="center" class="form-control" placeholder="Center" id="center"> --}}
                                <select name="center" class="form-control" id="center" onclick="getBorrowers()">
                                    <option value="">Select a branch first</option>
                                </select>
                            </div>
                        </div>



                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <strong>Group Number:</strong>
                                {{-- <input type="text" name="group_no" class="form-control" placeholder="Group Number"
                                       id="group_no"> --}}
                                <select name="group_no" class="form-control" id="group_no" onclick="getBorrowers()">
                                    <option value="1">Group 1</option>
                                    <option value="2">Group 2</option>
                                    <option value="3">Group 3</option>
                                    <option value="4">Group 4</option>
                                    <option value="5">Group 5</option>
                                    <option value="6">Group 6</option>
                                    <option value="7">Group 7</option>
                                    <option value="8">Group 8</option>
                                    <option value="9">Group 9</option>
                                </select>
                            </div>
                        </div>

                        <div class="bg-danger d-none" id="center_leader_message">
                            This Center has no Center Leader
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Borrower No:</strong>
                                <select name="borrower_no" class="form-control" id="borrower_no">
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group col-6">
                                <strong>Loan Stage:</strong><br>
                                {{-- <select name="loan_stage" class="form-control">
                                    <option value="first">First</option>
                                    <option value="secound">Secound</option>
                                    <option value="third">Third</option>
                                    <option value="fourth">Fourth</option>
                                    <option value="fifth">Fifth</option>
                                </select> --}}
                                <input type="text" name="loan_stage" class="form-control" id="loan_stage" readonly>
                            </div>
                            {{-- </div>

                        <div class="col-xs-12 col-sm-12 col-md-12"> --}}
                            <div class="form-group col-6">
                                <strong>Loan Release Date:</strong>
                                <input class="date form-control" type="date" name="release_date" id="release_date">
                                {{-- <input type="text" name="release_date" class="form-control" placeholder="2020-12-12"> --}}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Loan Category:</strong><br><br>
                                <select name="loan_purpose" class="form-control" id="loan_purpose" value="loan_purpose">

                                    <option value="0" selected="true">-Select-</option>
                                    @foreach($categories as $category)
                                    <option value="{{$category->category_type}}">{{$category->category_type}}</option>
                                    @endforeach

                                </select>

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <button class="btn btn-danger d-none" id="incompleteBtn"></button>
                            </div>
                        </div>
                        <div class="d-none">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group col-6">
                                    <strong>Loan Amount:</strong>
                                    <input type="text" name="loan_amount" class="form-control" placeholder="Loan Amount"
                                        id="loan_amount">
                                </div>
                                {{-- </div>
                        <div class="col-xs-12 col-sm-12 col-md-12"> --}}
                                <div class="form-group col-6">
                                    <strong>Interest:</strong>
                                    <input type="text" name="interest" class="form-control" required
                                        placeholder="Interest" id="interest">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Loan Duration:(weeks)</strong>
                                    <input type="number" name="loan_duration" class="form-control" id="loan_duration"
                                        placeholder="" oninput="">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Duration </strong>
                                    <select name="duration_period" class="form-control" id="duration_period"
                                        value="duration_period" oninput="">
                                        {{-- <option value="0">-Select-</option> --}}
                                        <option value="Weeks">Weeks</option>
                                        {{-- <option value="Months">Months</option>
                                    <option value="Years">Years</option> --}}
                                    </select>

                                </div>
                            </div>
                            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Repayment Cycle :</strong>
                                <select name="repayment_cycle"  class="form-control" id="repayment_cycle" value="repayment_cycle" oninput="">
                                    <option value="0">-Select-</option>
                                    <option value="Weeks">Weeks</option>
                                    <option value="Months">Months</option>
                                </select>

                            </div>
                        </div> --}}
                            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Number of Repayments:</strong>
                                <input type="text" name="number_of_repayments" class="form-control" id="number_of_repayments" placeholder="" readonly>
                            </div>
                        </div> --}}


                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group col-6">
                                    <strong>Interest Rate:</strong>
                                    <input type="text" name="interest_rate" class="form-control"
                                        placeholder="Interest Rate" id="interest_rate">
                                </div>
                                {{-- </div>
                        <div class="col-xs-12 col-sm-12 col-md-12"> --}}
                                <div class="form-group col-6">
                                    <strong>Instalment:</strong>
                                    <input type="text" name="instalment" class="form-control" placeholder="Instalment"
                                        id="instalment" readonly>
                                </div>
                            </div>


                            {{-- <hr>
                        <form action="">
                            @csrf
                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label for="witness_type">Witness Type</label>
                                        <input type="text" name="witness_name" id="witness_name" class="form-control">
                                        <select name="witness_type" id="witness_type" class="form-control">
                                            <option value='0'>-- Select --</option>
                                            <option value='Center Leader'>Center Leader</option>
                                            <option value='Agent'>Agent</option>

                                        </select>

                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label for="witness_name ">Witness Name</label>
                                        {{-- <input type="text" name="witness_name " id="witness_name"
                                               class="form-control"> --}}
                            {{-- <select name="witness_name" id="witness_name" class="form-control">
                                            <option>-- Select --</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4"
                                ">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-sm d-none" id="add_witness_btn">
                                        Add Witness
                                        {{-- aantrawai --}}
                            {{-- </button>
                                    <button class="btn btn-primary btn-sm " id="add_witness_btnn">
                                        Add Witness
                                    </button>
                                </div>
                            </div>
                    </div> --}}
                            {{--                </form>--}}

                            {{-- <div class="row">
                    <table class="table table-bordered table-white col-xs-12 col-sm-12 col-md-12 " id="witnesses_table">
                        <tr>
                            <th>Witness Name</th>
                            <th>NIC</th>
                            <th>Address</th>
                            <th>Mobile Number</th>
                            <th>Type</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </table>
                </div>
            </div>  --}}
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    {{-- <strong>Status:</strong> --}}

                                    <input type="text" name="status" hidden class="form-control" id="status"
                                        placeholder="Pending" readonly>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Witnesses</strong>
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-6 ">
                                            Witness 1
                                            <select class="form-control" name="witnessC" id="witnessC">
                                                <option value="">Click to see</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6 ">
                                            Witness 2
                                            <select class="form-control" name="witnessA" id="witnessA">
                                                <option value="">Click to see</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" id="sub_btn" class="btn btn-primary">Submit</button>
                        </div>

                        <div class="form-group bg-success d-none" id="suc_mess">
                            Loan added successfully
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    @section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            document.querySelector('#sub_btn').addEventListener('click', e => {
            e.preventDefault()

            if(!document.querySelector('#interest').value){
                return  Swal.fire({
                title: 'Error!',
                text: 'Please fill all fields',
                icon: 'error',
                confirmButtonText: 'Hako'
                })
            }
            if(!document.querySelector('#release_date').value){
                return  Swal.fire({
                title: 'Error!',
                text: 'Please fill Release Date',
                icon: 'error',
                confirmButtonText: 'Hako'
                })
            }

            $.ajax({
                type: 'POST',
                url: '{{('/addloanajax')}}',
                data: {
                    'branch' : document.querySelector('#branch').value,
                    'center' : document.querySelector('#center').value,
                    'group_no' : document.querySelector('#group_no').value,
                    'borrower_no' : document.querySelector('#borrower_no').value,
                    'loan_stage' : document.querySelector('#loan_stage').value,
                    'release_date' : document.querySelector('#release_date').value,
                    'loan_purpose' : document.querySelector('#loan_purpose').value,
                    'loan_amount' : document.querySelector('#loan_amount').value,
                    'interest' : document.querySelector('#interest').value,
                    'loan_duration' : document.querySelector('#loan_duration').value,
                    'duration_period' : document.querySelector('#duration_period').value,
                    'interest_rate' : document.querySelector('#interest_rate').value,
                    'instalment' : document.querySelector('#instalment').value,
                    'status' : document.querySelector('#status').value,
                    'witnessC' : document.querySelector('#witnessC').value,
                    'witnessA' : document.querySelector('#witnessA').value,
                },
                success: function(data){
                    console.log(data);
                    // document.querySelector('#suc_mess').classList.remove('d-none')
                    document.querySelector('#loan_purpose').value = null
                    document.querySelector('#loan_amount').value = null
                    document.querySelector('#interest').value = null
                    document.querySelector('#loan_duration').value = null
                    document.querySelector('#duration_period').value = null
                    document.querySelector('#interest_rate').value = null
                    document.querySelector('#instalment').value = null
                        // document.querySelector('#suc_mess').classList.add('d-none')
                    Swal.fire({
                    title: 'Loan Added',
                    text: 'Successfull',
                    icon: 'success',
                    timer: 2000
                    // confirmButtonText: 'Hako'
                    })
                },
                // error:
            })

        })
            // const borrowerNo = document.querySelector('#borrower_no');
            const witnessName = document.querySelector('#witness_name');
            const witnessNIC = document.querySelector('#witness_nic');
            const witnessAdd = document.querySelector('#witness_address');
            const witnessType = document.querySelector('#witness_type');
            const add_witness_btnn = document.querySelector('#add_witness_btnn')

            $(document).ready(function () {

                $(loan_purpose).click(function () {
                    var cat = $(this).val();
                    console.log(cat)
                    $.ajax({
                        type: 'GET',
                        url: '{{ url('/loan/create/rate') }}',
                        data: {
                            'id': cat,
                            'borrower_no' : borrower_no.value
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            console.log(data);

                            let loanStatus = data[1]
                            let cat = data[0]['category_type']
                            console.log(cat)

                            if(loanStatus === 'Incomplete'){
                                // console.log(loanStatus)
                                if(incompleteBtn.classList.contains('d-none')){
                                    incompleteBtn.classList.remove('d-none')
                                    sub_btn.classList.add('d-none')
                                    // incompleteBtn.textContent = `Selected Borrower has incomplete loans from ${cat} category`
                                    incompleteBtn.textContent = `මෙම අංකයට ${cat}  category එකෙන් අවසන් නොකල Loan එකක් පවතී`
                                    // sub_btn.disabled = true
                                }

                            } else if (loanStatus === 'Complete'){
                                if(!incompleteBtn.classList.contains('d-none')){
                                    incompleteBtn.classList.add('d-none')
                                    sub_btn.classList.remove('d-none')

                                    sub_btn.disabled = false
                                }
                            }

                            $("#loan_duration").val(data[0].duration_period);
                            $("#instalment").val(data[0].instalment);
                            $("#loan_amount").val(data[0].fixed_ammount);
                            $("#interest").val(data[0].interest);
                            $("#interest_rate").val(data[0].category_rate)


                        }

                    });
                });
                 });
                });

            // $('.date').datepicker({

            //     format: 'yyyy-mm-dd'

            // });

            const branch = document.querySelector('#branch')
            const center = document.querySelector('#center')
            const group_no = document.querySelector('#group_no')
            const borrower_no = document.querySelector('#borrower_no')
            const witnessC = document.querySelector('#witnessC')
            const witnessA = document.querySelector('#witnessA')
            const loan_stage = document.querySelector('#loan_stage')
            const incompleteBtn = document.querySelector('#incompleteBtn')
            const sub_btn = document.querySelector('#sub_btn')

            branch.addEventListener('click', e=> {
                console.log(branch.value)
                $.ajax({
                    type: 'GET',
                    url : '{{('/addloan/getcenters')}}',
                    dataType: 'JSON',
                    data: {'branch': branch.value},
                    success: function(data){
                        console.log(data, 'centers here')

                        center.innerHTML = `
                        <select name="center" class="form-control" id="center"> </select>
                        `

                        data.forEach(record => {
                            html = `
                            <option id="${record.id}" value="${record.center_name}">${record.center_name}</option>
                            `

                            center.innerHTML += html
                            borrower_no.value = ''

                        })
                        center.click()
                    }
                })
            })

           function getBorrowers(){
               $.ajax({
                   type: 'GET',
                   url : '{{('/addloan/getborrowers')}}',
                   data: {
                       'branch': branch.value,
                       'center': center.value,
                       'group': group_no.value,
                    },
                    success: function(data){
                        console.log(data)

                        borrower_no.innerHTML = `
                        <select name="borrower_no" class="form-control" id="borrower_no" > </select>
                        `

                        data.forEach(record => {
                            html = `
                            <option id="${record.id}" value="${record.borrower_no}">${record.nic}</option>
                            `

                            borrower_no.innerHTML += html

                        })
                        borrower_no.click()
                    }
               })
           }

        //    let loanstage = 0;
           borrower_no.addEventListener('click', e => {
                witnessC.textContent = ''
                $.ajax({
                    type: 'GET',
                    url: '{{('/addloan/getloanstage')}}',
                    data: {'borrower_no' : borrower_no.value},
                    success: function(data){
                        console.log(data)

                        // let loanStatus = data[1]

                        // if(loanStatus === 'Incomplete'){
                        //     // console.log(loanStatus)
                        //     if(incompleteBtn.classList.contains('d-none')){
                        //         incompleteBtn.classList.remove('d-none')
                        //         sub_btn.disabled = true
                        //     }

                        // } else if (loanStatus === 'Complete'){
                        //     if(!incompleteBtn.classList.contains('d-none')){
                        //         incompleteBtn.classList.add('d-none')
                        //         sub_btn.disabled = false
                        //     }
                        // }

                        let loanStage = parseInt(data.n_of_loans) + parseInt(1)
                        // console.log(loanStage)

                        if(loanStage === 1){
                            loan_stage.value = 'First'
                            // console.log(loan_stage.value)
                        }
                        else if(loanStage == 2){
                            loan_stage.value = 'Second'
                        }
                        else if(loanStage == 3){
                            loan_stage.value = 'Third'
                        }
                        else if(loanStage == 4){
                            loan_stage.value = 'Fourth'
                        }
                        else if(loanStage == 5){
                            loan_stage.value = 'Fifth'
                        }
                    }
                })
           })

           witnessC.addEventListener('click' , e => {
               $.ajax({
                   type: 'GET',
                   url: '{{('/addloan/getwitnessc')}}',
                   data: {
                       'borrower_no' : borrower_no.value
                   },
                   success: function(data){
                    console.log(data)

                    if(!Array.isArray(data)){
                        witnessC.innerHTML =  `
                            <option id="${data.id}" value="${data.borrower_id}">${data.borrower_id}</option>
                            `
                    } else{
                        witnessC.innerHTML = `
                        <select class="form-control" name="witnessC" id="witnessC"> </select>
                        `

                        data.forEach(record => {
                            html = `
                            <option id="${record.id}" value="${record.borrower_no}">${record.borrower_no}</option>
                            `

                            witnessC.innerHTML += html
                        })
                    }
                   }

               })
           })

           witnessA.addEventListener('click' , e => {
               $.ajax({
                   type: 'GET',
                   url: '{{('/addloan/getwitnessa')}}',
                   data: {
                       'borrower_no' : borrower_no.value
                   },
                   success: function(data){
                    console.log(data)

                        witnessA.innerHTML = `
                        <select class="form-control" name="witnessA" id="witnessA"> </select>
                        `

                        data.forEach(record => {
                            html = `
                            <option id="${record.id}" value="${record.name}">${record.name}</option>
                            `

                            witnessA.innerHTML += html
                        })
                    }

               })
           })

           const center_leader_message = document.querySelector('#center_leader_message')
           center.addEventListener('click', e => {
            $.ajax({
                   type: 'GET',
                   url: '{{('/checkcenterleader')}}',
                   data: {
                       'center' : center.value,
                       'branch' : branch.value,
                   },
                   success: function(data){
                       if(data.length > 0){
                        center_leader_message.classList.add('d-none')

                       } else {
                        center_leader_message.classList.remove('d-none')
                       }

                    }

               })
           })





    </script>
    @endsection
</body>

</html>
@endsection
