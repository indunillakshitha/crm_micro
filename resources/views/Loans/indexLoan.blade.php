@extends('layouts.master')
@section('title')

Loans
@endsection
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Loans</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('loan.create') }}"> Add Loan</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered" id="data_table">
    <thead>
        <tr>
            <th>No</th>
            <th>Borrower No</th>
            <th>Loan Stage</th>
            <th>Loan Amount</th>

            <th>Status</th>


            <th width="200px">Action</th>
        </tr>
    </thead>

    <tbody>
        {{$i=1}}
        @foreach ($loans as $loan)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $loan->borrower_no }}</td>
            <td>{{ $loan->loan_stage }}</td>
            <td>{{ $loan->loan_amount }}</td>

            <td>{{ $loan->status }}</td>

            <td width="10px">
                <form action="{{ route('loan.destroy',$loan->id) }}" method="POST">

                    <a class="btn btn-info" href="{{ route('loan.show',$loan->id) }}"><i class="fa fa-eye"></i></a>
                    {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Show</button>--}}

                    <a class="btn btn-primary" href="{{ route('loan.edit',$loan->id) }}"><i class="fa fa-bars"></i></a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>



                </form>
            </td>

        </tr>
        @endforeach
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>
{{-- {!! $loans->links() !!} --}}

@endsection
