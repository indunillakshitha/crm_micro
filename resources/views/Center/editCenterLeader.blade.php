@extends('layouts.master')
@section('title')

  Edit Center
@endsection
@section('content')
<div class="">
    <div class="card">
        <div   class="card-body">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Center Leader</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('centerleader.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Warning!</strong> Please check input field code<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

            <form action=" href=/center/update/{{$leader->id}}" method="POST">
                @csrf
                @method('PUT')

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Borrower ID:</strong>
                            <input type="text" name="borrower_id" value="{{ $leader->borrower_id }}" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Branch:</strong>
                            <input type="text" name="branch" value="{{ $leader->branch }}" class="form-control" placeholder=" ">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Center :</strong>
                            <input type="text" name="center" value="{{ $leader->center }}" class="form-control" placeholder=" ">
                        </div>
                    </div>


                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection
