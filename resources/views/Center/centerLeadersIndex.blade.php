@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')


    <div class="modal fade " id="exampleModalagent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Leader</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('centerleader.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Type :</label>
                            <select name="type" class="form-control">
                                <option value="Center Leader">Center Leader</option>
                                {{-- <option value="Agent">Agent</option> --}}

                            </select>
                        </div>


                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Borrower No</label>
                            <input type="text" class="form-control" name="borrower_no" id="Cap" oninput="toCap()">
                        </div>
                        {{-- <div class="form-group">
                            <label for="recipient-name" class="col-form-label">NIC:</label>
                            <input type="text" class="form-control" name="nic" id="nic" oninput="genBday()">
                            <input type="text" readonly id="nic_status" class="btn d-none white">
                            {{-- <button class="btn btn-danger" id="nic_status">aaadd</button> --}}
                        {{-- </div> --}}
                        {{-- <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Address:</label>
                            <input type="text" class="form-control" name="address" id="Cap" oninput="toCap()">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Contact Number:</label>
                            <input type="text" class="form-control" name="mobile">
                        </div> --}}

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" name="submit" class="btn btn-primary">SUBMIT</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 borrower_id">
                        <div class="pull-left">
                            <h2>Center Leaders</h2>
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#exampleModalagent">Add/Update Center Leader
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">


        <table class="table table-bordered">
            <tr>
                <th>center</th>
                <th>Borrower  No</th>
                <th>Name</th>
                {{-- <th>Type</th> --}}

            </tr>
        </table>
        @foreach ($leaders as $leader)
            <div class="card" style="width: 100%;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">{{ $leader ['center'] }}</div>
                        <div class="col-sm-3">{{ $leader ['borrower_no'] }}</div>
                        <div class="col-sm-3">{{ $leader['full_name'] }}</div>
                        {{-- <div class="col-sm-3">{{ $leader->type }}</div> --}}
                        <div class="col-sm-3" width="10px">
                            <div class="row">
                                <form action="" method="POST">
                                    {{-- <a class="btn btn-info row-cols-sm-4"
                                       href="">Show</a> --}}
                                    {{-- <a class="btn btn-primary row-cols-sm-4"
                                       href="">Edit</a> --}}
                                    @csrf
                                    @method('DELETE')
                                    {{-- <button type="submit" class="btn btn-danger row-cols-sm-4">Delete</button> --}}
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach

    </div>

@endsection

@section('scripts')
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        const branch = document.querySelector('#select-branch');
        const center = document.querySelector('#select_center');
        branch.addEventListener('click', () => {
            $.ajax({
                type: 'GET',
                url: '{{('/getcentername')}}',
                data: {'name': center.value},
                success: function (data) {
                    console.log(data, 'hakuna matata')
                    centerString = `00${data}`;
                    console.log(centerString)
                }
            })
        })


    </script>



@endsection
