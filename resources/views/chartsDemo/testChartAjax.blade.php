@extends('layouts.master')

@section('title')

Dashboard | CRM
@endsection

@section('content')





<div class="card  col-12">
    <div class="card-body ">
        <div class="stat-widget-one col-12">
        <canvas id="bar-chart4" width="800" height="450"></canvas>
         </div>
    </div>

        <div class="col-12">

                <div class="pull-right col-3">
                    <label for="">Select Branch:</label>
                    <select name="select_branchC" id="select_branchC">
                    <option value="">Select</option>
                    @foreach($branches as $branch)
                    <option value="{{$branch->branch_name}}" >{{$branch->branch_name}}</option>
                    @endforeach
                    </select>
                </div>


                <div class="pull-right col-3">
                    <label for="">Select Center:</label>
                    <select name="" id="select_centerC">
                    <option value="">Select</option>
                    @foreach($centers as $center)
                    <option value="{{$center->center_name}}" >{{$center->center_name}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="col-3">
                    <label for="">From:</label>
                    <input type="date" name="da" id="selected_date1C"class="form-control"  >
                </div>
                <div class="col-3">
                    <label for="">To:</label>
                    <input type="date" name="da" id="selected_date2C"class="form-control"  >
                </div>

    </div>

    <div class="col-7 ">
        <br>
        <button type="button" class="btn pull-right col-4 btn-success" onclick="collectionBar(this)" id="search_btnC" >Check</button>
    </div>
<a class="btn btn-outline-light btn-lg btn-block " href="/collectionsreport" >See More Details</a>
</div>




@endsection

@section('scripts')
<script type="text/javascript">
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    const select_branchC = document.querySelector('#select_branchC');//take a value of selected branch
    const select_centerC = document.querySelector('#select_centerC');//take a value of selected center
    const selected_date1C = document.querySelector('#selected_date1C');//take a date of selected dateFrom
    const selected_date2C = document.querySelector('#selected_date2C');//take a date of selected dateTo
var myChart1; // declare a variable for above chart


function collectionBar(){

    $.ajax({

        type: 'GET',
        url: '{{('/testChart/testCollection')}}',
        data:{'branch':select_branchC.value,'center':select_centerC.value,'dateFrom':selected_date1C.value,'dateTo':selected_date2C.value},
        dataType: 'JSON',
        success: function (data) {
            console.log(data);
            if (myChart1) {
        myChart1.destroy(); //destroy chart from reloading(just one time load)
      }
            var ctx = document.getElementById("bar-chart4").getContext("2d");
           myChart1 = new Chart(document.getElementById("bar-chart4"), {

                type: 'bar',
                data: {
                labels: data.ClAdv,//set incoming array name as ClAdv (lables array)
                datasets: [
                    {
                    label: "Ammount",
                    backgroundColor:   "#3cba9f",
                    data: data.CoAdv //set incoming array name as CoAdv (value array)
                    }
                ]
                },
                options: {
                legend: { display: false },
                title: {
                    display: true,
                    text: 'Collections'
                }
                }
            });

        }
    })
}



</script>
@endsection
