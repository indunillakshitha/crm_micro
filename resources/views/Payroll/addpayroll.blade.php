@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')

    


    

<div class="animated fadeIn">
    <div class="card">
        <div   class="card-body">

            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Add Payroll</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('payroll.index') }}"> Back</a>
                    </div>
                </div>
            </div>
        </div></div></div>
        <div class="animated fadeIn">
    <div class="card">
        <div   class="card-body">
                <form action="#" method="POST">
                    @csrf
                    <div class="row">
                     <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong for="recipient-name" class="col-form-label">Designation:</strong>
                                <select name="designation" class="form-control" id="designation">
                                    <option value="0">---Select---</option>
                                    <option value="Director">Director</option>
                                    <option value="Operation Manager">Operation Manager</option>
                                    <option value="Senior Manager">Senior Manager</option>
                                    <option value="Branch Manager">Branch Manager</option>
                                    <option value="Admin Officer">Admin Officer</option>
                                    <option value="Assistant Manager">Assistant Manager</option>
                                    <option value="Account Head">Account Head</option>
                                    <option value="Account Executive">Account Executive</option>
                                    <option value="Field Executive">Field Executive</option>
                                    <option value="Agent">Agent</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <strong>Employee Name:</strong>
                                <input type="text" name="emp_name" class="form-control" placeholder="Employee Name" id="emp_name"> 
                                
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <strong>Branch:</strong>
                                {{-- <input type="text" name="branch" class="form-control" placeholder="Center" id="branch"> --}}
                                
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <strong>Center:</strong>
                                {{-- <input type="text" name="center" class="form-control" placeholder="Branch" id="center"> --}}
                                
                            </div>
                        </div>
                        
                         <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Employee No:</strong>
                                <select name="emp_num" class="form-control" id="emp_num" >
                                </select>
                            </div>
                        </div>
                         <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Business Name:</strong>
                                <select name="business_name" class="form-control" id="business_name" >
                                </select>
                            </div>
                        </div>
                        

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Payroll Date:</strong>
                                <input class="date form-control" type="text" name="payroll_date">
                                {{-- <input type="text" name="payroll_date" class="form-control" placeholder="2020-12-12"> --}}
                            </div>
                        </div>
                       
                        
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Basic Pay:</strong>
                                <input type="number" name="basic" value="basic" class="form-control" placeholder="Basic Pay"
                                       id="basic" oninput="genPay()">
                            </div>
                        </div>
                         <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Pension:</strong>
                                <input type="number" name="pension" value="pension" class="form-control" id="pension"
                                       placeholder="" oninput="" oninput="genPay()">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Overtime:</strong>
                                <input type="number" name="overtime" value="overtime" class="form-control" placeholder="Overtime"
                                       id="overtime" oninput="genPay()">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Health Insurance:</strong>
                                <input type="number" name="health" value="health" class="form-control" id="health"
                                       placeholder="" oninput="" oninput="genPay()">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Paid Leaves:</strong>
                                <input type="number" name="leaves" value="leaves" class="form-control" id="leaves"
                                       placeholder="" oninput="" oninput="genPay()">
                            </div>
                        </div>
                         <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Unpaid Leave:</strong>
                                <input type="number" name="unpaid_leave" value="unpaid_leave" class="form-control" id="unpaid_leave"
                                       placeholder="" oninput="" oninput="genPay()">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Transport Allowance:</strong>
                                <input type="number" name="transport" value="transport" class="form-control" id="transport"
                                       placeholder="" oninput="" oninput="genPay()">
                            </div>
                        </div>
                         <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Tax Deduction:</strong>
                                <input type="number" name="tax" value="tax" class="form-control" id="tax"
                                       placeholder="" oninput="" oninput="genPay()">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Medical Allowance:</strong>
                                <input type="number" name="medical" value="medical" class="form-control" id="medical"
                                       placeholder="" oninput="" oninput="genPay()">
                            </div>
                        </div>
                        
                         <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Salary Loan:</strong>
                                <input type="number" name="loan" value="loan" class="form-control" id="loan"
                                       placeholder="" oninput="" oninput="genPay()">
                            </div>
                        </div>
                        
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Bonus:</strong>
                                <input type="number" name="bonus" value="bonus" class="form-control" id="bonus"
                                       placeholder="" oninput="" oninput="genPay()">
                            </div>
                        </div>
                         <div class="col-xs-6 col-sm-6 col-md-6">
                            
                        </div>
                       <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Other Allowance:</strong>
                                <input type="number" name="other" value="other" class="form-control" id="other"
                                       placeholder="" oninput="" oninput="genPay()">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Total Pay:</strong>
                                <input type="number" name="tot_pay" value="tot_pay" class="form-control" id="tot_pay"
                                       placeholder=""  >
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Total Deductions:</strong>
                                <input type="number" name="tot_deductions" value="tot_deductions" class="form-control" id="tot_deductions"
                                       placeholder="" >
                            </div>
                        </div>
                         <div class="col-xs-6 col-sm-6 col-md-6">
                            
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Net Pay:</strong>
                                <input type="number" name="net_pay" value="net_pay" class="form-control" id="net_pay"
                                       placeholder="" >
                            </div>
                        </div>
                       
                       
                       
                       
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" id="sub_btn" class="btn btn-primary">Submit</button>
            </div>
        </div>
        </form>
    </div>
    </div>
    </div>

@section('scripts')

<script type="text/javascript">

    const basic = document.querySelector('#basic');
    const pension = document.querySelector('#pension');
    const overtime = document.querySelector('#overtime');
    const health = document.querySelector('#health');
    const leaves = document.querySelector('#leaves');
    const unpaid_leave = document.querySelector('#unpaid_leave');
    const transport = document.querySelector('#transport');
    const tax = document.querySelector('#tax');
    const medical = document.querySelector('#medical');
    const loan = document.querySelector('#loan');
    const bonus = document.querySelector('#bonus');
    const other = document.querySelector('#other');
    const tot_pay = document.querySelector('#tot_pay');
    const tot_deductions = document.querySelector('#tot_deductions');
    const net_pay = document.querySelector('#net_pay');


    setInterval(() => {
        genPay();
    }, 50);


    function genPay(){

        let Basic = basic.value;
        let Pension = pension.value;
        let Overtime = overtime.value;
        let Health = health.value;
        let Leaves = leaves.value;
        let Un_Leave = unpaid_leave.value;
        let Transport = transport.value;
        let Tax = tax.value;
        let Medical = medical.value;
        let Loan = loan.value;
        let Bonus = bonus.value;
        let Other = other.value;
        let total_pay = 0;
        let total_deduction = 0;
        let Net_pay = 0;
        

        total_pay = Number(Basic)+Number(Overtime)+Number(Leaves)+Number(Transport)+Number(Medical)+Number(Bonus)+Number(Other);
        total_deduction = Number(Pension)+Number(Health)+Number(Un_Leave)+Number(Tax)+Number(Loan);
        Net_pay = Number(total_pay)-Number(total_deduction);
        tot_pay.value = total_pay;
        tot_deductions.value =total_deduction;
        net_pay.value =Net_pay;

    }


</script>

@endsection 

@endsection