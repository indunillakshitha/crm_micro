@extends('layouts.master')
@section('title')

    Loans
@endsection
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                        <h2>Payrolls</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('payroll.create') }}"> Add Payroll</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Borrower No</th>
        <th>Loan Stage</th>
        <th>Loan Amount</th>

        <th>Release Date</th>


        <th width="200px">Action</th>
    </tr>
    @foreach ($payrolls as $payroll)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $payroll->borrower_no }}</td>
        <td>{{ $payroll->loan_stage }}</td>
        <td>{{ $payroll->loan_amount }}</td>

        <td>{{ $payroll->release_date }}</td>

        <td width="10px">
            <form action="{{ route('payroll.destroy',$payroll->borrower_no) }}" method="POST">

                <a class="btn btn-info" href="{{ route('payroll.show',$payroll->borrower_no) }}"><i class="fa fa-eye"></i></a>
                {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Show</button>--}}

                <a class="btn btn-primary" href="{{ route('payroll.edit',$payroll->borrower_no) }}"><i class="fa fa-bars"></i></a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
            </form>
        </td>

    </tr>
    @endforeach
</table>

{!! $payrolls->links() !!}

@endsection
