@extends('layouts.master')
@section('title')

Expences
@endsection
@section('content')
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="pull-left">
                        <h2>Expences Categories</h2>
                    </div>
                    <div class="pull-right">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row justify-content-center container-fluid">
    <div class="col-md-12">
        <div class="card p-3">
            <form action="{{ route("compant_ex_cat.store")}}" method="POST">
                @csrf
                <div class="form-row justify-content-center">
                    <div class="col-md-4 mb-3 ml-3">
                        <label for="validationServerUsername">Category</label>
                        <div class="input-group">
                        <div class="input-group-prepend">
                        </div>
                        <input type="text" class="form-control is-valid" id="validationServerUsername" value="{{ old('category') }}" name="category" placeholder="Enter a Package title" aria-describedby="inputGroupPrepend3" >
                        @if ($errors->has('category'))
                        <div class="valid-feedback">
                            {{ $errors->first('category') }}                    </div>
                        @endif
                        </div>
                    </div>

                </div>



                <div class="form-row justify-content-center">
                    <button class="btn btn-primary" type="submit">Submit </button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- <div class="card animated fadeIn" style="width: 100%;">
    <div class="card-body">
        <table class="table table-bordered" id="data_table">
            <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Center No</th>
                <th>Branch Name</th>
                <th> Address</th>
                <th> Mobile</th>
                <th width="200px">Action</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($centers as $center)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $center->center_name }}</td>
                <td>{{ $center->center_no }}</td>
                <td>{{ $center->branch_no }}</td>
                <td>{{ $center->center_address }}</td>
                <!-- <td>{{ $center->center_province  }}</td>
            <td>{{ $center->center_city }}</td>
            <td>{{ $center->center_postal }}</td> -->
                <td>{{ $center->center_mobile }}</td>
                <!-- <td>{{ $center->center_land }}</td>
            <td>{{ $center->center_email }}</td> -->
                <td width="10px">
                    <form action="{{ route('center.destroy',$center->id) }}" method="POST">

                        <a class="btn btn-info" href="{{ route('center.show',$center->id) }}"><i class="fa fa-eye"></i></a>

                        <a class="btn btn-primary" href="{{ route('center.edit',$center->id) }}"><i class="fa fa-bars"></i></a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                    </form>
                </td>

            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
</div>
{!! $centers->links() !!} --}}

<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>

@endsection
