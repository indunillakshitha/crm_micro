@extends('layouts.master')
@section('content')


{{--//--------------------------------------------------------------------------------------------------------}}

<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                    </div>
                </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 "  >
                        <h6>Loans</h6><br>
                        <a  type="button" href="/exportloanbase" name="filter" id="filter" class="btn btn-info btn-sm">Loans</a>
                    </div>
            </div>
        </div>
    </div>
</div>
<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                    </div>
                </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 "  >
                        <h6>Borrowers</h6><br>
                        <a  type="button" name="filter" href="/exportborrowerbase" id="filter" class="btn btn-info btn-sm">Borrowers</a>

                    </div>
            </div>
        </div>
    </div>
</div>
<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                    </div>
                </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 "  >
                        <h6> Repayments</h6><br>
                        <a  type="button" href="/exportrepaymentbase"  name="filter" id="filter" class="btn btn-info btn-sm"> Repayments</a>
                    </div>
            </div>
        </div>
    </div>
</div>
<div class="">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                    <div class="pull-left">
                    </div>
                </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 "  >
                        <h6> Locations</h6><br>
                        <a  type="button" href="/exportlocmiss"  name="filter" id="filter" class="btn btn-info btn-sm"> Location Missings</a>
                    </div>
            </div>
        </div>
    </div>
</div>




@endsection

