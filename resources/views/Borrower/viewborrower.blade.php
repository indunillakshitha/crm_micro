@extends('layouts.master')

@section('title')

    Canty International
@endsection

@section('content')




<div class="">
    <div class="card">
        <div   class="card-body">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Borrower Details:</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('borrower.index') }}"> Back</a>
                    </div>
                </div>
            </div>

         <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 row">
             <div class="form-group col">
                <strong>Application No:</strong>
                <input type="text" name="application_no" disabled class="form-control" placeholder="{{ $borrower->application_no }}">
            </div>
            <div class="form-group col">
                <strong>Branch :</strong>
                <input type="text" name="branch" disabled class="form-control" placeholder="{{ $borrower->branch }}">
            </div>
            <div class="form-group col">
                <strong>Branch No:</strong>
                <input type="text" name="branch_no" disabled class="form-control" placeholder="{{ $borrower->branch_no }}">
            </div>
            <div class="form-group col">
                <strong>Center:</strong>
                <input type="text" name="center" disabled class="form-control" placeholder="{{ $borrower->center }}">
            </div>
            <div class="form-group col">
                <strong>Group No:</strong>
                <input type="text" name="group_no" disabled class="form-control" placeholder="{{ $borrower->group_no }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 row">
            <div class="form-group col">
                <strong>Unique Id:</strong>
                <input type="text" name="borrower_no" disabled class="form-control" placeholder="{{ $borrower->borrower_no }}">
            </div>
         </div>
	<div class="col-xs-12 col-sm-12 col-md-12 row">
            <div class="form-group col">
                <strong>Full Name:</strong>
                <input type="text" name="full_name" disabled class="form-control" placeholder="{{ $borrower->full_name }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 row">
            <div class="form-group col">
                <strong>NIC:</strong>
                <input type="text" name="nic" disabled class="form-control" placeholder="  {{ $borrower->nic }}">
            </div>
            <div class="form-group col">
                <strong>Birthday:</strong>
                <input type="text" name="birthday" disabled class="form-control" placeholder="  {{ $borrower->birthday }}">
            </div>
            <div class="form-group col">
                <strong>Gender:</strong>
                <input type="text" name="gender" disabled class="form-control" placeholder="  {{ $borrower->gender }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 row">
            <div class="form-group col">
                <strong>Address:</strong>
                <input type="text" name="address" disabled class="form-control" placeholder="  {{ $borrower->address }}">
            </div>


            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Mobile :</strong>

                <input type="text" name="mobile_no" disabled class="form-control" placeholder="{{ $borrower->mobile_no }}">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group col">
                <strong>Civil Status :</strong>
                <input type="text" name="civil_status" disabled class="form-control" placeholder="  {{ $borrower->civil_status }}">
            </div>
            </div>
         <div class="col-xs-12 col-sm-12 col-md-12 row">

            <div class="form-group col">
                <strong>Relationship :</strong>
                <input type="text" name="related_civil_status" disabled class="form-control" placeholder="  {{ $borrower->related_civil_status }}">
            </div>
            <div class="form-group col">
                <strong>Name :</strong>
                <input type="text" name="h_g_name" disabled class="form-control" placeholder="  {{ $borrower->h_g_name }}">
            </div>
            <div class="form-group col">
                <strong>Relation NIC :</strong>
                <input type="text" name="lp_no" disabled class="form-control" placeholder="  {{ $borrower->lp_no }}">
            </div>
             <div class="form-group col">
                <strong>Relation Contact Number :</strong>
                <input type="text" name="h_g_number" disabled class="form-control" placeholder="  {{ $borrower->h_g_number }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Remarks :</strong>
                <input type="text" name="remarks" disabled class="form-control" placeholder="{{ $borrower->remarks}}">
                </div>
            </div>
        </div>            </div>

        </div>
    </div>
</div>




@endsection

@section('scripts')

@endsection
