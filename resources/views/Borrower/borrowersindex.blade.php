@extends('layouts.master')
@section('title')


@endsection
@section('content')
<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="pull-left">
                        <h2>Borrowers All</h2>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="card animated fadeIn" style="width: 100%;">
    <div class="card-body">
        <table class="table table-bordered" id="data_table">
            <thead>
            <tr>
                <th>Center</th>
                <th>Group No</th>
                <th>Name</th>
                <th>NIC No</th>
                <th>Borrower No</th>
                <th>Address</th>
                <th>Mobile</th>
                <th>Relation Mobile</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($borrowers as $borrower)
            <tr>
                <td>{{ $borrower->center}}</td>
                <td>{{ $borrower->group_no}}</td>
                <td>{{ $borrower->full_name}}</td>
                <td>{{ $borrower->nic }}</td>
                <td>{{ $borrower->borrower_no }}</td>
                <td>{{ $borrower->address }}</td>
                <td>{{ $borrower->mobile_no  }}</td>
                <td>{{ $borrower->h_g_number }}</td>

            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
         $('#data_table').DataTable();
    } );

</script>

@endsection
