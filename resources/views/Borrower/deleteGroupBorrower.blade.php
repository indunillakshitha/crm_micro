@extends('layouts.master')

@section('title')

Canty International
@endsection

@section('content')
<div class="card">

    <table class="table">
        <tr>
            <th>Name</th>
            <th>NIC</th>
            <th>Borrower No</th>
            <th></th>
        </tr>

        @foreach($borrowers as $borrower)
        <tr>
            <td>{{$borrower->full_name}}</td>
            <td>{{$borrower->nic}}</td>
            <td>{{$borrower->borrower_no}}</td>
            <td>
                <form action="/addborrower/deleteborrower/{{$borrower->id}}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-danger btn-sm">Remove Borrower</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div>
@endsection

@section('scripts')

@endsection
