@extends('layouts.master')
@section('title')

    Canty International
@endsection
@section('content')
<div class="container">
    <div class="card">
        <div   class="card-body">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Branch</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('borrower.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Warning!</strong> Please check input field code<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

            <form action="{{ route('borrower.update',$borrower->borrower_no) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Branch :</strong>
                            <input type="text" disabled name="branch" value="{{ $borrower->branch }}" class="form-control" placeholder="Branch">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Center:</strong>
                            <input type="text" name="center" disabled value="{{ $borrower->center }}" class="form-control" placeholder="Center">
                        </div>
                    </div>
                  <!-- <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Branch No:</strong>
                            <input type="text" name="branch_no" value="{{ $borrower->branch_no }}" class="form-control" placeholder="Branch No">
                        </div>
                    </div>  -->

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Group No:</strong>
                            <input type="text" name="group_no" disabled value="{{ $borrower->group_no }}" class="form-control" placeholder="Group No">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Unique Id:</strong>
                            <input type="text" name="borrower_no" disabled value="{{ $borrower->borrower_no }}" class="form-control" placeholder="Unique Id">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Full NAme:</strong>
                            <input type="text" name="full_name"  id="full_name" value="{{ $borrower->full_name }}" oninput="toCap(this.value, this.id)" class="form-control" placeholder="Branch Mobile">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Address:</strong>
                            <input type="text" name="address" id="address" value="{{ $borrower->address }}" oninput="toCap(this.value, this.id)" class="form-control" placeholder="Address">
                        </div>
                    </div>
                    <!-- <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Application No:</strong>
                            <input type="text" name="application_no" value="{{ $borrower->application_no }}" class="form-control" placeholder="Application No">
                        </div>
                    </div> -->


                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>NIC :</strong>
                            <input type="text" name="nic" disabled value="{{ $borrower->nic }}"  class="form-control" placeholder="NIC">
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <strong>Borrower Contact:</strong>
                            <input type="text" name="mobile_no" value="{{ $borrower->mobile_no }}" class="form-control" placeholder="Branch Email">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Birthday:</strong>
                            <input type="text" name="birthday" value="{{ $borrower->birthday }}" class="form-control" placeholder="Birthday">
                        </div>
                    </div>


                    <div class="col-xs-8 col-sm-8 col-md-8">
                        <div class="form-group">
                            <strong>Civil Status:</strong>
                            <input type="text" name="civil_status" value="{{ $borrower->civil_status }}" class="form-control" placeholder="Civil Status">
                        </div>
                    </div>

                    <div class="col-xs-8 col-sm-8 col-md-8">

                        <div class="form-group">
                            <strong>Joint Holder Name:</strong>
                            <input type="text" name="h_g_name" id="h_g_name" value="{{ $borrower->h_g_name }}" oninput="toCap(this.value, this.id)" class="form-control" placeholder="Name">
                        </div>
                     </div> <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <strong>Joint Holder Contact:</strong>
                            <input type="text" name="h_g_number" value="{{ $borrower->h_g_number }}" class="form-control" placeholder="Mobile Number">
                        </div>
                    </div>
                  <div class="col-xs-4 col-sm-4 col-md-4">
                        {{-- <div class="form-group">
                            <strong>Salary:</strong>
                            <input type="text" name="h_g_sallery" value="{{ $borrower->h_g_sallery }}" class="form-control" placeholder="Sallery">
                        </div> --}}
                    {{-- </div> --}}
                    {{-- <div class="col-xs-6 col-sm-6 col-md-6"> --}}
                        <div class="form-group">
                            <strong>Joint Holder NIC:</strong>
                            <input type="text" name="lp_no" value="{{ $borrower->lp_no }}" class="form-control" placeholder="LP Number">
                        </div>
                    </div>

                    <!-- <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>E Mail:</strong>
                            <input type="text" name="email" value="{{ $borrower->email }}" class="form-control" placeholder="E Mail">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Occupation:</strong>
                            <input type="text" name="occupation" value="{{ $borrower->occupation }}" class="form-control" placeholder="Occupation">
                        </div>
                    </div> -->
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            {{-- <strong>Remarks:</strong> --}}
                            <input type="hidden" name="occupation" id="occupation" value="{{ $borrower->remarks }}" oninput="toCap(this.value, this.id)" class="form-control" placeholder="Occupation">
                        </div>
                    </div>
                     <!-- <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Amount:</strong>
                            <input type="text" name="requested_amount" value="{{ $borrower->requested_amount }}" class="form-control" placeholder="Amount">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Duration(weeks):</strong>
                            <input type="text" name="duration_weeks" value="{{ $borrower->duration_weeks }}" class="form-control" placeholder="Duration(weeks)">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Loan Stage:</strong>
                            <input type="text" name="loan_stage" value="{{ $borrower->loan_stage }}" class="form-control" placeholder="Loan Stage">
                        </div>
                    </div>  -->

                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection
