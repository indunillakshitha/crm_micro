@extends('layouts.master')
@section('content')


<div class="container">
    <div class="card">
        <div   class="card-body">
            <div class="row">
                <div class="col-lg-12 borrower_id">
                         <div class="pull-left">
                        <h2>Add Postal Codes</h2>
                         </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card ">
        <div   class="card-body ">
                        <form action="/postalcode/store" method="POST">
                            @csrf

                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="husband" class="col-form-label">Province :</label>
                                    </div>
                                    <div class="col-6">
                                        <select name="province" class="form-control">
                                            <option value="Western Province">Western Province</option>
                                            <option value="Central Province">Central Province</option>
                                            <option value="Eastern Province">Eastern Province</option>
                                            <option value="Northern Province">Northern Province</option>
                                            <option value="Southern Province">Southern Province</option>
                                            <option value="North Western Province">North Western Province</option>
                                            <option value="North Central Province">North Central Province</option>
                                            <option value="Uva Province">Uva Province</option>
                                            <option value="Sabaragamuwa Province">Sabaragamuwa Province</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="son" class="col-form-label">City :</label>
                                    </div>
                                    <div class="col-6">
                                        <input type="text" class="form-control" name="city">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="recipient-name" class="col-form-label">Postal Code :</label>
                                    </div>
                                    <div class="col-6">
                                        <input type="number" class="form-control" name="postal_code">
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit"class="btn btn-primary">Add Postal Code</button>
                            </div>

                        </form>
                    </div>
                </div>

            </div>


</div>





@endsection
