<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDenominationDepositeVariancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('denomination_deposite_variances', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('denomination_center_totals_id');
        //     $table->double('atm_rejection')->default(0);
        //     $table->double('payments')->default(0);
        //     $table->double('loan_disbursment')->default(0);

        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('denomination_deposite_variances');
    }
}
