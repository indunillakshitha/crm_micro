<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuarantorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guarantors', function (Blueprint $table) {
            $table->bigIncrements('id');

//            $table->string('guarantor_application_no');
//            $table->string('guarantor_branch');
//            $table->string('guarantor_branch_no')->nullable();
//            $table->string('guarantor_center');
//            $table->string('group_no');
            $table->string('guarantor_no');
            $table->string('guarantor_full_name');
            $table->string('guarantor_nic');
            $table->string('guarantor_birthday');
            $table->string('guarantor_gender');
            $table->string('guarantor_address');
            $table->string('guarantor_civil_status');
//            $table->string('related_civil_status')->nullable();
//            $table->string('h_g_name')->nullable();
//            $table->string('h_g_number')->nullable();
//            $table->string('h_g_sallery')->nullable();
            $table->string('guarantor_lp_no');
            $table->string('guarantor_mobile_no');
            $table->string('guarantor_email');
            $table->string('guarantor_occupation');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guarantors');
    }
}
