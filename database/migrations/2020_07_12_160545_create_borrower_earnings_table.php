<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBorrowerEarningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrower_earnings', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('borrower_id')->nullable();
                $table->string('product_name')->nullable();
                $table->integer('period')->nullable();
                $table->integer('total_units')->nullable();
                $table->float('unit_price')->nullable();
                $table->double('earnings')->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrower_earnings');
    }
}
