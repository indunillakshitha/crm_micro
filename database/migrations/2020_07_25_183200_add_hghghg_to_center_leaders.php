<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHghghgToCenterLeaders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('center_leaders');
        
        Schema::create('center_leaders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('borrower_id');
            $table->string('branch');
            $table->string('center');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    //    Schema::dropIfExists('center_leaders');
    }
}
