<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('branch');
            $table->string('name');
            $table->string('designation');
            $table->string('nic');
            $table->string('address');
            $table->string('mobile');
            $table->string('mail');
            $table->string('edu_qual');
            $table->string('pro_qual');
            $table->string('pro_exp');
            $table->string('civil_status');
            $table->string('img_nic')->nullable();
            $table->string('img_birth_certificate')->nullable();
            $table->string('img_gr_cer')->nullable();
            $table->string('img_police_cer')->nullable();
            $table->string('img_married_cer')->nullable();
            $table->string('img_edu_1_cer')->nullable();
            $table->string('img_edu_2_cer')->nullable();
            $table->string('img_edu_3_cer')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
