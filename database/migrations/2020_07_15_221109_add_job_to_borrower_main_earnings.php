<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJobToBorrowerMainEarnings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('borrower_main_earnings', function (Blueprint $table) {
            $table->string('job_type')->nulabale();
            $table->double('salary')->nulabale();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('borrower_main_earnings', function (Blueprint $table) {
            //
        });
    }
}
