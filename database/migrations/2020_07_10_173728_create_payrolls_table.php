<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payrolls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('designation');
            $table->string('emp_name');
            $table->string('branch');
            $table->string('center');
            $table->string('emp_num');
            $table->date('payroll_date');
            $table->string('business_name')->nullable();
            $table->integer('basic');
            $table->integer('overtime');
            $table->integer('leaves');
            $table->integer('transport');
            $table->integer('medical');
            $table->integer('bonus');
            $table->integer('other');
            $table->integer('pension');
            $table->integer('health');
            $table->integer('unpaid_leave');
            $table->integer('tax');
            $table->integer('loan');
            $table->integer('tot_pay');
            $table->integer('tot_deductions');
            $table->integer('net_pay');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrolls');
    }
}
