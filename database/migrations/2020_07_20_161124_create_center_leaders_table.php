<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCenterLeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('center_leaders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('branch');
            $table->string('center_no');
            $table->string('name');
            $table->string('nic');
            $table->string('address');
            $table->string('mobile');
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('center_leaders');
    }
}
