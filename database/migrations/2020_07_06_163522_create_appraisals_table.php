<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppraisalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('borrower_nic');
            $table->string('self_employee_total')->nullable();
            $table->integer('husband')->nullable();
            $table->integer('son')->nullable();
            $table->integer('daughter')->nullable();
            $table->integer('guardian')->nullable();
            $table->integer('other')->nullable();
            $table->integer('total_earn');
            $table->integer('household_expenses')->nullable();
            $table->integer('other_expenses')->nullable();
            $table->string('self_employee_expenses')->nullable();
            $table->string('loan_purpose')->nullable();

            $table->integer('foods')->nullable();
            $table->integer('hire_purchasing')->nullable();
            $table->integer('accomadation')->nullable();
            $table->integer('health')->nullable();
            $table->integer('education')->nullable();
            $table->integer('repayment')->nullable();
            $table->integer('water_bill')->nullable();
            $table->integer('electricity_bill')->nullable();

            $table->integer('as_borrower_facility_count')->nullable();
            $table->integer('as_borrower_total_amount')->nullable();
            $table->integer('as_guarantor_facility_count')->nullable();
            $table->integer('as_guarantor_total_amount')->nullable();
            $table->integer('total_expenses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisals');
    }
}
