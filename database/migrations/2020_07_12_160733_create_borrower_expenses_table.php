<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBorrowerExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrower_expenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('borrower_id')->nullable();
            $table->integer('period')->nullable();
            $table->integer('total_units')->nullable();
            $table->string('product_name')->nullable();
            $table->string('buying_price')->nullable();
            $table->double('expenses')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrower_expenses');
    }
}
