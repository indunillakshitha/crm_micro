<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('borrower_no')->is_null();
            $table->string('branch')->is_null();
            $table->string('center')->is_null();
            $table->string('group_no')->is_null();
            $table->string('loan_stage')->is_null();
            $table->integer('loan_amount')->is_null();
            $table->date('release_date')->is_null();
            $table->integer('loan_duration')->is_null();
            $table->string('duration_period')->nullable();
            $table->string('repayment_cycle')->nullable();
            $table->integer('number_of_repayments')->nullable();
            $table->string('loan_purpose')->is_null();
            $table->integer('interest_rate')->is_null();
            $table->float('interest')->is_null();
            $table->float('instalment')->is_null();
            $table->float('principal')->nullable();
            $table->date('maturity')->nullable();
            $table->integer('fees')->nullable();
            $table->integer('penalty')->nullable();
            $table->float('due')->nullable();
            $table->integer('paid')->nullable();
            $table->float('balance')->nullable();
            $table->string('comment')->nullable();
            $table->string('status')->default('Pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
