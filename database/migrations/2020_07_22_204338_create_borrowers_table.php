<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBorrowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrowers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('application_no');
            $table->string('branch');
            $table->string('branch_no')->nullable();
            $table->string('center');
            $table->string('group_no');
            $table->string('borrower_no');
            $table->string('full_name');
            $table->string('nic');
            $table->string('birthday');
            $table->string('gender');
            $table->string('address');
            $table->string('civil_status');
            $table->string('related_civil_status')->nullable();
            $table->string('h_g_name')->nullable();
            $table->string('h_g_number')->nullable();
            $table->string('h_g_sallery')->nullable();
            $table->string('lp_no');
            $table->string('mobile_no');
            $table->string('email');
            $table->string('occupation');
            $table->string('remarks')->nullable();
            $table->string('status')->default('active');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrowers');
    }
}
