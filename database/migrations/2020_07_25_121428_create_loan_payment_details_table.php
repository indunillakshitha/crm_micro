<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanPaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_payment_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('issueType');
            $table->string('borrower_no')->nullable();
            $table->string('group_no')->nullable();
            $table->string('paymentType');
            $table->string('bank_name')->nullable();
            $table->string('branch_name')->nullable();
            $table->string('acc_no')->nullable();
            $table->string('cheque_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_payment_details');
    }
}
