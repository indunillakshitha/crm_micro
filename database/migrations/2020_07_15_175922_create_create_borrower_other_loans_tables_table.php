<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreateBorrowerOtherLoansTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrower_other_loans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('borrower_id')->nullable();
            $table->string('bank_name')->nullable();
            $table->double('total_loan')->nullable();
            $table->double('monthly_paymentt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_borrower_other_loans_tables');
    }
}
