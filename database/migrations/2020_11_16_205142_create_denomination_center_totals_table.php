<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDenominationCenterTotalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('denomination_center_totals', function (Blueprint $table) {
            // $table->bigIncrements('id');
            // $table->string('branch');
            // $table->string('agent_id');
            // $table->double('total_cash_collected');
            // $table->double('total_cash_available');
            // $table->double('varians_avail_n_collected')->default(0);
            // $table->string('comment_for_vNc')->nullable();
            // $table->double('total_cash_deposited')->nullable();
            // $table->double('varians_avail_n_deposited')->default(0);
            // $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('denomination_center_totals');
    }
}
