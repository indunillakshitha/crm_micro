<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShitToRepayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repayments', function (Blueprint $table) {
            $table->string('payed_date_index')->nullable()->after('year');
            $table->string('payed_amount')->nullable()->after('payed_date_index');
            // $table->dropColumn('status');
            $table->string('status')->default('PENDING')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('repayments', function (Blueprint $table) {
            // $table->dropColumn('status');
        });
    }
}
