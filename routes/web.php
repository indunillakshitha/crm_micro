<?php

// use Symfony\Component\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout', function () {
    // return abort(404);
    return redirect('/');
});

Route::group(['middleware' => ['auth']], function () {

// Route::get('/location', 'FirebaseController@index');
    // Route::get('/location', function(){
    //     return view ('Location.location');
    // });

// -------------------------------------------------------------- location
    Route::get('/agentforappraisal/{id}/{lat}/{lng}', 'AppraisalController@agentAppraisal');

    Route::get('/execTestRoute', function () {
        return view('execTestRoute');
    })->middleware('executive');

////////////////////////////  add user  /////////////////////////////////////////
    // Route::get('/account/adduser', 'AccountController@addUser')->middleware('admin');
    Route::resource('/account', 'AccountController');

// Route::group(['middleware' => ['auth', 'admin', 'executive']], function () {
    // Route::get('/home', function () {
    //     // return view('admin.dashboard');
    // });
    Route::get('/home', 'HomeController@index');
    Route::get('/home/chrtNreport', 'HomeController@chrtNreport');

//------------------Branch Routs-----------------------

    Route::resource('branch', 'Branch\BranchController');
// Route::get('/showBranch','Branch\BranchController@index');
    //  Route::get('/addbranch','Branch\BranchController@create');
    // Route::get('/storebranch','Branch\BranchController@store');

//------------------Center Routs-----------------------

    Route::resource('center', 'Center\CenterController');
    Route::get('centeri', 'Center\CenterController@index');
    Route::resource('centerleader', 'Center\CenterLeadersController');
    Route::get('/center/edit/{id}', 'Center\CenterLeadersController@edit');
    Route::get('/center/update/{id}', 'Center\CenterLeadersController@update');

//------------------Category Routs---------------------

    Route::resource('category', "CategoryController");
// Route::resource('loan', 'LoanController');

//------------------Routes borrower-------------------

    Route::resource('borrower', 'BorrowerController');
    Route::get('/addborrower', 'BorrowerController@create');
    Route::get('/borrowers', 'BorrowerController@all');
    Route::get('/getcentername', 'BorrowerController@getCenterName');
    Route::get('/getrightcenters', 'BorrowerController@getRightCenters');
    Route::get('/getborrowerscount', 'BorrowerController@getBorrowersCount');
    Route::get('/getborrowerscountofgroup', 'BorrowerController@getborrowerscountofgroup');
    Route::post('/addborrowerajax', 'BorrowerController@addBorrowerAjax');
    Route::get('/borrower/show/{id}', 'BorrowerController@show');
    Route::get('/borrower/edit/{id}', 'BorrowerController@edit');
    Route::get('/addborrower/deleteborrower/{branch}/{center}/{group}', 'BorrowerController@addBorrowerDeleteBorrower');
    Route::post('/addborrower/deleteborrower/{id}', 'BorrowerController@deleteBorrowerChangeVisibility');

    Route::post('/getnic', 'BorrowerController@getNIC');
    Route::post('/checkguardiannic', 'BorrowerController@checkGuardianNIC');

//------------------Loans----------------------------

    Route::resource('loan', 'Loans\LoanController');
    Route::get('/loan/create/rate', 'Loans\LoanController@rate');

    Route::post('/loan/create/addwitness', 'Loans\LoanController@addWitness');
    Route::get('/loan/create/getwitness', 'Loans\LoanController@getWitness');

    Route::get('/loan/create/addbcg', 'Loans\LoanController@addBCG');
    Route::get('/loan/create/witness', 'Loans\LoanController@witness');
    Route::get('/loan/create/take', 'Loans\LoanController@take');

    Route::get('/addloan/getcenters', 'Loans\LoanController@getCenters');
    Route::get('/addloan/getborrowers', 'Loans\LoanController@getBorrowers');
    Route::get('/addloan/getborrowersforrep', 'Loans\LoanController@getBorrowersForRep');
    Route::get('/addloan/getborrowersbynic', 'Loans\LoanController@getBorrowersbynic');
    Route::get('/addloan/getwitnessc', 'Loans\LoanController@getWitnessC');
    Route::get('/addloan/getwitnessa', 'Loans\LoanController@getWitnessA');
    Route::get('/addloan/getloanstage', 'Loans\LoanController@getLoanStage');
    Route::post('/addloanajax', 'Loans\LoanController@store');
    Route::get('/checkcenterleader', 'Loans\LoanController@checkCenterLeader');

//------------------Appraisal-------------------------

    Route::resource('appraisal', 'AppraisalController');
    Route::resource('appraisalearning', 'addAppraisalEarningProductController');
    Route::resource('appraisalcategory', 'addAppraisalProductCategoryController');
    Route::resource('appraisalingredients', 'addAppraisalExpensesProductController');
    Route::post('addAppraisalExpensesProduct', 'addAppraisalExpensesProductController@addAppraisalExpensesProduct');
    Route::get('/createapprasialpage', 'AppraisalController@createAppraisalPage');
    Route::get('/createapprasialpage/products', 'AppraisalController@getUnitPrice');
    Route::get('/createapprasialpage/products', 'AppraisalController@getUnitPrice');
    Route::get('/createapprasialpage/expenses', 'AppraisalController@getExpenses');
    Route::post('/getnicappraisals', 'AppraisalController@getNicAppraisals');
    Route::get('/delprevapp', 'AppraisalController@delPrevApp');
    Route::post('/createapprasialpage/earnings/delearning', 'AppraisalController@delEarning');
    Route::post('/createapprasialpage/expenses/delexpense', 'AppraisalController@delExpense');
    Route::post('/createapprasialpage/earnings/delrelayincome', 'AppraisalController@delRelayIncome');
    Route::post('/createapprasialpage/expenses/delloans', 'AppraisalController@delLoans');

// earnings
    Route::post('/createapprasialpage/expenses/addearning', 'BorrowerEarningsController@addEarning');
    Route::get('/createapprasialpage/expenses/getearning', 'BorrowerEarningsController@getEarning');

// relationship incomes
    Route::post('/addmainincome', 'BorrowerEarningsController@addRelationshipIncome');
    Route::get('/getmainincome', 'BorrowerEarningsController@getRelationshipIncome');

// other loans
    Route::post('/appraisalloans/addloan', 'BorrowerOtherLoansController@addLoan');
    Route::get('/appraisalloans/getloan', 'BorrowerOtherLoansController@getLoan');

// ingredients
    Route::post('/addingredient', 'AppraisalIngredientsController@addIngredient');
    Route::get('/getings', 'AppraisalIngredientsController@getIngs');
    Route::get('/getingprice', 'AppraisalIngredientsController@getIngsPrice');

// products
    Route::get('/appraisalproducts', 'AppraisalExpensesProductsController@index');
    Route::post('/addexpenseproduct', 'AppraisalExpensesProductsController@store');

Route::get('/appraisalall', 'AppraisalController@all');


//------------------Approving Loans-------------------

    Route::resource('approving', 'ApprovingController');
    Route::get('/approving/test/{id}', 'ApprovingController@test');
    Route::get('approving/{id}', 'ApprovingController@show');
    Route::get('/bseclector', 'ApprovingController@seclectorB');
    Route::get('/cseclector', 'ApprovingController@seclectorC');
    Route::get('/gseclector', 'ApprovingController@seclectorG');
    Route::post('approving/store/{id}', 'ApprovingController@store');

    Route::get('/createapprasialpage/products', 'AppraisalController@getUnitPrice');
    Route::get('/createapprasialpage/expenses', 'AppraisalController@getExpenses');
    Route::post('/createapprasialpage/expenses/addexpense/', 'AppraisalController@addExpense');
    Route::get('/createapprasialpage/expenses/getexpense', 'AppraisalController@getExpense');

    Route::resource('addexpense', 'ExpensesController');
    Route::resource('otherincomes', 'OtherIncomeController');

    Route::resource('appraisal', 'AppraisalController');
    Route::resource('appraisalearning', 'addAppraisalEarningProductController');

//-------------------printing----------------------------------------
    Route::get('/cheque', 'PrintController@cheque');
    Route::get('/promissory/export/{id}', 'PrintController@exportPromissory');
    Route::get('/agreement/export/{id}', 'PrintController@exportAgreement');

    Route::get('/voucher/export/{voucher_id}', 'PrintController@exportVoucher');
    Route::get('/disbursement/{branchName}/{centerName}', 'PrintController@exportDisbursementCenter');
    Route::get('/disbursement-group/export/{id}', 'PrintController@exportDisbursementGroup');
    Route::get('/guarantor-bond/export/{id}', 'PrintController@exportGuarantorBond');
    Route::get('/repayments/{branchName}/{centerName}', 'PrintController@exportRepayments');
    Route::get('/print-receipt', function () {
        return view('Receipt.receipt');
    });

//------------------------------------witness---------------------

    Route::resource('witness', 'Witness\WitnessController');
    Route::post('/addwitnessbtn', 'Witness\WitnessController@addWitness');

//------------------------------guarantor------------>

    Route::resource('guarantor', 'Guarantor\GuarantorController');

    Route::resource('approved', 'ApprovedController');

    Route::get('/approved/issue/{id}', 'ApprovedController@issue');
    Route::post('/approved/pprCharg/{id}', 'ApprovedController@pprCharg');
    Route::get('/approved/pprChrgView/{id}', 'ApprovedController@pprChrgView');

//------------------------------staff---------------->
    Route::resource('staff', 'StaffController');
    Route::get('/my_profile', 'StaffController@my_profile');
    Route::resource('change_pasword', 'ChangeStaffPasswordController');
// });

//--------------------------Issue Loan----------------------------
    Route::resource('issued', 'IssueLoanContrapprovedoller');
    Route::get('/loanpaymentmethod', 'IssueLoanController@paymentMethod');
    Route::get('/singleissue/{id}', 'IssueLoanController@singleIssue');
    Route::get('/groupissue/{id}', 'IssueLoanController@groupIssue');
    Route::get('/groupissue/{id}', 'IssueLoanController@groupIssue');
    Route::get('/markaspayedsingle/{id}', 'IssueLoanController@markAsPayedSingle');
    Route::get('/markaspayedgroup/{id}', 'IssueLoanController@markAsPayedGroup');
    Route::get('/issuedcenter', 'IssueLoanController@issueCenter');
    Route::get('/markaspayedcenter/{id}', 'IssueLoanController@markAsPayedCenter');

//------------------------Repayments-------------------------
    Route::resource('repayment', 'RepaymentController');
    Route::get('/repayment/update/{id}', 'RepaymentController@collected');
    Route::get('/repayment/bulk/{id}', 'RepaymentController@bulkCollect');
    Route::get('/repayments/updateajax', 'RepaymentController@collected');
    Route::get('/repayments/officecollection', 'RepaymentController@officeCollection');
    Route::get('/repayments/centercollectionajax', 'RepaymentController@officeCollect');
    Route::get('/repayments/bulkcollection', 'RepaymentController@bulkCollection');
    Route::post('/repayments/payedajax', 'RepaymentController@getPayed');
    Route::get('/repayments/edit', 'RepaymentController@edit');
    Route::post('/repayments/getbydate', 'RepaymentController@getByDate');
    Route::get('/repayment/edit/{id}', 'RepaymentController@editSingleRepayment');
    Route::post('/editrepayments/updaterepayment/{id}', 'RepaymentController@updateSingleRepayment');

    Route::resource('repayment_date', 'RepaymentsForSelectedDateController');
    Route::get('/getrepaymentsdate', 'RepaymentsForSelectedDateController@getRepaymentsDate');
    Route::post('/getrepaymentsdate', 'RepaymentsForSelectedDateController@setRepaymentsDate');
    Route::resource('repayment_any', 'OldRepaymentController');
    Route::post('/setrepaymentsdate', 'OldRepaymentController@setRepaymentsDate');
    Route::resource('repayments_centers', 'RepaymentsOfCentersController');
    Route::get('/getcenterrepayments', 'RepaymentsOfCentersController@getCenterRepayments');
    Route::get('/getcentercollection', 'RepaymentsOfCentersController@getCenterCollection');

    //--------------------------------------new repayment collect
    Route::get('/repaymentsfromcenter','TempController@collectIndex');


## center collections
    Route::get('/centercollection/getgroups', 'RepaymentCenterCollections@getGroup');
// Route::get('/centercollection/getgroups','RepaymentController@index');

// -----------------Verification-------------------------------
    Route::resource('verify', 'VerificationController');
    Route::get('/verify/test/{id}', 'VerificationController@test');

// --------------------Arrears---------------

    Route::resource('arrears', 'ArrearsController');

// ---------------No Repayments-----------

    Route::resource('norepayments', 'NoRepaymentsController');

// ----------------------One Month----------------

    Route::resource('onemonth', 'OneMonthLateController');

// -------------------Two Month-------------------

    Route::resource('threemonth', 'ThreeMonthLateController');

// -------------------Payroll----------------------
    Route::resource('payroll', 'PayrollController');
    Route::get('/addPayroll', 'PayrollController@create');
    Route::get('/payroll/create/empD', 'PayrollController@empD');

//-------------------------Dont delete-------------------------------------------
    Route::get('/viewborrower/{id}', 'BorrowerController@view');

    Route::get('/repaymentdoc', 'RepaymentController@doc');

// Route::get('/home', 'HomeController@index')->name('home');

###Postal codes
    Route::get('/postalcode/create', 'PostalCodesController@create');
    Route::post('/postalcode/store', 'PostalCodesController@store');
    Route::get('/getpostalcode', 'PostalCodesController@getPostalCode');

//--------------------------------Expenses-------------------------

// Route::resource('/expense/expensestype', 'ExpensesTypeController');
// Route::resource('expense', 'ExpensesController');
// Route::get('expenseA', 'ExpensesController@create');
Route::resource('/addexpense', 'ExpensesController');
// Route::get('/expense/expenseA', 'ExpensesController@create');
Route::resource('/expensestype', 'ExpensesTypeController');


Route::resource('/referenceExpense', 'ReferenceExpensesController');
Route::get('/referenceExpense/create/{id}', 'ReferenceExpensesController@create');

Route::get('/cap','BorrowerController@capital');

// -------------------------------Reports-----------------------

    Route::get('/summery', 'ReportController@index');
    Route::get('/collectionsreport', 'ReportController@collectionsReport');
    Route::get('/collectionsreport/getdaily', 'ReportController@getdaily');
    Route::get('/collectionsreport/getbranch', 'ReportController@getbranch');
    Route::get('/collectionsreport/getcenter', 'ReportController@getcenter');
    Route::get('/collectionsreport/testing', 'ReportController@testing');


    Route::get('/interestCollectionsReport', 'ReportController@interestCollectionsReport');
    Route::get('/interestCollectionsReport/getdaily', 'ReportController@getdaily');
    Route::get('/interestCollectionsReport/getbranch', 'ReportController@getbranch');
    Route::get('/interestCollectionsReport/getcenter', 'ReportController@getcenter');

    Route::get('/capitalCollectionsReport', 'ReportController@capitalCollectionsReport');
    Route::get('/capitalCollectionsReport/getdaily', 'ReportController@getdaily');
    Route::get('/capitalCollectionsReport/getbranch', 'ReportController@getbranch');
    Route::get('/capitalCollectionsReport/getcenter', 'ReportController@getcenter');

    Route::get('/arrearsInterestCollectionsReport', 'ReportController@arrearsInterestCollectionsReport');
    Route::get('/arrearsInterestCollectionsReport/getdaily', 'ReportController@getdaily');
    Route::get('/arrearsInterestCollectionsReport/getbranch', 'ReportController@getbranch');
    Route::get('/arrearsInterestCollectionsReport/getcenter', 'ReportController@getcenter');

    Route::get('/arrearsCustomerReviewReport', 'ReportController@arrearsCustomerReviewReport');
    Route::get('/arrearsCustomerReviewReport/getBranchCV', 'ReportController@getBranchCV');
    Route::get('/arrearsCustomerReviewReport/week', 'ReportController@weekCustomerReview');
    Route::get('/arrearsCustomerReviewReport/getWeekBranchCV', 'ReportController@getWeekBranchCV');
    Route::get('/arrearsCustomerReviewReport/customer', 'ReportController@getCustomer');
    Route::get('/arrearsCustomerReviewReport/getCustomerBranchCV', 'ReportController@getCustomerBranchCV');
    Route::get('/home/WeekRepayDetalis', 'ReportController@WeekRepayDetalis');

    Route::get('/arrearseCapitalCollectionsReport', 'ReportController@arrearseCapitalCollectionsReport');
    Route::get('/arrearseCapitalCollectionsReport/getdaily', 'ReportController@getdaily');
    Route::get('/arrearseCapitalCollectionsReport/getbranch', 'ReportController@getbranch');
    Route::get('/arrearseCapitalCollectionsReport/getcenter', 'ReportController@getcenter');

    Route::get('/advancedPaymentsCollectionsReport', 'ReportController@advancedPaymentsCollectionsReport');
    Route::get('/advancedPaymentsCollectionsReport/getdaily', 'ReportController@getdaily');
    Route::get('/advancedPaymentsCollectionsReport/getbranch', 'ReportController@getbranch');
    Route::get('/advancedPaymentsCollectionsReport/getcenter', 'ReportController@getcenter');

    Route::get('/totalOutstandingCollectionsReport', 'ReportController@totalOutstandingCollectionsReport');
    Route::get('/advancedPaymentsCollectionsReport/getdaily', 'ReportController@getdaily');
    Route::get('/advancedPaymentsCollectionsReport/getbranch', 'ReportController@getbranch');
    Route::get('/advancedPaymentsCollectionsReport/getcenter', 'ReportController@getcenter');


    Route::get('/dashboardView', 'ReportController@dashboardView');
    Route::get('/dashboardView/details', 'ReportController@details');

    // Route::get('/dashboardViewCollection', 'ReportController@dashboardViewCollection');
        Route::get('/dashboardViewCollection', 'ReportController@chartsOne');

    Route::get('/dashboardViewCollection/details', 'ReportController@detailsCollection');
    Route::get('/centertotalreport', 'ReportController@centerTotal');
    Route::get('/centerdetails/{id}', 'ReportController@centerDetails');


    Route::get('/home/getCloChart', 'HomeController@getCloChart');
    Route::get('/home/getArrChart', 'HomeController@getArrChart');
    Route::get('/home/branchExp', 'HomeController@branchExp');








    Route::resources([
        'company_ex'=> 'CompantExpencesController',
        'ongoing_check'=> 'OngoingCheckController',
        'compant_ex_cat'=> 'CompanyExpencesCategoryController',
        'verifycomex'=> 'PendingCompanyExpencesController',
        'approvedex'=> 'ApprovedExpencesController',

    ]);
    Route::post('/companyexpenses/addexpense', 'CompantExpencesController@addExpense');
    Route::post('/companyexpenses/getexpense', 'CompantExpencesController@getExpense');
    Route::post('/companyexpenses/deleteexpense', 'CompantExpencesController@deleteExpense');
    Route::get('/companyexpenses/all', 'CompantExpencesController@all');

    Route::get('/pendingapprovals/{id}', 'PendingCompanyExpencesController@show');
    Route::get('/issueapprovals/{id}', 'ApprovedExpencesController@issue');
    Route::get('/comexapprove/{id}', 'PendingCompanyExpencesController@approve');

    Route::get('/temp/issueIndex', 'TempController@issueIndex');
    Route::get('/temp/repaymentIndex', 'TempController@repaymentIndex');
    Route::get('/temp/repaymentView', 'TempController@viewtIndex');
    Route::get('/temp/totalByDate', 'TempController@totalByDateIndex');
    Route::post('/temp/getloans', 'TempController@getLoans');
    Route::post('/temp/repayments/getloans', 'TempController@repaymentGetLoans');
    Route::post('/temp/view/getloans', 'TempController@viewGetLoans');
    Route::post('/temp/totalbydate/getrepayments', 'TempController@totalByDateGetRepayments');
    Route::post('/temp/view/deleteLoans', 'TempController@viewDeleteLoans');
    Route::post('/temp/issueloans', 'TempController@issueLoans');
    Route::post('/temp/repayments/collect', 'TempController@repaymentCollect');

    //change branch
    Route::get('/branches/change', 'Branch\BranchController@changeBranch');
    Route::get('/branches/change/{branch}', 'Branch\BranchController@changeBranchDB');


    //-----------------------------------------------------group change by inl-----------------------
    Route::resource('groupchange', 'GroupController');
    Route::post('/groupchange', 'GroupController@changeGroup');


    //-----------------------------------------------------emergency---------------------------------
    Route::get('/rtot/{id}','EmergencyController@gettotal');
    Route::get('/rcount/{id}','EmergencyController@getcount');
    Route::get('/rp/{id}','EmergencyController@report');
    Route::get('/settle','EmergencyController@settle');
    Route::get('/paymentcount','EmergencyController@correctCounts');
    Route::get('/maturity','EmergencyController@setMaturityDate');
    Route::get('/notpaid','EmergencyController@setNotPaids');
    Route::get('/docup','EmergencyController@docChargeUpdate');
    Route::get('/excorrect','EmergencyController@corectExpenceTotals');
    Route::get('/weekindexes','EmergencyController@weekIndexesinrepayments');
    Route::get('/addyearandmonth','EmergencyController@addYearandMonthNumber');
    Route::get('/cleardenominations','EmergencyController@clearDenominatioons');


    // ---------------------------------------------------------------------delimnation by inl---------------
    Route::get('/delimnation','DelimnationController@index');
    Route::post('/totalcoll','DelimnationController@getTotal');
    Route::get('/adddelimination','DelimnationController@store');
    Route::get('/adddeliminationdeposite','DelimnationController@depositeAdd');
    Route::get('/denominatiionprint','DelimnationController@print');
    Route::get('/denominatiionprint/{id}','DelimnationController@printDe');
    Route::get('/deposites','DelimnationController@deposites');
    Route::get('/denominationprint/{id}','DelimnationController@printDe');
    Route::get('/denominationdep/{id}','DelimnationController@deposite');
    Route::get('/add_slip','DelimnationController@add_slip');


    //-------------------------------------------------------------------------document charge------------
    Route::get('/documentcharges', 'TempController@docchargeIndex');
    Route::get('/deletedocumentcharges', 'TempController@docchargeDeleteIndex');
    Route::post('/temp/view/getdocumentcharges', 'TempController@viewGetDocumentCharges');
    Route::post('/temp/view/deletedocs', 'TempController@viewDeleteDocumntCharges');

    Route::post('/temp/repayments/getloansfordocs', 'TempController@repaymentGetLoansForDocs');
    Route::post('/temp/docs/collect', 'TempController@docChargeCollect');
    Route::get('/centertotalsget', 'TempController@centerTotaltIndex');
    Route::post('/temp/view/gettotal', 'TempController@viewCenterTotal');

//-----------------------------------------------------------------------------by INL-----------
Route::get('/totalofaday','TempController@totalOfADay');
Route::get('/totalofadayex','TempController@totalOfADayEx');
Route::post('/getcentertotofaday', 'TempController@CenterSelectedDate');
Route::post('/getcentertotofadayex', 'TempController@CenterSelectedDateEx');
Route::post('/getcenterdocofaday', 'TempController@CenterSelectedDateDoc');

//-----------------------------------------------------------------------------by INL-----------
Route::get('/getledger','TempController@ledger');
Route::get('/ledger/getl','TempController@loans');
Route::get('/ledger/getledger','TempController@getLedger');

    //------------------------------------------------------------------------------investors & investments----------------
    Route::resources([
        'investors'=>'InvestorController',
        'investments'=>'InvestmentController',
        ]);

    //------------------------------------------------------------------------------Center Schedule----------------
    Route::resources([
        'shedule'=>'SheduleController',
        ]);
    //------------------------------------------------------------------------------Pettie Cach----------------
    Route::resources([
        'petty'=>'SheduleController',
        ]);

//-----------------------------------------------------new repayments----------------------
Route::get('/newrepayments','NewRepaymentController@newRepayments');
Route::post('/newrepayments/getloans', 'NewRepaymentController@newRepaymentGetLoans');
Route::post('/newrepayments/collect', 'NewRepaymentController@newRepaymentCollect');
Route::post('/newrepayments/notcollect', 'NewRepaymentController@newRepaymentNotCollect');
Route::post('/temp/repayments/notpaid', 'NewRepaymentController@TempNotCollect');
Route::get('/resetCenter', 'NewRepaymentController@resetCenters');
Route::get('/getnotpaidcustomers', 'NewRepaymentController@notPaidCustomers');
Route::get('/setnotpaids', 'NewRepaymentController@setNotPaids');

//-----------------------------------------------------new not paids----------------------
Route::get('/newnotpaids','NewNotpaidController@newRepayments');
Route::post('/newnotpaids/getloans', 'NewNotpaidController@newRepaymentGetLoans');
Route::post('/newnotpaids/collect', 'NewNotpaidController@newRepaymentCollect');
// Route::post('/newrepayments/notcollect', 'NewRepaymentController@newRepaymentNotCollect');
Route::get('/npreport','NewRepaymentController@npReport');
Route::get('/npcount','NewRepaymentController@npCount');
Route::get('/notpaidmore/{id}','NewRepaymentController@notPaidMore');

//--------------------------------------------------------------------agent summery
Route::get('/agentsummery','AgentDailySummeryController@index');


// ------------------------------------------------Test------------------------------------------------
Route::get('/testChart','ReportController@testChart');
Route::get('/testChart/testCollection','ReportController@testCollection');

//----------------------------------due
Route::get('/due','DueController@index');


//--------------------------------------------SOS
Route::get('/sossettlement','SosController@removeSettlements');
Route::get('/sossettlementchange','SosController@removeSettlementsAsReq');
Route::get('/sosinactive','SosController@deactivateUsers');
Route::get('/sosinactiveusers','SosController@deactivateUsersAsReq');


//--------------------------nw reports
Route::get('/centersdue/{id}','NewReportController@centerWiseDue');
Route::get('/newdashboard','NewReportController@newDashboard');
Route::get('/newcollection','NewReportController@collectionReport');


//------------------------------------export
Route::get('/exportloanbase','ExportController@exportLoans');
Route::get('/exportborrowerbase','ExportController@exportBorrowers');
Route::get('/exportrepaymentbase','ExportController@exportRepayments');
Route::get('/exportindex','ExportController@index');
Route::get('/exportlocmiss','ExportController@locMiss');




//------------firebase test---
Route::get('/testfire','FirebaseController@index');


//location update
Route::get('/bulklocation','EmergencyController@bulkLocation');


Route::get('/fixledger','EmergencyController@correctLedger');
});
